#include "dialWinDlg.h"
#include "UIMessage.h"
#include "dlg.h"
#include "LcdControl.h"
#include "GUI.h"
#include "GUI_Timer.h"
#include "GUI_LCD.h"
#include "call_mgr.h"

Window dialWin;

Widget *pDialWidget[9];
WidgetList dialWidgetList[9];

Window *pDialWindowSource;

static void dialWinCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_NOTIFY_RFRX:
      Msg.pTarget = &mainWin;
      Msg.pSource = pMsg->pSource;
      Msg.msgType = pMsg->msgType;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_SendMessage(&Msg);
      break;
    case USER_MSG_HUNG_UP:
      printf("%u hangup\n\n", pMsg->data_v);
      WM_Defaultproc(pMsg);
      break;
    case USER_MSG_TELL_BUSY:
      WM_Defaultproc(pMsg);
      break;
    case USER_MSG_CALL_FAIL:
      WM_Defaultproc(pMsg);
      break;
    case USER_MSG_CONNECT:
      printf("%u connect\n\n", pMsg->data_v);
      WM_Defaultproc(pMsg);
      break;
    case USER_MSG_RING:
      WM_Defaultproc(pMsg);
      break;
    case USER_MSG_DIALTALKING:
      pDialWindowSource = pMsg->pSource;
      WM_SetFocus(&dialWin);
      WG_SetFocus(dialDialSucceeded);
      CallMgr_Connect(pMsg->data_v);
      dialShipNumSetMode(DIAL_DISP_SHIPNUM);
      dialShipNumSetVal(DIAL_DISP_SHIPNUM, (uint32_t)pMsg->data_p);
      WG_SetVisible(dialShipNum);
      dialDialNumSetVal(pMsg->data_v);
      WG_SetVisible(dialDialNum);
      WM_BringToTop(&dialWin);
      break;
    case USER_MSG_CALL_IN:
      WM_Defaultproc(pMsg);
      break;
    case USER_MSG_DIAL:
      CallMgr_Call(pMsg->data_v);
      pDialWindowSource = pMsg->pSource;
      WM_SetFocus(&dialWin);
      WG_SetFocus(dialDialing);
      WG_SetVisible(dialDialNum);
      dialDialNumSetVal(pMsg->data_v);
      WM_BringToTop(&dialWin);
      break;
    case WM_KEY:
      WM_Defaultproc(pMsg);
      break;
    case WM_CREATE:
      pDialWidget[0] = dialDialNumCreate();
      pDialWidget[1] = dialDialFialedCreate();
      pDialWidget[2] = dialDialingCreate();
      pDialWidget[3] = dialDialOverCreate();
      pDialWidget[4] = dialDialSucceededCreate();
      pDialWidget[5] = dialOtherDialNumCreate();
      pDialWidget[6] = dialRoundButtonCreate();
      pDialWidget[7] = dialShipNumCreate();
      pDialWidget[8] = dialDialStateCreate();
    
      WM_AddMyWidget(&dialWin, dialWidgetList, pDialWidget, 9);
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        Msg.pTarget = &mainWin;
        Msg.msgType = USER_MSG_MENUNUM;
        Msg.data_v =  LCD_S64;
        WM_PostMessage(&Msg);
      }
      else
      {
        WG_SetInVisible(dialDialNum);
        WG_Paint(dialDialNum);
        WG_SetInVisible(dialDialFialed);
        WG_Paint(dialDialFialed);
        WG_SetInVisible(dialDialing);
        WG_Paint(dialDialing);
        WG_SetInVisible(dialDialOver);
        WG_Paint(dialDialOver);
        WG_SetInVisible(dialDialSucceeded);
        WG_Paint(dialDialSucceeded);
        WG_SetInVisible(dialOtherDialNum);
        WG_Paint(dialOtherDialNum);
        WG_SetInVisible(dialRoundButton);
        WG_Paint(dialRoundButton);
        WG_SetInVisible(dialShipNum);
        WG_Paint(dialShipNum);
        WG_SetInVisible(dialDialState);
        WG_Paint(dialDialState);
        AudioStopPlay();
        Msg.pTarget = &mainWin;
        Msg.msgType = USER_MSG_NOTIFY_RFRX;
        Msg.data_v = 0;
        WM_SendMessage(&Msg);
      }
      break;
    case WM_PAINT:
      WM_Defaultproc(pMsg);
      break;
    case WM_TIMER:
      switch(pMsg->data_v)
      {
        case 1:
          GUI_DestroyTimer(&dialWin, 1);
          WG_SetInVisible(dialDialing);
          WG_Paint(dialDialing);
          WG_SetFocus(dialDialFialed);
          WG_Paint(dialDialFialed);
          break;
      }
      break;
  }
}

Window* WIN_dialWinCreate(void)
{
  WM_Create(&dialWin, dialWinCallback);
  return &dialWin;
}