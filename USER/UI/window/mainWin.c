#include "UIMessage.h"
#include "dlg.h"
#include "mainWinDlg.h"
#include "system_time.h"


Window mainWin;
 
Widget *pMainWidget[3];
WidgetList mainWidgetList[3];

static void mainWinCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
		case USER_MSG_NOTIFY_RSSI_UPDATE:
			mainSignalSetVal(MAIN_SIGNAL_IN, pMsg->data_v);
      WG_Paint(mainSignal);
			break;
    case USER_MSG_NOTIFY_RFRX:
      if(pMsg->data_v)
      {
        WG_SetVisible(mainSignal);
        mainSignalSetVal(MAIN_SIGNAL_IN, pMsg->data_v);
      }
      else
      {
        WG_SetInVisible(mainSignal);
      }
      WG_Paint(mainSignal);
      break;
						
	case USER_MSG_NOTIFY_RFTX:
		if(pMsg->data_v){ 
			mainSignalSetVal(MAIN_SIGNAL_OUT, 10);
			WG_SetVisible(mainSignal);
		}
		else{
			WG_SetInVisible(mainSignal);
		}
		WG_Paint(mainSignal);
	 break;
						
    case USER_MSG_SECOND_TICK:
    {
      Time time;
      SysTime_Get(&time);
      dateTimeSetMonth(time.month);
      dateTimeSetDay(time.day);
      dateTimeSetHour(time.hour);
      dateTimeSetMinute(time.minute);
      dateTimeSetSecond(time.second);
      if(WG_GetStates(dateTime) >= WG_VISIBLE)
        WG_Paint(dateTime);
    }
      break;
    case USER_MSG_MAINTIMEVISIBLE:
      if(pMsg->data_v)
      {
        Time time;
        SysTime_Get(&time);
        dateTimeSetMonth(time.month);
        dateTimeSetDay(time.day);
        dateTimeSetHour(time.hour);
        dateTimeSetMinute(time.minute);
        dateTimeSetSecond(time.second);
        WG_SetVisible(dateTime);
        WG_Paint(dateTime);
      }
      else
      {
        WG_SetInVisible(dateTime);
        WG_Paint(dateTime);
        WM_Paint(&calogWin, 1);
      }
      break;
    case WM_KEY:
      break;
    case WM_CREATE:
      pMainWidget[0] = menuCreate();
      pMainWidget[1] = dateTimeCreate();
      pMainWidget[2] = mainSignalCreate();
      WG_SetVisible(dateTime);
      WM_AddMyWidget(&mainWin, mainWidgetList, pMainWidget, 3);
      break;
    case WM_FOCUS:
      WG_SetFocus(menu);
      break;
    case WM_PAINT:
      WM_Defaultproc(pMsg);
      break;
    case USER_MSG_MENUNUM:
      WG_SetInVisible(menu);
      WG_Paint(menu);
      Menu_SetValue(pMsg->data_v);
      WG_SetVisible(menu);
      WG_Paint(menu);
      break;
  }
}


Window* WIN_mainWinCreate(void)
{
  WM_Create(&mainWin, mainWinCallback);
  return &mainWin;
}












