#include "UIMessage.h"
#include "dlg.h"
#include "LcdControl.h"
#include "calllogWinDlg.h"
#include "GUI_LCD.h"
#include "call_log.h"


Window calogWin;

Widget *pcalogWInWidget[6];
WidgetList calogWidList[6];

static void calogWinCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      WM_Defaultproc(pMsg);
      break;
    case WM_KEY:
      WM_Defaultproc(pMsg);
      break;
    case WM_CREATE:
      pcalogWInWidget[0] = calogShipNumCreate();
      pcalogWInWidget[1] = calogOrderNumCreate();
      pcalogWInWidget[2] = calogDialNumCreate();
      pcalogWInWidget[3] = calogDelCreate();
      pcalogWInWidget[4] = calogTimeCreate();
      pcalogWInWidget[5] = calogDialStateCreate();
    
      WM_AddMyWidget(&calogWin, calogWidList, pcalogWInWidget, 6);
      
      WG_SetVisible(calogTime);
      WG_SetVisible(calogOrderNum);
      WG_SetVisible(calogShipNum);
      WG_SetVisible(calogDialState);
      WG_SetFocus(calogDialNum);
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        CallLogItem* callLogItem;
        callLogItem = CallLog_GetCurrLog();
        if(callLogItem)
        {
          calogShipNumSetVal(callLogItem->who.shipNumber);
          calogDialNumSetVal(callLogItem->who.phoneNumber);
          calogTimeSetVal(callLogItem->when.year, callLogItem->when.month, callLogItem->when.day, callLogItem->when.hour,
                   callLogItem->when.minute, callLogItem->when.second);
          calogOrderNumSetVal(CallLog_GetCurrPos());
          calogDialStateSet(callLogItem->isMiss, callLogItem->dir);
        }
        else
        {
          calogShipNumSetVal(0);
          calogDialNumSetVal(0);
          calogTimeSetVal(0,0,0,0,0,0);
          calogOrderNumSetVal(0);
          calogDialStateSet(0,0);
        }
        Msg.pTarget = &mainWin;
        Msg.msgType = USER_MSG_MENUNUM;
        Msg.data_v =  LCD_S67;
        WM_PostMessage(&Msg);
        Msg.msgType = USER_MSG_MAINTIMEVISIBLE;
        Msg.data_v = 0;
        WM_SendMessage(&Msg);
      }
      else
      {
        Msg.pTarget = &mainWin;
        Msg.msgType = USER_MSG_MAINTIMEVISIBLE;
        Msg.data_v = 1;
        WM_SendMessage(&Msg);
      }
      break;
    case WM_PAINT:
      WM_Defaultproc(pMsg);
      break;
  }
}

Window* WIN_calogWinCreate(void)
{
  WM_Create(&calogWin, calogWinCallback);
  return &calogWin;
}