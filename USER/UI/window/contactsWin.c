#include "dlg.h"
#include "contactsWinDlg.h"
#include "UIMessage.h"
#include "LcdControl.h"
#include "contact.h"
#include "contactsWinDlg.h"
#include "GUI_LCD.h"
#include "GUI_Timer.h"

Window contactsWin;

Widget *pContactsWidget[6];
WidgetList contactsWidList[6];

static void contactsWinCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      WM_Defaultproc(pMsg);
      break;
    case USER_MSG_LOGTOCONTACTS:
      GUI_CreateTimer(&contactsWin, CONT_EDIT_TIMER, CONT_EDIT_TIME_LENGTH);
      WM_SetFocus(&contactsWin);
      WG_SetInVisible(contShipNum);
      WG_SetInVisible(contDialNum);
      WG_SetFocus(contEditShipNum);
      WG_SetVisible(contEditDialNum);
      contEditDialNum->CallBack(pMsg);
      contEditShipNum->CallBack(pMsg);
      WM_BringToTop(&contactsWin);
      break;
    case WM_KEY:
      WM_Defaultproc(pMsg);
      break;
    case WM_CREATE:
      pContactsWidget[0] = contShipNumCreate();
      pContactsWidget[1] = contOrderNumCreate();
      pContactsWidget[2] = contDialNumCreate();
      pContactsWidget[3] = contDelCreate();
      pContactsWidget[4] = contEditDialNumCreate();
      pContactsWidget[5] = contEditShipNumCreate();
    
      WM_AddMyWidget(&contactsWin, contactsWidList, pContactsWidget, 6);
      
      WG_SetVisible(contOrderNum);
      WG_SetVisible(contShipNum);
      WG_SetFocus(contDialNum);
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        LinkMan *linkMan;
        Msg.pTarget = &mainWin;
        Msg.msgType = USER_MSG_MENUNUM;
        Msg.data_v =  LCD_S68;
        WM_PostMessage(&Msg);
        
        linkMan = Contact_GetCurrLinkMan();
        if(linkMan)
        {
          contDialNumSetVal(linkMan->phoneNumber);
          contShipNumSetVal(linkMan->shipNumber);
          contOrderNumSetVal(Contact_GetPosOfLinkMan(linkMan));
        }
        else
        {
          contDialNumSetVal(0);
          contShipNumSetVal(0);
          contOrderNumSetVal(0);
        }
      }
      break;
    case WM_PAINT:
      WM_Defaultproc(pMsg);
      break;
    case WM_TIMER:
      switch(pMsg->data_v)
      {
        case CONT_EDIT_TIMER:
          GUI_DestroyTimer(&contactsWin, CONT_EDIT_TIMER);
          WM_Defaultproc(pMsg);
          break;
      }
      break;
    
  }
}

Window* WIN_contactsWinCreate(void)
{
  WM_Create(&contactsWin, contactsWinCallback);
  return &contactsWin;
}
