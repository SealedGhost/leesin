#ifndef _BECALLEDWINDLG_H_
#define _BECALLEDWINDLG_H_

#define BC_DISP_SHIPNUM      1
#define BC_DISP_OTHERSHIPNUM 2

#include "stdio.h"
#include "stdint.h"
#include "Widget.h"
#include "tones.h"

extern Widget *bcShipNum;
extern Widget *bcRoundButton;
extern Widget *bcNotPickUp;
extern Widget *bcDialNum;
extern Widget* bcOtherDialNum;

extern Window *beCallSourceWin;

Widget* bcShipNumCreate(void);
Widget* bcRoundButtonCreate(void);
Widget* bcNotPickUpCreate(void);
Widget* bcDialNumCreate(void);
Widget* bcOtherDialNumCreate(void);

void bcShipNumSetVal(uint8_t DispMode, uint32_t ShipNum);
void bcDialNumSetVal(uint32_t DialNum);
void bcShipNumSetDispMode(uint8_t DispMode);
uint32_t bcDialNumGetVal(void);
uint32_t bcShipNumGetVal(void);

void bcOtherDialNumSetVal(uint32_t DialNum);
uint32_t bcOtherDialNumGetVal(void);
#endif