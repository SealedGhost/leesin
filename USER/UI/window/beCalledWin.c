#include "beCalledWinDlg.h"
#include "UIMessage.h"
#include "dlg.h"
#include "LcdControl.h"
#include "GUI.h"
#include "GUI_Timer.h"
#include "GUI_LCD.h"

Window beCalledWin;

Window *beCallSourceWin;

Widget* pBeCalledWidget[5];
WidgetList beCalledWidgetList[5];

static void beCalledWinnCallback(WM_Message *pMsg)
{
  switch(pMsg->msgType)
  {
	  case USER_MSG_HUNG_UP:
      WM_Defaultproc(pMsg);
		  break;
    case USER_MSG_BECALLED:
      WG_SetFocus(bcRoundButton);
      WG_SetVisible(bcDialNum);
      WG_SetVisible(bcShipNum);
      beCallSourceWin = pMsg->pSource;
      bcShipNumSetVal(BC_DISP_SHIPNUM, (uint32_t)pMsg->data_p);
    printf("bc shipnum %u\n\n", (uint32_t)pMsg->data_p);
      bcDialNumSetVal(pMsg->data_v);
      WM_SetFocus(&beCalledWin);
      WM_BringToTop(&beCalledWin);
	    AudioPlayCnt(AUDIO_TYPE_CALLER,AUDIO_PLAY_TIMES_CALLER);
      break;
    case USER_MSG_CALL_IN:
      WM_Defaultproc(pMsg);
      break;
    case WM_KEY:
      WM_Defaultproc(pMsg);
      break;
    case WM_CREATE:
      pBeCalledWidget[0] = bcDialNumCreate();
      pBeCalledWidget[1] = bcNotPickUpCreate();
      pBeCalledWidget[2] = bcRoundButtonCreate();
      pBeCalledWidget[3] = bcShipNumCreate();
      pBeCalledWidget[4] = bcOtherDialNumCreate();
    
      WM_AddMyWidget(&beCalledWin, beCalledWidgetList, pBeCalledWidget, 5);

      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
      
      }
      else
      {
        WG_SetInVisible(bcDialNum);
        WG_Paint(bcDialNum);
        WG_SetInVisible(bcNotPickUp);
        WG_Paint(bcNotPickUp);
        WG_SetInVisible(bcRoundButton);
        WG_Paint(bcRoundButton);
        WG_SetInVisible(bcShipNum);
        WG_Paint(bcShipNum);
        WG_SetInVisible(bcOtherDialNum);
        WG_Paint(bcOtherDialNum);
      }
      break;
    case WM_PAINT:
      GUI_LCDSetS(LCD_S61, 0);
      GUI_LCDSetS(LCD_S62, 0);
      GUI_LCDSetS(LCD_S63, 0);
      GUI_LCDSetS(LCD_S64, 0);
      GUI_LCDSetS(LCD_S65, 0);
      GUI_LCDSetS(LCD_S66, 0);
      GUI_LCDSetS(LCD_S67, 0);
      GUI_LCDSetS(LCD_S68, 0);
      GUI_LCDSetS(LCD_S69, 0);
      GUI_LCDSetS(LCD_S70, 0);
      WM_Defaultproc(pMsg);
      break;
  }
}


Window* WIN_beCalledWinCreate(void)
{
  WM_Create(&beCalledWin, beCalledWinnCallback);
  return &dialWin;
}