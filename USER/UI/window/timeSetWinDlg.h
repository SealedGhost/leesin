#ifndef _TIMESETWINDLG_H_
#define _TIMESETWINDLG_H_
#include "stdio.h"
#include "stdint.h"
#include "Widget.h"
#include "tones.h"

#define TMST_EDIT_ACTIVATE   1
#define TMST_EDIT_UNACTIVATE 2

#define TMST_YEAR_LENGTH   4
#define TMST_MONTH_LENGTH  2
#define TMST_DAY_LENGTH    2
#define TMST_TIME_LENGTH   2

typedef struct{
  uint16_t Year;
  uint8_t  Month;
  uint8_t  Day;
}TMST_DATE;

typedef struct{
  uint8_t Hour;
  uint8_t Minute;
  uint8_t Second;
}TMST_TIME;

extern Widget* tmstDate;
extern Widget* tmstTime;

Widget* tmstDateCreate(void);
Widget* tmstTimeCreate(void);

uint8_t tmstTimeGetUnderNum(void);
uint8_t tmstDateGetUnderNum(void);

void tmstDateGetVal(TMST_DATE *data);
void tmstTimeGetVal(TMST_TIME *Time);

void tmstTimeSetActive(uint8_t Active);
void tmstDateSetActive(uint8_t Active);
#endif