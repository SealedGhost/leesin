#ifndef _ANALOGWINDLG_H_
#define _ANALOGWINDLG_H_
#include "stdio.h"
#include "stdint.h"
#include "Widget.h"
#include "tones.h"

#define ANA_EDIT_TIMER 3

enum{
  SCAN_ADD = (uint8_t)1,
  SCAN_DEC = (uint8_t)2,
};

extern Widget* anaChannel;
extern Widget* anaScan;
extern Widget* anaEditChannel;

Widget* anaChannelCreate(void);
Widget* anaScanCreate(void);
Widget* anaEditChannelCreate(void);

void anaChannelSetValue(uint32_t Channel);
uint32_t anaChannelGetValue(void);

void anaScanSetValue(uint32_t Channel);
void anaScanSetModeUp(uint8_t Mode);

void anaChannelSetFlag(uint8_t Flag);

#endif