#include "UIMessage.h"
#include "dlg.h"
#include "LcdControl.h"
#include "callShipNumWinDlg.h"
#include "GUI_LCD.h"
#include "GUI_Timer.h"

Window callShipNumWin;

Widget *pcallShipNumWidget[6];
WidgetList callShipNumWidgetList[6];

static void callShipNumWinCallback(WM_Message *pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_BOAT_INFO_RESPONSE:
      WM_Defaultproc(pMsg);
      break;
    case USER_MSG_CALL_IN:
    {
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &callShipNumWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
    }
      break;
    case WM_KEY:
      WM_Defaultproc(pMsg);
      break;
    case WM_CREATE:
      pcallShipNumWidget[0] = csnShipNumCreate();
      pcallShipNumWidget[1] = csnOrderNumCreate();
      pcallShipNumWidget[2] = csnEditShipNumCreate();
      pcallShipNumWidget[3] = csnDialNumCreate();
      pcallShipNumWidget[4] = csnFialedCreate();
      pcallShipNumWidget[5] = csnSmallShipNumCreate();
      
      WM_AddMyWidget(&callShipNumWin, callShipNumWidgetList, pcallShipNumWidget, 6);
      WG_SetFocus(csnEditShipNum);
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        Msg.pTarget = &mainWin;
        Msg.msgType = USER_MSG_MENUNUM;
        Msg.data_v =  LCD_S62;
        WM_PostMessage(&Msg);      
      }
      break;
    case WM_PAINT:
      WM_Defaultproc(pMsg);
      break;
    case WM_TIMER:
      switch(pMsg->data_v)
      {
        case 1:
          GUI_DestroyTimer(&callShipNumWin, 1);
          WG_SetFocus(csnFialed);
          WG_Paint(csnFialed);
          break;
      }
      break;
  }
}

Window* WIN_callShipNumWinCreate(void)
{
  WM_Create(&callShipNumWin, callShipNumWinCallback);
  return &callShipNumWin;

}

