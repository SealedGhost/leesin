#ifndef _REMEMBERCHWINDLG_H_
#define _REMEMBERCHWINDLG_H_
#include "stdio.h"
#include "stdint.h"
#include "Widget.h"
#include "tones.h"

#define REMBCH_EDIT_TIMER   3

enum{
  SCAN_ADD = (uint8_t)1,
  SCAN_DEC = (uint8_t)2,
};

extern Widget* rembchDel;
extern Widget* rembchChannel;
extern Widget* rembchScan;
extern Widget *rembchOrderNum;


Widget* rembchDelCreate();
Widget* rembchChannelCreate();
Widget* rembchScanCreate();
Widget* rembchOrderNumCreate(void);

void rembchOrderNumSetVal(uint8_t OrderNum);

void rembchChannelSetVal(uint16_t Channel);
void rembchScanSetVal(uint16_t Channel);

uint8_t rembchOrderNumGetVal(void);
#endif