#ifndef _DIALWINDLG_H_
#define _DIALWINDLG_H_
#include "Widget.h"
#include "tones.h"

#define DIAL_DISP_SHIPNUM        1
#define DIAL_DISP_OTHERSHIPNUM   2
#define DIAL_EDITDIALNUM_LENGTH  7

extern Window *pDialWindowSource;

extern Widget *dialDialNum;
extern Widget *dialDialFialed;
extern Widget *dialDialing;
extern Widget *dialDialOver;
extern Widget *dialDialSucceeded;
extern Widget *dialOtherDialNum;
extern Widget *dialRoundButton;
extern Widget *dialShipNum;
extern Widget *dialDialState;


Widget* dialDialNumCreate(void);
Widget* dialDialFialedCreate(void);
Widget* dialDialingCreate(void);
Widget* dialDialOverCreate(void);
Widget* dialDialSucceededCreate(void);
Widget* dialOtherDialNumCreate(void);
Widget* dialRoundButtonCreate(void);
Widget* dialShipNumCreate(void);
Widget* dialDialStateCreate(void);

void dialDialNumSetVal(uint32_t DialNum);
void dialShipNumSetVal(uint8_t DispMode, uint32_t ShipNum);
void dialShipNumSetMode(uint8_t DispMode);
uint32_t dialDialNumGetVal(void);

void dialOtherDialNumSetVal(uint32_t OtherDialNum);
uint32_t dialOtherDialNumGetVal(void);

void dialDialStateSet(uint8_t IsMiss, uint8_t Dir);

uint32_t dialShipNumGetVal(uint8_t DispMode);
#endif