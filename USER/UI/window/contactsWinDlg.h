#ifndef _CONTACTSWINDLG_H_
#define _CONTACTSWINDLG_H_
#include "stdio.h"
#include "stdint.h"
#include "Widget.h"
#include "tones.h"

#define CONT_EDIT_TIME_LENGTH  200

#define CONT_SHIPID_LENGTH   5
#define CONT_DIALNUM_LENGTH  7

#define CONT_EDIT_TIMER   1

extern Widget *contOrderNum;
extern Widget *contShipNum;
extern Widget *contDialNum;
extern Widget *contDel;
extern Widget *contEditShipNum;
extern Widget *contEditDialNum;

Widget* contShipNumCreate(void);
Widget* contOrderNumCreate(void);
Widget* contDialNumCreate(void);
Widget* contDelCreate(void);
Widget* contEditShipNumCreate(void);
Widget* contEditDialNumCreate(void);

void contOrderNumSetVal(uint8_t OrderNum);

void contShipNumSetVal(uint32_t ShipID);
void contDialNumSetVal(uint32_t DialNum);

uint8_t contEditDialNumGetUnderlineNum(void);
uint8_t contEditShipNumGetUnderlineNum(void);

uint32_t contEditShipNumGetVal(void);
uint32_t contEditDialNumGetVal(void);

void contEditShipNumSetUnderlineNum(void);
void contEditDialNumSetUnderlineNum(void);
uint32_t  gdialEditPasswordGetVal(void);
#endif
