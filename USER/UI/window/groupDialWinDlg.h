#ifndef _GROUPDIALWINDLG_H_
#define _GROUPDIALWINDLG_H_
#include "stdio.h"
#include "stdint.h"
#include "Widget.h"
#include "tones.h"

#define GDIAL_PASSWORD_LENGTH 5
#define GDIAL_GROUPID_LENGTH  4

#define GDIAL_EDIT_ACTIVETE 1
#define GDIAL_EDIT_UNACTIVETE 2

#define GROUP_CHANMENUM_LENGTH  3

#define GROUP_RECOMEND_TIMER   1
#define GROUP_EDIT_TIMER       2

extern Widget* gdialChannel;
extern Widget* gdialDialNum;
extern Widget* gdialGroupID;
extern Widget* gdialRecommmendCh;
extern Widget* gdialRoundButton;
extern Widget* gdialShipNum;
extern Widget* gdialEditGroupID;
extern Widget* gdialEditChannel;
extern Widget* gdialEditPassword;
extern Widget *gdialDialState;

Widget* gdialChannelCreate(void);
Widget* gdialDialNumCreate(void);
Widget* gdialGroupIDCreate(void);
Widget* gdialRecommendChCreate(void);
Widget* gdialRoundButtonCreate(void);
Widget* gdialShipNumCreate(void);
Widget* gdialEditGroupIDCreate(void);
Widget* gdialEditChannelCreate(void);
Widget* gdialEditPasswordCreate(void);
Widget* gdialDialStateCreate(void);

uint8_t gdialEditGroupIDGetUnderLineNum(void);
uint8_t gdialEditPasswordGetUnderlineNum(void);
uint8_t gdialGroupIDGetUnderlineNum(void);

void gdialChannelSetValue(uint16_t Channel);
void gdialGroupIDSetValue(uint32_t GroupID);

void gdialEditGroupIDWriteToGroupID(void);

void gdialRecommendChSetValue(uint16_t Channel);

uint16_t gdialRecommendChGetValue(void);

void gdialDialNumSetVal(uint32_t DialNum);

void gdialEditGroupIDSetActive(uint8_t val);
uint8_t gdialEditPasswordSetActive(uint8_t Val);

void gdialEditPasswordSetUnderline(void);
void gdialEditGroupIDSetUnderline(void);

uint16_t gdialEditGroupIDGetVal(void);
uint32_t  gdialEditPasswordGetVal(void);

uint32_t gdialDialNumGetVal(void);

void gdialShipNumSetVal(uint32_t ShipNum);
uint32_t gdialShipNumGetVal(void);

void gdialDialStateSet(uint8_t IsMiss, uint8_t Dir);

uint16_t gdialChannelGetValue(void);
#endif

