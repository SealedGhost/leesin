#include "UIMessage.h"
#include "dlg.h"
#include "LcdControl.h"
#include "digitWinDlg.h"
#include "digital_channel.h"
#include "GUI_LCD.h"
#include "digital_channel.h"
#include "GUI_Timer.h"

Window digitWin;

Widget *pdigitWinWidget[2];
WidgetList digitWinWidgetList[2];

static void digitWinCallback(WM_Message *pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_NOTIFY_RFRX:
      Msg.pTarget = &mainWin;
      Msg.pSource = pMsg->pSource;
      Msg.msgType = pMsg->msgType;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
      break;
    case USER_MSG_CALL_IN:
      WM_Defaultproc(pMsg);
      break;
    case WM_KEY:
      WM_Defaultproc(pMsg);
      break;
    case WM_CREATE:
      pdigitWinWidget[0] = digChannelCreate();
      pdigitWinWidget[1] = digEditChannelCreate();
    
      WM_AddMyWidget(&digitWin, digitWinWidgetList, pdigitWinWidget, 2);
      WG_SetFocus(digChannel);
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        Msg.pTarget = &mainWin;
        Msg.msgType = USER_MSG_MENUNUM;
        Msg.data_v =  LCD_S66;
        WM_PostMessage(&Msg);
        DCH_Enter();
      }
      else
      {
        DCH_Exit();
        Msg.pTarget = &mainWin;
        Msg.msgType = USER_MSG_NOTIFY_RFRX;
        Msg.data_v = 0;
        WM_SendMessage(&Msg);
      }
      break;
    case WM_PAINT:
      WM_Defaultproc(pMsg);
      break;
    case WM_TIMER:
      switch(pMsg->data_v)
      {
        case DIG_EDIT_TIMER:
          GUI_DestroyTimer(&digitWin, DIG_EDIT_TIMER);
          WM_Defaultproc(pMsg);
          break;
      }
      break;
  }
}

Window* WIN_digitWinCreate(void)
{
  WM_Create(&digitWin, digitWinCallback);
  return &digitWin;
}




