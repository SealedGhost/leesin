#include "UIMessage.h"
#include "dlg.h"
#include "LcdControl.h"
#include "weatherWinDlg.h"
#include "GUI.h"
#include "GUI_LCD.h"
#include "tones.h"
#include "channel480.h"

Window weatherWin;


static void weatherWinCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case USER_MSG_CALL_IN:
          {
            Msg.pTarget = &beCalledWin;
            Msg.pSource = &weatherWin;
            Msg.msgType = USER_MSG_BECALLED;
            Msg.data_v = pMsg->data_v;
            Msg.data_p = pMsg->data_p;
            WM_PostMessage(&Msg);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
          }
            break;
          case GUI_KEY_PICKUP:
            WM_SetFocus(&editDialNumWin);
            WM_BringToTop(&editDialNumWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_0_CALLSHIP:
            WM_SetFocus(&callShipNumWin);
            WM_BringToTop(&callShipNumWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_1_GROUP:
            
            WM_SetFocus(&gdialWin);
            WM_BringToTop(&gdialWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_2_DIGIT:
            WM_SetFocus(&digitWin);
            WM_BringToTop(&digitWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_3_SIMULATE:
            WM_BringToTop(&analogWin);
            WM_SetFocus(&analogWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_4_LOG:
            WM_SetFocus(&calogWin);
            WM_BringToTop(&calogWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_5_DIRECTION:
            WM_SetFocus(&contactsWin);
            WM_BringToTop(&contactsWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_6_REMEMBER:
            WM_SetFocus(&rememberChWin);       
            WM_BringToTop(&rememberChWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        Msg.pTarget = &mainWin;
        Msg.msgType = USER_MSG_MENUNUM;
        Msg.data_v =  LCD_S63;
        WM_PostMessage(&Msg);
        EnterWeather(WEATHER_CHANNEL);
      }
      else
      {
        ExitWeather();
      }
      break;
    case WM_PAINT: 
      if(pMsg->data_v)
      {
        GUI_LCDSetNumChar(LCD_NUM14, 'C', 0);
        GUI_LCDSetNumChar(LCD_NUM15, 'H', 0);
        GUI_LCDSetNumChar(LCD_NUM16, '-', 0);
        GUI_LCDSetNumChar(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM18, '2', 0);
        GUI_LCDSetNumChar(LCD_NUM19, '2', 0);
        GUI_LCDSetNumChar(LCD_NUM20, '5', 0);
      }
      else
      {
        GUI_LCDSetNumChar(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM20, LCD_DISPNULL, 0);
      }
      break;
  }
}

Window* WIN_weatherWinCreate(void)
{
  WM_Create(&weatherWin, weatherWinCallback);
  return &weatherWin;
}








