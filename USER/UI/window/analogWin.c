#include "dlg.h"
#include "analogWinDlg.h"
#include "UIMessage.h"
#include "LcdControl.h"
#include "GUI.h"
#include "GUI_Timer.h"
#include "analog_channel.h"
#include "GUI_LCD.h"
#include "channel480.h"
#include "GUI_Timer.h"
#include "channel_favorite.h"

Window analogWin;

Widget *panalogWidget[3];
WidgetList analogWidgetList[3];

static void analogWinCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_NOTIFY_RFRX:
      WM_Defaultproc(pMsg);
      Msg.pTarget = &mainWin;
      Msg.pSource = pMsg->pSource;
      Msg.msgType = pMsg->msgType;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_SendMessage(&Msg);
      break;
    case USER_MSG_CALL_IN:
      WM_Defaultproc(pMsg);
      break;
    case WM_CREATE:
      panalogWidget[0] = anaChannelCreate();
      panalogWidget[1] = anaScanCreate();
      panalogWidget[2] = anaEditChannelCreate();
      
      WG_SetVisible(anaChannel);
      WM_AddMyWidget(&analogWin, analogWidgetList, panalogWidget, 3);
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        anaChannelSetFlag(ChnFavorite_LookFor(ACH_GetCurrChannel()));
        WG_SetFocus(anaChannel);
        Msg.pTarget = &mainWin;
        Msg.msgType = USER_MSG_MENUNUM;
        Msg.data_v =  LCD_S70;
        WM_PostMessage(&Msg);
        EnterAnalog(ACH_GetCurrChannel());
      }
      else
      {
        ExitAnalog();
        Msg.pTarget = &mainWin;
        Msg.msgType = USER_MSG_NOTIFY_RFRX;
        Msg.data_v = 0;
        WM_SendMessage(&Msg);
      }
      break;
    case WM_PAINT:
      WM_Defaultproc(pMsg);
      break;
    case WM_TIMER:
      switch(pMsg->data_v)
      {
        case 1:
          anaScan->CallBack(pMsg);
          GUI_RestartTimer(&analogWin, 1, SCAN_TIMER_LENGTH);
          break;
        case 2:
          anaScan->CallBack(pMsg);
          GUI_DestroyTimer(&analogWin, 2);
          break;
        case ANA_EDIT_TIMER:
          GUI_DestroyTimer(&analogWin, ANA_EDIT_TIMER);
          WM_Defaultproc(pMsg);
          break;
      }
      break;
    default:
      WM_Defaultproc(pMsg);
      break;
  }
}

Window* WIN_analogWinCreate(void)
{
  WM_Create(&analogWin, analogWinCallback);
  return &analogWin;
}