#include "UIMessage.h"
#include "dlg.h"
#include "LcdControl.h"
#include "groupDialWinDlg.h"
#include "GUI_LCD.h"
#include "system_config.h"
#include "group_chat.h"
#include "GUI_Timer.h"

Window gdialWin;

Widget *pGdialWidget[10];
WidgetList gdialWidList[10];

static void gdialWinCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_NOTIFY_RFRX:
      Msg.pTarget = &mainWin;
      Msg.pSource = pMsg->pSource;
      Msg.msgType = pMsg->msgType;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_SendMessage(&Msg);
      break;
    case USER_MSG_HUNG_UP:
      WM_Defaultproc(pMsg);
      break;
    case USER_MSG_CHAN_RECOMMEND:
      WM_Defaultproc(pMsg);
      break;
    case USER_MSG_GRP_SING:
      WM_Defaultproc(pMsg);
      break;
    case USER_MSG_GRP_MUTE:
      WM_Defaultproc(pMsg);
      break;
    case USER_MSG_CALL_IN:
      WM_Defaultproc(pMsg);
      break;
    case WM_KEY:
      WM_Defaultproc(pMsg);
      break;
    case WM_CREATE:
    {
      SysCfg sysCfg;
      SysCfg_GetConfig(&sysCfg);
      
      pGdialWidget[0] = gdialChannelCreate();
      pGdialWidget[1] = gdialDialNumCreate();
      pGdialWidget[2] = gdialGroupIDCreate();
      pGdialWidget[3] = gdialRecommendChCreate();
      pGdialWidget[4] = gdialRoundButtonCreate();
      pGdialWidget[5] = gdialShipNumCreate();
      pGdialWidget[6] = gdialEditChannelCreate();
      pGdialWidget[7] = gdialEditGroupIDCreate();
      pGdialWidget[8] = gdialEditPasswordCreate();
      pGdialWidget[9] = gdialDialStateCreate();
      WM_AddMyWidget(&gdialWin, gdialWidList, pGdialWidget, 10);
      
      gdialGroupIDSetValue(sysCfg.groupNumber);
      if(sysCfg.groupNumber)
      {
        WG_SetVisible(gdialChannel);
        WG_SetVisible(gdialGroupID);
        WG_SetFocus(gdialChannel);
        gdialGroupIDSetValue(sysCfg.groupNumber);
      }
      else
      {
        WG_SetVisible(gdialEditPassword);
        WG_SetFocus(gdialEditGroupID);
      }
    }
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        SysCfg sysCfg;
        SysCfg_GetConfig(&sysCfg);
        
        Msg.pTarget = &mainWin;
        Msg.msgType = USER_MSG_MENUNUM;
        Msg.data_v =  LCD_S69;
        WM_PostMessage(&Msg);
        if(sysCfg.groupNumber)
          Grp_Enter();
        WG_SetInVisible(gdialDialState);
        WG_Paint(gdialDialState);
      }
      else
      {
        Grp_Exit();
        Msg.pTarget = &mainWin;
        Msg.msgType = USER_MSG_NOTIFY_RFRX;
        Msg.data_v = 0;
        WM_SendMessage(&Msg);
      }
      break;
    case WM_PAINT:
      WM_Defaultproc(pMsg);
      break;
    case WM_TIMER:
      switch(pMsg->data_v)
      {
        case GROUP_RECOMEND_TIMER:
          GUI_DestroyTimer(&gdialWin, GROUP_RECOMEND_TIMER);
          WM_Defaultproc(pMsg);
          break;
        case GROUP_EDIT_TIMER:
          GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
          WM_Defaultproc(pMsg);
          break;
      }
      break;
  }
}

Window* Win_gdialWinCreate(void)
{
  WM_Create(&gdialWin, gdialWinCallback);
  return &gdialWin;
}
