#include "UIMessage.h"
#include "dlg.h"
#include "LcdControl.h"
#include "groupSetWinDlg.h"
#include "GUI.h"
#include "GUI_Timer.h"

Window groupSetWin;

Widget* pgroupSetWinWidget[2];
WidgetList groupSetWinWidgetList[2];

static void groupSetWinCallback(WM_Message *pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      WM_Defaultproc(pMsg);
      break;
    case WM_CREATE:
      pgroupSetWinWidget[0] = gsGroupNumCreate();
      pgroupSetWinWidget[1] = gsPasswordCreate();
    
      WM_AddMyWidget(&groupSetWin, groupSetWinWidgetList, pgroupSetWinWidget, 2);
      WG_SetVisible(gsGroupNum);
      WG_SetFocus(gsPassword);
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      WM_Defaultproc(pMsg);
      break;
    case WM_TIMER:
      switch(pMsg->data_v)
      {
        case GS_EDIT_TIMER:
          GUI_DestroyTimer(&groupSetWin, GS_EDIT_TIMER);
          WM_Defaultproc(pMsg);
          break;
      }
      break;
  }
}
  

Window* WIN_groupSetWinCreate(void)
{
  WM_Create(&groupSetWin, groupSetWinCallback);
  return &groupSetWin;
}



