#include "UIMessage.h"
#include "dlg.h"
#include "LcdControl.h"
#include "selfLogWinDlg.h"

Window selfLogWin;

Widget* pselfLogWinWidget[3];
WidgetList selfLogWinWidgetList[3];

static void selfLogWinCallback(WM_Message *pMsg)
{
  switch(pMsg->msgType)
  {
    case USER_MSG_SLSETSHIPNUM:
      WG_SetFocus(slShipNum);
      WG_SetInVisible(slLog);
      WG_SetInVisible(slPassword);
      WM_SetFocus(&selfLogWin);
      WM_Paint(&selfLogWin, 1);
      break;
    case WM_KEY:
      WM_Defaultproc(pMsg);
      break;
    case WM_CREATE:
      pselfLogWinWidget[0] = slPasswordCreate();
      pselfLogWinWidget[1] = slLogCreate();
      pselfLogWinWidget[2] = slShipNumCreate();
      
      WM_AddMyWidget(&selfLogWin, selfLogWinWidgetList, pselfLogWinWidget, 3);
      WG_SetFocus(slLog);
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      WM_Defaultproc(pMsg);
      break;
  }

}

Window* WIN_selfLogWinCreate(void)
{
  WM_Create(&selfLogWin, selfLogWinCallback);
  return &selfLogWin;
}


