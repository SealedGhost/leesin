#ifndef _DIGITWINDLG_H_
#define _DIGITWINDLG_H_
#include "stdio.h"
#include "stdint.h"
#include "Widget.h"
#include "tones.h"

#define DIG_CHANNEL_LENGTH   3

#define DIG_EDIT_TIMER 1

extern Widget* digEditChannel;
extern Widget* digChannel;

Widget* digChannelCreate(void);
Widget* digEditChannelCreate(void);

void digChannelSetVal(uint16_t Channel);
#endif