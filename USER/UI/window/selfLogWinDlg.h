#ifndef _SELFLOGWINDLG_H_
#define _SELFLOGWINDLG_H_
#include "stdio.h"
#include "stdint.h"
#include "Widget.h"
#include "tones.h"

#define SL_EDITSHIPNUM_ACTIVATE   1
#define SL_EDITSHIPNUM_UNACTIVATE 2

#define SL_DISPMODE_DIALNUM  1
#define SL_DISPMODE_SHIPNUM  2
#define SL_DISPMODE_VERSION  3

#define SL_EDITSHIPNUM_LENGTH  5
#define SL_EDITPASSWORD_LENGTH 5

extern Widget* slPassword;
extern Widget* slLog;
extern Widget* slShipNum;


Widget* slPasswordCreate(void);
Widget* slLogCreate(void);
Widget* slShipNumCreate(void);



#endif