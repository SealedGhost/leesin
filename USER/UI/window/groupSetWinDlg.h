#ifndef _GROUPSETWINDLG_H_
#define _GROUPSETWINDLG_H_
#include "stdio.h"
#include "stdint.h"
#include "Widget.h"
#include "tones.h"

#define GS_PASSWORD_LENGTH   5
#define GS_FROUPIS_LENGTH    4

#define GS_EDIT_ACTIVATE      1
#define GS_EDIT_UNACTIVATE    2

#define GS_EDIT_TIMER    1

extern Widget* gsGroupNum;
extern Widget* gsPassword;

Widget* gsGroupNumCreate(void);
Widget* gsPasswordCreate(void);

uint8_t gsPasswordGetUnderLineNum(void);
uint8_t gsGroupNumGetUnderLineNum(void);

void gsPasswordSetUnderLine(void);
uint32_t gsPasswordGetVal(void);
uint16_t gsGroupNumGetVal(void);

void gsPasswordSetActivate(uint8_t ActivateState);

uint8_t gsGroupNumSetVal(uint16_t GroupNum);
void gsGroupNumSetUnderLine();
#endif