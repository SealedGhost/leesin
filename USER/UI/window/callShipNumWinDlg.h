#ifndef _CALLSHIPNUMWINDLG_H_
#define _CALLSHIPNUMWINDLG_H_
#include "stdio.h"
#include "stdint.h"
#include "Widget.h"
#include "tones.h"

#define CSN_EDITSHIPNUM_LENGTH  5

#define CSN_EDITSHIPNUM_ACTIVATE   1
#define CSN_EDITSHIPNUM_UNACTIVATE 2

extern Widget* csnOrderNum;
extern Widget* csnShipNum;
extern Widget *csnEditShipNum;
extern Widget *csnDialNum;
extern Widget *csnFialed;
extern Widget *csnSmallShipNum;


Widget* csnOrderNumCreate(void);
Widget* csnShipNumCreate(void);
Widget* csnEditShipNumCreate(void);
Widget* csnDialNumCreate(void);
Widget* csnSmallShipNumCreate(void);

void csnDialNumSetVal(uint32_t DialNum);

void csnShipNumSetVal(uint32_t ShipNum);

void csnOrderNumSetVal(uint8_t OrderNum);
Widget* csnFialedCreate(void);

void csnSmallShipNumSetVal(uint32_t ShipNum);
#endif