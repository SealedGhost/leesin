#ifndef _MAINWINDLG_H_
#define _MAINWINDLG_H_

#include "Widget.h"

#define MAIN_SIGNAL_OUT 1
#define MAIN_SIGNAL_IN  2

extern Widget *menu;
extern Widget *dateTime;
extern Widget* mainSignal;

Widget* menuCreate(void);
void Menu_SetValue(uint16_t value);

Widget* mainSignalCreate(void);
void mainSignalSetVal(uint8_t Mode, uint8_t Strength);

Widget* dateTimeCreate(void);
void dateTimeSetYear(uint8_t Year);
void dateTimeSetMonth(uint8_t Month);
void dateTimeSetDay(uint8_t Day);
void dateTimeSetHour(uint8_t Hour);
void dateTimeSetMinute(uint8_t Minute);
void dateTimeSetSecond(uint8_t Second);

#endif
