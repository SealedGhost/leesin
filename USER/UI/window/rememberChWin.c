#include "UIMessage.h"
#include "dlg.h"
#include "LcdControl.h"
#include "rememberChWinDlg.h"
#include "GUI_Timer.h"
#include "channel_favorite.h"
#include "GUI_LCD.h"
#include "analog_channel.h"
#include "channel480.h"
#include "GUI_Timer.h"

Window rememberChWin;

Widget* prememberChWinWidget[4];
WidgetList rememberWinWidgetList[4];

static void rememberChWinCallback(WM_Message *pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_NOTIFY_RFTX:
      WM_Defaultproc(pMsg);
      break;
    case USER_MSG_NOTIFY_RFRX:
      WM_Defaultproc(pMsg);
      Msg.pTarget = &mainWin;
      Msg.pSource = pMsg->pSource;
      Msg.msgType = pMsg->msgType;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_SendMessage(&Msg);
      break;
    case USER_MSG_CALL_IN:
      WM_Defaultproc(pMsg);
      break;
    case WM_KEY:
      WM_Defaultproc(pMsg);
      break;
    case WM_CREATE:
      prememberChWinWidget[0] = rembchChannelCreate();
      prememberChWinWidget[1] = rembchScanCreate();
      prememberChWinWidget[2] = rembchDelCreate();
      prememberChWinWidget[3] = rembchOrderNumCreate();
    
      WM_AddMyWidget(&rememberChWin, rememberWinWidgetList, prememberChWinWidget, 4);
      WG_SetVisible(rembchOrderNum);
      WG_SetFocus(rembchChannel);
    
      ChnFavorite_GoHome();
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        Msg.pTarget = &mainWin;
        Msg.msgType = USER_MSG_MENUNUM;
        Msg.data_v =  LCD_S65;
        WM_PostMessage(&Msg);
        rembchOrderNumSetVal(ChnFavorite_GetCurrPos());
        WG_Paint(rembchOrderNum);
        rembchChannelSetVal(ChnFavorite_GetCurrChannel());
        WG_Paint(rembchChannel);
        if(ChnFavorite_GetCurrPos())
        {
          ConfigChannel(ChnFavorite_GetCurrChannel());
          EnterAnalog(ChnFavorite_GetCurrChannel());
        }
      }
      else
      {
        Msg.pTarget = &mainWin;
        Msg.msgType = USER_MSG_NOTIFY_RFRX;
        Msg.data_v = 0;
        WM_SendMessage(&Msg);
        ExitAnalog();
      }
      break;
    case WM_PAINT:
      WM_Defaultproc(pMsg);
      break;
    case WM_TIMER:
      switch(pMsg->data_v)
      {
        case 1:
          GUI_RestartTimer(&rememberChWin, 1, SCAN_TIMER_LENGTH);
          rembchScan->CallBack(pMsg);
          break;
        case 2:
          GUI_DestroyTimer(&rememberChWin, 2);
          rembchScan->CallBack(pMsg);
          break;
        case REMBCH_EDIT_TIMER:
          GUI_DestroyTimer(&rememberChWin, REMBCH_EDIT_TIMER);
          WM_Defaultproc(pMsg);
          break;
      }
      break;
  }
}


Window* WIN_rememberChWinCreate(void)
{
  WM_Create(&rememberChWin, rememberChWinCallback);
  return &rememberChWin;
}