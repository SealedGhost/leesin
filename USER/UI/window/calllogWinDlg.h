#ifndef _CALLLOG_H_
#define _CALLLOG_H_
#include "stdio.h"
#include "stdint.h"
#include "Widget.h"
#include "tones.h"

extern Widget *calogShipNum;
extern Widget *calogOrderNum;
extern Widget *calogDialNum;
extern Widget *calogDel;
extern Widget *calogTime;
extern Widget *calogDialState;

Widget* calogShipNumCreate(void);
Widget* calogOrderNumCreate(void);
Widget* calogDialNumCreate(void);
Widget* calogDelCreate(void);
Widget* calogTimeCreate(void);
Widget* calogDialStateCreate(void);


void calogOrderNumSetVal(uint8_t OrderNum);
void calogShipNumSetVal(uint32_t ShipID);
void calogTimeSetVal(uint16_t Year, uint8_t Month, uint8_t Day, uint8_t Hour, uint8_t Minute, uint8_t Second);
void calogDialStateSet(uint8_t IsMiss, uint8_t Dir);
void calogDialNumSetVal(uint32_t DialNum);
uint8_t calogOrderNumGetVal(void);
uint32_t calogShipNumGetVal(void);
#endif