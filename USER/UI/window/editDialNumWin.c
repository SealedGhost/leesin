#include "editDialNumWinDlg.h"
#include "UIMessage.h"
#include "dlg.h"
#include "LcdControl.h"
#include "GUI.h"
#include "GUI_Timer.h"
#include "GUI_LCD.h"

Window editDialNumWin;

Widget* peditDialNumWidget[1];
WidgetList editDialNumWidgetList[1];

static void editDialNumWinnCallback(WM_Message *pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      WM_Defaultproc(pMsg);
      break;
    case WM_KEY:
      WM_Defaultproc(pMsg);
      break;
    case WM_CREATE:
      peditDialNumWidget[0] = edEditDialNumCreate();
    
      WM_AddMyWidget(&editDialNumWin, editDialNumWidgetList, peditDialNumWidget, 1);
      WG_SetFocus(edEditDialNum);
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        Msg.pTarget = &mainWin;
        Msg.msgType = USER_MSG_MENUNUM;
        Msg.data_v =  LCD_S64;
        WM_PostMessage(&Msg);
        edEditDialNumSetUnderline();
        edEditDialNumSetStates(ED_EDIT_UNACTIVATE);
      }
      break;
    case WM_PAINT:
      WM_Defaultproc(pMsg);
      break;
  }
}


Window* WIN_editDialNumWinCreate(void)
{
  WM_Create(&editDialNumWin, editDialNumWinnCallback);
  return &editDialNumWin;
}