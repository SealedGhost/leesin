#ifndef _EDITDIALNUMWINDLG_H_
#define _EDITDIALNUMWINDLG_H_

#define EDIT_DEIALNUM_LENGTH 7

#include "stdio.h"
#include "stdint.h"
#include "Widget.h"
#include "tones.h"

#define ED_EDIT_ACTIVATE   1
#define ED_EDIT_UNACTIVATE 2

extern Widget *edEditDialNum;


Widget* edEditDialNumCreate();

void edEditDialNumSetUnderline();
uint32_t edEditDialNumGetVal();
void edEditDialNumSetStates(uint8_t Mode);
#endif