#ifndef _DLG_H_
#define _DLG_H_
#include "stdio.h"
#include "stdint.h"
#include "WindowManager.h"

extern Window mainWin;
extern Window dialWin;
extern Window contactsWin;
extern Window calogWin;
extern Window gdialWin;
extern Window analogWin;
extern Window callShipNumWin;
extern Window digitWin;
extern Window groupSetWin;
extern Window rememberChWin;
extern Window selfLogWin;
extern Window timeSetWin;
extern Window weatherWin;
extern Window beCalledWin;
extern Window editDialNumWin;

Window* WIN_mainWinCreate(void);
Window* WIN_dialWinCreate(void);
Window* WIN_contactsWinCreate(void);
Window* WIN_calogWinCreate(void);
Window* Win_gdialWinCreate(void);
Window* WIN_analogWinCreate(void);
Window* WIN_callShipNumWinCreate(void);
Window* WIN_digitWinCreate(void);
Window* WIN_groupSetWinCreate(void);
Window* WIN_rememberChWinCreate(void);
Window* WIN_selfLogWinCreate(void);
Window* WIN_timeSetWinCreate(void);
Window* WIN_weatherWinCreate(void);
Window* WIN_beCalledWinCreate(void);
Window* WIN_editDialNumWinCreate(void);
#endif

