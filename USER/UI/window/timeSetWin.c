#include "UIMessage.h"
#include "dlg.h"
#include "LcdControl.h"
#include "timeSetWinDlg.h"


Window timeSetWin;

Widget* ptimeSetWinWidget[2];
WidgetList timeSetWinWidgetList[2];

static void timeSetWinCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      WM_Defaultproc(pMsg);
      break;
    case WM_CREATE:
      ptimeSetWinWidget[0] = tmstDateCreate();
      ptimeSetWinWidget[1] = tmstTimeCreate();
    
      WM_AddMyWidget(&timeSetWin, timeSetWinWidgetList, ptimeSetWinWidget, 2);
      
      WG_SetFocus(tmstDate);
      WG_SetVisible(tmstTime);
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        WG_SetFocus(tmstDate);
      }
      break;
    case WM_PAINT:
      WM_Defaultproc(pMsg);
      break;
  }
}


Window* WIN_timeSetWinCreate(void)
{
  WM_Create(&timeSetWin, timeSetWinCallback);
  return &timeSetWin;
}

