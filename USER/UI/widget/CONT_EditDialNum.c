#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "contactsWinDlg.h"
#include "GUI.h"
#include "contact.h"
#include "GUI_LCD.h"
#include "GUI_Timer.h"

typedef struct WG_ContEditDialNum{
  Widget Widget;
  char DialNum[8];
}WG_ContEditDialNum;

WG_ContEditDialNum _cont_editdialnum = {0};
Widget *contEditDialNum;


static void contEditDialNumCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  uint8_t Length;
  switch(pMsg->msgType)
  {
    case USER_MSG_LOGTOCONTACTS:
      GUI_Uint32ToChar(pMsg->data_v, _cont_editdialnum.DialNum, CONT_DIALNUM_LENGTH);
      break;
    case USER_MSG_CALL_IN:
      GUI_DestroyTimer(&contactsWin, CONT_EDIT_TIMER);
      GUI_SetCharUnderLine(_cont_editdialnum.DialNum, CONT_DIALNUM_LENGTH);
      contEditShipNumSetUnderlineNum();
      WG_SetInVisible(contEditDialNum);
      WG_Paint(contEditDialNum);
      WG_SetInVisible(contEditShipNum);
      WG_Paint(contEditShipNum);
      WG_SetVisible(contShipNum);
      WG_SetFocus(contDialNum);
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &contactsWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        GUI_RestartTimer(&contactsWin, CONT_EDIT_TIMER, CONT_EDIT_TIME_LENGTH);
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_STAR:
          case GUI_KEY_DEL:
            GUI_CharToUint32(_cont_editdialnum.DialNum, &Length);
            if(Length != CONT_DIALNUM_LENGTH)
            {
              GUI_SetCharUnderLine(_cont_editdialnum.DialNum, CONT_DIALNUM_LENGTH);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(contEditDialNum);
            break;
          case GUI_KEY_BACK:
            GUI_DestroyTimer(&contactsWin, CONT_EDIT_TIMER);
            GUI_SetCharUnderLine(_cont_editdialnum.DialNum, CONT_DIALNUM_LENGTH);
            contOrderNumSetVal(Contact_GetPosOfLinkMan(Contact_GetCurrLinkMan()));
            contEditShipNumSetUnderlineNum();
            WG_SetInVisible(contEditDialNum);
            WG_Paint(contEditDialNum);
            WG_SetInVisible(contEditShipNum);
            WG_Paint(contEditShipNum);
            WG_SetVisible(contShipNum);
            WG_Paint(contShipNum);
            WG_SetFocus(contDialNum);
            WG_Paint(contDialNum);
            WG_Paint(contOrderNum);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_ENTER:
            if(!contEditDialNumGetUnderlineNum() && !contEditShipNumGetUnderlineNum())
            {
              LinkMan *linkMan;
              GUI_DestroyTimer(&contactsWin, CONT_EDIT_TIMER);
              Contact_Add(contEditShipNumGetVal(), contEditDialNumGetVal());
              linkMan = Contact_FindByPhoneId(contEditDialNumGetVal());
              contOrderNumSetVal(Contact_GetPosOfLinkMan(linkMan));
              contShipNumSetVal(linkMan->shipNumber);
              contDialNumSetVal(linkMan->phoneNumber);
              GUI_SetCharUnderLine(_cont_editdialnum.DialNum, CONT_DIALNUM_LENGTH);
              contEditShipNumSetUnderlineNum();
              WG_SetInVisible(contEditDialNum);
              WG_Paint(contEditDialNum);
              WG_SetInVisible(contEditShipNum);
              WG_Paint(contEditShipNum);
              WG_SetVisible(contShipNum);
              WG_Paint(contShipNum);
              WG_SetFocus(contDialNum);
              WG_Paint(contDialNum);
              WG_Paint(contOrderNum);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          case GUI_KEY_UP:
          case GUI_KEY_DOWN:
            WG_SetFocus(contEditShipNum);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_0_CALLSHIP:
            Length = GUI_AddAChar(_cont_editdialnum.DialNum, '0', CONT_DIALNUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_DIALNUM_LENGTH)
            {
              if(contEditShipNumGetUnderlineNum())
              {
                WG_Paint(contEditDialNum);
                WG_SetFocus(contEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditDialNum);
            break;
          case GUI_KEY_1_GROUP:
            Length = GUI_AddAChar(_cont_editdialnum.DialNum, '1', CONT_DIALNUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_DIALNUM_LENGTH)
            {
              if(contEditShipNumGetUnderlineNum())
              {
                WG_Paint(contEditDialNum);
                WG_SetFocus(contEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditDialNum);
            break;
          case GUI_KEY_2_DIGIT:
            Length = GUI_AddAChar(_cont_editdialnum.DialNum, '2', CONT_DIALNUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_DIALNUM_LENGTH)
            {
              if(contEditShipNumGetUnderlineNum())
              {
                WG_Paint(contEditDialNum);
                WG_SetFocus(contEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditDialNum);
            break;
          case GUI_KEY_3_SIMULATE:
            Length = GUI_AddAChar(_cont_editdialnum.DialNum, '3', CONT_DIALNUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_DIALNUM_LENGTH)
            {
              if(contEditShipNumGetUnderlineNum())
              {
                WG_Paint(contEditDialNum);
                WG_SetFocus(contEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditDialNum);
            break;
          case GUI_KEY_4_LOG:
            Length = GUI_AddAChar(_cont_editdialnum.DialNum, '4', CONT_DIALNUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_DIALNUM_LENGTH)
            {
              if(contEditShipNumGetUnderlineNum())
              {
                WG_Paint(contEditDialNum);
                WG_SetFocus(contEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditDialNum);
            break;
          case GUI_KEY_5_DIRECTION:
            Length = GUI_AddAChar(_cont_editdialnum.DialNum, '5', CONT_DIALNUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_DIALNUM_LENGTH)
            {
              if(contEditShipNumGetUnderlineNum())
              {
                WG_Paint(contEditDialNum);
                WG_SetFocus(contEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditDialNum);
            break;
          case GUI_KEY_6_REMEMBER:       
            Length = GUI_AddAChar(_cont_editdialnum.DialNum, '6', CONT_DIALNUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_DIALNUM_LENGTH)
            {
              if(contEditShipNumGetUnderlineNum())
              {
                WG_Paint(contEditDialNum);
                WG_SetFocus(contEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditDialNum);
            break;
          case GUI_KEY_7_SIGN: 
            Length = GUI_AddAChar(_cont_editdialnum.DialNum, '7', CONT_DIALNUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_DIALNUM_LENGTH)
            {
              if(contEditShipNumGetUnderlineNum())
              {
                WG_Paint(contEditDialNum);
                WG_SetFocus(contEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditDialNum);
            break;
          case GUI_KEY_8_WEATHER:        
            Length = GUI_AddAChar(_cont_editdialnum.DialNum, '8', CONT_DIALNUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_DIALNUM_LENGTH)
            {
              if(contEditShipNumGetUnderlineNum())
              {
                WG_Paint(contEditDialNum);
                WG_SetFocus(contEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditDialNum);
            break;
          case GUI_KEY_9_SCAN:         
            Length = GUI_AddAChar(_cont_editdialnum.DialNum, '9', CONT_DIALNUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_DIALNUM_LENGTH)
            {
              if(contEditShipNumGetUnderlineNum())
              {
                WG_Paint(contEditDialNum);
                WG_SetFocus(contEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditDialNum);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      GUI_SetCharUnderLine(_cont_editdialnum.DialNum, CONT_DIALNUM_LENGTH);
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        GUI_SetCharUnderLine(_cont_editdialnum.DialNum, CONT_DIALNUM_LENGTH);
        WG_Paint(contEditDialNum);
      }
      break;
    case WM_PAINT:
      if(_cont_editdialnum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        uint8_t flash = 1;
        if(flash && _cont_editdialnum.DialNum[0] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM14, _cont_editdialnum.DialNum[0], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM14, _cont_editdialnum.DialNum[0], 0);
        }
        
        if(flash && _cont_editdialnum.DialNum[1] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM15, _cont_editdialnum.DialNum[1], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM15, _cont_editdialnum.DialNum[1], 0);
        }
        
        if(flash && _cont_editdialnum.DialNum[2] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM16, _cont_editdialnum.DialNum[2], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM16, _cont_editdialnum.DialNum[2], 0);
        }
        
        if(flash && _cont_editdialnum.DialNum[3] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM17, _cont_editdialnum.DialNum[3], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM17, _cont_editdialnum.DialNum[3], 0);
        }
        
        if(flash && _cont_editdialnum.DialNum[4] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM18, _cont_editdialnum.DialNum[4], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM18, _cont_editdialnum.DialNum[4], 0);
        }
        
        if(flash && _cont_editdialnum.DialNum[5] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM19, _cont_editdialnum.DialNum[5], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM19, _cont_editdialnum.DialNum[5], 0);
        }
        
        if(flash)
        {
          GUI_LCDSetNumChar(LCD_NUM20, _cont_editdialnum.DialNum[6], 1);
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM20, _cont_editdialnum.DialNum[6], 0);
        }
      }
      else
      {
        GUI_LCDDisableNumFlash();
        GUI_LCDSetNumChar(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM20, LCD_DISPNULL, 0);
      }
      break;
    case WM_TIMER:
      if(pMsg->data_v == CONT_EDIT_TIMER)
      {
        GUI_SetCharUnderLine(_cont_editdialnum.DialNum, CONT_DIALNUM_LENGTH);
        contEditShipNumSetUnderlineNum();
        contOrderNumSetVal(Contact_GetPosOfLinkMan(Contact_GetCurrLinkMan()));
        contEditDialNumSetUnderlineNum();
        WG_SetInVisible(contEditDialNum);
        WG_Paint(contEditDialNum);
        WG_SetInVisible(contEditShipNum);
        WG_Paint(contEditShipNum);
        WG_SetVisible(contShipNum);
        WG_Paint(contShipNum);
        WG_SetFocus(contDialNum);
        WG_Paint(contDialNum);
        WG_Paint(contOrderNum);
      }
      break;
  }
  return;
}

Widget* contEditDialNumCreate(void)
{
  WG_Create((Widget*)&_cont_editdialnum, &contactsWin, contEditDialNumCallback);
  contEditDialNum = (Widget*)&_cont_editdialnum;
  return contEditDialNum;
}

void contEditDialNumSetUnderlineNum(void)
{
  GUI_SetCharUnderLine(_cont_editdialnum.DialNum, CONT_DIALNUM_LENGTH);
}

uint8_t contEditDialNumGetUnderlineNum(void)
{
  uint8_t length = 0;
  GUI_CharToUint32(_cont_editdialnum.DialNum, &length);
  return length;
}

uint32_t contEditDialNumGetVal(void)
{
  return GUI_CharToUint32(_cont_editdialnum.DialNum, 0);
}
