#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "beCalledWinDlg.h"
#include "string.h"
#include "GUI.h"
#include "GUI_LCD.h"


typedef struct WG_bcShipNum{
  Widget Widget;
  uint32_t ShipNum;
  uint32_t OtherShipNum;
  uint8_t DispMode;
}WG_bcShipNum;

WG_bcShipNum _bc_shipnum = {0};
Widget* bcShipNum;

static void bcShipNumCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      break;
    case WM_CREATE:
      _bc_shipnum.DispMode = BC_DISP_SHIPNUM;
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_bc_shipnum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S51, 1);
        GUI_LCDSetS(LCD_S40, 1);
        GUI_LCDSetS(LCD_S38, 1);
        GUI_LCDSetS(LCD_S36, 1);
        if(_bc_shipnum.DispMode == BC_DISP_SHIPNUM)
        {
          GUI_LCDSetNumInt(LCD_NUM9, _bc_shipnum.ShipNum / 10000 % 10, 0);
          GUI_LCDSetNumInt(LCD_NUM10, _bc_shipnum.ShipNum / 1000 % 10, 0);
          GUI_LCDSetNumInt(LCD_NUM11, _bc_shipnum.ShipNum / 100 % 10, 0);
          GUI_LCDSetNumInt(LCD_NUM12, _bc_shipnum.ShipNum / 10 % 10, 0);
          GUI_LCDSetNumInt(LCD_NUM13, _bc_shipnum.ShipNum % 10, 0);
        }
        else
        {
          GUI_LCDSetNumInt(LCD_NUM9, _bc_shipnum.OtherShipNum / 10000 % 10, 0);
          GUI_LCDSetNumInt(LCD_NUM10, _bc_shipnum.OtherShipNum / 1000 % 10, 0);
          GUI_LCDSetNumInt(LCD_NUM11, _bc_shipnum.OtherShipNum / 100 % 10, 0);
          GUI_LCDSetNumInt(LCD_NUM12, _bc_shipnum.OtherShipNum / 10 % 10, 0);
          GUI_LCDSetNumInt(LCD_NUM13, _bc_shipnum.OtherShipNum % 10, 0);
        }
      }
      else
      {
        GUI_LCDSetS(LCD_S51, 0);
        GUI_LCDSetS(LCD_S40, 0);
        GUI_LCDSetS(LCD_S38, 0);
        GUI_LCDSetS(LCD_S36, 0);
        GUI_LCDSetNumInt(LCD_NUM9, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM10, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM11, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM12, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM13, LCD_DISPNULL, 0);
      }
      break;
  }
}


Widget* bcShipNumCreate(void)
{
  WG_Create((Widget*)&_bc_shipnum, &beCalledWin, bcShipNumCallback);
  bcShipNum = (Widget*)&_bc_shipnum;
  return bcShipNum;
}

void bcShipNumSetVal(uint8_t DispMode, uint32_t ShipNum)
{
  if(DispMode == BC_DISP_SHIPNUM)
    _bc_shipnum.ShipNum = ShipNum;
  else
    _bc_shipnum.OtherShipNum = ShipNum;
}

uint32_t bcShipNumGetVal()
{
  if(_bc_shipnum.DispMode == BC_DISP_SHIPNUM)
    return _bc_shipnum.ShipNum;
  else
    return _bc_shipnum.OtherShipNum;
}

void bcShipNumSetDispMode(uint8_t DispMode)
{
  _bc_shipnum.DispMode = DispMode;
}








