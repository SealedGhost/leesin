#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "contactsWinDlg.h"
#include "GUI_LCD.h"


typedef struct WG_ContOrderNum{
  Widget Widget;
  uint8_t OrderNum;
}WG_ContOrderNum;

WG_ContOrderNum _cont_orderNum = {0};
Widget *contOrderNum;

static void contOrderNumCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_cont_orderNum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S46, 1);
        GUI_LCDSetS(LCD_S45, 1);
        GUI_LCDSetS(LCD_S44, 1);
        GUI_LCDSetNumInt(LCD_NUM1, _cont_orderNum.OrderNum / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM2, _cont_orderNum.OrderNum % 10, 0);
      }
      else
      {
        GUI_LCDSetS(LCD_S46, 0);
        GUI_LCDSetS(LCD_S45, 0);
        GUI_LCDSetS(LCD_S44, 0);
        GUI_LCDSetNumInt(LCD_NUM1, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM2, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* contOrderNumCreate(void)
{
  WG_Create((Widget*)&_cont_orderNum, &contactsWin, contOrderNumCallback);
  contOrderNum = (Widget*)&_cont_orderNum;
  return contOrderNum;
}

void contOrderNumSetVal(uint8_t OrderNum)
{
  _cont_orderNum.OrderNum = OrderNum;
}


