#include "dialWinDlg.h"
#include "UIMessage.h"
#include "dlg.h"
#include "lcdControl.h"
#include "GUI_LCD.h"
#include "UIMessage.h"
#include "GUI_Key.h"
#include "call_log.h"
#include "call_mgr.h"

typedef struct WG_RoundButton{
  Widget Widget;
  uint8_t val;
}WG_RoundButton;

WG_RoundButton _roundButton = {0};
Widget *dialRoundButton;

static void roundButtonBack(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_HUNG_UP:
      if(pMsg->data_v == dialDialNumGetVal())
      {
        Msg.pTarget = &beCalledWin;
        Msg.pSource = pDialWindowSource;
        Msg.msgType = USER_MSG_BECALLED;
        Msg.data_v = dialOtherDialNumGetVal();
        Msg.data_p = (void*)dialShipNumGetVal(DIAL_DISP_OTHERSHIPNUM);
        WM_PostMessage(&Msg);
      }
      else if(pMsg->data_v == dialOtherDialNumGetVal())
      {
        WG_SetInVisible(dialRoundButton);
        WG_Paint(dialRoundButton);
        dialDialStateSet(CALL_MISS, 0);
        WG_SetVisible(dialDialState);
        WG_Paint(dialDialState);
        WG_SetInVisible(dialOtherDialNum);
        WG_Paint(dialOtherDialNum);
        dialShipNumSetMode(DIAL_DISP_SHIPNUM);
        WG_Paint(dialShipNum);
        WG_SetFocus(dialDialSucceeded);
        AudioStopPlay();
      }
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      { 
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_PICKUP:
            CallMgr_Connect(dialOtherDialNumGetVal());
            dialDialNumSetVal(dialOtherDialNumGetVal());
            WG_SetInVisible(dialOtherDialNum);
            WG_Paint(dialOtherDialNum);
            dialShipNumSetMode(DIAL_DISP_SHIPNUM);
            dialShipNumSetVal(DIAL_DISP_SHIPNUM, dialShipNumGetVal(DIAL_DISP_OTHERSHIPNUM));
            WG_Paint(dialOtherDialNum);
            WG_SetFocus(dialDialSucceeded);
            WG_SetInVisible(dialRoundButton);
            WG_Paint(dialDialSucceeded);
            WG_Paint(dialRoundButton);
            WG_Paint(dialDialNum);
            AudioStopPlay();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_HANGUP:
            CallMgr_HangUp(dialOtherDialNumGetVal());
            dialDialStateSet(CALL_MISS, 0);
            WG_SetVisible(dialDialState);
            WG_Paint(dialDialState);
            WG_SetInVisible(dialOtherDialNum);
            WG_Paint(dialOtherDialNum);
            dialShipNumSetMode(DIAL_DISP_SHIPNUM);
            WG_Paint(dialShipNum);
            WG_SetFocus(dialDialSucceeded);
            WG_SetInVisible(dialRoundButton);
            WG_Paint(dialRoundButton);
            AudioStopPlay();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_roundButton.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S15, 1);
      }
      else
      {
        GUI_LCDSetS(LCD_S15, 0);
      }
      break;
  }
}


Widget* dialRoundButtonCreate(void)
{
  WG_Create((Widget*)&_roundButton, &dialWin, roundButtonBack);
  dialRoundButton = (Widget*)&_roundButton;
  return dialRoundButton;
}