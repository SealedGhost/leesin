#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "beCalledWinDlg.h"
#include "string.h"
#include "GUI.h"
#include "GUI_LCD.h"


typedef struct WG_bcOtherDialNum{
  Widget Widget;
  uint32_t DialNum;
}WG_bcOtherDialNum;

WG_bcOtherDialNum _bc_otherdialnum = {0};
Widget* bcOtherDialNum;

static void bcOtherDialNumCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_bc_otherdialnum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S47, 1);
        GUI_LCDSetS(LCD_S45, 1);
        GUI_LCDSetS(LCD_S43, 1);
        GUI_LCDSetS(LCD_S41, 1);
        GUI_LCDSetNumInt(LCD_NUM1, _bc_otherdialnum.DialNum / 1000000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM2, _bc_otherdialnum.DialNum / 100000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM3, _bc_otherdialnum.DialNum / 10000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM4, _bc_otherdialnum.DialNum / 1000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM5, _bc_otherdialnum.DialNum / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM6, _bc_otherdialnum.DialNum / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM7, _bc_otherdialnum.DialNum % 10, 0);
      }
      else
      {
        GUI_LCDSetS(LCD_S47, 0);
        GUI_LCDSetS(LCD_S45, 0);
        GUI_LCDSetS(LCD_S43, 0);
        GUI_LCDSetS(LCD_S41, 0);
        GUI_LCDSetNumInt(LCD_NUM1, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM2, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM3, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM4, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM5, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM6, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM7, LCD_DISPNULL, 0);
      }
      break;
  }
}


Widget* bcOtherDialNumCreate(void)
{
  WG_Create((Widget*)&_bc_otherdialnum, &beCalledWin, bcOtherDialNumCallback);
  bcOtherDialNum = (Widget*)&_bc_otherdialnum;
  return bcOtherDialNum;
}

void bcOtherDialNumSetVal(uint32_t DialNum)
{
  _bc_otherdialnum.DialNum = DialNum;
}

uint32_t bcOtherDialNumGetVal(void)
{
  return _bc_otherdialnum.DialNum;
}


