#include "UIMessage.h"
#include "dlg.h"
#include "rememberChWinDlg.h"
#include "lcdControl.h"
#include "GUI.h"
#include "GUI_Timer.h"
#include "channel_favorite.h"
#include "GUI_LCD.h"
#include "channel480.h"
#include "GUI_Timer.h"


typedef struct WG_rmbchChannel{
  Widget Widget;
  uint16_t Channel;
}WG_rmbchChannel;

WG_rmbchChannel _rmbch_channel = {0};
Widget *rembchChannel;

static void remberChannelCallback(WM_Message *pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &rememberChWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_PICKUP:
            WM_SetFocus(&editDialNumWin);
            WM_BringToTop(&editDialNumWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_9_SCAN:
            if(ChnFavorite_GetCurrPos())
            {
              rembchScanSetVal(_rmbch_channel.Channel);
              WG_SetInVisible(rembchChannel);
              WG_Paint(rembchChannel);
              WG_SetFocus(rembchScan);
              WG_Paint(rembchScan);
              GUI_CreateTimer(&rememberChWin, 1, SCAN_TIMER_LENGTH);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          case GUI_KEY_DEL:
            if(ChnFavorite_GetCurrPos())
            {
              GUI_CreateTimer(&rememberChWin, REMBCH_EDIT_TIMER, EDIT_TIMEROUT_LENGTH);
              WG_SetInVisible(rembchChannel);
              WG_Paint(rembchChannel);
              WG_SetFocus(rembchDel);
              WG_Paint(rembchDel);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          case GUI_KEY_UP:
            if(ChnFavorite_GetCurrPos())
            {
              _rmbch_channel.Channel = ChnFavorite_GetNextChannel();
              rembchOrderNumSetVal(ChnFavorite_GetCurrPos());
              ConfigChannel(_rmbch_channel.Channel);
              WG_Paint(rembchOrderNum);
              WG_Paint(rembchChannel);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          case GUI_KEY_DOWN:
            if(ChnFavorite_GetCurrPos())
            {
              _rmbch_channel.Channel = ChnFavorite_GetPrevChannel();
              rembchOrderNumSetVal(ChnFavorite_GetCurrPos());
              ConfigChannel(_rmbch_channel.Channel);
              WG_Paint(rembchOrderNum);
              WG_Paint(rembchChannel);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          case GUI_KEY_0_CALLSHIP:
            WM_SetFocus(&callShipNumWin);
            WM_BringToTop(&callShipNumWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_1_GROUP:
            WM_SetFocus(&gdialWin);
            WM_BringToTop(&gdialWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_2_DIGIT:
            WM_SetFocus(&digitWin);
            WM_BringToTop(&digitWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_3_SIMULATE:
            WM_BringToTop(&analogWin);
            WM_SetFocus(&analogWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_4_LOG:
            WM_SetFocus(&calogWin);
            WM_BringToTop(&calogWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_5_DIRECTION:
            WM_SetFocus(&contactsWin);
            WM_BringToTop(&contactsWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_8_WEATHER:
            WM_SetFocus(&weatherWin);        
            WM_BringToTop(&weatherWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      ChnFavorite_GoHome();
      _rmbch_channel.Channel = ChnFavorite_GetCurrChannel();
      rembchOrderNumSetVal(ChnFavorite_GetCurrPos());
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_rmbch_channel.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S32, 1);
        GUI_LCDSetNumInt(LCD_NUM14, 'C', 0);
        GUI_LCDSetNumInt(LCD_NUM15, 'H', 0);
        GUI_LCDSetNumInt(LCD_NUM16, '-', 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, _rmbch_channel.Channel / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM19, _rmbch_channel.Channel / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM20, _rmbch_channel.Channel % 10, 0);
      }
      else
      {
        GUI_LCDSetS(LCD_S32, 0);
        GUI_LCDSetNumInt(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM20, LCD_DISPNULL, 0);
      }
      break;
  }
}

Widget* rembchChannelCreate(void)
{
  WG_Create((Widget*)&_rmbch_channel, &rememberChWin, remberChannelCallback);
  rembchChannel = (Widget*)&_rmbch_channel;
  return rembchChannel;
}

void rembchChannelSetVal(uint16_t Channel)
{
  _rmbch_channel.Channel = Channel;
  ConfigChannel(Channel);
}
