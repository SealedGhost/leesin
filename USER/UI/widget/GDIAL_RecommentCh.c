#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "groupDialWinDlg.h"
#include "GUI.h"
#include "GUI_LCD.h"
#include "group_chat.h"
#include "GUI_Timer.h"
#include "channel480.h"

typedef struct WG_GDailRecommendCh{
  Widget Widget;
  uint16_t RecommendCh;
}WG_GDailRecommendCh;

WG_GDailRecommendCh _gdial_recommendch = {0};
Widget *gdialRecommmendCh;


static void gdialRecommendChCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      GUI_DestroyTimer(&gdialWin, GROUP_RECOMEND_TIMER);
      gdialShipNumSetVal((uint32_t)pMsg->data_p);
      gdialDialNumSetVal(pMsg->data_v);
      WG_SetInVisible(gdialRecommmendCh);
      WG_Paint(gdialRecommmendCh);
      WG_SetInVisible(gdialGroupID);
      WG_Paint(gdialGroupID);
      WG_SetVisible(gdialDialNum);
      WG_Paint(gdialDialNum);
      WG_SetVisible(gdialShipNum);
      WG_Paint(gdialShipNum);
      WG_SetFocus(gdialRoundButton);
      WG_Paint(gdialRoundButton);
      AudioPlayCnt(AUDIO_TYPE_TALKING_CALL,AUDIO_PLAY_TIMES_TALKING_CALL);
      break;
    case USER_MSG_CHAN_RECOMMEND:
      _gdial_recommendch.RecommendCh = pMsg->data_v;
      WG_Paint(gdialRecommmendCh);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_ENTER:
            GUI_DestroyTimer(&gdialWin, GROUP_RECOMEND_TIMER);
            gdialChannelSetValue(_gdial_recommendch.RecommendCh);
            Grp_SetChannel(_gdial_recommendch.RecommendCh);
            ConfigChannel(_gdial_recommendch.RecommendCh);
            WG_SetFocus(gdialChannel);
            WG_SetInVisible(gdialRecommmendCh);
            WG_Paint(gdialRecommmendCh);
            WG_Paint(gdialChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_BACK:
            GUI_DestroyTimer(&gdialWin, GROUP_RECOMEND_TIMER);
            WG_SetInVisible(gdialRecommmendCh);
            WG_Paint(gdialRecommmendCh);
            WG_SetFocus(gdialChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_NEW:
          case GUI_KEY_7_SIGN:
          case GUI_KEY_9_SCAN:
          case GUI_KEY_DEL:
          case GUI_KEY_HANGUP:
          case GUI_KEY_STAR:
          case GUI_KEY_1_GROUP:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          default:
            GUI_DestroyTimer(&gdialWin, GROUP_RECOMEND_TIMER);
            WG_SetInVisible(gdialRecommmendCh);
            WG_Paint(gdialRecommmendCh);
            WG_SetFocus(gdialChannel);
            WM_SendMessage(pMsg);
            break;
        }
      }
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        Grp_RequestRecommendChannel();
      }
      break;
    case WM_PAINT:
      if(_gdial_recommendch.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S52, 1);
        GUI_LCDSetS(LCD_S54, 1);
        GUI_LCDSetS(LCD_S39, 1);
        GUI_LCDSetS(LCD_S38, 1);
        GUI_LCDSetS(LCD_S36, 1);
        GUI_LCDSetNumInt(LCD_NUM11, _gdial_recommendch.RecommendCh / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM12, _gdial_recommendch.RecommendCh / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM13, _gdial_recommendch.RecommendCh % 10, 0);
      }
      else
      {
        GUI_LCDSetS(LCD_S52, 0);
        GUI_LCDSetS(LCD_S54, 0);
        GUI_LCDSetS(LCD_S39, 0);
        GUI_LCDSetS(LCD_S38, 0);
        GUI_LCDSetS(LCD_S36, 0);
        GUI_LCDSetNumInt(LCD_NUM11, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM12, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM13, LCD_DISPNULL, 0);
      }
      break;
    case WM_TIMER:
      WG_SetFocus(gdialChannel);
      WG_SetInVisible(gdialRecommmendCh);
      WG_Paint(gdialRecommmendCh);
      WG_Paint(gdialChannel);
      break;
  }
  return;
}

Widget* gdialRecommendChCreate(void)
{
  WG_Create((Widget*)&_gdial_recommendch, &gdialWin, gdialRecommendChCallback);
  gdialRecommmendCh = (Widget*)&_gdial_recommendch;
  return gdialRecommmendCh;
}

void gdialRecommendChSetValue(uint16_t Channel)
{
  _gdial_recommendch.RecommendCh = Channel;
}

uint16_t gdialRecommendChGetValue(void)
{
  return _gdial_recommendch.RecommendCh;
}

