#include "UIMessage.h"
#include "dlg.h"
#include "rememberChWinDlg.h"
#include "lcdControl.h"
#include "channel_favorite.h"
#include "GUI_LCD.h"


typedef struct WG_rmbchOrderNum{
  Widget Widget;
  uint8_t OrderNum;
  uint8_t Max;
}WG_rmbchOrderNum;

WG_rmbchOrderNum _rmbch_ordernum;
Widget *rembchOrderNum;

static void remberOrderNumCallback(WM_Message *pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_rmbch_ordernum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S46, 1);
        GUI_LCDSetS(LCD_S45, 1);
        GUI_LCDSetS(LCD_S44, 1);
        GUI_LCDSetNumInt(LCD_NUM1, _rmbch_ordernum.OrderNum / 10 %10, 0);
        GUI_LCDSetNumInt(LCD_NUM2, _rmbch_ordernum.OrderNum % 10, 0);
      }
      else
      {
        GUI_LCDSetS(LCD_S46, 0);
        GUI_LCDSetS(LCD_S45, 0);
        GUI_LCDSetS(LCD_S44, 0);
        GUI_LCDSetNumInt(LCD_NUM1, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM2, LCD_DISPNULL, 0);
      }
      break;
  }
}

Widget* rembchOrderNumCreate(void)
{
  WG_Create((Widget*)&_rmbch_ordernum, &rememberChWin, remberOrderNumCallback);
  rembchOrderNum = (Widget*)&_rmbch_ordernum;
  return rembchOrderNum;
}

void rembchOrderNumSetVal(uint8_t OrderNum)
{
  _rmbch_ordernum.OrderNum = OrderNum;
}


uint8_t rembchOrderNumGetVal(void)
{
  return _rmbch_ordernum.OrderNum;
}
  
