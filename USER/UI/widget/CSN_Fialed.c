#include "UIMessage.h"
#include "dlg.h"
#include "callShipNumWinDlg.h"
#include "lcdControl.h"
#include "GUI.h"
#include "GUI_LCD.h"


typedef struct WG_CsnFialed{
  Widget Widget;
}WG_CsnFialed;

WG_CsnFialed _csn_filaled;
Widget *csnFialed;

static void csnFialedCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &callShipNumWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg); 
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      { 
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_BACK:
            WG_SetInVisible(csnFialed);
            WG_Paint(csnFialed);
            WG_SetFocus(csnEditShipNum);
            WG_Paint(csnEditShipNum);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_PTTDOWN:
          case GUI_KEY_0_CALLSHIP:
          case GUI_KEY_7_SIGN:
          case GUI_KEY_9_SCAN:
          case GUI_KEY_STAR:
          case GUI_KEY_DEL:
          case GUI_KEY_ENTER:
          case GUI_KEY_NEW:
          case GUI_KEY_UP:
          case GUI_KEY_DOWN:
          case GUI_KEY_HANGUP:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          default:
            WG_SetInVisible(csnFialed);
            WG_Paint(csnFialed);
            WG_SetFocus(csnEditShipNum);
            WG_Paint(csnEditShipNum);
            csnEditShipNum->CallBack(pMsg);
            break;
        }
      }
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_csn_filaled.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
         GUI_LCDSetS(LCD_S13, 1);
      }
      else
      {
         GUI_LCDSetS(LCD_S13, 0);
      }
      break;
  }
  return;
}

Widget* csnFialedCreate(void)
{
  WG_Create((Widget*)&_csn_filaled, &callShipNumWin, csnFialedCallback);
  csnFialed = (Widget*)&_csn_filaled;
  return csnFialed;
}
