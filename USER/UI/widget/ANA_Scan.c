#include "analogWinDlg.h"
#include "UIMessage.h"
#include "dlg.h"
#include "lcdControl.h"
#include "string.h"
#include "GUI.h"
#include "GUI_Timer.h"
#include "analog_channel.h"
#include "channel_favorite.h"
#include "GUI_LCD.h"
#include "channel480.h"

typedef struct WG_AnaScan{
  Widget Widget;
  uint32_t Channel;
  uint8_t Length;
  uint8_t ScanMode;
  uint8_t ChannelFlag;
}WG_AnaScan;

WG_AnaScan _ana_scan;
Widget* anaScan;

static void anaScanCallBack(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
		case USER_MSG_NOTIFY_RFTX:
      if(pMsg->data_v)
      {
        anaChannelSetValue(_ana_scan.Channel);
        GUI_DestroyTimer(&analogWin, 1);
        GUI_DestroyTimer(&analogWin, 2);
        WG_SetInVisible(anaScan);
        WG_Paint(anaScan);
        WG_SetFocus(anaChannel);
        WG_Paint(anaChannel);
      }
			break;
    case USER_MSG_NOTIFY_RFRX:
      if(pMsg->data_v)
      {
        GUI_DestroyTimer(&analogWin, 1);
      }
      else
      {
        GUI_CreateTimer(&analogWin, 2, 40);
      }
      break;
    case USER_MSG_CALL_IN:
    {
      anaChannelSetValue(_ana_scan.Channel);
      GUI_DestroyTimer(&analogWin, 1);
      GUI_DestroyTimer(&analogWin, 2);
      WG_SetInVisible(anaScan);
      WG_Paint(anaScan);
      WG_SetFocus(anaChannel);
      WG_Paint(anaChannel);
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &analogWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
    }
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_UP:
            anaScanSetModeUp(SCAN_ADD);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_DOWN:
            anaScanSetModeUp(SCAN_DEC);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_BACK:
          case GUI_KEY_9_SCAN:
          case GUI_KEY_ENTER:
            anaChannelSetValue(_ana_scan.Channel);
            GUI_DestroyTimer(&analogWin, 1);
            GUI_DestroyTimer(&analogWin, 2);
            WG_SetInVisible(anaScan);
            WG_Paint(anaScan);
            WG_SetFocus(anaChannel);
            WG_Paint(anaChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
//          case GUI_KEY_BACK:
//            ACH_SetCurrentChannel(anaChannelGetValue());
//            GUI_DestroyTimer(&analogWin, 1);
//            GUI_DestroyTimer(&analogWin, 2);
//            WG_SetInVisible(anaScan);
//            WG_Paint(anaScan);
//            WG_SetFocus(anaChannel);
//            WG_Paint(anaChannel);
//            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
//            break;
          case GUI_KEY_3_SIMULATE:
          case GUI_KEY_7_SIGN:
          case GUI_KEY_STAR:
          case GUI_KEY_DEL:
          case GUI_KEY_NEW:
          case GUI_KEY_HANGUP:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          default:
            anaChannelSetValue(_ana_scan.Channel);
            GUI_DestroyTimer(&analogWin, 1);
            GUI_DestroyTimer(&analogWin, 2);
            WG_SetInVisible(anaScan);
            WG_Paint(anaScan);
            WG_SetFocus(anaChannel);
            WG_Paint(anaChannel);
            anaChannel->CallBack(pMsg);
            break;
        }
      }
      break;
    case WM_CREATE:
      _ana_scan.ScanMode = SCAN_ADD;
      _ana_scan.Length = 3;
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_ana_scan.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S31, 1);
        GUI_LCDSetNumInt(LCD_NUM14, 'C', 0);
        GUI_LCDSetNumInt(LCD_NUM15, 'H', 0);
        GUI_LCDSetNumInt(LCD_NUM16, '-', 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, _ana_scan.Channel / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM19, _ana_scan.Channel / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM20, _ana_scan.Channel % 10, 0);
        if(_ana_scan.ChannelFlag)
        {
          GUI_LCDSetS(LCD_S32, 1);
        }
        else
        {
          GUI_LCDSetS(LCD_S32, 0);
        }
      }
      else 
      {
        GUI_LCDSetS(LCD_S31, 0);
        GUI_LCDSetNumInt(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM20, LCD_DISPNULL, 0);
        GUI_LCDSetS(LCD_S32, 0);
      }
      
      break;
    case WM_TIMER:
    {
      switch(pMsg->data_v)
      {
        case 1:
          if(_ana_scan.ScanMode == SCAN_ADD)
          {
            _ana_scan.Channel = ACH_GetNextChannel();
          }
          else
          {
            _ana_scan.Channel = ACH_GetPrevChannel();
          }
          ConfigChannel(_ana_scan.Channel);
          _ana_scan.ChannelFlag = ChnFavorite_LookFor(_ana_scan.Channel);
          WG_Paint(anaScan);
          break;
        case 2:
          GUI_CreateTimer(&analogWin, 1, SCAN_TIMER_LENGTH);
          break;
      }
    }
      break;
  }
}


Widget* anaScanCreate(void)
{
  WG_Create((Widget*)&_ana_scan, &analogWin, anaScanCallBack);
  anaScan = (Widget*)&_ana_scan;
  return anaScan;
}

void anaScanSetValue(uint32_t Channel)
{
  _ana_scan.Channel = Channel;
}

void anaScanSetModeUp(uint8_t Mode)
{
  _ana_scan.ScanMode = Mode;
}
