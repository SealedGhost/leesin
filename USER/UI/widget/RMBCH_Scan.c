#include "UIMessage.h"
#include "dlg.h"
#include "rememberChWinDlg.h"
#include "lcdControl.h"
#include "GUI.h"
#include "GUI_Timer.H"
#include "channel_favorite.h"
#include "GUI_LCD.h"
#include "channel480.h"

typedef struct WG_rmbchScan{
  Widget Widget;
  uint8_t ScanMode;
  uint16_t Channel;
}WG_rmbchScan;

WG_rmbchScan _rmbch_scan;
Widget *rembchScan;

static void remberScanCallback(WM_Message *pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
		case USER_MSG_NOTIFY_RFTX:
      if(pMsg->data_v)
      {
        GUI_DestroyTimer(&rememberChWin, 1);
        GUI_DestroyTimer(&rememberChWin, 2);
        rembchChannelSetVal(ChnFavorite_GetNextChannel());
        WG_SetInVisible(rembchScan);
        WG_Paint(rembchScan);
        WG_SetFocus(rembchChannel);
        WG_Paint(rembchChannel);
      }
			break;
    case USER_MSG_NOTIFY_RFRX:
      if(pMsg->data_v)
      {
        GUI_DestroyTimer(&rememberChWin, 1);
      }
      else
      {
        GUI_CreateTimer(&rememberChWin, 2, 40);
      }
      break;
    case USER_MSG_CALL_IN:
      GUI_DestroyTimer(&rememberChWin, 1);
      GUI_DestroyTimer(&rememberChWin, 2);
      rembchChannelSetVal(ChnFavorite_GetNextChannel());
      WG_SetInVisible(rembchScan);
      WG_Paint(rembchScan);
      WG_SetFocus(rembchChannel);
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &rememberChWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_UP:
            _rmbch_scan.ScanMode = SCAN_ADD;
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_DOWN:
            _rmbch_scan.ScanMode = SCAN_DEC;
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_BACK:
          case GUI_KEY_PTTDOWN:
          case GUI_KEY_ENTER:
            GUI_DestroyTimer(&rememberChWin, 1);
            GUI_DestroyTimer(&rememberChWin, 2);
            rembchChannelSetVal(_rmbch_scan.Channel);
            WG_SetInVisible(rembchScan);
            WG_Paint(rembchScan);
            WG_SetFocus(rembchChannel);
            WG_Paint(rembchChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_6_REMEMBER:
          case GUI_KEY_7_SIGN:
          case GUI_KEY_9_SCAN:
          case GUI_KEY_STAR:
          case GUI_KEY_DEL:
          case GUI_KEY_NEW:
          case GUI_KEY_HANGUP:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          default:
              printf("befor destory time\n");
            GUI_DestroyTimer(&rememberChWin, 1);
            GUI_DestroyTimer(&rememberChWin, 2);
              printf("after destory time\n");
            rembchChannelSetVal(ChnFavorite_GetNextChannel());
            WG_SetInVisible(rembchScan);
            WG_Paint(rembchScan);
            WG_SetFocus(rembchChannel);
            rembchChannel->CallBack(pMsg);
            break;
        }
      }
      break;
    case WM_CREATE:
      _rmbch_scan.ScanMode = SCAN_ADD;
      _rmbch_scan.Channel = 0;  
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_rmbch_scan.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S32, 1);
        GUI_LCDSetS(LCD_S31, 1);
        GUI_LCDSetNumInt(LCD_NUM14, 'C', 0);
        GUI_LCDSetNumInt(LCD_NUM15, 'H', 0);
        GUI_LCDSetNumInt(LCD_NUM16, '-', 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, _rmbch_scan.Channel / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM19, _rmbch_scan.Channel / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM20, _rmbch_scan.Channel % 10, 0);
      }
      else
      {
        GUI_LCDSetS(LCD_S32, 0);
        GUI_LCDSetS(LCD_S31, 0);
        GUI_LCDSetNumInt(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM20, LCD_DISPNULL, 0);
      }
      break;
    case WM_TIMER:
    {
      switch(pMsg->data_v)
      {
        case 1:
          if(_rmbch_scan.ScanMode == SCAN_ADD)
          {
            _rmbch_scan.Channel = ChnFavorite_GetNextChannel();
            rembchOrderNumSetVal(ChnFavorite_GetCurrPos());
          }
          else
          {
            _rmbch_scan.Channel = ChnFavorite_GetPrevChannel();
            rembchOrderNumSetVal(ChnFavorite_GetCurrPos());
          }
          ConfigChannel(_rmbch_scan.Channel);
          WG_Paint(rembchScan);
          WG_Paint(rembchOrderNum);
          break;
        case 2:
          GUI_CreateTimer(&rememberChWin, 1, SCAN_TIMER_LENGTH);
          break;
      }
    }
      break;
  }
}

Widget* rembchScanCreate(void)
{
  WG_Create((Widget*)&_rmbch_scan, &rememberChWin, remberScanCallback);
  rembchScan = (Widget*)&_rmbch_scan;
  return rembchScan;
}

void rembchScanSetVal(uint16_t Channel)
{
  _rmbch_scan.Channel = Channel;
}