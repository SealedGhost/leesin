#include "UIMessage.h"
#include "dlg.h"
#include "timeSetWinDlg.h"
#include "LcdControl.h"
#include "GUI.h"
#include "GUI_LCD.h"
#include "system_time.h"

uint8_t tmstDateAddChar(char Ch);
uint8_t tmstDateCheck(char Ch);
typedef struct WG_tmstDate{
  Widget Widget;
  char Year[5];
  char Month[3];
  char Day[3];
  uint8_t Activate;
}WG_tmstDate;

WG_tmstDate _tmst_date = {0};
Widget *tmstDate;

static void tmstDateCallback(WM_Message *pMsg)
{
  uint8_t Length;
  switch(pMsg->msgType)
  {
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      { 
        if(_tmst_date.Activate == TMST_EDIT_ACTIVATE)
        {
            switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
            {
              case GUI_KEY_DEL:
              case GUI_KEY_STAR:
                if(tmstDateGetUnderNum())
                {
                  GUI_SetCharUnderLine(_tmst_date.Year, TMST_YEAR_LENGTH);
                  GUI_SetCharUnderLine(_tmst_date.Month, TMST_MONTH_LENGTH);
                  GUI_SetCharUnderLine(_tmst_date.Day, TMST_DAY_LENGTH);
                  WG_Paint(tmstDate);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                }
                else
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                }
                break;
              case GUI_KEY_ENTER:
                if(!tmstDateGetUnderNum() && !tmstTimeGetUnderNum())
                {
                  TMST_DATE Date;
                  TMST_TIME Time;
                  _tmst_date.Activate = TMST_EDIT_UNACTIVATE;
                  WG_Paint(tmstDate);
                  tmstDateGetVal(&Date);
                  tmstTimeGetVal(&Time);
                  SysTime_Set(Date.Year, Date.Month, Date.Day, 
                      Time.Hour, Time.Minute, Time.Second);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                }
                else
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                }
                break;
              case GUI_KEY_UP:
              case GUI_KEY_DOWN:
                if(tmstDateGetUnderNum())
                {
                  GUI_SetCharUnderLine(_tmst_date.Year, TMST_YEAR_LENGTH);
                  GUI_SetCharUnderLine(_tmst_date.Month, TMST_MONTH_LENGTH);
                  GUI_SetCharUnderLine(_tmst_date.Day, TMST_DAY_LENGTH);
                  WG_Paint(tmstDate);
                }
                WG_SetFocus(tmstTime);
                tmstTimeSetActive(TMST_EDIT_ACTIVATE);
                WG_Paint(tmstTime);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              case GUI_KEY_0_CALLSHIP:
                if(!tmstDateCheck('0'))
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                }  
                Length = tmstDateAddChar('0');
                if(!Length)
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                }
                if(Length == 8)
                {
                  if(tmstTimeGetUnderNum())
                  {
                    WG_SetFocus(tmstTime);
                    WG_Paint(tmstTime);
                  }
                }
                WG_Paint(tmstDate);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              case GUI_KEY_1_GROUP:
                if(!tmstDateCheck('1'))
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                } 
                Length = tmstDateAddChar('1');
                if(!Length)
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                }
                if(Length == 8)
                {
                  if(tmstTimeGetUnderNum())
                  {
                    WG_SetFocus(tmstTime);
                    WG_Paint(tmstTime);
                  }
                }
                WG_Paint(tmstDate);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              case GUI_KEY_2_DIGIT:
                if(!tmstDateCheck('2'))
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                } 
                Length = tmstDateAddChar('2');
                if(!Length)
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                }
                if(Length == 8)
                {
                  if(tmstTimeGetUnderNum())
                  {
                    WG_SetFocus(tmstTime);
                    WG_Paint(tmstTime);
                  }
                }
                WG_Paint(tmstDate);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              case GUI_KEY_3_SIMULATE:
                if(!tmstDateCheck('3'))
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                } 
                Length = tmstDateAddChar('3');
                if(!Length)
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                }
                if(Length == 8)
                {
                  if(tmstTimeGetUnderNum())
                  {
                    WG_SetFocus(tmstTime);
                    WG_Paint(tmstTime);
                  }
                }
                WG_Paint(tmstDate);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              case GUI_KEY_4_LOG:
                if(!tmstDateCheck('4'))
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                } 
                Length = tmstDateAddChar('4');
                if(!Length)
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                }
                if(Length == 8)
                {
                  if(tmstTimeGetUnderNum())
                  {
                    WG_SetFocus(tmstTime);
                    WG_Paint(tmstTime);
                  }
                }
                WG_Paint(tmstDate);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              case GUI_KEY_5_DIRECTION:
                if(!tmstDateCheck('5'))
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                } 
                Length = tmstDateAddChar('5');
                if(!Length)
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                }
                if(Length == 8)
                {
                  if(tmstTimeGetUnderNum())
                  {
                    WG_SetFocus(tmstTime);
                    WG_Paint(tmstTime);
                  }
                }
                WG_Paint(tmstDate);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              case GUI_KEY_6_REMEMBER:       
                if(!tmstDateCheck('6'))
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                } 
                Length = tmstDateAddChar('6');
                if(!Length)
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                }
                if(Length == 8)
                {
                  if(tmstTimeGetUnderNum())
                  {
                    WG_SetFocus(tmstTime);
                    WG_Paint(tmstTime);
                  }
                }
                WG_Paint(tmstDate);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              case GUI_KEY_7_SIGN: 
                if(!tmstDateCheck('7'))
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                } 
                Length = tmstDateAddChar('7');
                if(!Length)
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                }
                if(Length == 8)
                {
                  if(tmstTimeGetUnderNum())
                  {
                    WG_SetFocus(tmstTime);
                    WG_Paint(tmstTime);
                  }
                }
                WG_Paint(tmstDate);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              case GUI_KEY_8_WEATHER:  
                if(!tmstDateCheck('8'))
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                }             
                Length = tmstDateAddChar('8');
                if(!Length)
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                }
                if(Length == 8)
                {
                  if(tmstTimeGetUnderNum())
                  {
                    WG_SetFocus(tmstTime);
                    WG_Paint(tmstTime);
                  }
                }
                WG_Paint(tmstDate);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              case GUI_KEY_9_SCAN:      
                if(!tmstDateCheck('9'))
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                } 
                Length = tmstDateAddChar('9');
                if(!Length)
                {
                  AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                  break;
                }
                if(Length == 8)
                {
                  if(tmstTimeGetUnderNum())
                  {
                    WG_SetFocus(tmstTime);
                    WG_Paint(tmstTime);
                  }
                }
                WG_Paint(tmstDate);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              default:
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
            }
        }
        else
        {
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
        }
      }
      break;
    case WM_CREATE:
      GUI_SetCharUnderLine(_tmst_date.Year, TMST_YEAR_LENGTH);
      GUI_SetCharUnderLine(_tmst_date.Month, TMST_MONTH_LENGTH);
      GUI_SetCharUnderLine(_tmst_date.Day, TMST_DAY_LENGTH);
      _tmst_date.Activate = TMST_EDIT_ACTIVATE;
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        GUI_SetCharUnderLine(_tmst_date.Year, TMST_YEAR_LENGTH);
        GUI_SetCharUnderLine(_tmst_date.Month, TMST_MONTH_LENGTH);
        GUI_SetCharUnderLine(_tmst_date.Day, TMST_DAY_LENGTH);
      }
      else
      {
        _tmst_date.Activate = TMST_EDIT_UNACTIVATE;
      }
      break;
    case WM_PAINT:
      if(_tmst_date.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        uint8_t flash = 1;
        GUI_LCDSetS(LCD_S49, 1);
        GUI_LCDSetS(LCD_S41, 1);
        GUI_LCDSetS(LCD_S42, 1);
        
        if(_tmst_date.Activate == TMST_EDIT_UNACTIVATE) flash = 0;
        
        if(flash && _tmst_date.Year[0] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM4, _tmst_date.Year[0], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM4, _tmst_date.Year[0], 0);
        }
        
        if(flash && _tmst_date.Year[1] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM5, _tmst_date.Year[1], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM5, _tmst_date.Year[1], 0);
        }
        
        if(flash && _tmst_date.Year[2] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM6, _tmst_date.Year[2], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM6, _tmst_date.Year[2], 0);
        }
        
        if(flash && _tmst_date.Year[3] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM7, _tmst_date.Year[3], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM7, _tmst_date.Year[3], 0);
        }
        
        GUI_LCDSetS(LCD_S50, 1);
        GUI_LCDSetS(LCD_S40, 1);
        GUI_LCDSetS(LCD_S39, 1);
        
        if(flash && _tmst_date.Month[0] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM9, _tmst_date.Month[0], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM9, _tmst_date.Month[0], 0);
        }
        
        if(flash && _tmst_date.Month[1] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM10, _tmst_date.Month[1], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM10, _tmst_date.Month[1], 0);
        }
        
        GUI_LCDSetS(LCD_S53, 1);
        GUI_LCDSetS(LCD_S37, 1);
        GUI_LCDSetS(LCD_S36, 1);
        
        if(flash && _tmst_date.Day[0] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM12, _tmst_date.Day[0], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM12, _tmst_date.Day[0], 0);
        }
        
        if(flash)
        {
          GUI_LCDSetNumChar(LCD_NUM13, _tmst_date.Day[1], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM13, _tmst_date.Day[1], 0);
        }
      }
      else
      {
        GUI_LCDDisableNumFlash();
        GUI_LCDSetS(LCD_S49, 0);
        GUI_LCDSetS(LCD_S41, 0);
        GUI_LCDSetS(LCD_S42, 0);
        
        GUI_LCDSetNumChar(LCD_NUM4, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM5, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM6, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM7, LCD_DISPNULL, 0);
        
        GUI_LCDSetS(LCD_S50, 0);
        GUI_LCDSetS(LCD_S40, 0);
        GUI_LCDSetS(LCD_S39, 0);
        
        GUI_LCDSetNumChar(LCD_NUM9, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM10, LCD_DISPNULL, 0);
        
        GUI_LCDSetS(LCD_S53, 0);
        GUI_LCDSetS(LCD_S37, 0);
        GUI_LCDSetS(LCD_S36, 0);
        
        GUI_LCDSetNumChar(LCD_NUM12,LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM13,LCD_DISPNULL, 0);
        
      }
      break;
  }
}

Widget* tmstDateCreate(void)
{
  WG_Create((Widget*)&_tmst_date, &timeSetWin, tmstDateCallback);
  tmstDate = (Widget*)&_tmst_date;
  return tmstDate;
}

uint8_t tmstDateAddChar(char Ch)
{
  uint8_t length = 0;
  GUI_CharToUint16(_tmst_date.Year, &length);
  if(length)
  {
    return GUI_AddAChar(_tmst_date.Year, Ch, TMST_YEAR_LENGTH);
  }
  length = 0;
  GUI_CharToUint8(_tmst_date.Month, &length);
  if(length)
  {
    return GUI_AddAChar(_tmst_date.Month, Ch, TMST_MONTH_LENGTH) + 4;
  }
  length = 0;
  GUI_CharToUint8(_tmst_date.Day, &length);
  if(length)
  {
    return GUI_AddAChar(_tmst_date.Day, Ch, TMST_DAY_LENGTH) + 6;
  }
  return 0;
}

uint8_t tmstDateGetUnderNum(void)
{
  uint8_t length = 0, sum = 0;
  GUI_CharToUint16(_tmst_date.Year, &length);
  sum += length;
  GUI_CharToUint8(_tmst_date.Month, &length);
  sum += length;
  GUI_CharToUint8(_tmst_date.Day, &length);
  sum += length;
  return sum;
}

void tmstDateGetVal(TMST_DATE *data)
{
  data->Year = GUI_CharToUint16(_tmst_date.Year, 0);
  data->Month = GUI_CharToUint8(_tmst_date.Month, 0);
  data->Day = GUI_CharToUint8(_tmst_date.Day, 0);
}

void tmstDateSetActive(uint8_t Active)
{
  _tmst_date.Activate = Active;
}

uint8_t tmstDateCheck(char Ch)
{
  uint8_t length = 0;
  uint16_t tmp = 0;
  uint16_t Year = 0, Month = 0;
  
  tmp = GUI_CharToUint16(_tmst_date.Year, &length);
  if(length)
  {
    if(length == 1)
    {
      tmp = tmp * 10 + Ch - '0';
      if(tmp == 0)
        return 0;
    }
    return 1;
  }
  else
    Year = tmp;
  
  length = 0;
  tmp = 0;
  tmp = GUI_CharToUint8(_tmst_date.Month, &length);
  if(length)
  {
    if(length == 2 && (Ch > '1' || Ch < '0'))
      return 0;
    if(length == 1)
    {
      tmp = tmp * 10 + Ch - '0';
      if(tmp > 12 || tmp < 1)
        return 0;
    }
    return 1;
  }
  else
    Month = tmp;
  
  length = 0;
  tmp = 0;
  tmp = GUI_CharToUint8(_tmst_date.Day, &length);
  if(length)
  {
    if(length == 2)
    {
      if(Month != 2)
      {
        if(Ch > '3' || Ch < '0')
          return 0;
        else
          return 1;
      }
      else
      {
        if(Ch > '2' || Ch < '0')
          return 0;
        else
          return 1;
      }
    }
    
    if(length == 1)
    {
      tmp = tmp * 10 + Ch - '0';
      if(Month == 1 || Month == 3 || Month == 5 || Month == 7 || Month == 8 || Month == 10 || Month == 12)
      {
        if(tmp > 31 || tmp < 1)
          return 0;
        return 1;
      }
      else if(Month == 2)
      {
        if(((!(Year % 4)) && (Year % 100)) || (!(Year % 400)))
        {
          if(tmp > 29 || tmp < 1)
            return 0;
          return 1;
        }
        else
        {
          if(tmp > 28 || tmp < 1)
            return 0;
          return 1;
        }
      }
      else if(Month == 4 || Month == 9 || Month == 11)
      {
        if(tmp > 30 || tmp < 1)
          return 0;
        return 1;
      }
    }
  }
  return 1;
}
