#include "UIMessage.h"
#include "dlg.h"
#include "timeSetWinDlg.h"
#include "LcdControl.h"
#include "GUI.h"
#include "GUI_LCD.h"
#include "system_time.h"

uint8_t tmstTimeCheck(char Ch);

uint8_t tmstTimeAddChar(char Ch);

typedef struct WG_tmstTime{
  Widget Widget;
  char Hour[3];
  char Minute[3];
  char Second[3];
  uint8_t Activate;
}WG_tmstTime;

WG_tmstTime _tmst_time = {0};
Widget *tmstTime;

static void tmstTimeCallback(WM_Message *pMsg)
{
  uint8_t Length;
  switch(pMsg->msgType)
  {
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      { 
        if(_tmst_time.Activate == TMST_EDIT_ACTIVATE)
        {
          switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
          {
            case GUI_KEY_STAR:
            case GUI_KEY_DEL:
              if(tmstTimeGetUnderNum())
              {
                GUI_SetCharUnderLine(_tmst_time.Hour, TMST_TIME_LENGTH);
                GUI_SetCharUnderLine(_tmst_time.Minute, TMST_TIME_LENGTH);
                GUI_SetCharUnderLine(_tmst_time.Second, TMST_TIME_LENGTH);
                WG_Paint(tmstTime);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              break;
            case GUI_KEY_ENTER:
              if(!tmstDateGetUnderNum() && !tmstTimeGetUnderNum())
              {
                TMST_DATE Date;
                TMST_TIME Time;
                _tmst_time.Activate = TMST_EDIT_UNACTIVATE;
                WG_Paint(tmstTime);
                tmstDateGetVal(&Date);
                tmstTimeGetVal(&Time);
                SysTime_Set(Date.Year, Date.Month, Date.Day, 
                    Time.Hour, Time.Minute, Time.Second);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              break;
            case GUI_KEY_UP:
            case GUI_KEY_DOWN:
              if(tmstTimeGetUnderNum())
              {
                GUI_SetCharUnderLine(_tmst_time.Hour, TMST_TIME_LENGTH);
                GUI_SetCharUnderLine(_tmst_time.Minute, TMST_TIME_LENGTH);
                GUI_SetCharUnderLine(_tmst_time.Second, TMST_TIME_LENGTH);
                WG_Paint(tmstTime);
              }
              WG_SetFocus(tmstDate);
              tmstDateSetActive(TMST_EDIT_ACTIVATE);
              WG_Paint(tmstDate);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_0_CALLSHIP:
              if(!tmstTimeCheck('0'))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              Length = tmstTimeAddChar('0');
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == 6)
              {
                if(tmstTimeGetUnderNum())
                {
                  WG_SetFocus(tmstDate);
                  WG_Paint(tmstDate);
                }
              }
              WG_Paint(tmstTime);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_1_GROUP:
              if(!tmstTimeCheck('1'))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              Length = tmstTimeAddChar('1');
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == 6)
              {
                if(tmstTimeGetUnderNum())
                {
                  WG_SetFocus(tmstDate);
                  WG_Paint(tmstDate);
                }
              }
              WG_Paint(tmstTime);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_2_DIGIT:
              if(!tmstTimeCheck('2'))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
             Length = tmstTimeAddChar('2');
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == 6)
              {
                if(tmstTimeGetUnderNum())
                {
                  WG_SetFocus(tmstDate);
                  WG_Paint(tmstDate);
                }
              }
              WG_Paint(tmstTime);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_3_SIMULATE:
              if(!tmstTimeCheck('3'))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              Length = tmstTimeAddChar('3');
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == 6)
              {
                if(tmstTimeGetUnderNum())
                {
                  WG_SetFocus(tmstDate);
                  WG_Paint(tmstDate);
                }
              }
              WG_Paint(tmstTime);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_4_LOG:
              if(!tmstTimeCheck('4'))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              Length = tmstTimeAddChar('4');
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == 6)
              {
                if(tmstTimeGetUnderNum())
                {
                  WG_SetFocus(tmstDate);
                  WG_Paint(tmstDate);
                }
              }
              WG_Paint(tmstTime);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_5_DIRECTION:
              if(!tmstTimeCheck('5'))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              Length = tmstTimeAddChar('5');
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == 6)
              {
                if(tmstTimeGetUnderNum())
                {
                  WG_SetFocus(tmstDate);
                  WG_Paint(tmstDate);
                }
              }
              WG_Paint(tmstTime);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_6_REMEMBER:      
              if(!tmstTimeCheck('6'))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }            
              Length = tmstTimeAddChar('6');
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == 6)
              {
                if(tmstTimeGetUnderNum())
                {
                  WG_SetFocus(tmstDate);
                  WG_Paint(tmstDate);
                }
              }
              WG_Paint(tmstTime);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_7_SIGN:
              if(!tmstTimeCheck('7'))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              Length = tmstTimeAddChar('7');
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == 6)
              {
                if(tmstTimeGetUnderNum())
                {
                  WG_SetFocus(tmstDate);
                  WG_Paint(tmstDate);
                }
              }
              WG_Paint(tmstTime);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_8_WEATHER:  
              if(!tmstTimeCheck('8'))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }            
              Length = tmstTimeAddChar('8');
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == 6)
              {
                if(tmstTimeGetUnderNum())
                {
                  WG_SetFocus(tmstDate);
                  WG_Paint(tmstDate);
                }
              }
              WG_Paint(tmstTime);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_9_SCAN:
              if(!tmstTimeCheck('9'))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              Length = tmstTimeAddChar('9');
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == 6)
              {
                if(tmstTimeGetUnderNum())
                {
                  WG_SetFocus(tmstDate);
                  WG_Paint(tmstDate);
                }
              }
              WG_Paint(tmstTime);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            default:
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
          }
        }
        else
        {
          AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
        }
      }
      break;
    case WM_CREATE:
      GUI_SetCharUnderLine(_tmst_time.Hour, TMST_TIME_LENGTH);
      GUI_SetCharUnderLine(_tmst_time.Minute, TMST_TIME_LENGTH);
      GUI_SetCharUnderLine(_tmst_time.Second, TMST_TIME_LENGTH);
      _tmst_time.Activate = TMST_EDIT_ACTIVATE;
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        GUI_SetCharUnderLine(_tmst_time.Hour, TMST_TIME_LENGTH);
        GUI_SetCharUnderLine(_tmst_time.Minute, TMST_TIME_LENGTH);
        GUI_SetCharUnderLine(_tmst_time.Second, TMST_TIME_LENGTH);
        
      }
      else
      {
        _tmst_time.Activate = TMST_EDIT_UNACTIVATE;
      }
      break;
    case WM_PAINT:
      if(_tmst_time.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        uint8_t flash = 1;
        GUI_LCDSetS(LCD_S20, 1);
        GUI_LCDSetS(LCD_S18, 1);
        
        if(_tmst_time.Activate == TMST_EDIT_UNACTIVATE) flash = 0;
        
        if(flash && _tmst_time.Hour[0] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM15, _tmst_time.Hour[0], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM15, _tmst_time.Hour[0], 0);
        }
        
        if(flash && _tmst_time.Hour[1] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM16, _tmst_time.Hour[1], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM16, _tmst_time.Hour[1], 0);
        }
        
        if(flash && _tmst_time.Minute[0] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM17, _tmst_time.Minute[0], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM17, _tmst_time.Minute[0], 0);
        }
        
        if(flash && _tmst_time.Minute[1] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM18, _tmst_time.Minute[1], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM18, _tmst_time.Minute[1], 0);
        }
        
        if(flash && _tmst_time.Second[0] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM19, _tmst_time.Second[0], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM19, _tmst_time.Second[0], 0);
        }
        
        if(flash)
        {
          GUI_LCDSetNumChar(LCD_NUM20, _tmst_time.Second[1], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM20, _tmst_time.Second[1], 0);
        }
      }
      else
      {
        GUI_LCDSetS(LCD_S20, 0);
        GUI_LCDSetS(LCD_S18, 0);
        
        GUI_LCDSetNumChar(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM20, LCD_DISPNULL, 0);
      }
      break;
      break;
  }
}

Widget* tmstTimeCreate(void)
{
  WG_Create((Widget*)&_tmst_time, &timeSetWin, tmstTimeCallback);
  tmstTime = (Widget*)&_tmst_time;
  return tmstTime;
}

uint8_t tmstTimeAddChar(char Ch)
{
  uint8_t length = 0;
  GUI_CharToUint16(_tmst_time.Hour, &length);
  if(length)
  {
    return GUI_AddAChar(_tmst_time.Hour, Ch, TMST_TIME_LENGTH);
  }
  length = 0;
  GUI_CharToUint8(_tmst_time.Minute, &length);
  if(length)
  {
    return GUI_AddAChar(_tmst_time.Minute, Ch, TMST_TIME_LENGTH) + 2;
  }
  length = 0;
  GUI_CharToUint8(_tmst_time.Second, &length);
  if(length)
  {
    return GUI_AddAChar(_tmst_time.Second, Ch, TMST_TIME_LENGTH) + 4;
  }
  return 0;
}

uint8_t tmstTimeGetUnderNum()
{
  uint8_t length = 0;
  uint8_t sum = 0;
  GUI_CharToUint8(_tmst_time.Second, &length);
  sum += length;
  GUI_CharToUint8(_tmst_time.Minute, &length);
  sum += length;
  GUI_CharToUint8(_tmst_time.Second, &length);
  sum += length;
  return sum;
}

void tmstTimeGetVal(TMST_TIME *Time)
{
  Time->Hour = GUI_CharToUint8(_tmst_time.Hour, 0);
  Time->Minute = GUI_CharToUint8(_tmst_time.Minute, 0);
  Time->Second = GUI_CharToUint8(_tmst_time.Second, 0);
}

void tmstTimeSetActive(uint8_t Active)
{
  _tmst_time.Activate = Active;
}

uint8_t tmstTimeCheck(char Ch)
{
  uint8_t length = 0;
  uint16_t tmp = 0;
  tmp = GUI_CharToUint16(_tmst_time.Hour, &length);
  if(length)
  {
    if(length == 2 && (Ch > '2' || Ch < '0'))
      return 0;
    if(length == 1)
    {
      tmp = tmp * 10 + Ch - '0';
      if(tmp > 23)
        return 0;
    }
    return 1;
  }
  
  length = 0;
  tmp = 0;
  tmp = GUI_CharToUint8(_tmst_time.Minute, &length);
  if(length)
  {
    if(length == 2 && (Ch > '5' || Ch < '0'))
      return 0;
    if(length == 1)
    {
      tmp = tmp * 10 + Ch - '0';
      if(tmp > 59)
        return 0;
    }
    return 1;
  }
  
  length = 0;
  tmp = 0;
  tmp = GUI_CharToUint8(_tmst_time.Second, &length);
  if(length)
  {
    if(length == 2 && (Ch > '5' || Ch < '0'))
      return 0;
    if(length == 1)
    {
      tmp = tmp * 10 + Ch - '0';
      if(tmp > 59)
        return 0;
    }
    return 1;
  }
  return 1;
}
