#include "analogWinDlg.h"
#include "UIMessage.h"
#include "dlg.h"
#include "GUI.h"
#include "LcdControl.h"
#include "string.h"
#include "GUI_Timer.h"
#include "analog_channel.h"
#include "channel_favorite.h"
#include "GUI_LCD.h"
#include "channel480.h"
#include "GUI_Timer.h"

typedef struct WG_AnaChannel{
  Widget Widget;
  uint32_t Channel;
  uint8_t Length;
  uint8_t ChannelFlag;
}WG_AnaChannel;

WG_AnaChannel _ana_channel = {0};
Widget* anaChannel;


static void anaChannelCallBack(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
    {
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &analogWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
    }
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_PICKUP:
            WM_SetFocus(&editDialNumWin);
            WM_BringToTop(&editDialNumWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_DOWN:
            _ana_channel.Channel = ACH_GetPrevChannel();
            _ana_channel.ChannelFlag = ChnFavorite_LookFor(_ana_channel.Channel);
            ConfigChannel(_ana_channel.Channel);
            WG_Paint(anaChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_UP:
            _ana_channel.Channel = ACH_GetNextChannel();
            _ana_channel.ChannelFlag = ChnFavorite_LookFor(_ana_channel.Channel);
            ConfigChannel(_ana_channel.Channel);
            WG_Paint(anaChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_0_CALLSHIP:
            WM_SetFocus(&callShipNumWin);
            WM_BringToTop(&callShipNumWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_1_GROUP:
            WM_SetFocus(&gdialWin);
            WM_BringToTop(&gdialWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_2_DIGIT:
            WM_SetFocus(&digitWin);
            WM_BringToTop(&digitWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_4_LOG:
            WM_SetFocus(&calogWin);
            WM_BringToTop(&calogWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_5_DIRECTION:
            WM_SetFocus(&contactsWin);
            WM_BringToTop(&contactsWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_6_REMEMBER:
            WM_SetFocus(&rememberChWin);       
            WM_BringToTop(&rememberChWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_7_SIGN:
            if(_ana_channel.ChannelFlag)
            {
              ChnFavorite_DeleteByChn(ACH_GetCurrChannel());
            }
            else
            {
              if(!ChnFavorite_Add(ACH_GetCurrChannel()))
              {
                AudioPlay(AUDIO_TYPE_KEY_UNNORMAL);
                break;
              }
            }
            _ana_channel.ChannelFlag = ChnFavorite_LookFor(ACH_GetCurrChannel());
            WG_Paint(anaChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_8_WEATHER:
            WM_SetFocus(&weatherWin);        
            WM_BringToTop(&weatherWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_9_SCAN:  
            anaScanSetValue(ACH_GetCurrChannel());
            WG_SetFocus(anaScan);
            WG_Paint(anaScan);
            anaScanSetModeUp(SCAN_ADD);
            GUI_CreateTimer(&analogWin, 1, SCAN_TIMER_LENGTH);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_STAR:
            GUI_CreateTimer(&analogWin, ANA_EDIT_TIMER, EDIT_TIMEROUT_LENGTH);
            WG_SetInVisible(anaChannel);
            WG_Paint(anaChannel);
            WG_SetFocus(anaEditChannel);
            WG_Paint(anaEditChannel);
            ExitAnalog();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      else if(((WM_KEY_INFO*)pMsg->data_p)->Pressed >= KEY_DOWN + 1 && ((WM_KEY_INFO*)pMsg->data_p)->Pressed <= KEY_DOWN + 3)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_DOWN:
            _ana_channel.Channel = ACH_GetPrevChannel();
            _ana_channel.ChannelFlag = ChnFavorite_LookFor(_ana_channel.Channel);
            ConfigChannel(_ana_channel.Channel);
            WG_Paint(anaChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_UP:
            _ana_channel.Channel = ACH_GetNextChannel();
            _ana_channel.ChannelFlag = ChnFavorite_LookFor(_ana_channel.Channel);
            ConfigChannel(_ana_channel.Channel);
            WG_Paint(anaChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
        }
      }
      else if(((WM_KEY_INFO*)pMsg->data_p)->Pressed > KEY_DOWN + 3)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_DOWN:
            _ana_channel.Channel = ACH_GetPrevChannel();
            _ana_channel.ChannelFlag = ChnFavorite_LookFor(_ana_channel.Channel);
            ConfigChannel(_ana_channel.Channel);
            WG_Paint(anaChannel);
            break;
          case GUI_KEY_UP:
            _ana_channel.Channel = ACH_GetNextChannel();
            _ana_channel.ChannelFlag = ChnFavorite_LookFor(_ana_channel.Channel);
            ConfigChannel(_ana_channel.Channel);
            WG_Paint(anaChannel);
            break;
        }
      }
      break;
    case WM_CREATE: 
      _ana_channel.Length = 3;
      _ana_channel.Channel = ACH_GetCurrChannel();
      _ana_channel.ChannelFlag = ChnFavorite_LookFor(_ana_channel.Channel);
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_ana_channel.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetNumInt(LCD_NUM14, 'C', 0);
        GUI_LCDSetNumInt(LCD_NUM15, 'H', 0);
        GUI_LCDSetNumInt(LCD_NUM16, '-', 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, _ana_channel.Channel / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM19, _ana_channel.Channel / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM20, _ana_channel.Channel % 10, 0);
        if(_ana_channel.ChannelFlag)
        {
          GUI_LCDSetS(LCD_S32, 1);
        }
        else
        {
          GUI_LCDSetS(LCD_S32, 0);
        }
      }
      else
      {
        GUI_LCDSetNumInt(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM20, LCD_DISPNULL, 0);
        GUI_LCDSetS(LCD_S32, 0);
      }
      break;
  }
}

Widget* anaChannelCreate(void)
{
  WG_Create((Widget*)&_ana_channel, &analogWin, anaChannelCallBack);
  anaChannel = (Widget*)&_ana_channel;
  return anaChannel;
}

void anaChannelSetValue(uint32_t Channel)
{
  ACH_SetCurrentChannel(Channel);
  _ana_channel.Channel = ACH_GetCurrChannel();
  _ana_channel.ChannelFlag = ChnFavorite_LookFor(_ana_channel.Channel);
}

//uint32_t anaChannelGetValue()
//{
//  return _ana_channel.Channel;
//}

void anaChannelSetFlag(uint8_t Flag)
{
  _ana_channel.ChannelFlag = Flag;
}


