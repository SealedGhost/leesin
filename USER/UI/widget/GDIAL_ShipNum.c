#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "groupDialWinDlg.h"
#include "GUI_LCD.h"


typedef struct WG_GDailShipNum{
  Widget Widget;
  uint32_t ShipNum;
}WG_GDailShipNum;

WG_GDailShipNum _gdail_shipnum = {0};
Widget *gdialShipNum;

static void gdialShipNumCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      break;
    case WM_CREATE:
      _gdail_shipnum.ShipNum = 12345;
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_gdail_shipnum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S51, 1);
        GUI_LCDSetS(LCD_S40, 1);
        GUI_LCDSetS(LCD_S38, 1);
        GUI_LCDSetS(LCD_S36, 1);
        GUI_LCDSetNumInt(LCD_NUM9, _gdail_shipnum.ShipNum / 10000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM10, _gdail_shipnum.ShipNum / 1000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM11, _gdail_shipnum.ShipNum / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM12, _gdail_shipnum.ShipNum / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM13, _gdail_shipnum.ShipNum % 10, 0);
      }
      else
      {
        GUI_LCDSetS(LCD_S51, 0);
        GUI_LCDSetS(LCD_S40, 0);
        GUI_LCDSetS(LCD_S38, 0);
        GUI_LCDSetS(LCD_S36, 0);
        GUI_LCDSetNumInt(LCD_NUM9, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM10, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM11, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM12, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM13, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* gdialShipNumCreate(void)
{
  WG_Create((Widget*)&_gdail_shipnum, &gdialWin, gdialShipNumCallback);
  gdialShipNum = (Widget*)&_gdail_shipnum;
  return gdialShipNum;
}

void gdialShipNumSetVal(uint32_t ShipNum)
{
  _gdail_shipnum.ShipNum = ShipNum;
}

uint32_t gdialShipNumGetVal(void)
{
  return _gdail_shipnum.ShipNum;
}
