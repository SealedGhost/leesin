#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "groupDialWinDlg.h"
#include "string.h"
#include "GUI.h"
#include "GUI_LCD.h"
#include "system_config.h"


typedef struct WG_GDialGroupID{
  Widget Widget;
  uint32_t GroupID;
}WG_GDialGroupID;

WG_GDialGroupID _gdial_groupid = {0};
Widget* gdialGroupID;

static void gdialGroupIDCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_gdial_groupid.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S48, 1);
        GUI_LCDSetS(LCD_S41, 1);
        GUI_LCDSetS(LCD_S42, 1);
        GUI_LCDSetNumInt(LCD_NUM4, _gdial_groupid.GroupID / 1000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM5, _gdial_groupid.GroupID / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM6, _gdial_groupid.GroupID / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM7, _gdial_groupid.GroupID % 10, 0 );
      }
      else
      {
        GUI_LCDSetS(LCD_S48, 0);
        GUI_LCDSetS(LCD_S41, 0);
        GUI_LCDSetS(LCD_S42, 0);
        GUI_LCDSetNumInt(LCD_NUM4, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM5, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM6, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM7, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}


Widget* gdialGroupIDCreate(void)
{
  WG_Create((Widget*)&_gdial_groupid, &gdialWin, gdialGroupIDCallback);
  gdialGroupID = (Widget*)&_gdial_groupid;
  return gdialGroupID;
}


void gdialGroupIDSetValue(uint32_t GroupID)
{
  _gdial_groupid.GroupID = GroupID;
  return;
}


