#include "UIMessage.h"
#include "dlg.h"
#include "selfLogWinDlg.h"
#include "GUI_LCD.h"
#include "GUI.h"
#include "system_info.h"

typedef struct WG_SlPassword{
  Widget Widget;
  char Passwoed[6];
}WG_SlPassword;

WG_SlPassword _sl_password = {0};
Widget *slPassword;

static void slPasswordCallback(WM_Message* pMsg)
{
  uint8_t Length;
  switch(pMsg->msgType)
  {
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      { 
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_BACK:
            WG_SetInVisible(slPassword);
            WG_Paint(slPassword);
            WG_SetFocus(slLog);
            WG_Paint(slLog);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_DEL:
          case GUI_KEY_STAR:
          {
            uint8_t Length = 0;
            GUI_CharToUint32(_sl_password.Passwoed, &Length);
            if(Length != SL_EDITPASSWORD_LENGTH)
            {
              GUI_SetCharUnderLine(_sl_password.Passwoed, SL_EDITPASSWORD_LENGTH);
              WG_Paint(slPassword);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
          }
          break;
          case GUI_KEY_ENTER:
          {
            uint32_t Password = GUI_CharToUint32(_sl_password.Passwoed, &Length);
            if(!Length)
            {
              if(Password == SysInfo_GetPassword())
              {
                WG_SetFocus(slShipNum);
                WG_SetInVisible(slPassword);
                WG_Paint(slPassword);
                WG_Paint(slShipNum);
              }
              else
              {
                GUI_SetCharUnderLine(_sl_password.Passwoed, SL_EDITPASSWORD_LENGTH);
                WG_Paint(slPassword);
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
          }
            break;
          case GUI_KEY_0_CALLSHIP:
            if(GUI_AddAChar(_sl_password.Passwoed, '0', SL_EDITPASSWORD_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slPassword);
            break;
          case GUI_KEY_1_GROUP:
            if(GUI_AddAChar(_sl_password.Passwoed, '1', SL_EDITPASSWORD_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slPassword);
            break;
          case GUI_KEY_2_DIGIT:
            if(GUI_AddAChar(_sl_password.Passwoed, '2', SL_EDITPASSWORD_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slPassword);
            break;
          case GUI_KEY_3_SIMULATE:
            if(GUI_AddAChar(_sl_password.Passwoed, '3', SL_EDITPASSWORD_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slPassword);
            break;
          case GUI_KEY_4_LOG:
            if(GUI_AddAChar(_sl_password.Passwoed, '4', SL_EDITPASSWORD_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slPassword);
            break;
          case GUI_KEY_5_DIRECTION:
            if(GUI_AddAChar(_sl_password.Passwoed, '5', SL_EDITPASSWORD_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slPassword);
            break;
          case GUI_KEY_6_REMEMBER:       
            if(GUI_AddAChar(_sl_password.Passwoed, '6', SL_EDITPASSWORD_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slPassword);
            break;
          case GUI_KEY_7_SIGN: 
            if(GUI_AddAChar(_sl_password.Passwoed, '7', SL_EDITPASSWORD_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slPassword);
            break;
          case GUI_KEY_8_WEATHER:        
            if(GUI_AddAChar(_sl_password.Passwoed, '8', SL_EDITPASSWORD_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slPassword);
            break;
          case GUI_KEY_9_SCAN:         
            if(GUI_AddAChar(_sl_password.Passwoed, '9', SL_EDITPASSWORD_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slPassword);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          
        }
      }
      break;
    case WM_CREATE:
      GUI_SetCharUnderLine(_sl_password.Passwoed, SL_EDITPASSWORD_LENGTH);
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        GUI_SetCharUnderLine(_sl_password.Passwoed, SL_EDITPASSWORD_LENGTH);
      }
      break;
    case WM_PAINT:
      if(_sl_password.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        uint8_t flash = 1;
        
        GUI_LCDSetS(LCD_S29, 1);
        GUI_LCDSetS(LCD_S26, 1);
        GUI_LCDSetS(LCD_S28, 1);
        if(flash && _sl_password.Passwoed[0] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM15, _sl_password.Passwoed[0], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM15, _sl_password.Passwoed[0], 0);
        }
        
        if(flash && _sl_password.Passwoed[1] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM16, _sl_password.Passwoed[1], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM16, _sl_password.Passwoed[1], 0);
        }
        
        if(flash && _sl_password.Passwoed[2] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM17, _sl_password.Passwoed[2], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM17, _sl_password.Passwoed[2], 0);
        }
        
        if(flash && _sl_password.Passwoed[3] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM18, _sl_password.Passwoed[3], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM18, _sl_password.Passwoed[3], 0);
        }
        
        if(flash)
        {
          GUI_LCDSetNumChar(LCD_NUM19, _sl_password.Passwoed[4], 1);
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM19, _sl_password.Passwoed[4], 0);
        }
      }
      else
      {
        GUI_LCDSetS(LCD_S29, 0);
        GUI_LCDSetS(LCD_S26, 0);
        GUI_LCDSetS(LCD_S28, 0);
        GUI_LCDSetNumChar(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM19, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* slPasswordCreate(void)
{
  WG_Create((Widget*)&_sl_password, &selfLogWin, slPasswordCallback);
  slPassword = (Widget*)&_sl_password;
  return slPassword;
}