#include "UIMessage.h"
#include "dlg.h"
#include "callShipNumWinDlg.h"
#include "lcdControl.h"
#include "GUI_LCD.h"
#include "GUI_Timer.h"
#include "boat_search.h"
#include "GUI_key.h"


typedef struct WG_CsnShipNum{
  Widget Widget;
  uint32_t ShipNum;
}WG_CsnShipNum;

WG_CsnShipNum _csn_shipnum;
Widget *csnShipNum;

static void csnShipNumCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      GUI_DestroyTimer(&callShipNumWin, 1);
      csnOrderNumSetVal(BoatSch_GetCurrPos());
      WG_SetVisible(csnOrderNum);
      csnDialNumSetVal(BoatSch_GetCurrResult());
      WG_SetFocus(csnDialNum);
      csnSmallShipNumSetVal(_csn_shipnum.ShipNum);
      WG_SetVisible(csnSmallShipNum);
      WG_SetInVisible(csnShipNum);
      WG_Paint(csnShipNum);
      WG_Paint(csnSmallShipNum);
      WG_Paint(csnDialNum);
      WG_Paint(csnOrderNum);
    
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &callShipNumWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);  
      break;
    case USER_MSG_BOAT_INFO_RESPONSE:
      GUI_DestroyTimer(&callShipNumWin, 1);
      csnOrderNumSetVal(BoatSch_GetCurrPos());
      WG_SetVisible(csnOrderNum);
      csnDialNumSetVal(BoatSch_GetCurrResult());
      WG_SetFocus(csnDialNum);
      csnSmallShipNumSetVal(_csn_shipnum.ShipNum);
      WG_SetVisible(csnSmallShipNum);
      WG_SetInVisible(csnShipNum);
      WG_Paint(csnShipNum);
      WG_Paint(csnSmallShipNum);
      WG_Paint(csnDialNum);
      WG_Paint(csnOrderNum);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_UP)
      {
        AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
      }
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_csn_shipnum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S25, 1);
        GUI_LCDSetS(LCD_S26, 1);
        GUI_LCDSetS(LCD_S28, 1);
        GUI_LCDSetNumInt(LCD_NUM15, _csn_shipnum.ShipNum / 10000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM16, _csn_shipnum.ShipNum / 1000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM17, _csn_shipnum.ShipNum / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM18, _csn_shipnum.ShipNum / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM19, _csn_shipnum.ShipNum % 10, 0);
      }
      else
      {
        GUI_LCDSetS(LCD_S25, 0);
        GUI_LCDSetS(LCD_S26, 0);
        GUI_LCDSetS(LCD_S28, 0);
        GUI_LCDSetNumInt(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM19, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* csnShipNumCreate(void)
{
  WG_Create((Widget*)&_csn_shipnum, &callShipNumWin, csnShipNumCallback);
  csnShipNum = (Widget*)&_csn_shipnum;
  return csnShipNum;
}

void csnShipNumSetVal(uint32_t ShipNum)
{
  _csn_shipnum.ShipNum = ShipNum;
}