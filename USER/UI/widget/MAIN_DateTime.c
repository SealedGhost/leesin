#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "mainWinDlg.h"
#include "GUI_LCD.h"
#include "system_time.h"

typedef struct WG_DateTime{
  Widget Widget;
  uint8_t Year;
  uint8_t Month;
  uint8_t Day;
  uint8_t Hour;
  uint8_t Minute;
  uint8_t Second;
}WG_DateTime;

WG_DateTime _dateTime = {0};
Widget *dateTime;

static void dataTimeCallBack(WM_Message *pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_CREATE:
    {
      Time time;
      SysTime_Get(&time);
      dateTimeSetMonth(time.month);
      dateTimeSetDay(time.day);
      dateTimeSetHour(time.hour);
      dateTimeSetMinute(time.minute);
      dateTimeSetSecond(time.second);
    }
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(dateTime->States == WG_VISIBLE)
      {
        GUI_LCDSetS(LCD_S58, 1);
        GUI_LCDSetS(LCD_S57, 1);
        GUI_LCDSetS(LCD_S56, 1);
        GUI_LCDSetS(LCD_S55, 1);
        if(_dateTime.Month < 10)
        {
          GUI_LCDSetS(LCD_S60, 1);
          GUI_LCDSetS(LCD_S59, 1);
        }
        else
        {
          GUI_LCDSetS(LCD_S60, 0);
          GUI_LCDSetS(LCD_S59, 1);
        }
        GUI_LCDSetNumInt(LCD_NUM21, _dateTime.Month % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM22, _dateTime.Day / 10, 0);
        GUI_LCDSetNumInt(LCD_NUM23, _dateTime.Day % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM24, _dateTime.Hour / 10, 0);
        GUI_LCDSetNumInt(LCD_NUM25, _dateTime.Hour % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM26, _dateTime.Minute / 10, 0);
        GUI_LCDSetNumInt(LCD_NUM27, _dateTime.Minute % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM28, _dateTime.Second / 10, 0);
        GUI_LCDSetNumInt(LCD_NUM29, _dateTime.Second % 10, 0);
        
      }
      else
      {
        GUI_LCDSetS(LCD_S58, 0);
        GUI_LCDSetS(LCD_S57, 0);
        GUI_LCDSetS(LCD_S56, 0);
        GUI_LCDSetS(LCD_S55, 0);
        GUI_LCDSetS(LCD_S60, 0);
        GUI_LCDSetS(LCD_S59, 0);
        GUI_LCDSetNumInt(LCD_NUM21, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM22, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM23, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM24, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM25, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM26, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM27, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM28, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM29, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* dateTimeCreate(void)
{
  WG_Create((Widget*)&_dateTime, &mainWin, dataTimeCallBack);
  dateTime = (Widget*)&_dateTime;
  return dateTime;
}

void dateTimeSetYear(uint8_t Year)
{
  _dateTime.Year = Year;
  return;
}

void dateTimeSetMonth(uint8_t Month)
{
  _dateTime.Month = Month;
  return;
}

void dateTimeSetDay(uint8_t Day)
{
  _dateTime.Day = Day;
  return;
}

void dateTimeSetHour(uint8_t Hour)
{
  _dateTime.Hour = Hour;
  return;
}

void dateTimeSetMinute(uint8_t Minute)
{
  _dateTime.Minute = Minute;
  return;
}

void dateTimeSetSecond(uint8_t Second)
{
  _dateTime.Second = Second;
  return;
}
