#include "UIMessage.h"
#include "dlg.h"
#include "contactsWinDlg.h"
#include "GUI.h"
#include "lcdControl.h"
#include "contact.h"
#include "GUI_LCD.h"
#include "GUI_Timer.h"

typedef struct WG_ContEditShipNum{
  Widget Widget;
  char ShipID[6];
}WG_ContEditShipNum;

WG_ContEditShipNum _cont_editshipNum = {0};
Widget *contEditShipNum;

static void contEditShipNumCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  uint8_t Length;
  switch(pMsg->msgType)
  {
    case USER_MSG_LOGTOCONTACTS:
      GUI_Uint32ToChar((uint32_t)pMsg->data_p, _cont_editshipNum.ShipID, CONT_SHIPID_LENGTH);
      break;
    case USER_MSG_CALL_IN:
      GUI_DestroyTimer(&contactsWin, CONT_EDIT_TIMER);
      GUI_SetCharUnderLine(_cont_editshipNum.ShipID, CONT_SHIPID_LENGTH);
      contEditDialNumSetUnderlineNum();
      WG_SetInVisible(contEditDialNum);
      WG_Paint(contEditDialNum);
      WG_SetInVisible(contEditShipNum);
      WG_Paint(contEditShipNum);
      WG_SetVisible(contShipNum);
      WG_Paint(contShipNum);
      WG_SetFocus(contDialNum);
      WG_Paint(contDialNum);
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &contactsWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        GUI_RestartTimer(&contactsWin, CONT_EDIT_TIMER, CONT_EDIT_TIME_LENGTH);
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_STAR:
          case GUI_KEY_DEL:
            GUI_CharToUint32(_cont_editshipNum.ShipID, &Length);
            if(Length != CONT_SHIPID_LENGTH)
            {
              GUI_SetCharUnderLine(_cont_editshipNum.ShipID, CONT_SHIPID_LENGTH);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(contEditShipNum);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_BACK:
            GUI_DestroyTimer(&contactsWin, CONT_EDIT_TIMER);
            GUI_SetCharUnderLine(_cont_editshipNum.ShipID, CONT_SHIPID_LENGTH);
            contOrderNumSetVal(Contact_GetPosOfLinkMan(Contact_GetCurrLinkMan()));
            contEditDialNumSetUnderlineNum();
            WG_SetInVisible(contEditDialNum);
            WG_Paint(contEditDialNum);
            WG_SetInVisible(contEditShipNum);
            WG_Paint(contEditShipNum);
            WG_SetVisible(contShipNum);
            WG_Paint(contShipNum);
            WG_SetFocus(contDialNum);
            WG_Paint(contDialNum);
            WG_Paint(contOrderNum);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_ENTER:
            if(!contEditDialNumGetUnderlineNum() && !contEditShipNumGetUnderlineNum())
            {
              LinkMan* linkMan;
              GUI_DestroyTimer(&contactsWin, CONT_EDIT_TIMER);
              Contact_Add(contEditShipNumGetVal(), contEditDialNumGetVal());
              linkMan = Contact_FindByPhoneId(contEditDialNumGetVal());
              contOrderNumSetVal(Contact_GetPosOfLinkMan(linkMan));
              contShipNumSetVal(linkMan->shipNumber);
              contDialNumSetVal(linkMan->phoneNumber);
              GUI_SetCharUnderLine(_cont_editshipNum.ShipID, CONT_SHIPID_LENGTH);
              contEditDialNumSetUnderlineNum();
              WG_SetInVisible(contEditDialNum);
              WG_Paint(contEditDialNum);
              WG_SetInVisible(contEditShipNum);
              WG_Paint(contEditShipNum);
              WG_SetVisible(contShipNum);
              WG_Paint(contShipNum);
              WG_SetFocus(contDialNum);
              WG_Paint(contDialNum);
              WG_Paint(contOrderNum);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          case GUI_KEY_UP:
          case GUI_KEY_DOWN:
            WG_SetFocus(contEditDialNum);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_0_CALLSHIP:
            Length = GUI_AddAChar(_cont_editshipNum.ShipID, '0', CONT_SHIPID_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_SHIPID_LENGTH)
            {
              if(contEditDialNumGetUnderlineNum())
              {
                WG_Paint(contEditShipNum);
                WG_SetFocus(contEditDialNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditShipNum);
            break;
          case GUI_KEY_1_GROUP:
            Length = GUI_AddAChar(_cont_editshipNum.ShipID, '1', CONT_SHIPID_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_SHIPID_LENGTH)
            {
              if(contEditDialNumGetUnderlineNum())
              {
                WG_Paint(contEditShipNum);
                WG_SetFocus(contEditDialNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditShipNum);
            break;
          case GUI_KEY_2_DIGIT:
            Length = GUI_AddAChar(_cont_editshipNum.ShipID, '2', CONT_SHIPID_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_SHIPID_LENGTH)
            {
              if(contEditDialNumGetUnderlineNum())
              {
                WG_Paint(contEditShipNum);
                WG_SetFocus(contEditDialNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditShipNum);
            break;
          case GUI_KEY_3_SIMULATE:
            Length = GUI_AddAChar(_cont_editshipNum.ShipID, '3', CONT_SHIPID_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_SHIPID_LENGTH)
            {
              if(contEditDialNumGetUnderlineNum())
              {
                WG_Paint(contEditShipNum);
                WG_SetFocus(contEditDialNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditShipNum);
            break;
          case GUI_KEY_4_LOG:
            Length = GUI_AddAChar(_cont_editshipNum.ShipID, '4', CONT_SHIPID_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_SHIPID_LENGTH)
            {
              if(contEditDialNumGetUnderlineNum())
              {
                WG_Paint(contEditShipNum);
                WG_SetFocus(contEditDialNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditShipNum);
            break;
          case GUI_KEY_5_DIRECTION:
            Length = GUI_AddAChar(_cont_editshipNum.ShipID, '5', CONT_SHIPID_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_SHIPID_LENGTH)
            {
              if(contEditDialNumGetUnderlineNum())
              {
                WG_Paint(contEditShipNum);
                WG_SetFocus(contEditDialNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditShipNum);
            break;
          case GUI_KEY_6_REMEMBER:       
            Length = GUI_AddAChar(_cont_editshipNum.ShipID, '6', CONT_SHIPID_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_SHIPID_LENGTH)
            {
              if(contEditDialNumGetUnderlineNum())
              {
                WG_Paint(contEditShipNum);
                WG_SetFocus(contEditDialNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditShipNum);
            break;
          case GUI_KEY_7_SIGN: 
            Length = GUI_AddAChar(_cont_editshipNum.ShipID, '7', CONT_SHIPID_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_SHIPID_LENGTH)
            {
              if(contEditDialNumGetUnderlineNum())
              {
                WG_Paint(contEditShipNum);
                WG_SetFocus(contEditDialNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditShipNum);
            break;
          case GUI_KEY_8_WEATHER:        
            Length = GUI_AddAChar(_cont_editshipNum.ShipID, '8', CONT_SHIPID_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_SHIPID_LENGTH)
            {
              if(contEditDialNumGetUnderlineNum())
              {
                WG_Paint(contEditShipNum);
                WG_SetFocus(contEditDialNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditShipNum);
            break;
          case GUI_KEY_9_SCAN:         
            Length = GUI_AddAChar(_cont_editshipNum.ShipID, '9', CONT_SHIPID_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == CONT_SHIPID_LENGTH)
            {
              if(contEditDialNumGetUnderlineNum())
              {
                WG_Paint(contEditShipNum);
                WG_SetFocus(contEditDialNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(contEditShipNum);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      GUI_SetCharUnderLine(_cont_editshipNum.ShipID, CONT_SHIPID_LENGTH);
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        GUI_SetCharUnderLine(_cont_editshipNum.ShipID, CONT_SHIPID_LENGTH);
        WG_Paint(contEditShipNum);
      }
      break;
    case WM_PAINT:
      if(_cont_editshipNum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        uint8_t flash = 1;
        if(flash && _cont_editshipNum.ShipID[0] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM9, _cont_editshipNum.ShipID[0], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM9, _cont_editshipNum.ShipID[0], 0);
        }
        
        if(flash && _cont_editshipNum.ShipID[1] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM10, _cont_editshipNum.ShipID[1], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM10, _cont_editshipNum.ShipID[1], 0);
        }
        
        if(flash && _cont_editshipNum.ShipID[2] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM11, _cont_editshipNum.ShipID[2], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM11, _cont_editshipNum.ShipID[2], 0);
        }
        
        if(flash && _cont_editshipNum.ShipID[3] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM12, _cont_editshipNum.ShipID[3], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM12, _cont_editshipNum.ShipID[3], 0);
        }
        
        if(flash)
        {
          GUI_LCDSetNumChar(LCD_NUM13, _cont_editshipNum.ShipID[4], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM13, _cont_editshipNum.ShipID[4], 0);
        }
      }
      else
      {
        GUI_LCDDisableNumFlash();
        GUI_LCDSetNumChar(LCD_NUM9, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM10, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM11, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM12, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM13, LCD_DISPNULL, 0);
      }
      break;
    case WM_TIMER:
      if(pMsg->data_v == CONT_EDIT_TIMER)
      {
        GUI_SetCharUnderLine(_cont_editshipNum.ShipID, CONT_SHIPID_LENGTH);
        contOrderNumSetVal(Contact_GetPosOfLinkMan(Contact_GetCurrLinkMan()));
        contEditDialNumSetUnderlineNum();
        contEditDialNumSetUnderlineNum();
        WG_SetInVisible(contEditDialNum);
        WG_Paint(contEditDialNum);
        WG_SetInVisible(contEditShipNum);
        WG_Paint(contEditShipNum);
        WG_SetVisible(contShipNum);
        WG_Paint(contShipNum);
        WG_SetFocus(contDialNum);
        WG_Paint(contDialNum);
        WG_Paint(contOrderNum);
      }
      break;
  }
  return;
}

Widget* contEditShipNumCreate(void)
{
  WG_Create((Widget*)&_cont_editshipNum, &contactsWin, contEditShipNumCallback);
  contEditShipNum = (Widget*)&_cont_editshipNum;
  return contEditShipNum;
}

void contEditShipNumSetUnderlineNum(void)
{
  GUI_SetCharUnderLine(_cont_editshipNum.ShipID, CONT_SHIPID_LENGTH);
}

uint8_t contEditShipNumGetUnderlineNum(void)
{
  uint8_t length = 0;
  GUI_CharToUint32(_cont_editshipNum.ShipID, &length);
  return length;
}

uint32_t contEditShipNumGetVal(void)
{
  return GUI_CharToUint32(_cont_editshipNum.ShipID, 0);
}

