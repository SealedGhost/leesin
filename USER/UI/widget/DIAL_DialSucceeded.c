#include "dialWinDlg.h"
#include "UIMessage.h"
#include "dlg.h"
#include "GUI.h"
#include "lcdControl.h"
#include "GUI_LCD.h"
#include "call_mgr.h"

typedef struct WG_DialSucceed{
  Widget Widget;
  uint8_t val;
}WG_DialSucceeded;

WG_DialSucceeded _dialSucceeded = {0};
Widget *dialDialSucceeded;

static void dialSucceededCallBack(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case USER_MSG_HUNG_UP:
      if(pMsg->data_v == dialDialNumGetVal())
      {
        if(pDialWindowSource != &gdialWin)
        {
          WG_SetFocus(dialDialOver);
          WG_SetInVisible(dialDialSucceeded);
          WG_Paint(dialDialOver);
          WG_Paint(dialDialSucceeded);
          AudioPlayCnt(AUDIO_TYPE_TALK_END,AUDIO_PLAY_TIMES_TALK_END);
        }
        else
        {
          WM_SetFocus(pDialWindowSource);
          WM_BringToTop(pDialWindowSource);
        }
      }
      break;
    case USER_MSG_CALL_IN:
      WG_SetVisible(dialOtherDialNum);
      WG_SetVisible(dialShipNum);
      WG_SetInVisible(dialDialFialed);
      WG_Paint(dialDialFialed);
      dialOtherDialNumSetVal(pMsg->data_v);
      dialShipNumSetVal(DIAL_DISP_OTHERSHIPNUM, (uint32_t)(pMsg->data_p));
      dialShipNumSetMode(DIAL_DISP_OTHERSHIPNUM);
      WG_SetFocus(dialRoundButton);
      WG_Paint(dialOtherDialNum);
      WG_Paint(dialShipNum);
      WG_Paint(dialRoundButton);
      AudioPlayCnt(AUDIO_TYPE_TALKING_CALL,AUDIO_PLAY_TIMES_TALKING_CALL);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_HANGUP:
            CallMgr_HangUp(dialDialNumGetVal());
            if(pDialWindowSource != &gdialWin)
            {
              WG_SetFocus(dialDialOver);
              WG_SetInVisible(dialDialSucceeded);
              WG_Paint(dialDialOver);
              WG_Paint(dialDialSucceeded);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              AudioPlayCnt(AUDIO_TYPE_TALK_END,AUDIO_PLAY_TIMES_TALK_END);
            }
            else
            {
              WM_SetFocus(pDialWindowSource);
              WM_BringToTop(pDialWindowSource);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_dialSucceeded.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S16, 1);
      }
      else
      {
        GUI_LCDSetS(LCD_S16, 0);
      }
      break;
  }
}

Widget* dialDialSucceededCreate(void)
{
  WG_Create((Widget*)&_dialSucceeded, &dialWin, dialSucceededCallBack);
  dialDialSucceeded = (Widget*)&_dialSucceeded;
  return dialDialSucceeded;
}

