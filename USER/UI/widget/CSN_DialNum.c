#include "UIMessage.h"
#include "dlg.h"
#include "callShipNumWinDlg.h"
#include "lcdControl.h"
#include "GUI.h"
#include "GUI_LCD.h"
#include "boat_search.h"


typedef struct WG_CsnDialNum{
  Widget Widget;
  uint32_t DialNum;
}WG_CsnDialNum;

WG_CsnDialNum _csn_dialnum;
Widget *csnDialNum;

static void csnDialNumNumCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &callShipNumWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg); 
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      { 
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_UP:
            _csn_dialnum.DialNum = BoatSch_GetNextResult();
            csnOrderNumSetVal(BoatSch_GetCurrPos());
            WG_Paint(csnOrderNum);
            WG_Paint(csnOrderNum);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_DOWN:
            _csn_dialnum.DialNum = BoatSch_GetPrevResult();
            csnOrderNumSetVal(BoatSch_GetCurrPos());
            WG_Paint(csnOrderNum);
            WG_Paint(csnOrderNum);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_PICKUP:
            Msg.pSource = &callShipNumWin;
            Msg.pTarget = &dialWin;
            Msg.msgType = USER_MSG_DIAL;
            Msg.data_v = _csn_dialnum.DialNum;
            WM_PostMessage(&Msg);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_BACK:
            WG_SetFocus(csnEditShipNum);
            WG_SetInVisible(csnOrderNum);
            WG_SetInVisible(csnDialNum);
            WG_SetInVisible(csnSmallShipNum);
            WG_Paint(csnOrderNum);
            WG_Paint(csnDialNum);
            WG_Paint(csnSmallShipNum);
            WG_Paint(csnEditShipNum);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_1_GROUP:
          case GUI_KEY_2_DIGIT:
          case GUI_KEY_3_SIMULATE:
          case GUI_KEY_4_LOG:
          case GUI_KEY_5_DIRECTION:
          case GUI_KEY_6_REMEMBER:
          case GUI_KEY_8_WEATHER:
            WG_SetFocus(csnEditShipNum);
            WG_SetInVisible(csnOrderNum);
            WG_SetInVisible(csnDialNum);
            WG_SetInVisible(csnSmallShipNum);
            WG_SetFocus(csnEditShipNum);
            WG_Paint(csnOrderNum);
            WG_Paint(csnDialNum);
            WG_Paint(csnSmallShipNum);
            WG_Paint(csnEditShipNum);
            csnEditShipNum->CallBack(pMsg);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_csn_dialnum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetNumInt(LCD_NUM14, _csn_dialnum.DialNum / 1000000 %10, 0);
        GUI_LCDSetNumInt(LCD_NUM15, _csn_dialnum.DialNum / 100000 %10, 0);
        GUI_LCDSetNumInt(LCD_NUM16, _csn_dialnum.DialNum / 10000 %10, 0);
        GUI_LCDSetNumInt(LCD_NUM17, _csn_dialnum.DialNum / 1000 %10, 0);
        GUI_LCDSetNumInt(LCD_NUM18, _csn_dialnum.DialNum / 100 %10, 0);
        GUI_LCDSetNumInt(LCD_NUM19, _csn_dialnum.DialNum / 10 %10, 0);
        GUI_LCDSetNumInt(LCD_NUM20, _csn_dialnum.DialNum %10, 0);
      }
      else
      {
        GUI_LCDSetNumInt(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM20, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* csnDialNumCreate(void)
{
  WG_Create((Widget*)&_csn_dialnum, &callShipNumWin, csnDialNumNumCallback);
  csnDialNum = (Widget*)&_csn_dialnum;
  return csnDialNum;
}

void csnDialNumSetVal(uint32_t DialNum)
{
  _csn_dialnum.DialNum = DialNum;
}