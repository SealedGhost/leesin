#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "beCalledWinDlg.h"
#include "string.h"
#include "GUI.h"
#include "GUI_LCD.h"


typedef struct WG_bcNotPickUp{
  Widget Widget;
}WG_bcNotPickUp;

WG_bcNotPickUp _bc_notpickup;
Widget* bcNotPickUp;

static void bcNotPickUpWinCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      WG_SetInVisible(bcNotPickUp);
      WG_Paint(bcNotPickUp);
      WG_SetInVisible(bcOtherDialNum);
      WG_Paint(bcOtherDialNum);
    
      bcShipNumSetVal(BC_DISP_SHIPNUM, (uint32_t)pMsg->data_p);
      bcDialNumSetVal(pMsg->data_v);
      WG_SetFocus(bcRoundButton);
      WG_SetVisible(bcDialNum);
      WG_SetVisible(bcShipNum);
      WG_Paint(bcRoundButton);
      WG_Paint(bcDialNum);
      WG_Paint(bcShipNum);
      AudioPlayCnt(AUDIO_TYPE_CALLER,AUDIO_PLAY_TIMES_CALLER);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      { 
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_PICKUP:
          {
            WM_Message msg;
            msg.pSource = beCallSourceWin;
            msg.pTarget = &dialWin;
            msg.msgType = USER_MSG_DIAL;
            msg.data_v = bcDialNumGetVal();
            WM_PostMessage(&msg);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
          }
            break;
          case GUI_KEY_BACK:
            WM_SetFocus(beCallSourceWin);
            WM_BringToTop(beCallSourceWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_bc_notpickup.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S33, 1);
      }
      else
      {
        GUI_LCDSetS(LCD_S33, 0);
      }
      break;
  }
}


Widget* bcNotPickUpCreate(void)
{
  WG_Create((Widget*)&_bc_notpickup, &beCalledWin, bcNotPickUpWinCallback);
  bcNotPickUp = (Widget*)&_bc_notpickup;
  return bcNotPickUp;
}