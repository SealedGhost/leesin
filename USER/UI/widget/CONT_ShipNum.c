#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "contactsWinDlg.h"
#include "GUI_LCD.h"


typedef struct WG_ContShipNum{
  Widget Widget;
  uint32_t ShipID;
}WG_ContShipNum;

WG_ContShipNum _cont_shipNum = {0};
Widget *contShipNum;

static void contShipNumCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_cont_shipNum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S40, 1);
        GUI_LCDSetS(LCD_S38, 1);
        GUI_LCDSetS(LCD_S36, 1);
        GUI_LCDSetS(LCD_S51, 1);
        GUI_LCDSetNumInt(LCD_NUM9, _cont_shipNum.ShipID / 10000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM10, _cont_shipNum.ShipID / 1000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM11, _cont_shipNum.ShipID / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM12, _cont_shipNum.ShipID / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM13, _cont_shipNum.ShipID % 10, 0);
      }
      else
      {
        GUI_LCDSetS(LCD_S40, 0);
        GUI_LCDSetS(LCD_S38, 0);
        GUI_LCDSetS(LCD_S36, 0);
        GUI_LCDSetS(LCD_S51, 0);
        GUI_LCDSetNumInt(LCD_NUM9, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM10, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM11, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM12, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM13, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* contShipNumCreate(void)
{
  WG_Create((Widget*)&_cont_shipNum, &contactsWin, contShipNumCallback);
  contShipNum = (Widget*)&_cont_shipNum;
  return contShipNum;
}

void contShipNumSetVal(uint32_t ShipID)
{
  _cont_shipNum.ShipID = ShipID;
}


