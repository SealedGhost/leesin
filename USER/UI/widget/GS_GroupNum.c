#include "UIMessage.h"
#include "dlg.h"
#include "groupSetWinDlg.h"
#include "GUI.h"
#include "lcdControl.h"
#include "GUI_LCD.h"
#include "system_config.h"
#include "GUI_Timer.h"

typedef struct WG_GsGroupNum{
  Widget Widget;
  char GroupID[5];
  uint8_t Activate;
}WG_GsGroupNum;

WG_GsGroupNum _gs_groupnum = {0};
Widget *gsGroupNum;

static void gsGroupNumCallback(WM_Message *pMsg)
{
  uint8_t Length;
  switch(pMsg->msgType)
  {
    case WM_KEY:
      if(_gs_groupnum.Activate == GS_EDIT_ACTIVATE)
      {
        if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
        {
          GUI_RestartTimer(&groupSetWin, GS_EDIT_TIMER, EDIT_TIMEROUT_LENGTH);
          switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
          {
            case GUI_KEY_UP:
            case GUI_KEY_DOWN:
              if(gsGroupNumGetUnderLineNum())
                GUI_SetCharUnderLine(_gs_groupnum.GroupID, GS_FROUPIS_LENGTH);
              WG_Paint(gsGroupNum);
              WG_SetFocus(gsPassword);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_STAR:
              gsPasswordSetUnderLine();
              gsGroupNumSetUnderLine();
              WG_SetFocus(gsPassword);
              WG_Paint(gsGroupNum);
              WG_Paint(gsPassword);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_DEL:
              GUI_CharToUint32(_gs_groupnum.GroupID, &Length);
              if(Length != GS_FROUPIS_LENGTH)
              {
                GUI_SetCharUnderLine(_gs_groupnum.GroupID, GS_FROUPIS_LENGTH);
                WG_Paint(gsGroupNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              break;
            case GUI_KEY_BACK:
            {
              SysCfg sysCfg;
              GUI_DestroyTimer(&groupSetWin, GS_EDIT_TIMER);
              SysCfg_GetConfig(&sysCfg);
              GUI_Uint16ToChar(sysCfg.groupNumber, _gs_groupnum.GroupID, GS_FROUPIS_LENGTH);
              _gs_groupnum.Activate = GS_EDIT_UNACTIVATE;
              WG_Paint(gsGroupNum);
              gsPasswordSetUnderLine();
              gsPasswordSetActivate(GS_EDIT_UNACTIVATE);
              WG_SetFocus(gsPassword);
              WG_Paint(gsPassword);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
              break;
            case GUI_KEY_ENTER:
              if(!gsGroupNumGetUnderLineNum() && !gsPasswordGetUnderLineNum() && gsGroupNumGetVal() && gsPasswordGetVal())
              {
                SysCfg sysCfg;
                GUI_DestroyTimer(&groupSetWin, GS_EDIT_TIMER);
                SysCfg_GetConfig(&sysCfg);
                sysCfg.groupNumber = gsGroupNumGetVal();
                sysCfg.groupPwd = gsPasswordGetVal();
                SysCfg_SetConfig(&sysCfg);
                SysCfg_Store();
                _gs_groupnum.Activate = GS_EDIT_UNACTIVATE;
                WG_Paint(gsGroupNum);
                gsPasswordSetActivate(GS_EDIT_UNACTIVATE);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            case GUI_KEY_0_CALLSHIP:
              Length = GUI_AddAChar(_gs_groupnum.GroupID, '0', GS_FROUPIS_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_FROUPIS_LENGTH)
              {
                if(gsPasswordGetUnderLineNum())
                {
                  WG_Paint(gsGroupNum);
                  WG_SetFocus(gsPassword);
                  WG_Paint(gsPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsGroupNum);
              break;
            case GUI_KEY_1_GROUP:
              Length = GUI_AddAChar(_gs_groupnum.GroupID, '1', GS_FROUPIS_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_FROUPIS_LENGTH)
              {
                if(gsPasswordGetUnderLineNum())
                {
                  WG_Paint(gsGroupNum);
                  WG_SetFocus(gsPassword);
                  WG_Paint(gsPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsGroupNum);
              break;
            case GUI_KEY_2_DIGIT:
              Length = GUI_AddAChar(_gs_groupnum.GroupID, '2', GS_FROUPIS_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_FROUPIS_LENGTH)
              {
                if(gsPasswordGetUnderLineNum())
                {
                  WG_Paint(gsGroupNum);
                  WG_SetFocus(gsPassword);
                  WG_Paint(gsPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsGroupNum);
              break;
            case GUI_KEY_3_SIMULATE:
              Length = GUI_AddAChar(_gs_groupnum.GroupID, '3', GS_FROUPIS_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_FROUPIS_LENGTH)
              {
                if(gsPasswordGetUnderLineNum())
                {
                  WG_Paint(gsGroupNum);
                  WG_SetFocus(gsPassword);
                  WG_Paint(gsPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsGroupNum);
              break;
            case GUI_KEY_4_LOG:
              Length = GUI_AddAChar(_gs_groupnum.GroupID, '4', GS_FROUPIS_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_FROUPIS_LENGTH)
              {
                if(gsPasswordGetUnderLineNum())
                {
                  WG_Paint(gsGroupNum);
                  WG_SetFocus(gsPassword);
                  WG_Paint(gsPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsGroupNum);
              break;
            case GUI_KEY_5_DIRECTION:
              Length = GUI_AddAChar(_gs_groupnum.GroupID, '5', GS_FROUPIS_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_FROUPIS_LENGTH)
              {
                if(gsPasswordGetUnderLineNum())
                {
                  WG_Paint(gsGroupNum);
                  WG_SetFocus(gsPassword);
                  WG_Paint(gsPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsGroupNum);
              break;
            case GUI_KEY_6_REMEMBER:       
              Length = GUI_AddAChar(_gs_groupnum.GroupID, '6', GS_FROUPIS_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_FROUPIS_LENGTH)
              {
                if(gsPasswordGetUnderLineNum())
                {
                  WG_Paint(gsGroupNum);
                  WG_SetFocus(gsPassword);
                  WG_Paint(gsPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsGroupNum);
              break;
            case GUI_KEY_7_SIGN: 
              Length = GUI_AddAChar(_gs_groupnum.GroupID, '7', GS_FROUPIS_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_FROUPIS_LENGTH)
              {
                if(gsPasswordGetUnderLineNum())
                {
                  WG_Paint(gsGroupNum);
                  WG_SetFocus(gsPassword);
                  WG_Paint(gsPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsGroupNum);
              break;
            case GUI_KEY_8_WEATHER:        
              Length = GUI_AddAChar(_gs_groupnum.GroupID, '8', GS_FROUPIS_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_FROUPIS_LENGTH)
              {
                if(gsPasswordGetUnderLineNum())
                {
                  WG_Paint(gsGroupNum);
                  WG_SetFocus(gsPassword);
                  WG_Paint(gsPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsGroupNum);
              break;
            case GUI_KEY_9_SCAN:         
              Length = GUI_AddAChar(_gs_groupnum.GroupID, '9', GS_FROUPIS_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_FROUPIS_LENGTH)
              {
                if(gsPasswordGetUnderLineNum())
                {
                  WG_Paint(gsGroupNum);
                  WG_SetFocus(gsPassword);
                  WG_Paint(gsPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsGroupNum);
              break;
            default:
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
          }
        }
      }
      else
      {
        if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
        {
          switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
          {
            case GUI_KEY_STAR:
              WG_SetFocus(gsPassword );
              gsPasswordSetUnderLine();
              GUI_CreateTimer(&groupSetWin, GS_EDIT_TIMER, EDIT_TIMEROUT_LENGTH);
              gsPasswordSetActivate(GS_EDIT_ACTIVATE);
              WG_Paint(gsPassword);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            default:
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
          }
        }
      }
      break;
    case WM_CREATE:
    {
      SysCfg sysCfg;
      SysCfg_GetConfig(&sysCfg);
      GUI_Uint16ToChar(sysCfg.groupNumber, _gs_groupnum.GroupID, GS_FROUPIS_LENGTH);
		_gs_groupnum.GroupID[4] = 0;
      _gs_groupnum.Activate = GS_EDIT_UNACTIVATE;
    }
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        GUI_SetCharUnderLine(_gs_groupnum.GroupID, GS_FROUPIS_LENGTH);
        _gs_groupnum.Activate = GS_EDIT_ACTIVATE;
        WG_Paint(gsGroupNum);
      }
      else
      {
        _gs_groupnum.Activate = GS_EDIT_UNACTIVATE;
      }
      break;
    case WM_PAINT:
      if(_gs_groupnum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        uint8_t flash = 1;
        GUI_LCDSetS(LCD_S48, 1);
        GUI_LCDSetS(LCD_S42, 1);
        GUI_LCDSetS(LCD_S41, 1);
        if(_gs_groupnum.Activate == GS_EDIT_UNACTIVATE)
          flash = 0;
        if(flash && _gs_groupnum.GroupID[0] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM4, _gs_groupnum.GroupID[0], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM4, _gs_groupnum.GroupID[0], 0);
        }
        
        if(flash && _gs_groupnum.GroupID[1] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM5, _gs_groupnum.GroupID[1], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM5, _gs_groupnum.GroupID[1], 0);
        }
        
        if(flash && _gs_groupnum.GroupID[2] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM6, _gs_groupnum.GroupID[2], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM6, _gs_groupnum.GroupID[2], 0);
        }
        
        if(flash)
        {
          GUI_LCDSetNumChar(LCD_NUM7, _gs_groupnum.GroupID[3], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM7, _gs_groupnum.GroupID[3], 0);
        }
      }
      else
      {
        GUI_LCDDisableNumFlash();
        GUI_LCDSetS(LCD_S48, 0);
        GUI_LCDSetS(LCD_S42, 0);
        GUI_LCDSetS(LCD_S41, 0);
        GUI_LCDSetNumChar(LCD_NUM4, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM5, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM6, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM7, LCD_DISPNULL, 0);
      }
      break;
    case WM_TIMER:
      if(pMsg->data_v == GS_EDIT_TIMER)
      {
        SysCfg sysCfg;
        SysCfg_GetConfig(&sysCfg);
        GUI_Uint16ToChar(sysCfg.groupNumber, _gs_groupnum.GroupID, GS_FROUPIS_LENGTH);
        _gs_groupnum.Activate = GS_EDIT_UNACTIVATE;
        WG_Paint(gsGroupNum);
        gsPasswordSetUnderLine();
        gsPasswordSetActivate(GS_EDIT_UNACTIVATE);
        WG_SetFocus(gsPassword);
        WG_Paint(gsPassword);
      }
      break;
  }
}

Widget* gsGroupNumCreate(void)
{
  WG_Create((Widget*)&_gs_groupnum, &groupSetWin, gsGroupNumCallback);
  gsGroupNum = (Widget*)&_gs_groupnum;
  return gsGroupNum;
}

uint8_t gsGroupNumGetUnderLineNum()
{
  uint8_t length = 0;
  GUI_CharToUint16(_gs_groupnum.GroupID, &length);
  return length;
}

uint16_t gsGroupNumGetVal(void)
{
  return GUI_CharToUint16(_gs_groupnum.GroupID, 0);
}

uint8_t gsGroupNumSetVal(uint16_t GroupNum)
{
  GUI_Uint16ToChar(GroupNum, _gs_groupnum.GroupID, GS_FROUPIS_LENGTH);
}

void gsGroupNumSetUnderLine()
{
  GUI_SetCharUnderLine(_gs_groupnum.GroupID, GS_FROUPIS_LENGTH);
}
