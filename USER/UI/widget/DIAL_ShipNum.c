#include "dialWinDlg.h"
#include "UIMessage.h"
#include "dlg.h"
#include "lcdControl.h"
#include "GUI_LCD.h"


typedef struct WG_ShioNum{
  Widget Widget;
  uint32_t ShipNum;
  uint32_t OtherShipNum;
  uint8_t DispMode;
}WG_ShioNum;

WG_ShioNum _shipNum = {0};
Widget *dialShipNum;

static void shipNumBack(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      break;
    case WM_CREATE:
      _shipNum.DispMode = DIAL_DISP_SHIPNUM;
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_shipNum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S51, 1);
        GUI_LCDSetS(LCD_S40, 1);
        GUI_LCDSetS(LCD_S38, 1);
        GUI_LCDSetS(LCD_S36, 1);
        if(_shipNum.DispMode == DIAL_DISP_SHIPNUM)
        {
          GUI_LCDSetNumInt(LCD_NUM9, _shipNum.ShipNum / 10000 % 10, 0);
          GUI_LCDSetNumInt(LCD_NUM10, _shipNum.ShipNum / 1000 % 10, 0);
          GUI_LCDSetNumInt(LCD_NUM11, _shipNum.ShipNum / 100 % 10, 0);
          GUI_LCDSetNumInt(LCD_NUM12, _shipNum.ShipNum / 10 % 10, 0);
          GUI_LCDSetNumInt(LCD_NUM13, _shipNum.ShipNum % 10, 0);
        }
        else
        {
          GUI_LCDSetNumInt(LCD_NUM9, _shipNum.OtherShipNum / 10000 % 10, 0);
          GUI_LCDSetNumInt(LCD_NUM10, _shipNum.OtherShipNum / 1000 % 10, 0);
          GUI_LCDSetNumInt(LCD_NUM11, _shipNum.OtherShipNum / 100 % 10, 0);
          GUI_LCDSetNumInt(LCD_NUM12, _shipNum.OtherShipNum / 10 % 10, 0);
          GUI_LCDSetNumInt(LCD_NUM13, _shipNum.OtherShipNum % 10, 0);
        }
      }
      else
      {
        GUI_LCDSetS(LCD_S51, 0);
        GUI_LCDSetS(LCD_S40, 0);
        GUI_LCDSetS(LCD_S38, 0);
        GUI_LCDSetS(LCD_S36, 0);
        GUI_LCDSetNumInt(LCD_NUM9, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM10, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM11, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM12, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM13, LCD_DISPNULL, 0);
      }
      break;
  }
}


Widget* dialShipNumCreate(void)
{
  WG_Create((Widget*)&_shipNum, &dialWin, shipNumBack);
  dialShipNum = (Widget*)&_shipNum;
  return dialShipNum;
}

void dialShipNumSetVal(uint8_t DispMode, uint32_t ShipNum)
{   
  if(DispMode == DIAL_DISP_OTHERSHIPNUM)
  {
    _shipNum.OtherShipNum = ShipNum;
  }
  else
  {
    _shipNum.ShipNum = ShipNum;
  }
}

void dialShipNumSetMode(uint8_t DispMode)
{
  _shipNum.DispMode = DispMode;
}

uint32_t dialShipNumGetVal(uint8_t DispMode)
{
  if(DispMode == DIAL_DISP_OTHERSHIPNUM)
  {
    return _shipNum.OtherShipNum;
  }
  else
  {
    return _shipNum.ShipNum;
  }
}
