#include "dialWinDlg.h"
#include "UIMessage.h"
#include "dlg.h"
#include "lcdControl.h"
#include "GUI_LCD.h"

typedef struct WG_OtherDialNum{
  Widget Widget;
  uint32_t num;
}WG_OtherDialNum;

WG_OtherDialNum _otherDialNum = {0};
Widget *dialOtherDialNum;

static void otherDialNumBack(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_otherDialNum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S47, 1);
        GUI_LCDSetS(LCD_S45, 1);
        GUI_LCDSetS(LCD_S43, 1);
        GUI_LCDSetS(LCD_S41, 1);
        GUI_LCDSetNumInt(LCD_NUM1, _otherDialNum.num / 1000000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM2, _otherDialNum.num / 100000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM3, _otherDialNum.num / 10000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM4, _otherDialNum.num / 1000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM5, _otherDialNum.num / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM6, _otherDialNum.num / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM7, _otherDialNum.num % 10, 0);
      }
      else
      {
        GUI_LCDSetS(LCD_S47, 0);
        GUI_LCDSetS(LCD_S45, 0);
        GUI_LCDSetS(LCD_S43, 0);
        GUI_LCDSetS(LCD_S41, 0);
        GUI_LCDSetNumInt(LCD_NUM1, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM2, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM3, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM4, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM5, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM6, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM7, LCD_DISPNULL, 0);
      }
      break;
  }
}


Widget* dialOtherDialNumCreate(void)
{
  WG_Create((Widget*)&_otherDialNum, &dialWin, otherDialNumBack);
  dialOtherDialNum = (Widget*)&_otherDialNum;
  return dialOtherDialNum;
}

void dialOtherDialNumSetVal(uint32_t OtherDialNum)
{
  _otherDialNum.num = OtherDialNum;
}

uint32_t dialOtherDialNumGetVal()
{
  return _otherDialNum.num;
}
