#include "dialWinDlg.h"
#include "UIMessage.h"
#include "dlg.h"
#include "lcdControl.h"
#include "GUI_LCD.h"
#include "call_log.h"

typedef struct WG_DialDialState{
  Widget Widget;
  uint8_t IsMiss;
  uint8_t Dir;
}WG_DialDialState;

WG_DialDialState _dial_dialstate = {0};
Widget *dialDialState;

static void dialDialStateCallback(WM_Message *pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_dial_dialstate.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        if(_dial_dialstate.IsMiss == CALL_MISS)
          GUI_LCDSetS(LCD_S33, 1);
        else
          GUI_LCDSetS(LCD_S33, 0);
        if(_dial_dialstate.Dir == CALL_DIR_IN)
        {
          GUI_LCDSetS(LCD_S34, 1);
          GUI_LCDSetS(LCD_S35, 0);
        }
        else if(_dial_dialstate.Dir == CALL_DIR_OUT)
        {
          GUI_LCDSetS(LCD_S35, 1);
          GUI_LCDSetS(LCD_S34, 0);
        }
      }
      else
      {
        GUI_LCDSetS(LCD_S33, 0);
        GUI_LCDSetS(LCD_S34, 0);
        GUI_LCDSetS(LCD_S35, 0);
      }
      break;
    
  }
  return;



}

Widget* dialDialStateCreate(void)
{
  WG_Create((Widget*)&_dial_dialstate, &dialWin, dialDialStateCallback);
  dialDialState = (Widget*)&_dial_dialstate;
  return dialDialState;
}

void dialDialStateSet(uint8_t IsMiss, uint8_t Dir)
{
  _dial_dialstate.Dir = Dir;
  _dial_dialstate.IsMiss = IsMiss;
}



