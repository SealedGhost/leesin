#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "beCalledWinDlg.h"
#include "string.h"
#include "GUI.h"
#include "GUI_LCD.h"
#include "call_mgr.h"

typedef struct WG_bcRoundButton{
  Widget Widget;
}WG_bcRoundButton;

WG_bcRoundButton _bc_roundbutton;
Widget* bcRoundButton;

static void bcRoundButtonWinCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_HUNG_UP:
      WG_SetInVisible(bcRoundButton);
      WG_SetFocus(bcNotPickUp);
      WG_Paint(bcRoundButton);
      WG_Paint(bcNotPickUp);
      AudioStopPlay();
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      { 
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_PICKUP:
            Msg.pSource = beCallSourceWin;
            Msg.pTarget = &dialWin;
            Msg.msgType = USER_MSG_DIALTALKING;
            Msg.data_v = bcDialNumGetVal();
            Msg.data_p = (void*)bcShipNumGetVal();
            WM_PostMessage(&Msg);
            AudioStopPlay();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_HANGUP:
            CallMgr_HangUp(bcDialNumGetVal());
            WG_SetInVisible(bcRoundButton);
            WG_SetFocus(bcNotPickUp);
            WG_Paint(bcRoundButton);
            WG_Paint(bcNotPickUp);
            AudioStopPlay();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_bc_roundbutton.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S15, 1);
      }
      else
      {
        GUI_LCDSetS(LCD_S15, 0);
      }
      break;
  }
}


Widget* bcRoundButtonCreate(void)
{
  WG_Create((Widget*)&_bc_roundbutton, &beCalledWin, bcRoundButtonWinCallback);
  bcRoundButton = (Widget*)&_bc_roundbutton;
  return bcRoundButton;
}