#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "calllogWinDlg.h"
#include "GUI.h"
#include "GUI_LCD.h"
#include "call_log.h"

typedef struct WG_CalogDel{
  Widget Widget;
}WG_CalogDel;

WG_CalogDel _calog_del;
Widget *calogDel;

static void callogDelCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
    {
      WG_SetInVisible(calogDel);
      WG_Paint(calogDel);
      WG_SetVisible(calogShipNum);
      WG_SetVisible(calogDialState);
      WG_SetFocus(calogDialNum);
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &calogWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
    }
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      { 
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_BACK:
            WG_SetInVisible(calogDel);
            WG_Paint(calogDel);
            WG_SetVisible(calogShipNum);
            WG_Paint(calogShipNum);
            WG_SetVisible(calogDialState);
            WG_Paint(calogDialState);
            WG_SetFocus(calogDialNum);
            WG_Paint(calogDialNum);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_ENTER:
          {
            CallLogItem *calllogItem;
            CallLog_Delete(CallLog_GetCurrPos());
            if(CallLog_GetCurrPos())
            {
              calllogItem = CallLog_GetCurrLog();
              calogShipNumSetVal(calllogItem->who.shipNumber);
              calogDialNumSetVal(calllogItem->who.phoneNumber);
              calogOrderNumSetVal(CallLog_GetCurrPos());
              calogDialStateSet(calllogItem->isMiss, calllogItem->dir);
              calogTimeSetVal(calllogItem->when.year, calllogItem->when.month, calllogItem->when.day, 
                              calllogItem->when.hour, calllogItem->when.minute, calllogItem->when.second);
            }
            else
            {
              calogShipNumSetVal(0);
              calogDialNumSetVal(0);
              calogOrderNumSetVal(0);
              calogDialStateSet(0,0);
              calogTimeSetVal(0,0,0,0,0,0);
            }
            WG_SetInVisible(calogDel);
            WG_Paint(calogDel);
            WG_SetVisible(calogDialState);
            WG_Paint(calogDialState);
            WG_SetVisible(calogShipNum);
            WG_Paint(calogShipNum);
            WG_SetFocus(calogDialNum);
            WG_Paint(calogDialNum);
            WG_Paint(calogOrderNum);
            WG_Paint(calogTime);
          }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_calog_del.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetNumChar(LCD_NUM16, 'D', 0);
        GUI_LCDSetNumChar(LCD_NUM17, 'E', 0);
        GUI_LCDSetNumChar(LCD_NUM18, 'L', 0);
      }
      else
      {
        GUI_LCDSetNumChar(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM18, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* calogDelCreate(void)
{
  WG_Create((Widget*)&_calog_del, &calogWin, callogDelCallback);
  calogDel = (Widget*)&_calog_del;
  return calogDel;
}

