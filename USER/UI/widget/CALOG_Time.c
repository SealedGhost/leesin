#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "calllogWinDlg.h"
#include "GUI_LCD.h"

typedef struct WG_CalogTime{
  Widget Widget;
  uint16_t Year;
  uint8_t Month;
  uint8_t Day;
  uint8_t Hour;
  uint8_t Minute;
  uint8_t Second;
}WG_CalogTime;

WG_CalogTime _calog_time;
Widget *calogTime;

static void callogTimeCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_calog_time.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        if(_calog_time.Month > 9)
          GUI_LCDSetS(LCD_S59, 1);
        else
        {
          GUI_LCDSetS(LCD_S60, 1);
          GUI_LCDSetS(LCD_S59, 1);
        }
        GUI_LCDSetS(LCD_S58, 1);
        GUI_LCDSetS(LCD_S57, 1);
        GUI_LCDSetS(LCD_S56, 1);
        GUI_LCDSetS(LCD_S55, 1);
        GUI_LCDSetNumInt(LCD_NUM21, _calog_time.Month % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM22, _calog_time.Day / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM23, _calog_time.Day % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM24, _calog_time.Hour / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM25, _calog_time.Hour % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM26, _calog_time.Minute / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM27, _calog_time.Minute % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM28, _calog_time.Second / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM29, _calog_time.Second % 10, 0);
      }
      else
      {
        GUI_LCDSetS(LCD_S60, 0);
        GUI_LCDSetS(LCD_S59, 0);
        GUI_LCDSetS(LCD_S58, 0);
        GUI_LCDSetS(LCD_S57, 0);
        GUI_LCDSetS(LCD_S56, 0);
        GUI_LCDSetS(LCD_S55, 0);
        GUI_LCDSetNumInt(LCD_NUM21, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM22, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM23, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM24, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM25, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM26, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM27, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM28, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM29, LCD_DISPNULL, 0);
      }
      break;
    
  }
  return;
}

Widget* calogTimeCreate(void)
{
  WG_Create((Widget*)&_calog_time, &calogWin, callogTimeCallback);
  calogTime = (Widget*)&_calog_time;
  return calogTime;
}

void calogTimeSetVal(uint16_t Year, uint8_t Month, uint8_t Day, uint8_t Hour, uint8_t Minute, uint8_t Second)
{
  _calog_time.Year = Year;
  _calog_time.Month = Month;
  _calog_time.Day = Day;
  _calog_time.Hour = Hour;
  _calog_time.Minute = Minute;
  _calog_time.Second = Second;
}
