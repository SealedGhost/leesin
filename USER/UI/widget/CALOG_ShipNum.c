#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "calllogWinDlg.h"
#include "GUI_LCD.h"


typedef struct WG_CalogShipNum{
  Widget Widget;
  uint32_t ShipNum;
}WG_CalogShipNum;

WG_CalogShipNum _calog_shipnum;
Widget *calogShipNum;

static void callogShipNumCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_calog_shipnum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S51, 1);
        GUI_LCDSetS(LCD_S40, 1);
        GUI_LCDSetS(LCD_S38, 1);
        GUI_LCDSetS(LCD_S36, 1);
        GUI_LCDSetNumInt(LCD_NUM9, _calog_shipnum.ShipNum / 10000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM10, _calog_shipnum.ShipNum / 1000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM11, _calog_shipnum.ShipNum / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM12, _calog_shipnum.ShipNum / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM13, _calog_shipnum.ShipNum % 10, 0);
      }
      else
      {
        GUI_LCDSetS(LCD_S51, 0);
        GUI_LCDSetS(LCD_S40, 0);
        GUI_LCDSetS(LCD_S38, 0);
        GUI_LCDSetS(LCD_S36, 0);
        GUI_LCDSetNumInt(LCD_NUM9, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM10, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM11, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM12, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM13, LCD_DISPNULL, 0);
      }
      break;
    
  }
  return;
}

Widget* calogShipNumCreate(void)
{
  WG_Create((Widget*)&_calog_shipnum, &calogWin, callogShipNumCallback);
  calogShipNum = (Widget*)&_calog_shipnum;
  return calogShipNum;
}

void calogShipNumSetVal(uint32_t ShipID)
{
  _calog_shipnum.ShipNum = ShipID;
}

uint32_t calogShipNumGetVal(void)
{
  return _calog_shipnum.ShipNum;
}
