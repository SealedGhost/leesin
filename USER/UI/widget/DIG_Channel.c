#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "digitWinDlg.h"
#include "GUI.h"
#include "digital_channel.h"
#include "GUI_LCD.h"
#include "channel480.h"
#include "GUI_Timer.h"

typedef struct WG_DigChannel{
  Widget Widget;
  uint16_t Channel;
}WG_DigChannel;

WG_DigChannel _dig_channel = {0};
Widget* digChannel;

static void digChannelCallback(WM_Message *pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &digitWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_PICKUP:
            WM_SetFocus(&editDialNumWin);
            WM_BringToTop(&editDialNumWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_STAR:
            GUI_CreateTimer(&digitWin, DIG_EDIT_TIMER, EDIT_TIMEROUT_LENGTH);
            WG_SetInVisible(digChannel);
            WG_Paint(digChannel);
            WG_SetFocus(digEditChannel);
            WG_Paint(digEditChannel);
            DCH_Exit();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_UP:
            _dig_channel.Channel = DCH_GetNextChannel();
            ConfigChannel(_dig_channel.Channel);
            WG_Paint(digChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_DOWN:
            _dig_channel.Channel = DCH_GetPrevChannel();
            ConfigChannel(_dig_channel.Channel);
            WG_Paint(digChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_0_CALLSHIP:
            WM_SetFocus(&callShipNumWin);
            WM_BringToTop(&callShipNumWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_1_GROUP:
            WM_SetFocus(&gdialWin);
            WM_BringToTop(&gdialWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_3_SIMULATE:
            WM_BringToTop(&analogWin);
            WM_SetFocus(&analogWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_4_LOG:
            WM_SetFocus(&calogWin);
            WM_BringToTop(&calogWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_5_DIRECTION:
            WM_SetFocus(&contactsWin);
            WM_BringToTop(&contactsWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_6_REMEMBER:
            WM_SetFocus(&rememberChWin);       
            WM_BringToTop(&rememberChWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_8_WEATHER:
            WM_SetFocus(&weatherWin);        
            WM_BringToTop(&weatherWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      else if(((WM_KEY_INFO*)pMsg->data_p)->Pressed >= KEY_DOWN + 1 && ((WM_KEY_INFO*)pMsg->data_p)->Pressed <= KEY_DOWN + 3)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_UP:
            _dig_channel.Channel = DCH_GetNextChannel();
            ConfigChannel(_dig_channel.Channel);
            WG_Paint(digChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_DOWN:
            _dig_channel.Channel = DCH_GetPrevChannel();
            ConfigChannel(_dig_channel.Channel);
            WG_Paint(digChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
        }
      }
      else if(((WM_KEY_INFO*)pMsg->data_p)->Pressed > KEY_DOWN + 3)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_UP:
            _dig_channel.Channel = DCH_GetNextChannel();
            ConfigChannel(_dig_channel.Channel);
            WG_Paint(digChannel);
            break;
          case GUI_KEY_DOWN:
            _dig_channel.Channel = DCH_GetPrevChannel();
            ConfigChannel(_dig_channel.Channel);
            WG_Paint(digChannel);
            break;
        }
      }
      break;
    case WM_CREATE:
      _dig_channel.Channel = DCH_GetCurrChannel();
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_dig_channel.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetNumInt(LCD_NUM14, 'D', 0);
        GUI_LCDSetNumInt(LCD_NUM15, 'C', 0);
        GUI_LCDSetNumInt(LCD_NUM16, 'H', 0);
        GUI_LCDSetNumInt(LCD_NUM17, '-', 0);
        GUI_LCDSetNumInt(LCD_NUM18, _dig_channel.Channel/100%10, 0);
        GUI_LCDSetNumInt(LCD_NUM19, _dig_channel.Channel/10%10, 0);
        GUI_LCDSetNumInt(LCD_NUM20, _dig_channel.Channel%10, 0);
      }
      else
      {
        GUI_LCDSetNumInt(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM20, LCD_DISPNULL, 0);
      }
      break;
  }
}

Widget* digChannelCreate(void)
{
  WG_Create((Widget*)&_dig_channel, &digitWin, digChannelCallback);
  digChannel = (Widget*)&_dig_channel;
  return digChannel;
}

void digChannelSetVal(uint16_t Channel)
{
  DCH_SetCurrChannel(Channel);
  _dig_channel.Channel = DCH_GetCurrChannel();
}

