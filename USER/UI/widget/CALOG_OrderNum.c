#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "calllogWinDlg.h"
#include "GUI_LCD.h"

typedef struct WG_CalogOrderNum{
  Widget Widget;
  uint8_t OrderNum;
}WG_CalogOrderNum;

WG_CalogOrderNum _calog_ordernum = {0};
Widget *calogOrderNum;

static void callogOrderNumCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_calog_ordernum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S46, 1);
        GUI_LCDSetS(LCD_S45, 1);
        GUI_LCDSetS(LCD_S44, 1);
        GUI_LCDSetNumInt(LCD_NUM1, _calog_ordernum.OrderNum / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM2, _calog_ordernum.OrderNum % 10, 0);
      }
      else
      {
        GUI_LCDSetS(LCD_S46, 0);
        GUI_LCDSetS(LCD_S45, 0);
        GUI_LCDSetS(LCD_S44, 0);
        GUI_LCDSetNumInt(LCD_NUM1, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM2, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* calogOrderNumCreate(void)
{
  WG_Create((Widget*)&_calog_ordernum, &calogWin, callogOrderNumCallback);
  calogOrderNum = (Widget*)&_calog_ordernum;
  return calogOrderNum;
}

void calogOrderNumSetVal(uint8_t OrderNum)
{
  _calog_ordernum.OrderNum = OrderNum;
}

uint8_t calogOrderNumGetVal(void)
{
  return _calog_ordernum.OrderNum;
}
