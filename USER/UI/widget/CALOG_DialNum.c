#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "calllogWinDlg.h"
#include "GUI.h"
#include "call_log.h"
#include "GUI_LCD.h"


typedef struct WG_CalogDialNum{
  Widget Widget;
  uint32_t DialNum;
}WG_CalogDialNum;

WG_CalogDialNum _calog_dialnum;
Widget *calogDialNum;

static void callogDialNumCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
    {
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &calogWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
    }
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      { 
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_PICKUP:
            if(_calog_dialnum.DialNum)
            {
              Msg.pSource = &calogWin;
              Msg.pTarget = &dialWin;
              Msg.msgType = USER_MSG_DIAL;
              Msg.data_v = _calog_dialnum.DialNum;
              WM_PostMessage(&Msg);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          case GUI_KEY_UP:
          {
            CallLogItem *callLogItem;
            callLogItem = CallLog_GetNextLog();
            if(callLogItem)
            {
              calogOrderNumSetVal(CallLog_GetCurrPos());
              calogShipNumSetVal(callLogItem->who.shipNumber);
              calogTimeSetVal(callLogItem->when.year, callLogItem->when.month,
                        callLogItem->when.day, callLogItem->when.hour,
                        callLogItem->when.minute, callLogItem->when.second);
              _calog_dialnum.DialNum = callLogItem->who.phoneNumber;
              calogDialStateSet(callLogItem->isMiss, callLogItem->dir);
              WG_Paint(calogTime);
              WG_Paint(calogDialState);
              WG_Paint(calogDialNum);
              WG_Paint(calogShipNum);
              WG_Paint(calogOrderNum);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
          }
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          case GUI_KEY_DOWN:
          {
            CallLogItem *callLogItem;
            callLogItem = CallLog_GetPrevLog();
            if(callLogItem)
            {
              calogOrderNumSetVal(CallLog_GetCurrPos());
              calogShipNumSetVal(callLogItem->who.shipNumber);
              calogTimeSetVal(callLogItem->when.year, callLogItem->when.month,
                        callLogItem->when.day, callLogItem->when.hour,
                        callLogItem->when.minute, callLogItem->when.second);
              _calog_dialnum.DialNum = callLogItem->who.phoneNumber;
              calogDialStateSet(callLogItem->isMiss, callLogItem->dir);
              WG_Paint(calogTime);
              WG_Paint(calogDialState);
              WG_Paint(calogDialNum);
              WG_Paint(calogShipNum);
              WG_Paint(calogOrderNum);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
          }
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          case GUI_KEY_DEL:
            if(calogOrderNumGetVal())
            {
              WG_SetInVisible(calogDialState);
              WG_Paint(calogDialState);
              WG_SetInVisible(calogDialNum);
              WG_Paint(calogDialNum);
              WG_SetInVisible(calogShipNum);
              WG_Paint(calogShipNum);
              WG_SetFocus(calogDel);
              WG_Paint(calogDel);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          case GUI_KEY_0_CALLSHIP:
            WM_SetFocus(&callShipNumWin);
            WM_BringToTop(&callShipNumWin);
            CallLog_GoHome();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_1_GROUP:
            WM_SetFocus(&gdialWin);
            WM_BringToTop(&gdialWin);
            CallLog_GoHome();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_2_DIGIT:
            WM_SetFocus(&digitWin);
            WM_BringToTop(&digitWin);
            CallLog_GoHome();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_3_SIMULATE:
            WM_BringToTop(&analogWin);
            WM_SetFocus(&analogWin);
            CallLog_GoHome();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_NEW:
            if(calogOrderNumGetVal())
            {
              Msg.pTarget = &contactsWin;
              Msg.msgType = USER_MSG_LOGTOCONTACTS;
              Msg.data_v = _calog_dialnum.DialNum;
              Msg.data_p = (void*)calogShipNumGetVal();
              WM_PostMessage(&Msg);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          case GUI_KEY_5_DIRECTION:
            WM_SetFocus(&contactsWin);
            WM_BringToTop(&contactsWin);
            CallLog_GoHome();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_6_REMEMBER:
            WM_SetFocus(&rememberChWin);       
            WM_BringToTop(&rememberChWin);
            CallLog_GoHome();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_8_WEATHER:
            WM_SetFocus(&weatherWin);        
            WM_BringToTop(&weatherWin);
            CallLog_GoHome();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_calog_dialnum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetNumInt(LCD_NUM14, _calog_dialnum.DialNum / 1000000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM15, _calog_dialnum.DialNum / 100000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM16, _calog_dialnum.DialNum / 10000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM17, _calog_dialnum.DialNum / 1000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM18, _calog_dialnum.DialNum / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM19, _calog_dialnum.DialNum / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM20, _calog_dialnum.DialNum % 10, 0);
      }
      else
      {
        GUI_LCDSetNumInt(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM20, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* calogDialNumCreate(void)
{
  WG_Create((Widget*)&_calog_dialnum, &calogWin, callogDialNumCallback);
  calogDialNum = (Widget*)&_calog_dialnum;
  return calogDialNum;
}


void calogDialNumSetVal(uint32_t DialNum)
{
  _calog_dialnum.DialNum = DialNum;
}

uint32_t calogDialNumGetVal(void)
{

}

