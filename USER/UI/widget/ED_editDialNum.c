#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "editDialNumWinDlg.h"
#include "GUI.h"
#include "GUI_LCD.h"


typedef struct WG_editDialNum{
  Widget Widget;
  char DialNum[8];
  uint8_t Activate;
}WG_editDialNum;


WG_editDialNum _ed_dialnum = {0};
Widget *edEditDialNum;


static void edEditDialNumCallback(WM_Message *pMsg)
{
  WM_Message Msg;
  uint8_t Length;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      _ed_dialnum.Activate = ED_EDIT_UNACTIVATE;
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &editDialNumWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        if(_ed_dialnum.Activate == ED_EDIT_ACTIVATE)
        {
          switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
          {
            case GUI_KEY_BACK:
              GUI_SetCharUnderLine(_ed_dialnum.DialNum, EDIT_DEIALNUM_LENGTH);
              _ed_dialnum.Activate = ED_EDIT_UNACTIVATE;
              WG_Paint(edEditDialNum);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_PICKUP:
            {
              uint8_t length = 0;
              GUI_CharToUint32(_ed_dialnum.DialNum, &length);
              if(!length && edEditDialNumGetVal())
              {
                _ed_dialnum.Activate = ED_EDIT_UNACTIVATE;
                Msg.pSource = &editDialNumWin;
                Msg.pTarget = &dialWin;
                Msg.msgType = USER_MSG_DIAL;
                Msg.data_v = edEditDialNumGetVal();
                WM_PostMessage(&Msg);
                GUI_SetCharUnderLine(_ed_dialnum.DialNum, EDIT_DEIALNUM_LENGTH);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
              break;
            case GUI_KEY_STAR:
            case GUI_KEY_DEL:
              GUI_CharToUint32(_ed_dialnum.DialNum, &Length);
              if(Length != EDIT_DEIALNUM_LENGTH)
              {
                GUI_SetCharUnderLine(_ed_dialnum.DialNum, EDIT_DEIALNUM_LENGTH);
                WG_Paint(edEditDialNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              break;
            case GUI_KEY_0_CALLSHIP:
              if(GUI_AddAChar(_ed_dialnum.DialNum, '0', EDIT_DEIALNUM_LENGTH))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              WG_Paint(edEditDialNum);
              break;
            case GUI_KEY_1_GROUP:
              if(GUI_AddAChar(_ed_dialnum.DialNum, '1', EDIT_DEIALNUM_LENGTH))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              WG_Paint(edEditDialNum);
              break;
            case GUI_KEY_2_DIGIT:
              if(GUI_AddAChar(_ed_dialnum.DialNum, '2', EDIT_DEIALNUM_LENGTH))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              WG_Paint(edEditDialNum);
              break;
            case GUI_KEY_3_SIMULATE:
              if(GUI_AddAChar(_ed_dialnum.DialNum, '3', EDIT_DEIALNUM_LENGTH))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              WG_Paint(edEditDialNum);
              break;
            case GUI_KEY_4_LOG:
              if(GUI_AddAChar(_ed_dialnum.DialNum, '4', EDIT_DEIALNUM_LENGTH))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              WG_Paint(edEditDialNum);
              break;
            case GUI_KEY_5_DIRECTION:
              if(GUI_AddAChar(_ed_dialnum.DialNum, '5', EDIT_DEIALNUM_LENGTH))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              WG_Paint(edEditDialNum);
              break;
            case GUI_KEY_6_REMEMBER:       
              if(GUI_AddAChar(_ed_dialnum.DialNum, '6', EDIT_DEIALNUM_LENGTH))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              WG_Paint(edEditDialNum);
              break;
            case GUI_KEY_7_SIGN: 
              if(GUI_AddAChar(_ed_dialnum.DialNum, '7', EDIT_DEIALNUM_LENGTH))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              WG_Paint(edEditDialNum);
              break;
            case GUI_KEY_8_WEATHER:        
              if(GUI_AddAChar(_ed_dialnum.DialNum, '8', EDIT_DEIALNUM_LENGTH))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              WG_Paint(edEditDialNum);
              break;
            case GUI_KEY_9_SCAN:         
              if(GUI_AddAChar(_ed_dialnum.DialNum, '9', EDIT_DEIALNUM_LENGTH))
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              WG_Paint(edEditDialNum);
              break;
            default:
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
          }
        }
        else
        {
          switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
          {
            case GUI_KEY_STAR:
              _ed_dialnum.Activate = ED_EDIT_ACTIVATE;
              WG_Paint(edEditDialNum);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_0_CALLSHIP:
              WM_SetFocus(&callShipNumWin);
              WM_BringToTop(&callShipNumWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
            case GUI_KEY_1_GROUP:
              
              WM_SetFocus(&gdialWin);
              WM_BringToTop(&gdialWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_2_DIGIT:
              WM_SetFocus(&digitWin);
              WM_BringToTop(&digitWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_3_SIMULATE:
              WM_BringToTop(&analogWin);
              WM_SetFocus(&analogWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_4_LOG:
              WM_SetFocus(&calogWin);
              WM_BringToTop(&calogWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_5_DIRECTION:
              WM_SetFocus(&contactsWin);
              WM_BringToTop(&contactsWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_6_REMEMBER:
              WM_SetFocus(&rememberChWin);       
              WM_BringToTop(&rememberChWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_8_WEATHER:
              WM_SetFocus(&weatherWin);        
              WM_BringToTop(&weatherWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            default:
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
          }
        }
      }
      break;
    case WM_CREATE:
      GUI_SetCharUnderLine(_ed_dialnum.DialNum, EDIT_DEIALNUM_LENGTH);
      _ed_dialnum.Activate = ED_EDIT_UNACTIVATE;
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
      
      }
      else
      {
        _ed_dialnum.Activate = ED_EDIT_UNACTIVATE;
      }
      break;
    case WM_PAINT:
      if(_ed_dialnum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        if(_ed_dialnum.Activate == ED_EDIT_ACTIVATE)
        {
          uint8_t flash = 1;
          if(flash && _ed_dialnum.DialNum[0] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM14, _ed_dialnum.DialNum[0], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM14, _ed_dialnum.DialNum[0], 0);
          }
          
          if(flash && _ed_dialnum.DialNum[1] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM15, _ed_dialnum.DialNum[1], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM15, _ed_dialnum.DialNum[1], 0);
          }
          
          if(flash && _ed_dialnum.DialNum[2] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM16, _ed_dialnum.DialNum[2], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM16, _ed_dialnum.DialNum[2], 0);
          }
          
          if(flash && _ed_dialnum.DialNum[3] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM17, _ed_dialnum.DialNum[3], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM17, _ed_dialnum.DialNum[3], 0);
          }
          
          if(flash && _ed_dialnum.DialNum[4] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM18, _ed_dialnum.DialNum[4], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM18, _ed_dialnum.DialNum[4], 0);
          }
          
          if(flash && _ed_dialnum.DialNum[5] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM19, _ed_dialnum.DialNum[5], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM19, _ed_dialnum.DialNum[5], 0);
          }
          
          if(flash)
          {
            GUI_LCDSetNumChar(LCD_NUM20, _ed_dialnum.DialNum[6], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM20, _ed_dialnum.DialNum[6], 0);
          }
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM14, '0', 0);
          GUI_LCDSetNumChar(LCD_NUM15, '0', 0);
          GUI_LCDSetNumChar(LCD_NUM16, '0', 0);
          GUI_LCDSetNumChar(LCD_NUM17, '0', 0);
          GUI_LCDSetNumChar(LCD_NUM18, '0', 0);
          GUI_LCDSetNumChar(LCD_NUM19, '0', 0);
          GUI_LCDSetNumChar(LCD_NUM20, '0', 0);
        }
      }
      else
      {
        GUI_LCDDisableNumFlash();
        GUI_LCDSetNumChar(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM20, LCD_DISPNULL, 0);
      }
      break;
  }
}

Widget* edEditDialNumCreate()
{
  WG_Create((Widget*)&_ed_dialnum, &editDialNumWin, edEditDialNumCallback);
  edEditDialNum = (Widget*)&_ed_dialnum;
  return edEditDialNum;
}

void edEditDialNumSetUnderline()
{
  GUI_SetCharUnderLine(_ed_dialnum.DialNum, EDIT_DEIALNUM_LENGTH);
}

uint32_t edEditDialNumGetVal()
{
   return GUI_CharToUint32(_ed_dialnum.DialNum, 0);
}

void edEditDialNumSetStates(uint8_t Mode)
{
  _ed_dialnum.Activate = Mode;
}

