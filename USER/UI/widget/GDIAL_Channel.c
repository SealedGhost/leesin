#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "groupDialWinDlg.h"
#include "GUI.h"
#include "GUI_Timer.h"
#include "string.h"
#include "group_chat.h"
#include "GUI_LCD.h"
#include "system_config.h"
#include "channel480.h"
#include "GUI_Timer.h"

typedef struct WG_GDialChannel{
  Widget Widget;
  uint16_t Channel;
}WG_GDialChannel;

WG_GDialChannel _gdial_channel = {0};
Widget *gdialChannel;

static void gdialChannelCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_GRP_SING:
      WG_SetInVisible(gdialGroupID);
      WG_Paint(gdialGroupID);
      gdialDialNumSetVal(pMsg->data_v);
      WG_SetFocus(gdialDialNum);
      WG_Paint(gdialDialNum);
      gdialShipNumSetVal((uint32_t)pMsg->data_p);
      WG_SetVisible(gdialShipNum);
      WG_Paint(gdialShipNum);
      break;
    case USER_MSG_CALL_IN:
      gdialShipNumSetVal((uint32_t)pMsg->data_p);
      gdialDialNumSetVal(pMsg->data_v);
      WG_SetInVisible(gdialRecommmendCh);
      WG_Paint(gdialRecommmendCh);
      WG_SetInVisible(gdialGroupID);
      WG_Paint(gdialGroupID);
      WG_SetVisible(gdialDialNum);
      WG_Paint(gdialDialNum);
      WG_SetVisible(gdialShipNum);
      WG_Paint(gdialShipNum);
      WG_SetFocus(gdialRoundButton);
      WG_Paint(gdialRoundButton);
      AudioPlayCnt(AUDIO_TYPE_TALKING_CALL,AUDIO_PLAY_TIMES_TALKING_CALL);
      break;
    case WM_TIMER:
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_NEW:
            GUI_CreateTimer(&gdialWin, GROUP_RECOMEND_TIMER, 600);
            WG_SetFocus(gdialRecommmendCh);
            WG_Paint(gdialRecommmendCh);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_UP:
            _gdial_channel.Channel = Grp_GetNextChannel();
            ConfigChannel(_gdial_channel.Channel);
            WG_Paint(gdialChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_DOWN:
            _gdial_channel.Channel = Grp_GetPrevChannel();
            ConfigChannel(_gdial_channel.Channel);
            WG_Paint(gdialChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_STAR:
            GUI_CreateTimer(&gdialWin, GROUP_EDIT_TIMER, EDIT_TIMEROUT_LENGTH);
            WG_SetInVisible(gdialChannel);
            WG_Paint(gdialChannel);
            WG_SetFocus(gdialEditChannel);
            WG_Paint(gdialEditChannel);
            Grp_Exit();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_PICKUP:
            WM_SetFocus(&editDialNumWin);
            WM_BringToTop(&editDialNumWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_0_CALLSHIP:
            WM_SetFocus(&callShipNumWin);
            WM_BringToTop(&callShipNumWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_2_DIGIT:
            WM_SetFocus(&digitWin);
            WM_BringToTop(&digitWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_3_SIMULATE:
            WM_BringToTop(&analogWin);
            WM_SetFocus(&analogWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_4_LOG:
            WM_SetFocus(&calogWin);
            WM_BringToTop(&calogWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_5_DIRECTION:
            WM_SetFocus(&contactsWin);
            WM_BringToTop(&contactsWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_6_REMEMBER:
            WM_SetFocus(&rememberChWin);       
            WM_BringToTop(&rememberChWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_8_WEATHER:
            WM_SetFocus(&weatherWin);        
            WM_BringToTop(&weatherWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      else if(((WM_KEY_INFO*)pMsg->data_p)->Pressed >= KEY_DOWN + 1 && ((WM_KEY_INFO*)pMsg->data_p)->Pressed <= KEY_DOWN + 3)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_UP:
            _gdial_channel.Channel = Grp_GetNextChannel();
            ConfigChannel(_gdial_channel.Channel);
            WG_Paint(gdialChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_DOWN:
            _gdial_channel.Channel = Grp_GetPrevChannel();
            ConfigChannel(_gdial_channel.Channel);
            WG_Paint(gdialChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
        }
      }
      else if(((WM_KEY_INFO*)pMsg->data_p)->Pressed > KEY_DOWN + 3)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_UP:
            _gdial_channel.Channel = Grp_GetNextChannel();
            ConfigChannel(_gdial_channel.Channel);
            WG_Paint(gdialChannel);
            break;
          case GUI_KEY_DOWN:
            _gdial_channel.Channel = Grp_GetPrevChannel();
            ConfigChannel(_gdial_channel.Channel);
            WG_Paint(gdialChannel);
            break;
        }
      }
      break;
    case WM_CREATE:
      _gdial_channel.Channel = Grp_GetChannel();
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_gdial_channel.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetNumInt(LCD_NUM14, 'G', 0);
        GUI_LCDSetNumInt(LCD_NUM15, 'C', 0);
        GUI_LCDSetNumInt(LCD_NUM16, 'H', 0);
        GUI_LCDSetNumInt(LCD_NUM17, '-', 0);
        GUI_LCDSetNumInt(LCD_NUM18, _gdial_channel.Channel / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM19, _gdial_channel.Channel / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM20, _gdial_channel.Channel % 10, 0);
      }
      else
      {
        GUI_LCDSetNumInt(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM20, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* gdialChannelCreate(void)
{
  WG_Create((Widget*)&_gdial_channel, &gdialWin, gdialChannelCallback);
  gdialChannel = (Widget*)&_gdial_channel;
  return gdialChannel;
}

void gdialChannelSetValue(uint16_t Channel)
{
  Grp_SetChannel(Channel);
  _gdial_channel.Channel = Grp_GetChannel();
  return;
}

uint16_t gdialChannelGetValue(void)
{
  return _gdial_channel.Channel;
}


  