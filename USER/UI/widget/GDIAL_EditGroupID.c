#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "groupDialWinDlg.h"
#include "GUI.h"
#include "GUI_LCD.h"
#include "system_config.h"
#include "group_chat.h"
#include "GUI_Timer.h"

typedef struct WG_gdialEditGroupID{
  Widget Widget;
  char GroupID[5];
  uint8_t Activate;
}WG_gdialEditGroupID;


WG_gdialEditGroupID _gdial_editgroupid = {0};
Widget* gdialEditGroupID;

static void gdialEditGroupIDCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  uint8_t Length;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
      gdialEditPasswordSetActive(GDIAL_EDIT_UNACTIVETE);
      gdialEditGroupIDSetActive(GDIAL_EDIT_UNACTIVETE);
      gdialEditPasswordSetUnderline();
      gdialEditGroupIDSetUnderline();
      WG_SetFocus(gdialEditGroupID);
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &gdialWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        GUI_RestartTimer(&gdialWin, GROUP_EDIT_TIMER, EDIT_TIMEROUT_LENGTH);
        if(_gdial_editgroupid.Activate == GDIAL_EDIT_ACTIVETE)
        {
          switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
          {
            case GUI_KEY_PICKUP:
              GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
              gdialEditGroupIDSetUnderline();
              gdialEditPasswordSetUnderline();
              gdialEditGroupIDSetActive(GDIAL_EDIT_UNACTIVETE);
              gdialEditPasswordSetActive(GDIAL_EDIT_UNACTIVETE);
              WG_Paint(gdialEditGroupID);
              WG_Paint(gdialEditPassword);
              WG_SetFocus(gdialEditGroupID);
              WM_SetFocus(&editDialNumWin);
              WM_BringToTop(&editDialNumWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_DEL:
//              GUI_CharToUint32(_gdial_editgroupid.GroupID, &Length);
//              if(Length != GDIAL_GROUPID_LENGTH)
//              {
                GUI_SetCharUnderLine(_gdial_editgroupid.GroupID, GDIAL_GROUPID_LENGTH);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
//              }
//              else
//              {
//                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
//              }
              break;
            case GUI_KEY_STAR:
              gdialEditPasswordSetUnderline();
              gdialEditGroupIDSetUnderline();
              WG_SetFocus(gdialEditGroupID);
              WG_Paint(gdialEditPassword);
              WG_Paint(gdialEditGroupID);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_BACK:
              GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
              gdialEditGroupIDSetUnderline();
              gdialEditPasswordSetUnderline();
              gdialEditGroupIDSetActive(GDIAL_EDIT_UNACTIVETE);
              gdialEditPasswordSetActive(GDIAL_EDIT_UNACTIVETE);
              WG_Paint(gdialEditGroupID);
              WG_Paint(gdialEditPassword);
              WG_SetFocus(gdialEditGroupID);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_UP:
            case GUI_KEY_DOWN:
              if(gdialEditGroupIDGetUnderLineNum())
              {
                gdialEditGroupIDSetUnderline();
                WG_Paint(gdialEditGroupID);
              }
              WG_SetFocus(gdialEditPassword);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_ENTER:
              if(!gdialEditGroupIDGetUnderLineNum() && !gdialEditPasswordGetUnderlineNum() && gdialEditGroupIDGetVal() && gdialEditPasswordGetVal())
              {
                SysCfg sysCfg;
                GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
                SysCfg_GetConfig(&sysCfg);
                sysCfg.groupNumber = gdialEditGroupIDGetVal();
                sysCfg.groupPwd = gdialEditPasswordGetVal();
                SysCfg_SetConfig(&sysCfg);
                gdialGroupIDSetValue(sysCfg.groupNumber);
                WG_SetInVisible(gdialEditGroupID);
                WG_Paint(gdialEditGroupID);
                WG_SetInVisible(gdialEditPassword);
                WG_Paint(gdialEditPassword);
                WG_SetVisible(gdialGroupID);
                WG_Paint(gdialGroupID);
                WG_SetFocus(gdialChannel);
                WG_Paint(gdialChannel);
                Grp_Enter();
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            case GUI_KEY_0_CALLSHIP:
              Length = GUI_AddAChar(_gdial_editgroupid.GroupID, '0', GDIAL_GROUPID_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_GROUPID_LENGTH)
              {
                if(gdialEditPasswordGetUnderlineNum())
                {
                  WG_Paint(gdialEditGroupID);
                  WG_SetFocus(gdialEditPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              WG_Paint(gdialEditGroupID);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_1_GROUP:
              Length = GUI_AddAChar(_gdial_editgroupid.GroupID, '1', GDIAL_GROUPID_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_GROUPID_LENGTH)
              {
                if(gdialEditPasswordGetUnderlineNum())
                {
                  WG_Paint(gdialEditGroupID);
                  WG_SetFocus(gdialEditPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              WG_Paint(gdialEditGroupID);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_2_DIGIT:
              Length = GUI_AddAChar(_gdial_editgroupid.GroupID, '2', GDIAL_GROUPID_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_GROUPID_LENGTH)
              {
                if(gdialEditPasswordGetUnderlineNum())
                {
                  WG_Paint(gdialEditGroupID);
                  WG_SetFocus(gdialEditPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              WG_Paint(gdialEditGroupID);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_3_SIMULATE:
              Length = GUI_AddAChar(_gdial_editgroupid.GroupID, '3', GDIAL_GROUPID_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_GROUPID_LENGTH)
              {
                if(gdialEditPasswordGetUnderlineNum())
                {
                  WG_Paint(gdialEditGroupID);
                  WG_SetFocus(gdialEditPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              WG_Paint(gdialEditGroupID);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_4_LOG:
              Length = GUI_AddAChar(_gdial_editgroupid.GroupID, '4', GDIAL_GROUPID_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_GROUPID_LENGTH)
              {
                if(gdialEditPasswordGetUnderlineNum())
                {
                  WG_Paint(gdialEditGroupID);
                  WG_SetFocus(gdialEditPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              WG_Paint(gdialEditGroupID);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_5_DIRECTION:
              Length = GUI_AddAChar(_gdial_editgroupid.GroupID, '5', GDIAL_GROUPID_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_GROUPID_LENGTH)
              {
                if(gdialEditPasswordGetUnderlineNum())
                {
                  WG_Paint(gdialEditGroupID);
                  WG_SetFocus(gdialEditPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              WG_Paint(gdialEditGroupID);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_6_REMEMBER:       
              Length = GUI_AddAChar(_gdial_editgroupid.GroupID, '6', GDIAL_GROUPID_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_GROUPID_LENGTH)
              {
                if(gdialEditPasswordGetUnderlineNum())
                {
                  WG_Paint(gdialEditGroupID);
                  WG_SetFocus(gdialEditPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              WG_Paint(gdialEditGroupID);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_7_SIGN: 
              Length = GUI_AddAChar(_gdial_editgroupid.GroupID, '7', GDIAL_GROUPID_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_GROUPID_LENGTH)
              {
                if(gdialEditPasswordGetUnderlineNum())
                {
                  WG_Paint(gdialEditGroupID);
                  WG_SetFocus(gdialEditPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              WG_Paint(gdialEditGroupID);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_8_WEATHER:        
              Length = GUI_AddAChar(_gdial_editgroupid.GroupID, '8', GDIAL_GROUPID_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_GROUPID_LENGTH)
              {
                if(gdialEditPasswordGetUnderlineNum())
                {
                  WG_Paint(gdialEditGroupID);
                  WG_SetFocus(gdialEditPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              WG_Paint(gdialEditGroupID);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_9_SCAN:         
              Length = GUI_AddAChar(_gdial_editgroupid.GroupID, '9', GDIAL_GROUPID_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_GROUPID_LENGTH)
              {
                if(gdialEditPasswordGetUnderlineNum())
                {
                  WG_Paint(gdialEditGroupID);
                  WG_SetFocus(gdialEditPassword);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              WG_Paint(gdialEditGroupID);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            default:
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
          }
        }
        else
        {
          switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
          {
            case GUI_KEY_STAR:
              GUI_CreateTimer(&gdialWin, GROUP_EDIT_TIMER, EDIT_TIMEROUT_LENGTH);
              gdialEditGroupIDSetActive(GDIAL_EDIT_ACTIVETE);
              gdialEditPasswordSetActive(GDIAL_EDIT_ACTIVETE);
              WG_Paint(gdialEditPassword);
              WG_Paint(gdialEditGroupID);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_0_CALLSHIP:
              WM_SetFocus(&callShipNumWin);
              WM_BringToTop(&callShipNumWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_2_DIGIT:
              WM_SetFocus(&digitWin);
              WM_BringToTop(&digitWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_3_SIMULATE:
              WM_BringToTop(&analogWin);
              WM_SetFocus(&analogWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_4_LOG:
              WM_SetFocus(&calogWin);
              WM_BringToTop(&calogWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_5_DIRECTION:
              WM_SetFocus(&contactsWin);
              WM_BringToTop(&contactsWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_6_REMEMBER:
              WM_SetFocus(&rememberChWin);       
              WM_BringToTop(&rememberChWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_8_WEATHER:
              WM_SetFocus(&weatherWin);        
              WM_BringToTop(&weatherWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_PICKUP:
              WM_SetFocus(&editDialNumWin);
              WM_BringToTop(&editDialNumWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            default:
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
          }
        }          
      }
      break;
    case WM_CREATE:
      _gdial_editgroupid.Activate = GDIAL_EDIT_UNACTIVETE;
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        GUI_SetCharUnderLine(_gdial_editgroupid.GroupID, GDIAL_GROUPID_LENGTH);
        WG_Paint(gdialEditGroupID);
      }
      break;
    case WM_PAINT:
      if(_gdial_editgroupid.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        uint8_t flash = 1;
        GUI_LCDSetS(LCD_S48, 1);
        GUI_LCDSetS(LCD_S41, 1);
        GUI_LCDSetS(LCD_S42, 1);
        if(_gdial_editgroupid.Activate == GDIAL_EDIT_ACTIVETE)
        {
          if(flash && _gdial_editgroupid.GroupID[0] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM4, _gdial_editgroupid.GroupID[0], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM4, _gdial_editgroupid.GroupID[0], 0);
          }
          
          if(flash && _gdial_editgroupid.GroupID[1] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM5, _gdial_editgroupid.GroupID[1], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM5, _gdial_editgroupid.GroupID[1], 0);
          }
          
          if(flash && _gdial_editgroupid.GroupID[2] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM6, _gdial_editgroupid.GroupID[2], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM6, _gdial_editgroupid.GroupID[2], 0);
          }
          
          if(flash)
          {
            GUI_LCDSetNumChar(LCD_NUM7, _gdial_editgroupid.GroupID[3], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM7, _gdial_editgroupid.GroupID[3], 0);
          }
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM4, '0', 0);
          GUI_LCDSetNumChar(LCD_NUM5, '0', 0);
          GUI_LCDSetNumChar(LCD_NUM6, '0', 0);
          GUI_LCDSetNumChar(LCD_NUM7, '0', 0);
        }
      }
      else
      {
        GUI_LCDSetS(LCD_S48, 0);
        GUI_LCDSetS(LCD_S41, 0);
        GUI_LCDSetS(LCD_S42, 0);
        GUI_LCDSetNumChar(LCD_NUM4, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM5, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM6, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM7, LCD_DISPNULL, 0);
      }
      break;
    case WM_TIMER:
      gdialEditGroupIDSetUnderline();
      gdialEditPasswordSetUnderline();
      gdialEditGroupIDSetActive(GDIAL_EDIT_UNACTIVETE);
      gdialEditPasswordSetActive(GDIAL_EDIT_UNACTIVETE);
      WG_Paint(gdialEditGroupID);
      WG_Paint(gdialEditPassword);
      WG_SetFocus(gdialEditGroupID);
      break;
  }
  return;
}

Widget* gdialEditGroupIDCreate(void)
{
  WG_Create((Widget*)&_gdial_editgroupid, &gdialWin, gdialEditGroupIDCallback);
  gdialEditGroupID = (Widget*)&_gdial_editgroupid;
  return gdialEditGroupID;
}

void gdialEditGroupIDSetUnderline(void)
{
  GUI_SetCharUnderLine(_gdial_editgroupid.GroupID, GDIAL_GROUPID_LENGTH);
}

uint8_t gdialEditGroupIDGetUnderLineNum(void)
{
  uint8_t length = 0;
  GUI_CharToUint32(_gdial_editgroupid.GroupID, &length);
  return length;
}

void gdialEditGroupIDWriteToGroupID(void)
{
  gdialGroupIDSetValue(GUI_CharToUint32(_gdial_editgroupid.GroupID, 0));
  return;
}

void gdialEditGroupIDSetActive(uint8_t val)
{
  _gdial_editgroupid.Activate = val;
}

uint16_t gdialEditGroupIDGetVal(void)
{
  return GUI_CharToUint16(_gdial_editgroupid.GroupID, 0);
}