#include "UIMessage.h"
#include "dlg.h"
#include "mainWinDlg.h"
#include "GUI_LCD.h"

typedef struct WG_mainSignal{
  Widget Widget;
  uint8_t Mode;
  uint8_t Strength;
}WG_mainSignal;


WG_mainSignal _main_signal = {0};
Widget* mainSignal;

static void mainSignalCallback(WM_Message *pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_main_signal.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        if(_main_signal.Mode == MAIN_SIGNAL_IN)
        {
          GUI_LCDSetS(LCD_S12, 0);
          GUI_LCDSetS(LCD_S11, 1);
        }
        if(_main_signal.Mode == MAIN_SIGNAL_OUT)
        {
          GUI_LCDSetS(LCD_S12, 1);
          GUI_LCDSetS(LCD_S11, 0);
        }
        
        if(_main_signal.Strength >= 1)
          GUI_LCDSetS(LCD_S10, 1);
        else
          GUI_LCDSetS(LCD_S10, 0);
        
        if(_main_signal.Strength >= 2)
          GUI_LCDSetS(LCD_S9, 1);
        else
          GUI_LCDSetS(LCD_S9, 0);
        
        if(_main_signal.Strength >= 3)
          GUI_LCDSetS(LCD_S8, 1);
        else
          GUI_LCDSetS(LCD_S8, 0);
        
        if(_main_signal.Strength >= 4)
          GUI_LCDSetS(LCD_S7, 1);
        else
          GUI_LCDSetS(LCD_S7, 0);
        
        if(_main_signal.Strength >= 5)
          GUI_LCDSetS(LCD_S6, 1);
        else
          GUI_LCDSetS(LCD_S6, 0);
        
        if(_main_signal.Strength >= 6)
          GUI_LCDSetS(LCD_S5, 1);
        else
          GUI_LCDSetS(LCD_S5, 0);
        
        if(_main_signal.Strength >= 7)
          GUI_LCDSetS(LCD_S4, 1);
        else
          GUI_LCDSetS(LCD_S4, 0);
        
        if(_main_signal.Strength >= 8)
          GUI_LCDSetS(LCD_S3, 1);
        else
          GUI_LCDSetS(LCD_S3, 0);
        
        if(_main_signal.Strength >= 9)
          GUI_LCDSetS(LCD_S2, 1);
        else
          GUI_LCDSetS(LCD_S2, 0);
        
        if(_main_signal.Strength >= 10)
          GUI_LCDSetS(LCD_S1, 1);
        else
          GUI_LCDSetS(LCD_S1, 0);
      }
      else
      {
        GUI_LCDSetS(LCD_S1, 0);
        GUI_LCDSetS(LCD_S2, 0);
        GUI_LCDSetS(LCD_S3, 0);
        GUI_LCDSetS(LCD_S4, 0);
        GUI_LCDSetS(LCD_S5, 0);
        GUI_LCDSetS(LCD_S6, 0);
        GUI_LCDSetS(LCD_S7, 0);
        GUI_LCDSetS(LCD_S8, 0);
        GUI_LCDSetS(LCD_S9, 0);
        GUI_LCDSetS(LCD_S10, 0);
        GUI_LCDSetS(LCD_S11, 0);
        GUI_LCDSetS(LCD_S12, 0);
      }
      break;
  }
}

Widget* mainSignalCreate(void)
{
  WG_Create((Widget*)&_main_signal, &mainWin, mainSignalCallback);
  mainSignal = (Widget*)&_main_signal;
  return mainSignal;
}

void mainSignalSetVal(uint8_t Mode, uint8_t Strength)
{
  _main_signal.Mode = Mode;
  _main_signal.Strength = Strength;
}