#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "groupDialWinDlg.h"
#include "GUI.h"
#include "GUI_LCD.h"
#include "group_chat.h"
#include "GUI_Timer.h"

typedef struct WG_gdialEditChannel{
  Widget Widget;
  char Channel[4];
  uint32_t Max;
}WG_gdialEditChannel;


WG_gdialEditChannel _gdial_editchannel = {0};
Widget* gdialEditChannel;

static void gdialEditChannelCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  uint8_t Length;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
      gdialShipNumSetVal((uint32_t)pMsg->data_p);
      gdialDialNumSetVal(pMsg->data_v);
      WG_SetInVisible(gdialRecommmendCh);
      WG_Paint(gdialRecommmendCh);
    
      WG_SetInVisible(gdialGroupID);
      WG_Paint(gdialGroupID);
    
      WG_SetVisible(gdialDialNum);
      WG_Paint(gdialDialNum);
    
      WG_SetVisible(gdialShipNum);
      WG_Paint(gdialShipNum);
    
      WG_SetInVisible(gdialEditChannel);
      WG_SetVisible(gdialChannel);
      WG_Paint(gdialChannel);
    
      WG_SetFocus(gdialRoundButton);
      WG_Paint(gdialRoundButton);
      AudioPlayCnt(AUDIO_TYPE_TALKING_CALL,AUDIO_PLAY_TIMES_TALKING_CALL);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        GUI_RestartTimer(&gdialWin, GROUP_EDIT_TIMER, EDIT_TIMEROUT_LENGTH);
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_PICKUP:
            GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
            WG_SetFocus(gdialChannel);
            WG_Paint(gdialChannel);
            Grp_Enter();
            WM_SetFocus(&editDialNumWin);
            WM_BringToTop(&editDialNumWin);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_BACK:
            GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
            WG_SetFocus(gdialChannel);
            WG_Paint(gdialChannel);
            Grp_Enter();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_DEL:
          case GUI_KEY_STAR:
            GUI_CharToUint32(_gdial_editchannel.Channel, &Length);
            if(Length != GROUP_CHANMENUM_LENGTH)
            {
              GUI_SetCharUnderLine(_gdial_editchannel.Channel, GROUP_CHANMENUM_LENGTH);
              WG_Paint(gdialEditChannel);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            break;
          case GUI_KEY_0_CALLSHIP:
            if(!GUI_ChnIsAvailable480(_gdial_editchannel.Channel, 0))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            Length = GUI_AddAChar(_gdial_editchannel.Channel, '0', GROUP_CHANMENUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == GROUP_CHANMENUM_LENGTH)
            {
              GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
              gdialChannelSetValue(GUI_CharToUint16(_gdial_editchannel.Channel, 0));
              WG_SetFocus(gdialChannel);
              WG_Paint(gdialChannel);
              Grp_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(gdialEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_1_GROUP:
            if(!GUI_ChnIsAvailable480(_gdial_editchannel.Channel, 1))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            Length = GUI_AddAChar(_gdial_editchannel.Channel, '1', GROUP_CHANMENUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == GROUP_CHANMENUM_LENGTH)
            {
              GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
              gdialChannelSetValue(GUI_CharToUint16(_gdial_editchannel.Channel, 0));
              WG_SetFocus(gdialChannel);
              WG_Paint(gdialChannel);
              Grp_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(gdialEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);                 
            break;                                       
          case GUI_KEY_2_DIGIT:
            if(!GUI_ChnIsAvailable480(_gdial_editchannel.Channel, 2))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            Length = GUI_AddAChar(_gdial_editchannel.Channel, '2', GROUP_CHANMENUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == GROUP_CHANMENUM_LENGTH)
            {
              GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
              gdialChannelSetValue(GUI_CharToUint16(_gdial_editchannel.Channel, 0));
              WG_SetFocus(gdialChannel);
              WG_Paint(gdialChannel);
              Grp_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(gdialEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);                  
            break;                                      
          case GUI_KEY_3_SIMULATE: 
            if(!GUI_ChnIsAvailable480(_gdial_editchannel.Channel, 3))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            Length = GUI_AddAChar(_gdial_editchannel.Channel, '3', GROUP_CHANMENUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == GROUP_CHANMENUM_LENGTH)
            {
              GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
              gdialChannelSetValue(GUI_CharToUint16(_gdial_editchannel.Channel, 0));
              WG_SetFocus(gdialChannel);
              WG_Paint(gdialChannel);
              Grp_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(gdialEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);                  
            break;                                       
          case GUI_KEY_4_LOG:   
            if(!GUI_ChnIsAvailable480(_gdial_editchannel.Channel, 4))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            Length = GUI_AddAChar(_gdial_editchannel.Channel, '4', GROUP_CHANMENUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == GROUP_CHANMENUM_LENGTH)
            {
              GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
              gdialChannelSetValue(GUI_CharToUint16(_gdial_editchannel.Channel, 0));
              WG_SetFocus(gdialChannel);
              WG_Paint(gdialChannel);
              Grp_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(gdialEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);                  
            break;                                       
          case GUI_KEY_5_DIRECTION:
            if(!GUI_ChnIsAvailable480(_gdial_editchannel.Channel, 5))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            Length = GUI_AddAChar(_gdial_editchannel.Channel, '5', GROUP_CHANMENUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == GROUP_CHANMENUM_LENGTH)
            {
              GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
              gdialChannelSetValue(GUI_CharToUint16(_gdial_editchannel.Channel, 0));
              WG_SetFocus(gdialChannel);
              WG_Paint(gdialChannel);
              Grp_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(gdialEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);                  
            break;                                       
          case GUI_KEY_6_REMEMBER:     
            if(!GUI_ChnIsAvailable480(_gdial_editchannel.Channel, 6))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            Length = GUI_AddAChar(_gdial_editchannel.Channel, '6', GROUP_CHANMENUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == GROUP_CHANMENUM_LENGTH)
            {
              GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
              gdialChannelSetValue(GUI_CharToUint16(_gdial_editchannel.Channel, 0));
              WG_SetFocus(gdialChannel);
              WG_Paint(gdialChannel);
              Grp_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(gdialEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);                  
            break;                                       
          case GUI_KEY_7_SIGN:              
            if(!GUI_ChnIsAvailable480(_gdial_editchannel.Channel, 7))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            Length = GUI_AddAChar(_gdial_editchannel.Channel, '7', GROUP_CHANMENUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == GROUP_CHANMENUM_LENGTH)
            {
              GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
              gdialChannelSetValue(GUI_CharToUint16(_gdial_editchannel.Channel, 0));
              WG_SetFocus(gdialChannel);
              WG_Paint(gdialChannel);
              Grp_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(gdialEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);                 
            break;                                      
          case GUI_KEY_8_WEATHER:
            if(!GUI_ChnIsAvailable480(_gdial_editchannel.Channel, 8))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            Length = GUI_AddAChar(_gdial_editchannel.Channel, '8', GROUP_CHANMENUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == GROUP_CHANMENUM_LENGTH)
            {
              GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
              gdialChannelSetValue(GUI_CharToUint16(_gdial_editchannel.Channel, 0));
              WG_SetFocus(gdialChannel);
              WG_Paint(gdialChannel);
              Grp_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(gdialEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);                 
            break;                                      
          case GUI_KEY_9_SCAN:
            if(!GUI_ChnIsAvailable480(_gdial_editchannel.Channel, 9))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            Length = GUI_AddAChar(_gdial_editchannel.Channel, '9', GROUP_CHANMENUM_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == GROUP_CHANMENUM_LENGTH)
            {
              GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
              gdialChannelSetValue(GUI_CharToUint16(_gdial_editchannel.Channel, 0));
              WG_SetFocus(gdialChannel);
              WG_Paint(gdialChannel);
              Grp_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(gdialEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL); 
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      GUI_SetCharUnderLine(_gdial_editchannel.Channel, GROUP_CHANMENUM_LENGTH);
      _gdial_editchannel.Max = 480;
      break;
    case WM_FOCUS:
      if(pMsg->data_v == 1)
      {
        GUI_SetCharUnderLine(_gdial_editchannel.Channel, GROUP_CHANMENUM_LENGTH);
        WG_Paint(gdialEditChannel);
      }
      break;
    case WM_PAINT:
      if(_gdial_editchannel.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        uint8_t flash = 1;
        GUI_LCDSetNumChar(LCD_NUM14, 'G', 0);
        GUI_LCDSetNumChar(LCD_NUM15, 'C', 0);
        GUI_LCDSetNumChar(LCD_NUM16, 'H', 0);
        GUI_LCDSetNumChar(LCD_NUM17, '-', 0);
        if(flash && _gdial_editchannel.Channel[0] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM18, _gdial_editchannel.Channel[0], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM18, _gdial_editchannel.Channel[0], 0);
        }
        
        if(flash && _gdial_editchannel.Channel[1] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM19, _gdial_editchannel.Channel[1], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM19, _gdial_editchannel.Channel[1], 0);
        }
        
        if(flash && _gdial_editchannel.Channel[2] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM20, _gdial_editchannel.Channel[2], 1);
          flash = 0;
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM20, _gdial_editchannel.Channel[2], 0);
        }
      }
      else
      {
        GUI_LCDSetNumChar(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM20, LCD_DISPNULL, 0);
      }
      break;
    case WM_TIMER:
      if(pMsg->data_v == GROUP_EDIT_TIMER)
      {
        WG_SetFocus(gdialChannel);
        WG_Paint(gdialChannel);
        Grp_Enter();
      }
      break;
  }
  return;
}

Widget* gdialEditChannelCreate(void)
{
  WG_Create((Widget*)&_gdial_editchannel, &gdialWin, gdialEditChannelCallback);
  gdialEditChannel = (Widget*)&_gdial_editchannel;
  return gdialEditChannel;
}

