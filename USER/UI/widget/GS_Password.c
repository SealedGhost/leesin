#include "UIMessage.h"
#include "dlg.h"
#include "groupSetWinDlg.h"
#include "GUI.h"
#include "lcdControl.h"
#include "GUI_LCD.h"
#include "system_config.h"
#include "GUI_Timer.h"

typedef struct WG_GsPassword{
  Widget Widget;
  char Password[6];
  uint8_t Activate;
}WG_GsPassword;

WG_GsPassword _gs_password = {0};
Widget *gsPassword;

static void gsPasswordCallback(WM_Message *pMsg)
{
  uint8_t Length;
  switch(pMsg->msgType)
  {
    case WM_KEY:
      if(_gs_password.Activate == GS_EDIT_ACTIVATE)
      {
        if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
        {
          GUI_RestartTimer(&groupSetWin, GS_EDIT_TIMER, EDIT_TIMEROUT_LENGTH);
          switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
          {
            case GUI_KEY_UP:
            case GUI_KEY_DOWN:
              if(gsPasswordGetUnderLineNum())
                GUI_SetCharUnderLine(_gs_password.Password, GS_PASSWORD_LENGTH);
              WG_Paint(gsPassword);
              WG_SetFocus(gsGroupNum);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_STAR:
              gsPasswordSetUnderLine();
              gsGroupNumSetUnderLine();
              WG_SetFocus(gsPassword);
              WG_Paint(gsGroupNum);
              WG_Paint(gsPassword);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_DEL:
              GUI_CharToUint32(_gs_password.Password, &Length);
              if(Length != GS_PASSWORD_LENGTH)
              {
                GUI_SetCharUnderLine(_gs_password.Password, GS_PASSWORD_LENGTH);
                WG_Paint(gsPassword);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              break;
            case GUI_KEY_BACK:
            {
              SysCfg sysCfg;
              GUI_DestroyTimer(&groupSetWin, GS_EDIT_TIMER);
              SysCfg_GetConfig(&sysCfg);
              gsGroupNumSetVal(sysCfg.groupNumber);
              WG_Paint(gsGroupNum);
              gsPasswordSetUnderLine();
              gsPasswordSetActivate(GS_EDIT_UNACTIVATE);
              WG_Paint(gsPassword);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
              break;
            case GUI_KEY_ENTER:
              if(!gsGroupNumGetUnderLineNum() && !gsPasswordGetUnderLineNum() && gsGroupNumGetVal() && gsPasswordGetVal())
              {
                SysCfg sysCfg;
                GUI_DestroyTimer(&groupSetWin, GS_EDIT_TIMER);
                SysCfg_GetConfig(&sysCfg);
                sysCfg.groupNumber = gsGroupNumGetVal();
                sysCfg.groupPwd = gsPasswordGetVal();
                SysCfg_SetConfig(&sysCfg);
                SysCfg_Store();
                gsPasswordSetActivate(GS_EDIT_UNACTIVATE);
                WG_Paint(gsPassword);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            case GUI_KEY_0_CALLSHIP:
              Length = GUI_AddAChar(_gs_password.Password, '0', GS_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_PASSWORD_LENGTH)
              {
                if(gsGroupNumGetUnderLineNum())
                {
                  WG_SetFocus(gsGroupNum);
                  WG_Paint(gsPassword);
                  WG_Paint(gsGroupNum);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsPassword);
              break;
            case GUI_KEY_1_GROUP:
              Length = GUI_AddAChar(_gs_password.Password, '1', GS_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_PASSWORD_LENGTH)
              {
                if(gsGroupNumGetUnderLineNum())
                {
                  WG_SetFocus(gsGroupNum);
                  WG_Paint(gsPassword);
                  WG_Paint(gsGroupNum);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsPassword);
              break;
            case GUI_KEY_2_DIGIT:
              Length = GUI_AddAChar(_gs_password.Password, '2', GS_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_PASSWORD_LENGTH)
              {
                if(gsGroupNumGetUnderLineNum())
                {
                  WG_SetFocus(gsGroupNum);
                  WG_Paint(gsPassword);
                  WG_Paint(gsGroupNum);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsPassword);
              break;
            case GUI_KEY_3_SIMULATE:
              Length = GUI_AddAChar(_gs_password.Password, '3', GS_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_PASSWORD_LENGTH)
              {
                if(gsGroupNumGetUnderLineNum())
                {
                  WG_SetFocus(gsGroupNum);
                  WG_Paint(gsPassword);
                  WG_Paint(gsGroupNum);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsPassword);
              break;
            case GUI_KEY_4_LOG:
              Length = GUI_AddAChar(_gs_password.Password, '4', GS_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_PASSWORD_LENGTH)
              {
                if(gsGroupNumGetUnderLineNum())
                {
                  WG_SetFocus(gsGroupNum);
                  WG_Paint(gsPassword);
                  WG_Paint(gsGroupNum);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsPassword);
              break;
            case GUI_KEY_5_DIRECTION:
              Length = GUI_AddAChar(_gs_password.Password, '5', GS_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_PASSWORD_LENGTH)
              {
                if(gsGroupNumGetUnderLineNum())
                {
                  WG_SetFocus(gsGroupNum);
                  WG_Paint(gsPassword);
                  WG_Paint(gsGroupNum);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsPassword);
              break;
            case GUI_KEY_6_REMEMBER:       
              Length = GUI_AddAChar(_gs_password.Password, '6', GS_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_PASSWORD_LENGTH)
              {
                if(gsGroupNumGetUnderLineNum())
                {
                  WG_SetFocus(gsGroupNum);
                  WG_Paint(gsPassword);
                  WG_Paint(gsGroupNum);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsPassword);
              break;
            case GUI_KEY_7_SIGN: 
              Length = GUI_AddAChar(_gs_password.Password, '7', GS_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_PASSWORD_LENGTH)
              {
                if(gsGroupNumGetUnderLineNum())
                {
                  WG_SetFocus(gsGroupNum);
                  WG_Paint(gsPassword);
                  WG_Paint(gsGroupNum);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsPassword);
              break;
            case GUI_KEY_8_WEATHER:        
              Length = GUI_AddAChar(_gs_password.Password, '8', GS_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_PASSWORD_LENGTH)
              {
                if(gsGroupNumGetUnderLineNum())
                {
                  WG_SetFocus(gsGroupNum);
                  WG_Paint(gsPassword);
                  WG_Paint(gsGroupNum);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsPassword);
              break;
            case GUI_KEY_9_SCAN:         
              Length = GUI_AddAChar(_gs_password.Password, '9', GS_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GS_PASSWORD_LENGTH)
              {
                if(gsGroupNumGetUnderLineNum())
                {
                  WG_SetFocus(gsGroupNum);
                  WG_Paint(gsPassword);
                  WG_Paint(gsGroupNum);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gsPassword);
              break;
            default:
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
          }
        }
      }
      else
      {
        if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
        {
          switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
          {
            case GUI_KEY_STAR:
              gsPasswordSetUnderLine();
              GUI_CreateTimer(&groupSetWin, GS_EDIT_TIMER, EDIT_TIMEROUT_LENGTH);
              _gs_password.Activate = GS_EDIT_ACTIVATE;
              WG_Paint(gsPassword);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            default:
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
          }
        }
      }
      break;
    case WM_CREATE:
    {
      GUI_SetCharUnderLine(_gs_password.Password, GS_PASSWORD_LENGTH);
      _gs_password.Activate = GS_EDIT_UNACTIVATE;
    }
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        GUI_SetCharUnderLine(_gs_password.Password, GS_PASSWORD_LENGTH);
        WG_Paint(gsPassword);
      }
      break;
    case WM_PAINT:
      if(_gs_password.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        uint8_t flash = 1;
        GUI_LCDSetS(LCD_S29, 1);
        GUI_LCDSetS(LCD_S26, 1);
        GUI_LCDSetS(LCD_S28, 1);
        if(_gs_password.Activate == GS_EDIT_ACTIVATE)
        {     
          if(flash && _gs_password.Password[0] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM15, _gs_password.Password[0], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM15, _gs_password.Password[0], 0);
          }
          
          if(flash && _gs_password.Password[1] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM16, _gs_password.Password[1], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM16, _gs_password.Password[1], 0);
          }
          
          if(flash && _gs_password.Password[2] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM17, _gs_password.Password[2], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM17, _gs_password.Password[2], 0);
          }
          
          if(flash && _gs_password.Password[3] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM18, _gs_password.Password[3], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM18, _gs_password.Password[3], 0);
          }
          
          if(flash)
          {
            GUI_LCDSetNumChar(LCD_NUM19, _gs_password.Password[4], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM19, _gs_password.Password[4], 0);
          }
        }
        else
        {
          if(gsPasswordGetVal())
          {
              GUI_LCDSetS(LCD_S29, 1);
              GUI_LCDSetS(LCD_S26, 1);
              GUI_LCDSetS(LCD_S28, 1);
              GUI_LCDSetNumChar(LCD_NUM15, _gs_password.Password[0], 0);
              GUI_LCDSetNumChar(LCD_NUM16, _gs_password.Password[1], 0);
              GUI_LCDSetNumChar(LCD_NUM17, _gs_password.Password[2], 0);
              GUI_LCDSetNumChar(LCD_NUM18, _gs_password.Password[3], 0);
              GUI_LCDSetNumChar(LCD_NUM19, _gs_password.Password[4], 0);
          }
          else
          {
              GUI_LCDSetS(LCD_S29, 1);
              GUI_LCDSetS(LCD_S26, 1);
              GUI_LCDSetS(LCD_S28, 1);
              GUI_LCDSetNumChar(LCD_NUM15, '-', 0);
              GUI_LCDSetNumChar(LCD_NUM16, '-', 0);
              GUI_LCDSetNumChar(LCD_NUM17, '-', 0);
              GUI_LCDSetNumChar(LCD_NUM18, '-', 0);
              GUI_LCDSetNumChar(LCD_NUM19, '-', 0);
          }
          
        }
      }
      else
      {
        GUI_LCDSetS(LCD_S29, 0);
        GUI_LCDSetS(LCD_S26, 0);
        GUI_LCDSetS(LCD_S28, 0);
        GUI_LCDSetNumChar(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM19, LCD_DISPNULL, 0);
      }
      break;
    case WM_TIMER:
      if(pMsg->data_v == GS_EDIT_TIMER)
      {
        SysCfg sysCfg;
        SysCfg_GetConfig(&sysCfg);
        gsGroupNumSetVal(sysCfg.groupNumber);
        WG_Paint(gsGroupNum);
        gsPasswordSetUnderLine();
        gsPasswordSetActivate(GS_EDIT_UNACTIVATE);
        WG_Paint(gsPassword);
      }
      break;
  }
}

Widget* gsPasswordCreate(void)
{
  WG_Create((Widget*)&_gs_password, &groupSetWin, gsPasswordCallback);
  gsPassword = (Widget*)&_gs_password;
  return gsPassword;
}

uint8_t gsPasswordGetUnderLineNum(void)
{
  uint8_t length = 0;
  GUI_CharToUint32(_gs_password.Password, &length);
  return length;
}

void gsPasswordSetUnderLine(void)
{
  GUI_SetCharUnderLine(_gs_password.Password, GS_PASSWORD_LENGTH);
}

uint32_t gsPasswordGetVal(void)
{
  return GUI_CharToUint32(_gs_password.Password, 0);
}

void gsPasswordSetActivate(uint8_t ActivateState)
{
  _gs_password.Activate = ActivateState;
}
