#include "dialWinDlg.h"
#include "UIMessage.h"
#include "dlg.h"
#include "GUI.h"
#include "lcdControl.h"
#include "GUI_LCD.h"
#include "call_mgr.h"

typedef struct WG_DialFialed{
  Widget Widget;
  uint8_t val;
}WG_Dialing;

WG_Dialing _dialing = {0};
Widget *dialDialing;

static void dialingCallBack(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case USER_MSG_TELL_BUSY:
      WG_SetFocus(dialDialOver);
      WG_SetInVisible(dialDialing);
      WG_Paint(dialDialOver);
      WG_Paint(dialDialing);
      AudioPlayCnt(AUDIO_TYPE_NO_ANSWER,AUDIO_PLAY_TIMES_NO_ANSWER);
      break;
    case USER_MSG_CALL_FAIL:
      WG_SetFocus(dialDialFialed);
      WG_SetInVisible(dialDialing);
      WG_Paint(dialDialFialed);
      WG_Paint(dialDialing);
      AudioPlayCnt(AUDIO_TYPE_CALL_FAIL,AUDIO_PLAY_TIMES_CALL_FAIL);
      break;
    case USER_MSG_CONNECT:
      WG_SetFocus(dialDialSucceeded);
      WG_SetInVisible(dialDialing);
      WG_Paint(dialDialing);
      WG_Paint(dialDialSucceeded);
      AudioStopPlay();
      AudioPlayCnt(AUDIO_TYPE_PICK_UP,AUDIO_PLAY_TIMES_PICK_UP);
      break;
    case USER_MSG_RING:
      dialShipNumSetMode(DIAL_DISP_SHIPNUM);
      dialShipNumSetVal(DIAL_DISP_SHIPNUM, (uint32_t)pMsg->data_p);
      WG_SetVisible(dialShipNum);
      WG_Paint(dialShipNum);
      AudioPlayCnt(AUDIO_TYPE_CALL,AUDIO_PLAY_TIMES_CALL);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_HANGUP:
          case GUI_KEY_BACK:
            CallMgr_HangUp(dialDialNumGetVal());
            WG_SetInVisible(dialShipNum);
            WG_Paint(dialShipNum);
            WG_SetInVisible(dialDialing);
            WG_Paint(dialDialing);
            WM_SetFocus(pDialWindowSource);
            WM_BringToTop(pDialWindowSource);
            AudioStopPlay();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_dialing.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S14, 1);
      }
      else
      {
         GUI_LCDSetS(LCD_S14, 0);
      }
      break;
  }
}

Widget* dialDialingCreate(void)
{
  WG_Create((Widget*)&_dialing, &dialWin, dialingCallBack);
  dialDialing = (Widget*)&_dialing;
  return dialDialing;
}