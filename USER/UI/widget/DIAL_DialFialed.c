#include "dialWinDlg.h"
#include "UIMessage.h"
#include "dlg.h"
#include "lcdControl.h"
#include "GUI.h"
#include "GUI_Timer.h"
#include "GUI_LCD.h"
#include "call_mgr.h"


typedef struct WG_DialFialed{
  Widget Widget;
  uint8_t val;
}WG_DialFialed;

WG_DialFialed _dialFialed = {0};
Widget *dialDialFialed;

static void dialFialedCallBack(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      Msg.pTarget = &beCalledWin;
      Msg.pSource = pDialWindowSource;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      { 
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_BACK:
            WM_SetFocus(pDialWindowSource);
            WG_Paint(dialDialFialed);
            WM_BringToTop(pDialWindowSource);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_PICKUP:
            CallMgr_Call(dialDialNumGetVal());
            WG_SetFocus(dialDialing);
            WG_SetInVisible(dialDialFialed);
            WG_Paint(dialDialing);
            WG_Paint(dialDialFialed);
            AudioStopPlay();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_dialFialed.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S13, 1);
      }
      else
      {
        GUI_LCDSetS(LCD_S13, 0);
      }
      break;
  }
}

Widget* dialDialFialedCreate(void)
{
  WG_Create((Widget*)&_dialFialed, &dialWin, dialFialedCallBack);
  dialDialFialed = (Widget*)&_dialFialed;
  return dialDialFialed;
}