#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "groupDialWinDlg.h"
#include "GUI_LCD.h"
#include "GUI.h"
#include "GUI_Timer.h"
#include "group_chat.h"

typedef struct WG_GDialDialNum{
  Widget Widget;
  uint32_t DialNum;
}WG_GDialDialNum;

WG_GDialDialNum _gdial_dialnum = {0};
Widget* gdialDialNum;

static void gdialDialNumCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      gdialShipNumSetVal((uint32_t)pMsg->data_p);
      gdialDialNumSetVal(pMsg->data_v);
      WG_Paint(gdialDialNum);
      WG_Paint(gdialShipNum);
      WG_SetFocus(gdialRoundButton);
      WG_Paint(gdialRoundButton);
      AudioPlayCnt(AUDIO_TYPE_TALKING_CALL,AUDIO_PLAY_TIMES_TALKING_CALL);
      break;
    case USER_MSG_GRP_MUTE:
      WG_SetInVisible(gdialDialNum);
      WG_Paint(gdialDialNum);
      WG_SetInVisible(gdialShipNum);
      WG_Paint(gdialShipNum);
      WG_SetVisible(gdialGroupID);
      WG_Paint(gdialGroupID);
      WG_SetFocus(gdialChannel);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_STAR:
            GUI_CreateTimer(&gdialWin, GROUP_EDIT_TIMER, EDIT_TIMEROUT_LENGTH);
            WG_SetInVisible(gdialDialNum);
            WG_Paint(gdialDialNum);
            WG_SetInVisible(gdialShipNum);
            WG_Paint(gdialShipNum);
            WG_SetInVisible(gdialChannel);
            WG_Paint(gdialChannel);
            WG_SetFocus(gdialEditChannel);
            WG_Paint(gdialEditChannel);
            WG_SetVisible(gdialGroupID);
            WG_Paint(gdialGroupID);
            Grp_Exit();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_1_GROUP:
          case GUI_KEY_7_SIGN:
          case GUI_KEY_9_SCAN:
          case GUI_KEY_ENTER:
          case GUI_KEY_BACK:
          case GUI_KEY_PICKUP:
          case GUI_KEY_HANGUP:
          case GUI_KEY_BKLINGHT:
          case GUI_KEY_DEL:
          case GUI_KEY_PTTDOWN:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          default:
            WG_SetInVisible(gdialDialNum);
            WG_SetInVisible(gdialShipNum);
            WG_SetVisible(gdialGroupID);
            WG_Paint(gdialDialNum);
            WG_Paint(gdialShipNum);
            WG_Paint(gdialGroupID);
            WG_SetFocus(gdialChannel);
            WM_SendMessage(pMsg);
            break;
        }
      }
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_gdial_dialnum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S47, 1);
        GUI_LCDSetS(LCD_S45, 1);
        GUI_LCDSetS(LCD_S43, 1);
        GUI_LCDSetS(LCD_S41, 1);
        GUI_LCDSetNumInt(LCD_NUM1, _gdial_dialnum.DialNum / 1000000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM2, _gdial_dialnum.DialNum / 100000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM3, _gdial_dialnum.DialNum / 10000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM4, _gdial_dialnum.DialNum / 1000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM5, _gdial_dialnum.DialNum / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM6, _gdial_dialnum.DialNum / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM7, _gdial_dialnum.DialNum % 10, 0);
      }
      else
      {
        GUI_LCDSetS(LCD_S47, 0);
        GUI_LCDSetS(LCD_S45, 0);
        GUI_LCDSetS(LCD_S43, 0);
        GUI_LCDSetS(LCD_S41, 0);
        GUI_LCDSetNumInt(LCD_NUM1, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM2, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM3, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM4, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM5, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM6, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM7, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* gdialDialNumCreate(void)
{
  WG_Create((Widget*)&_gdial_dialnum, &gdialWin, gdialDialNumCallback);
  gdialDialNum = (Widget*)&_gdial_dialnum;
  return gdialDialNum;
}

void gdialDialNumSetVal(uint32_t DialNum)
{
  _gdial_dialnum.DialNum = DialNum;
}

uint32_t gdialDialNumGetVal(void)
{
  return _gdial_dialnum.DialNum;
}