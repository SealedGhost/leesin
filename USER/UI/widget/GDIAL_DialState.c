#include "UIMessage.h"
#include "dlg.h"
#include "groupDialWinDlg.h"
#include "GUI.h"
#include "GUI_LCD.h"
#include "call_log.h"

typedef struct WG_GDialDialState{
  Widget Widget;
  uint8_t IsMiss;
  uint8_t Dir;
}WG_GDialDialState;

WG_GDialDialState _gdial_dialstate = {0};
Widget *gdialDialState;

static void gdialDialStateCallback(WM_Message *pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_gdial_dialstate.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        if(_gdial_dialstate.IsMiss == CALL_MISS)
          GUI_LCDSetS(LCD_S33, 1);
        else
          GUI_LCDSetS(LCD_S33, 0);
        if(_gdial_dialstate.Dir == CALL_DIR_IN)
        {
          GUI_LCDSetS(LCD_S34, 1);
          GUI_LCDSetS(LCD_S35, 0);
        }
        else if(_gdial_dialstate.Dir == CALL_DIR_OUT)
        {
          GUI_LCDSetS(LCD_S35, 1);
          GUI_LCDSetS(LCD_S34, 0);
        }
      }
      else
      {
        GUI_LCDSetS(LCD_S33, 0);
        GUI_LCDSetS(LCD_S34, 0);
        GUI_LCDSetS(LCD_S35, 0);
      }
      break;
    
  }
  return;



}

Widget* gdialDialStateCreate(void)
{
  WG_Create((Widget*)&_gdial_dialstate, &dialWin, gdialDialStateCallback);
  gdialDialState = (Widget*)&_gdial_dialstate;
  return gdialDialState;
}

void gdialDialStateSet(uint8_t IsMiss, uint8_t Dir)
{
  _gdial_dialstate.Dir = Dir;
  _gdial_dialstate.IsMiss = IsMiss;
}