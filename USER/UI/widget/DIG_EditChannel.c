#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "digitWinDlg.h"
#include "GUI.h"
#include "GUI_LCD.h"
#include "digital_channel.h"
#include "GUI_Timer.h"


typedef struct WG_DigEditChannel{
  Widget Widget;
  char Channel[4];
  uint32_t Max;
}WG_DigEditChannel;

WG_DigEditChannel _dig_editchannel = {0};
Widget* digEditChannel;

static void digEditChannelCallback(WM_Message *pMsg)
{
  WM_Message Msg;
  uint8_t Length;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      GUI_DestroyTimer(&digitWin, DIG_EDIT_TIMER);
      WG_SetInVisible(digEditChannel);
      WG_Paint(digEditChannel);
      WG_SetFocus(digChannel);
      WG_Paint(digChannel);
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &digitWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        GUI_RestartTimer(&digitWin, DIG_EDIT_TIMER, EDIT_TIMEROUT_LENGTH);
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_BACK:
            GUI_DestroyTimer(&digitWin, DIG_EDIT_TIMER);
            WG_SetInVisible(digEditChannel);
            WG_Paint(digEditChannel);
            WG_SetFocus(digChannel);
            WG_Paint(digChannel);
            DCH_Enter();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_DEL:
          case GUI_KEY_STAR:
            GUI_CharToUint32(_dig_editchannel.Channel, &Length);
            if(Length != DIG_CHANNEL_LENGTH)
            {
              GUI_SetCharUnderLine(_dig_editchannel.Channel, DIG_CHANNEL_LENGTH);
              WG_Paint(digEditChannel);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            break;
          case GUI_KEY_0_CALLSHIP:
            if(!GUI_ChnIsAvailable480(_dig_editchannel.Channel, 0))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;          
            }
            Length = GUI_AddAChar(_dig_editchannel.Channel, '0', DIG_CHANNEL_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == DIG_CHANNEL_LENGTH) 
            {
              uint16_t Channel = GUI_CharToUint16(_dig_editchannel.Channel, 0);
              GUI_DestroyTimer(&digitWin, DIG_EDIT_TIMER);
              digChannelSetVal(Channel);
              WG_SetInVisible(digEditChannel);
              WG_Paint(digEditChannel);
              WG_SetFocus(digChannel);
              WG_Paint(digChannel);
              DCH_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(digEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_1_GROUP:
            if(!GUI_ChnIsAvailable480(_dig_editchannel.Channel, 1))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;          
            }
            Length = GUI_AddAChar(_dig_editchannel.Channel, '1', DIG_CHANNEL_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == DIG_CHANNEL_LENGTH) 
            {
              uint16_t Channel = GUI_CharToUint16(_dig_editchannel.Channel, 0);
              GUI_DestroyTimer(&digitWin, DIG_EDIT_TIMER);
              digChannelSetVal(Channel);
              WG_SetInVisible(digEditChannel);
              WG_Paint(digEditChannel);
              WG_SetFocus(digChannel);
              WG_Paint(digChannel);
              DCH_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(digEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_2_DIGIT:
            if(!GUI_ChnIsAvailable480(_dig_editchannel.Channel, 2))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;          
            }
            Length = GUI_AddAChar(_dig_editchannel.Channel, '2', DIG_CHANNEL_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == DIG_CHANNEL_LENGTH) 
            {
              uint16_t Channel = GUI_CharToUint16(_dig_editchannel.Channel, 0);
              GUI_DestroyTimer(&digitWin, DIG_EDIT_TIMER);
              digChannelSetVal(Channel);
              WG_SetInVisible(digEditChannel);
              WG_Paint(digEditChannel);
              WG_SetFocus(digChannel);
              WG_Paint(digChannel);
              DCH_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(digEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_3_SIMULATE:
            if(!GUI_ChnIsAvailable480(_dig_editchannel.Channel, 3))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;          
            }
            Length = GUI_AddAChar(_dig_editchannel.Channel, '3', DIG_CHANNEL_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == DIG_CHANNEL_LENGTH) 
            {
              uint16_t Channel = GUI_CharToUint16(_dig_editchannel.Channel, 0);
              GUI_DestroyTimer(&digitWin, DIG_EDIT_TIMER);
              digChannelSetVal(Channel);
              WG_SetInVisible(digEditChannel);
              WG_Paint(digEditChannel);
              WG_SetFocus(digChannel);
              WG_Paint(digChannel);
              DCH_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(digEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_4_LOG:
            if(!GUI_ChnIsAvailable480(_dig_editchannel.Channel, 4))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;          
            }
            Length = GUI_AddAChar(_dig_editchannel.Channel, '4', DIG_CHANNEL_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == DIG_CHANNEL_LENGTH) 
            {
              uint16_t Channel = GUI_CharToUint16(_dig_editchannel.Channel, 0);
              GUI_DestroyTimer(&digitWin, DIG_EDIT_TIMER);
              digChannelSetVal(Channel);
              WG_SetInVisible(digEditChannel);
              WG_Paint(digEditChannel);
              WG_SetFocus(digChannel);
              WG_Paint(digChannel);
              DCH_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(digEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_5_DIRECTION:
            if(!GUI_ChnIsAvailable480(_dig_editchannel.Channel, 5))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;          
            }
            Length = GUI_AddAChar(_dig_editchannel.Channel, '5', DIG_CHANNEL_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == DIG_CHANNEL_LENGTH) 
            {
              uint16_t Channel = GUI_CharToUint16(_dig_editchannel.Channel, 0);
              GUI_DestroyTimer(&digitWin, DIG_EDIT_TIMER);
              digChannelSetVal(Channel);
              WG_SetInVisible(digEditChannel);
              WG_Paint(digEditChannel);
              WG_SetFocus(digChannel);
              WG_Paint(digChannel);
              DCH_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(digEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_6_REMEMBER:   
            if(!GUI_ChnIsAvailable480(_dig_editchannel.Channel, 6))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;          
            }         
            Length = GUI_AddAChar(_dig_editchannel.Channel, '6', DIG_CHANNEL_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == DIG_CHANNEL_LENGTH) 
            {
              uint16_t Channel = GUI_CharToUint16(_dig_editchannel.Channel, 0);
              GUI_DestroyTimer(&digitWin, DIG_EDIT_TIMER);
              digChannelSetVal(Channel);
              WG_SetInVisible(digEditChannel);
              WG_Paint(digEditChannel);
              WG_SetFocus(digChannel);
              WG_Paint(digChannel);
              DCH_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(digEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_7_SIGN: 
            if(!GUI_ChnIsAvailable480(_dig_editchannel.Channel, 7))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;          
            }
            Length = GUI_AddAChar(_dig_editchannel.Channel, '7', DIG_CHANNEL_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == DIG_CHANNEL_LENGTH) 
            {
              uint16_t Channel = GUI_CharToUint16(_dig_editchannel.Channel, 0);
              GUI_DestroyTimer(&digitWin, DIG_EDIT_TIMER);
              digChannelSetVal(Channel);
              WG_SetInVisible(digEditChannel);
              WG_Paint(digEditChannel);
              WG_SetFocus(digChannel);
              WG_Paint(digChannel);
              DCH_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(digEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_8_WEATHER:
            if(!GUI_ChnIsAvailable480(_dig_editchannel.Channel, 8))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;          
            }    
            Length = GUI_AddAChar(_dig_editchannel.Channel, '8', DIG_CHANNEL_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == DIG_CHANNEL_LENGTH) 
            {
              uint16_t Channel = GUI_CharToUint16(_dig_editchannel.Channel, 0);
              GUI_DestroyTimer(&digitWin, DIG_EDIT_TIMER);
              digChannelSetVal(Channel);
              WG_SetInVisible(digEditChannel);
              WG_Paint(digEditChannel);
              WG_SetFocus(digChannel);
              WG_Paint(digChannel);
              DCH_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(digEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_9_SCAN: 
            if(!GUI_ChnIsAvailable480(_dig_editchannel.Channel, 9))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;          
            }
            Length = GUI_AddAChar(_dig_editchannel.Channel, '9', DIG_CHANNEL_LENGTH);
            if(!Length)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(Length == DIG_CHANNEL_LENGTH) 
            {
              uint16_t Channel = GUI_CharToUint16(_dig_editchannel.Channel, 0);
              GUI_DestroyTimer(&digitWin, DIG_EDIT_TIMER);
              digChannelSetVal(Channel);
              WG_SetInVisible(digEditChannel);
              WG_Paint(digEditChannel);
              WG_SetFocus(digChannel);
              WG_Paint(digChannel);
              DCH_Enter();
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(digEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      _dig_editchannel.Max = 480;
      GUI_SetCharUnderLine(_dig_editchannel.Channel, DIG_CHANNEL_LENGTH);
      break;
    case WM_FOCUS:
      GUI_SetCharUnderLine(_dig_editchannel.Channel, DIG_CHANNEL_LENGTH);
      break;
    case WM_PAINT:
      if(_dig_editchannel.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        uint8_t flashFlg = 1;
        GUI_LCDSetNumInt(LCD_NUM14, 'D', 0);
        GUI_LCDSetNumInt(LCD_NUM15, 'C', 0);
        GUI_LCDSetNumInt(LCD_NUM16, 'H', 0);
        GUI_LCDSetNumInt(LCD_NUM17, '-', 0);
        if(flashFlg && _dig_editchannel.Channel[0] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM18, _dig_editchannel.Channel[0], 1);
          flashFlg = 0;
        }
        else
          GUI_LCDSetNumChar(LCD_NUM18, _dig_editchannel.Channel[0], 0);
        
        if(flashFlg && _dig_editchannel.Channel[1] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM19, _dig_editchannel.Channel[1], 1);
          flashFlg = 0;
        }
        else
          GUI_LCDSetNumChar(LCD_NUM19, _dig_editchannel.Channel[1], 0);
        
        if(flashFlg)
        {
          GUI_LCDSetNumChar(LCD_NUM20, _dig_editchannel.Channel[2], 1);
          flashFlg = 0;
        }
        else
          GUI_LCDSetNumChar(LCD_NUM20, _dig_editchannel.Channel[2], 0);
      }
      else
      {
        GUI_LCDDisableNumFlash();
        GUI_LCDSetNumInt(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM20, LCD_DISPNULL, 0);
      }
      break;
    case WM_TIMER:
      if(pMsg->data_v == DIG_EDIT_TIMER)
      {
        WG_SetInVisible(digEditChannel);
        WG_Paint(digEditChannel);
        WG_SetFocus(digChannel);
        WG_Paint(digChannel);
        DCH_Enter();
      }
      break;
  }
}

Widget* digEditChannelCreate(void)
{
  WG_Create((Widget*)&_dig_editchannel, &digitWin, digEditChannelCallback);
  digEditChannel = (Widget*)&_dig_editchannel;
  return digEditChannel;
}