#include "UIMessage.h"
#include "dlg.h"
#include "callShipNumWinDlg.h"
#include "GUI.h"
#include "lcdControl.H"
#include "GUI_LCD.h"
#include "boat_search.h"
#include "GUI_Timer.h"

typedef struct CSN_SmallShipNum{
  Widget Widget;
  uint32_t ShipNum;
}CSN_SmallShipNum;


CSN_SmallShipNum _csn_smallshipnum = {0};
Widget *csnSmallShipNum;


static void csnSmallShipNumCallback(WM_Message *pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      break;
    case WM_CREATE:
      break;
    case WM_PAINT:
      if(_csn_smallshipnum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S51, 1);
        GUI_LCDSetS(LCD_S40, 1);
        GUI_LCDSetS(LCD_S38, 1);
        GUI_LCDSetS(LCD_S36, 1);
        GUI_LCDSetNumInt(LCD_NUM9, _csn_smallshipnum.ShipNum / 10000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM10, _csn_smallshipnum.ShipNum / 1000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM11, _csn_smallshipnum.ShipNum / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM12, _csn_smallshipnum.ShipNum / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM13, _csn_smallshipnum.ShipNum % 10, 0);
      }
      else
      {
        GUI_LCDSetS(LCD_S51, 0);
        GUI_LCDSetS(LCD_S40, 0);
        GUI_LCDSetS(LCD_S38, 0);
        GUI_LCDSetS(LCD_S36, 0);
        GUI_LCDSetNumInt(LCD_NUM9,  LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM10, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM11, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM12, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM13, LCD_DISPNULL, 0);
      }
      break;
  }
}


Widget* csnSmallShipNumCreate(void)
{
  WG_Create((Widget*)&_csn_smallshipnum, &callShipNumWin, csnSmallShipNumCallback);
  csnSmallShipNum = (Widget*)&_csn_smallshipnum;
  return csnSmallShipNum;
}

void csnSmallShipNumSetVal(uint32_t ShipNum)
{
  _csn_smallshipnum.ShipNum = ShipNum;
}
