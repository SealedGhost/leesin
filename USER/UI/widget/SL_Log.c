#include "UIMessage.h"
#include "dlg.h"
#include "selfLogWinDlg.h"
#include "GUI_LCD.h"
#include "GUI.h"
#include "system_config.h"
#include "system_info.h"


typedef struct WG_SlLog{
  Widget Widget;
  uint32_t DialNum;
  uint32_t ShipNum;
  uint8_t Version[3];
  uint8_t Mode;
}WG_SlLog;

WG_SlLog _sl_log;
Widget *slLog;

static void slLogCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      { 
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_DOWN:
            _sl_log.Mode++;
            if(_sl_log.Mode > SL_DISPMODE_VERSION)
              _sl_log.Mode = SL_DISPMODE_DIALNUM;
            WG_Paint(slLog);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_UP:
            _sl_log.Mode--;
            if(_sl_log.Mode < SL_DISPMODE_DIALNUM)
              _sl_log.Mode = SL_DISPMODE_VERSION;
            WG_Paint(slLog);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_STAR:
            if(_sl_log.Mode == SL_DISPMODE_SHIPNUM)
            {
              WG_SetFocus(slPassword);
              WG_SetInVisible(slLog);
              WG_Paint(slLog);
              WG_Paint(slPassword);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
    {
      SysCfg sysCfg;
      VersionInfo versionInfo;
      
      SysCfg_GetConfig(&sysCfg);
      _sl_log.ShipNum = sysCfg.boatNumber;
      _sl_log.DialNum = SysInfo_GetId();
      SysInfo_GetVersion(&versionInfo);
      _sl_log.Version[0] = versionInfo.major;
      _sl_log.Version[1] = versionInfo.minor;
      _sl_log.Version[2] = versionInfo.revise;
      _sl_log.Mode = SL_DISPMODE_DIALNUM;
    }
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        SysCfg sysCfg;
        VersionInfo versionInfo;

        SysCfg_GetConfig(&sysCfg);
        _sl_log.ShipNum = sysCfg.boatNumber;
        _sl_log.DialNum = SysInfo_GetId();
        SysInfo_GetVersion(&versionInfo);
        _sl_log.Version[0] = versionInfo.major;
        _sl_log.Version[1] = versionInfo.minor;
        _sl_log.Version[2] = versionInfo.revise;
      }
      break;
    case WM_PAINT:
      if(_sl_log.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        switch(_sl_log.Mode)
        {
          case SL_DISPMODE_DIALNUM:
            GUI_LCDSetS(LCD_S25, 0);
            GUI_LCDSetS(LCD_S21, 0);
            GUI_LCDSetS(LCD_S19, 0);
            GUI_LCDSetS(LCD_S24, 1);
            GUI_LCDSetNumInt(LCD_NUM14, _sl_log.DialNum / 1000000 % 10, 0);
            GUI_LCDSetNumInt(LCD_NUM15, _sl_log.DialNum / 100000 % 10, 0);
            GUI_LCDSetNumInt(LCD_NUM16, _sl_log.DialNum / 10000 % 10, 0);
            GUI_LCDSetNumInt(LCD_NUM17, _sl_log.DialNum / 1000 % 10, 0);
            GUI_LCDSetNumInt(LCD_NUM18, _sl_log.DialNum / 100 % 10, 0);
            GUI_LCDSetNumInt(LCD_NUM19, _sl_log.DialNum / 10 % 10, 0);
            GUI_LCDSetNumInt(LCD_NUM20, _sl_log.DialNum % 10, 0);
            break;
          case SL_DISPMODE_SHIPNUM:
            GUI_LCDSetS(LCD_S24, 0);
            GUI_LCDSetS(LCD_S21, 0);
            GUI_LCDSetS(LCD_S19, 0);
            GUI_LCDSetNumInt(LCD_NUM14, LCD_DISPNULL, 0);
            GUI_LCDSetNumInt(LCD_NUM20, LCD_DISPNULL, 0);
            GUI_LCDSetS(LCD_S25, 1);
            GUI_LCDSetNumInt(LCD_NUM15, _sl_log.ShipNum / 10000 % 10, 0);
            GUI_LCDSetNumInt(LCD_NUM16, _sl_log.ShipNum / 1000 % 10, 0);
            GUI_LCDSetNumInt(LCD_NUM17, _sl_log.ShipNum / 100 % 10, 0);
            GUI_LCDSetNumInt(LCD_NUM18, _sl_log.ShipNum / 10 % 10, 0);
            GUI_LCDSetNumInt(LCD_NUM19, _sl_log.ShipNum % 10, 0);
            break;
          case SL_DISPMODE_VERSION:
            GUI_LCDSetS(LCD_S24, 0);
            GUI_LCDSetS(LCD_S25, 0);
            GUI_LCDSetNumInt(LCD_NUM20, LCD_DISPNULL, 0);
            GUI_LCDSetS(LCD_S21, 1);
            GUI_LCDSetS(LCD_S19, 1);
            if(_sl_log.Version[0] / 10 % 10)
              GUI_LCDSetNumInt(LCD_NUM14, _sl_log.Version[0] / 10 % 10, 0);
            else
              GUI_LCDSetNumInt(LCD_NUM14, LCD_DISPNULL, 0);
            GUI_LCDSetNumInt(LCD_NUM15, _sl_log.Version[0] % 10, 0);
            
            if(_sl_log.Version[1] / 10 % 10)
              GUI_LCDSetNumInt(LCD_NUM16, _sl_log.Version[1] / 10 % 10, 0);
            else
              GUI_LCDSetNumInt(LCD_NUM16, LCD_DISPNULL, 0);
            GUI_LCDSetNumInt(LCD_NUM17, _sl_log.Version[1] % 10, 0);
            
            if(_sl_log.Version[2] / 10 % 10)
                GUI_LCDSetNumInt(LCD_NUM18, _sl_log.Version[2] / 10 % 10, 0);
            else
                GUI_LCDSetNumInt(LCD_NUM18, LCD_DISPNULL, 0);
            GUI_LCDSetNumInt(LCD_NUM19, _sl_log.Version[2] % 10, 0);
            break;
        }
      }
      else
      {
        GUI_LCDSetS(LCD_S24, 0);
        GUI_LCDSetS(LCD_S25, 0);
        GUI_LCDSetS(LCD_S21, 0);
        GUI_LCDSetS(LCD_S19, 0);
        GUI_LCDSetNumInt(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM20, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* slLogCreate(void)
{
  WG_Create((Widget*)&_sl_log, &selfLogWin, slLogCallback);
  slLog = (Widget*)&_sl_log;
  return slLog;
}