#include "UIMessage.h"
#include "dlg.h"
#include "callShipNumWinDlg.h"
#include "lcdControl.h"
#include "GUI_LCD.h"

typedef struct WG_CsnOrderNum{
  Widget Widget;
  uint8_t OrderNum;
}WG_CsnOrderNum;

WG_CsnOrderNum _csn_ordernum;
Widget *csnOrderNum;

static void csnOrderNumCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_csn_ordernum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S46, 1);
        GUI_LCDSetS(LCD_S45, 1);
        GUI_LCDSetS(LCD_S44, 1);
        GUI_LCDSetNumInt(LCD_NUM1, _csn_ordernum.OrderNum / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM2, _csn_ordernum.OrderNum % 10, 0);
      }
      else
      {
        GUI_LCDSetS(LCD_S46, 0);
        GUI_LCDSetS(LCD_S45, 0);
        GUI_LCDSetS(LCD_S44, 0);
        GUI_LCDSetNumInt(LCD_NUM1, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM2, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* csnOrderNumCreate(void)
{
  WG_Create((Widget*)&_csn_ordernum, &callShipNumWin, csnOrderNumCallback);
  csnOrderNum = (Widget*)&_csn_ordernum;
  return csnOrderNum;
}

void csnOrderNumSetVal(uint8_t OrderNum)
{
  _csn_ordernum.OrderNum = OrderNum;
}
