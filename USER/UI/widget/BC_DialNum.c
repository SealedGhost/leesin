#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "beCalledWinDlg.h"
#include "string.h"
#include "GUI.h"
#include "GUI_LCD.h"


typedef struct WG_bcDialNum{
  Widget Widget;
  uint32_t DialNum;
}WG_bcDialNum;

WG_bcDialNum _bc_dialnum;
Widget* bcDialNum;

static void bcDialNumWinCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_bc_dialnum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetNumInt(LCD_NUM14, _bc_dialnum.DialNum / 1000000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM15, _bc_dialnum.DialNum / 100000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM16, _bc_dialnum.DialNum / 10000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM17, _bc_dialnum.DialNum / 1000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM18, _bc_dialnum.DialNum / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM19, _bc_dialnum.DialNum / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM20, _bc_dialnum.DialNum % 10, 0);
      }
      else
      {
        GUI_LCDSetNumInt(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM20, LCD_DISPNULL, 0);
      }
      break;
  }
}


Widget* bcDialNumCreate(void)
{
  WG_Create((Widget*)&_bc_dialnum, &beCalledWin, bcDialNumWinCallback);
  bcDialNum = (Widget*)&_bc_dialnum;
  return bcDialNum;
}


void bcDialNumSetVal(uint32_t DialNum)
{
  _bc_dialnum.DialNum = DialNum;
}

uint32_t bcDialNumGetVal(void)
{
  return _bc_dialnum.DialNum;
}
