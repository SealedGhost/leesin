#include "UIMessage.h"
#include "dlg.h"
#include "selfLogWinDlg.h"
#include "GUI.h"
#include "GUI_LCD.h"
#include "system_config.h"

typedef struct WG_SlShipNum{
  Widget Widget;
  char ShipID[6];
  uint8_t Activate;
}WG_SlShipNum;

WG_SlShipNum _sl_shipnum;
Widget *slShipNum;

static void slShipNumCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      { 
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_BACK:
            WG_SetInVisible(slShipNum);
            WG_Paint(slShipNum);
            WG_SetFocus(slLog);
            WG_Paint(slLog);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_STAR:
          case GUI_KEY_DEL:
          {
            uint8_t Length = 0;
            GUI_CharToUint32(_sl_shipnum.ShipID, &Length);
            if(Length != SL_EDITSHIPNUM_LENGTH)
            {
              GUI_SetCharUnderLine(_sl_shipnum.ShipID, SL_EDITSHIPNUM_LENGTH);
              WG_Paint(slShipNum);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
          }
            break;
          case GUI_KEY_ENTER:
          {
            uint8_t length;
            uint32_t shipID = GUI_CharToUint32(_sl_shipnum.ShipID, &length);
            if(!length && shipID)
            {
              SysCfg sysCfg;
              SysCfg_GetConfig(&sysCfg);
              sysCfg.boatNumber = shipID;
              SysCfg_SetConfig(&sysCfg);
              WG_SetInVisible(slShipNum);
              WG_Paint(slShipNum);
              WG_SetFocus(slLog);
              WG_Paint(slLog);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
          }
            break;
          case GUI_KEY_0_CALLSHIP:
          {
            uint8_t length;
            uint32_t shipID = GUI_CharToUint32(_sl_shipnum.ShipID, &length);
            if(!shipID && length == 1) 
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(GUI_AddAChar(_sl_shipnum.ShipID, '0', SL_EDITSHIPNUM_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slShipNum);
          }
            break;
          case GUI_KEY_1_GROUP:
            if(GUI_AddAChar(_sl_shipnum.ShipID, '1', SL_EDITSHIPNUM_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slShipNum);
            break;
          case GUI_KEY_2_DIGIT:
            if(GUI_AddAChar(_sl_shipnum.ShipID, '2', SL_EDITSHIPNUM_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slShipNum);
            break;
          case GUI_KEY_3_SIMULATE:
            if(GUI_AddAChar(_sl_shipnum.ShipID, '3', SL_EDITSHIPNUM_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slShipNum);
            break;
          case GUI_KEY_4_LOG:
            if(GUI_AddAChar(_sl_shipnum.ShipID, '4', SL_EDITSHIPNUM_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slShipNum);
            break;
          case GUI_KEY_5_DIRECTION:
            if(GUI_AddAChar(_sl_shipnum.ShipID, '5', SL_EDITSHIPNUM_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slShipNum);
            break;
          case GUI_KEY_6_REMEMBER:       
            if(GUI_AddAChar(_sl_shipnum.ShipID, '6', SL_EDITSHIPNUM_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slShipNum);
            break;
          case GUI_KEY_7_SIGN: 
            if(GUI_AddAChar(_sl_shipnum.ShipID, '7', SL_EDITSHIPNUM_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slShipNum);
            break;
          case GUI_KEY_8_WEATHER:        
            if(GUI_AddAChar(_sl_shipnum.ShipID, '8', SL_EDITSHIPNUM_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slShipNum);
            break;
          case GUI_KEY_9_SCAN:         
            if(GUI_AddAChar(_sl_shipnum.ShipID, '9', SL_EDITSHIPNUM_LENGTH))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            WG_Paint(slShipNum);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          
        }
      }
      break;
    case WM_CREATE:
      GUI_SetCharUnderLine(_sl_shipnum.ShipID, SL_EDITSHIPNUM_LENGTH);
      _sl_shipnum.Activate = SL_EDITSHIPNUM_ACTIVATE;
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        GUI_SetCharUnderLine(_sl_shipnum.ShipID, SL_EDITSHIPNUM_LENGTH);
        _sl_shipnum.Activate = SL_EDITSHIPNUM_ACTIVATE;
      }
      break;
    case WM_PAINT:
      if(_sl_shipnum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        if(_sl_shipnum.Activate == SL_EDITSHIPNUM_ACTIVATE)
        {
          uint8_t  flash = 1;
          
          GUI_LCDSetS(LCD_S25, 1);
          GUI_LCDSetS(LCD_S26, 1);
          GUI_LCDSetS(LCD_S28, 1);
          if(flash && _sl_shipnum.ShipID[0] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM15, _sl_shipnum.ShipID[0], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM15, _sl_shipnum.ShipID[0], 0);
          }
          
          if(flash && _sl_shipnum.ShipID[1] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM16, _sl_shipnum.ShipID[1], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM16, _sl_shipnum.ShipID[1], 0);
          }
          
          if(flash && _sl_shipnum.ShipID[2] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM17, _sl_shipnum.ShipID[2], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM17, _sl_shipnum.ShipID[2], 0);
          }
          
          if(flash && _sl_shipnum.ShipID[3] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM18, _sl_shipnum.ShipID[3], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM18, _sl_shipnum.ShipID[3], 0);
          }
          
          if(flash)
          {
            GUI_LCDSetNumChar(LCD_NUM19, _sl_shipnum.ShipID[4], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM19, _sl_shipnum.ShipID[4], 0);
          }
        }
        else
        {
          GUI_LCDSetS(LCD_S25, 1);
          GUI_LCDSetS(LCD_S26, 1);
          GUI_LCDSetS(LCD_S28, 1);
          GUI_LCDSetNumChar(LCD_NUM15, _sl_shipnum.ShipID[0], 0);
          GUI_LCDSetNumChar(LCD_NUM16, _sl_shipnum.ShipID[1], 0);
          GUI_LCDSetNumChar(LCD_NUM17, _sl_shipnum.ShipID[2], 0);
          GUI_LCDSetNumChar(LCD_NUM18, _sl_shipnum.ShipID[3], 0);
          GUI_LCDSetNumChar(LCD_NUM19, _sl_shipnum.ShipID[4], 0);
        }
      }
      else
      {
        GUI_LCDSetS(LCD_S25, 0);
        GUI_LCDSetS(LCD_S26, 0);
        GUI_LCDSetS(LCD_S28, 0);
        GUI_LCDSetNumChar(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM19, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* slShipNumCreate(void)
{
  WG_Create((Widget*)&_sl_shipnum, &selfLogWin, slShipNumCallback);
  slShipNum = (Widget*)&_sl_shipnum;
  return slShipNum;
}