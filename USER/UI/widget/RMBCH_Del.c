#include "UIMessage.h"
#include "dlg.h"
#include "rememberChWinDlg.h"
#include "GUI.h"
#include "channel_favorite.h"
#include "GUI_LCD.h"
#include "analog_channel.h"
#include "channel480.h"
#include "GUI_Timer.h"

typedef struct WG_rmbchDel{
  Widget Widget;
}WG_rmbchDel;

WG_rmbchDel _rmbch_del;
Widget *rembchDel;

static void remberDelCallback(WM_Message *pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      GUI_DestroyTimer(&rememberChWin, REMBCH_EDIT_TIMER);
      WG_SetInVisible(rembchDel);
      WG_Paint(rembchDel);
      WG_SetFocus(rembchChannel);
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &rememberChWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        GUI_RestartTimer(&rememberChWin, REMBCH_EDIT_TIMER, EDIT_TIMEROUT_LENGTH);
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_ENTER:
            GUI_DestroyTimer(&rememberChWin, REMBCH_EDIT_TIMER);
            ChnFavorite_DeleteByChn(ChnFavorite_GetCurrChannel());
            rembchChannelSetVal(ChnFavorite_GetCurrChannel());
            rembchOrderNumSetVal(ChnFavorite_GetCurrPos());
            if(ChnFavorite_GetCurrPos())
            {
              EnterAnalog(ChnFavorite_GetCurrChannel());
            }
            WG_SetInVisible(rembchDel);
            WG_Paint(rembchDel);
            WG_SetFocus(rembchChannel);
            WG_Paint(rembchChannel);
            WG_Paint(rembchOrderNum);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_BACK:
            GUI_DestroyTimer(&rememberChWin, REMBCH_EDIT_TIMER);
            WG_SetInVisible(rembchDel);
            WG_Paint(rembchDel);
            WG_SetFocus(rembchChannel);
            WG_Paint(rembchChannel);
            EnterAnalog(ChnFavorite_GetCurrChannel());
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        ExitAnalog();
      }
      break;
    case WM_PAINT:
      if(_rmbch_del.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetNumChar(LCD_NUM16, 'D', 0);
        GUI_LCDSetNumChar(LCD_NUM17, 'E', 0);
        GUI_LCDSetNumChar(LCD_NUM18, 'L', 0);
      }
      else
      {
        GUI_LCDSetNumChar(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM18, LCD_DISPNULL, 0);
      }
      break;
    case WM_TIMER:
      WG_SetInVisible(rembchDel);
      WG_Paint(rembchDel);
      WG_SetFocus(rembchChannel);
      WG_Paint(rembchChannel);
      EnterAnalog(ChnFavorite_GetCurrChannel());
      break;
  }
}

Widget* rembchDelCreate(void)
{
  WG_Create((Widget*)&_rmbch_del, &rememberChWin, remberDelCallback);
  rembchDel = (Widget*)&_rmbch_del;
  return rembchDel;
}