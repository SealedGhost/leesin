#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "groupDialWinDlg.h"
#include "GUI.h"
#include "GUI_LCD.h"
#include "system_config.h"
#include "group_chat.h"
#include "call_mgr.h"

typedef struct WG_GDailRoundButton{
  Widget Widget;
}WG_GDailRoundButton;


WG_GDailRoundButton _gdial_roundbutton = {0};
Widget *gdialRoundButton;


static void gdialRoundButtonCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_HUNG_UP:
      WG_SetInVisible(gdialShipNum);
      WG_SetInVisible(gdialDialNum);
      WG_SetInVisible(gdialRoundButton);
      WG_Paint(gdialShipNum);
      WG_Paint(gdialDialNum);
      WG_Paint(gdialRoundButton);
      WG_SetVisible(gdialGroupID);
      WG_SetFocus(gdialChannel);
      WG_Paint(gdialGroupID);
      WG_Paint(gdialChannel);
      gdialDialStateSet(CALL_MISS, 0);
      WG_SetVisible(gdialDialState);
      WG_Paint(gdialDialState);
      AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
      AudioStopPlay();
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_PICKUP:
            WG_SetInVisible(gdialShipNum);
            WG_SetInVisible(gdialDialNum);
            WG_SetInVisible(gdialRoundButton);
            WG_Paint(gdialShipNum);
            WG_Paint(gdialDialNum);
            WG_Paint(gdialRoundButton);
            WG_SetVisible(gdialGroupID);
            WG_SetFocus(gdialChannel);
            Msg.pSource = &gdialWin;
            Msg.pTarget = &dialWin;
            Msg.msgType = USER_MSG_DIALTALKING;
            Msg.data_v = gdialDialNumGetVal();
            Msg.data_p = (void*)gdialShipNumGetVal();
            WM_PostMessage(&Msg);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            AudioStopPlay();
            break;
          case GUI_KEY_HANGUP:
            CallMgr_HangUp(gdialDialNumGetVal());
            WG_SetInVisible(gdialShipNum);
            WG_SetInVisible(gdialDialNum);
            WG_SetInVisible(gdialRoundButton);
            WG_Paint(gdialShipNum);
            WG_Paint(gdialDialNum);
            WG_Paint(gdialRoundButton);
            WG_SetVisible(gdialGroupID);
            WG_SetFocus(gdialChannel);
            WG_Paint(gdialGroupID);
            WG_Paint(gdialChannel);
            gdialDialStateSet(CALL_MISS, 0);
            WG_SetVisible(gdialDialState);
            WG_Paint(gdialDialState);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            AudioStopPlay();
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_gdial_roundbutton.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetS(LCD_S15, 1);
      }
      else
      {
        GUI_LCDSetS(LCD_S15, 0);
      }
      break;
  }
  return;
}

Widget* gdialRoundButtonCreate(void)
{
  WG_Create((Widget*)&_gdial_roundbutton, &gdialWin, gdialRoundButtonCallback);
  gdialRoundButton = (Widget*)&_gdial_roundbutton;
  return gdialRoundButton;
}