#include "UIMessage.h"
#include "dlg.h"
#include "callShipNumWinDlg.h"
#include "GUI.h"
#include "lcdControl.H"
#include "GUI_LCD.h"
#include "boat_search.h"
#include "GUI_Timer.h"

typedef struct WG_CsnEditShipNum{
  Widget Widget;
  char ShipNum[6];
  uint8_t Activate;
}WG_CsnEditShipNum;

WG_CsnEditShipNum _csn_editshipnum;
Widget *csnEditShipNum;

static void csnEditShipNumCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  uint8_t Length;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      _csn_editshipnum.Activate = CSN_EDITSHIPNUM_UNACTIVATE;
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &callShipNumWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg); 
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      { 
        if(_csn_editshipnum.Activate == CSN_EDITSHIPNUM_ACTIVATE)
        {
          switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
          {
            case GUI_KEY_BACK:
              _csn_editshipnum.Activate = CSN_EDITSHIPNUM_UNACTIVATE;
              GUI_SetCharUnderLine(_csn_editshipnum.ShipNum, CSN_EDITSHIPNUM_LENGTH);
              WG_Paint(csnEditShipNum);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_ENTER:
            {
              uint8_t Length = 0;
              uint32_t ShipNum = GUI_CharToUint32(_csn_editshipnum.ShipNum, &Length);
              if(!Length && ShipNum)
              {
                BoatSch_DoSearch(ShipNum);
                csnShipNumSetVal(ShipNum);
                WG_SetFocus(csnShipNum);
                WG_SetInVisible(csnEditShipNum);
                WG_Paint(csnEditShipNum);
                WG_Paint(csnShipNum);
                GUI_CreateTimer(&callShipNumWin, 1, 40);
                _csn_editshipnum.Activate = CSN_EDITSHIPNUM_UNACTIVATE;
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
            }
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            case GUI_KEY_DEL:
            case GUI_KEY_STAR:
              GUI_CharToUint32(_csn_editshipnum.ShipNum, &Length);
              if(Length != CSN_EDITSHIPNUM_LENGTH)
              {
                GUI_SetCharUnderLine(_csn_editshipnum.ShipNum, CSN_EDITSHIPNUM_LENGTH);
                WG_Paint(csnEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              break;
            case GUI_KEY_0_CALLSHIP:
              Length = GUI_AddAChar(_csn_editshipnum.ShipNum, '0', CSN_EDITSHIPNUM_LENGTH);
              if(Length)
              {
                WG_Paint(csnEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              break;
            case GUI_KEY_1_GROUP:
              Length = GUI_AddAChar(_csn_editshipnum.ShipNum, '1', CSN_EDITSHIPNUM_LENGTH);
              if(Length)
              {
                WG_Paint(csnEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              break;
            case GUI_KEY_2_DIGIT:
              Length = GUI_AddAChar(_csn_editshipnum.ShipNum, '2', CSN_EDITSHIPNUM_LENGTH);
              if(Length)
              {
                WG_Paint(csnEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              break;
            case GUI_KEY_3_SIMULATE:
              Length = GUI_AddAChar(_csn_editshipnum.ShipNum, '3', CSN_EDITSHIPNUM_LENGTH);
              if(Length)
              {
                WG_Paint(csnEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              break;
            case GUI_KEY_4_LOG:
              Length = GUI_AddAChar(_csn_editshipnum.ShipNum, '4', CSN_EDITSHIPNUM_LENGTH);
              if(Length)
              {
                WG_Paint(csnEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              break;
            case GUI_KEY_5_DIRECTION:
              Length = GUI_AddAChar(_csn_editshipnum.ShipNum, '5', CSN_EDITSHIPNUM_LENGTH);
              if(Length)
              {
                WG_Paint(csnEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              break;
            case GUI_KEY_6_REMEMBER:       
              Length = GUI_AddAChar(_csn_editshipnum.ShipNum, '6', CSN_EDITSHIPNUM_LENGTH);
              if(Length)
              {
                WG_Paint(csnEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              break;
            case GUI_KEY_7_SIGN: 
              Length = GUI_AddAChar(_csn_editshipnum.ShipNum, '7', CSN_EDITSHIPNUM_LENGTH);
              if(Length)
              {
                WG_Paint(csnEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              break;
            case GUI_KEY_8_WEATHER:        
              Length = GUI_AddAChar(_csn_editshipnum.ShipNum, '8', CSN_EDITSHIPNUM_LENGTH);
              if(Length)
              {
                WG_Paint(csnEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              break;
            case GUI_KEY_9_SCAN:         
              Length = GUI_AddAChar(_csn_editshipnum.ShipNum, '9', CSN_EDITSHIPNUM_LENGTH);
              if(Length)
              {
                WG_Paint(csnEditShipNum);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              }
              else
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              }
              break;
            default:
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
          }
        }
        else
        {
          switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
          {
            case GUI_KEY_PICKUP:
              WM_SetFocus(&editDialNumWin);
              WM_BringToTop(&editDialNumWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_STAR:
              _csn_editshipnum.Activate = CSN_EDITSHIPNUM_ACTIVATE;
              GUI_SetCharUnderLine(_csn_editshipnum.ShipNum, CSN_EDITSHIPNUM_LENGTH);
              WG_Paint(csnEditShipNum);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_1_GROUP:
              WM_SetFocus(&gdialWin);
              WM_BringToTop(&gdialWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_2_DIGIT:
              WM_SetFocus(&digitWin);
              WM_BringToTop(&digitWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_3_SIMULATE:
              WM_BringToTop(&analogWin);
              WM_SetFocus(&analogWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_4_LOG:
              WM_SetFocus(&calogWin);
              WM_BringToTop(&calogWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_5_DIRECTION:
              WM_SetFocus(&contactsWin);
              WM_BringToTop(&contactsWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_6_REMEMBER:
              WM_SetFocus(&rememberChWin);       
              WM_BringToTop(&rememberChWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_8_WEATHER:
              WM_SetFocus(&weatherWin);        
              WM_BringToTop(&weatherWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            default:
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
          }
        }
      }
      break;
    case WM_CREATE:
      GUI_SetCharUnderLine(_csn_editshipnum.ShipNum, CSN_EDITSHIPNUM_LENGTH);
      _csn_editshipnum.Activate = CSN_EDITSHIPNUM_UNACTIVATE;
      break;
    case WM_FOCUS:
      if(!pMsg->data_v)
      {
        _csn_editshipnum.Activate = CSN_EDITSHIPNUM_UNACTIVATE;
      }
      break;
    case WM_PAINT:
      if(_csn_editshipnum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        uint8_t flash = 1;
        GUI_LCDSetS(LCD_S25, 1);
        GUI_LCDSetS(LCD_S26, 1);
        GUI_LCDSetS(LCD_S28, 1);
        if(_csn_editshipnum.Activate == CSN_EDITSHIPNUM_ACTIVATE)
        {
          if(flash && _csn_editshipnum.ShipNum[0] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM15, _csn_editshipnum.ShipNum[0], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM15, _csn_editshipnum.ShipNum[0], 0);
          }
          
          if(flash && _csn_editshipnum.ShipNum[1] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM16, _csn_editshipnum.ShipNum[1], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM16, _csn_editshipnum.ShipNum[1], 0);
          }
          
          if(flash && _csn_editshipnum.ShipNum[2] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM17, _csn_editshipnum.ShipNum[2], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM17, _csn_editshipnum.ShipNum[2], 0);
          }
          
          if(flash && _csn_editshipnum.ShipNum[3] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM18, _csn_editshipnum.ShipNum[3], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM18, _csn_editshipnum.ShipNum[3], 0);
          }
          
          if(flash)
          {
            GUI_LCDSetNumChar(LCD_NUM19, _csn_editshipnum.ShipNum[4], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM19, _csn_editshipnum.ShipNum[4], 0);
          }
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM15, '0', 0);
          GUI_LCDSetNumChar(LCD_NUM16, '0', 0);
          GUI_LCDSetNumChar(LCD_NUM17, '0', 0);
          GUI_LCDSetNumChar(LCD_NUM18, '0', 0);
          GUI_LCDSetNumChar(LCD_NUM19, '0', 0);
        }
      }
      else
      {
        GUI_LCDSetS(LCD_S25, 0);
        GUI_LCDSetS(LCD_S26, 0);
        GUI_LCDSetS(LCD_S28, 0);
        GUI_LCDSetNumChar(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM19, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* csnEditShipNumCreate(void)
{
  WG_Create((Widget*)&_csn_editshipnum, &callShipNumWin, csnEditShipNumCallback);
  csnEditShipNum = (Widget*)&_csn_editshipnum;
  return csnEditShipNum;
}