#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "contactsWinDlg.h"
#include "GUI.h"
#include "contact.h"
#include "GUI_LCD.h"

typedef struct WG_ContDel{
  Widget Widget;
}WG_ContDel;

WG_ContDel _cont_del = {0};
Widget *contDel;

static void contDelCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      WG_SetInVisible(contDel);
      WG_Paint(contDel);
      WG_SetVisible(contShipNum);
      WG_Paint(contShipNum);
      WG_SetFocus(contDialNum);
      WG_Paint(contDialNum);
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &contactsWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_BACK:
            WG_SetInVisible(contDel);
            WG_Paint(contDel);
            WG_SetVisible(contShipNum);
            WG_Paint(contShipNum);
            WG_SetFocus(contDialNum);
            WG_Paint(contDialNum);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_ENTER:
          {
            LinkMan *linkMan;
            Contact_Delete(Contact_GetPosOfLinkMan(Contact_GetCurrLinkMan()));
            linkMan = Contact_GetCurrLinkMan();
            if(linkMan)
            {
              contDialNumSetVal(linkMan->phoneNumber);
              contShipNumSetVal(linkMan->shipNumber);
              contOrderNumSetVal(Contact_GetPosOfLinkMan(linkMan));
            }
            else
            {
              contDialNumSetVal(0);
              contShipNumSetVal(0);
              contOrderNumSetVal(0);
            }
            WG_SetInVisible(contDel);
            WG_Paint(contDel);
            WG_SetVisible(contShipNum);
            WG_Paint(contShipNum);
            WG_SetFocus(contDialNum);
            WG_Paint(contDialNum);
            WG_Paint(contOrderNum);
          }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          default:
            AudioPlay(AUDIO_TYPE_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_cont_del.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetNumInt(LCD_NUM16, 'D', 0);
        GUI_LCDSetNumInt(LCD_NUM17, 'E', 0);
        GUI_LCDSetNumInt(LCD_NUM18, 'L', 0);
      }
      else
      {
        GUI_LCDSetNumInt(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* contDelCreate(void)
{
  WG_Create((Widget*)&_cont_del, &contactsWin, contDelCallback);
  contDel = (Widget*)&_cont_del;
  return contDel;
}
