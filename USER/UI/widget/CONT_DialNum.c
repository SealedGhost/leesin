#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "contactsWinDlg.h"
#include "GUI.h"
#include "contact.h"
#include "GUI_LCD.h"
#include "GUI_Timer.h"


typedef struct WG_ContDialNum{
  Widget Widget;
  uint32_t DialNum;
}WG_ContDialNum;

WG_ContDialNum _cont_dialnum = {0};
Widget *contDialNum;


static void contDialNumCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &contactsWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
    printf("cont shipnum %u\n\n", (uint32_t)pMsg->data_p);
      WM_PostMessage(&Msg);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_PICKUP:
          {
            if(_cont_dialnum.DialNum)
            {
              Msg.pSource = &contactsWin;
              Msg.pTarget = &dialWin;
              Msg.msgType = USER_MSG_DIAL;
              Msg.data_v = _cont_dialnum.DialNum;
              WM_PostMessage(&Msg);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
          }
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          case GUI_KEY_DEL:
          {
            LinkMan* linkMan = Contact_GetCurrLinkMan();
            if(linkMan)
            {
              WG_SetInVisible(contDialNum);
              WG_Paint(contDialNum);
              WG_SetInVisible(contShipNum);
              WG_Paint(contShipNum);
              WG_SetFocus(contDel);
              WG_Paint(contDel);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
          }
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          case GUI_KEY_UP:
          {
            LinkMan *linkMan = Contact_GetNextLinkMan();
            if(linkMan)
            {
              _cont_dialnum.DialNum = linkMan->phoneNumber;
              contShipNumSetVal(linkMan->shipNumber);
              contOrderNumSetVal(Contact_GetPosOfLinkMan(linkMan));
              WG_Paint(contDialNum);
              WG_Paint(contOrderNum);
              WG_Paint(contShipNum);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
          }
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          case GUI_KEY_DOWN:
          {
            LinkMan *linkMan = Contact_GetPrevLinkMan();
            if(linkMan)
            {
              _cont_dialnum.DialNum = linkMan->phoneNumber;
              contShipNumSetVal(linkMan->shipNumber);
              contOrderNumSetVal(Contact_GetPosOfLinkMan(linkMan));
              WG_Paint(contDialNum);
              WG_Paint(contOrderNum);
              WG_Paint(contShipNum);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            
          }
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
          case GUI_KEY_NEW:
            if(Contact_GetLinkManNumber() >= 30)
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
            else
            {
              GUI_CreateTimer(&contactsWin, CONT_EDIT_TIMER, CONT_EDIT_TIME_LENGTH);
              contOrderNumSetVal(Contact_GetLinkManNumber() + 1);
              WG_SetInVisible(contShipNum);
              WG_SetInVisible(contDialNum);
              WG_SetFocus(contEditShipNum);
              WG_SetVisible(contEditDialNum);
              WM_Paint(&contactsWin, 1);
              WG_Paint(contOrderNum);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            break;
          case GUI_KEY_0_CALLSHIP:
            WM_SetFocus(&callShipNumWin);
            WM_BringToTop(&callShipNumWin);
            Contact_GoHome();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_1_GROUP:
            WM_SetFocus(&gdialWin);
            WM_BringToTop(&gdialWin);
            Contact_GoHome();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_2_DIGIT:
            WM_SetFocus(&digitWin);
            WM_BringToTop(&digitWin);
            Contact_GoHome();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_3_SIMULATE:
            WM_BringToTop(&analogWin);
            WM_SetFocus(&analogWin);
            Contact_GoHome();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_4_LOG:
            WM_SetFocus(&calogWin);
            WM_BringToTop(&calogWin);
            Contact_GoHome();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_6_REMEMBER:
            WM_SetFocus(&rememberChWin);       
            WM_BringToTop(&rememberChWin);
            Contact_GoHome();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_8_WEATHER:
            WM_SetFocus(&weatherWin);        
            WM_BringToTop(&weatherWin);
            Contact_GoHome();
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
    {
      LinkMan *linkMan = Contact_GetCurrLinkMan();
      if(linkMan)
      {
        _cont_dialnum.DialNum = linkMan->phoneNumber;
        contShipNumSetVal(linkMan->shipNumber);
        contOrderNumSetVal(Contact_GetPosOfLinkMan(linkMan));
      }
      else
      {
        _cont_dialnum.DialNum = 0;
        contShipNumSetVal(0);
        contOrderNumSetVal(0);
      }
    }
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_cont_dialnum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetNumInt(LCD_NUM14, _cont_dialnum.DialNum / 1000000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM15, _cont_dialnum.DialNum / 100000 %10, 0);
        GUI_LCDSetNumInt(LCD_NUM16, _cont_dialnum.DialNum / 10000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM17, _cont_dialnum.DialNum / 1000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM18, _cont_dialnum.DialNum / 100% 10, 0);
        GUI_LCDSetNumInt(LCD_NUM19, _cont_dialnum.DialNum / 10 %10, 0);
        GUI_LCDSetNumInt(LCD_NUM20, _cont_dialnum.DialNum % 10, 0);
      }
      else
      {
        GUI_LCDSetNumInt(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM20, LCD_DISPNULL, 0);
      }
      break;
  }
  return;
}

Widget* contDialNumCreate(void)
{
  WG_Create((Widget*)&_cont_dialnum, &contactsWin, contDialNumCallback);
  contDialNum = (Widget*)&_cont_dialnum;
  return contDialNum;
}

void contDialNumSetVal(uint32_t DialNum)
{
  _cont_dialnum.DialNum = DialNum;
}

