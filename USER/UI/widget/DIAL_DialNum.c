#include "dialWinDlg.h"
#include "UIMessage.h"
#include "dlg.h"
#include "GUI.h"
#include "lcdControl.h"
#include "GUI_Timer.h"
#include "GUI_LCD.h"

typedef struct WG_DialNum{
  Widget Widget;
  uint32_t DialNum;
}WG_DialNum;

WG_DialNum _dialNum = {0};
Widget *dialDialNum;

static void dialNumCallback(WM_Message* pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_KEY:
      break;
    case WM_CREATE:
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_dialNum.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetNumInt(LCD_NUM14, _dialNum.DialNum / 1000000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM15, _dialNum.DialNum / 100000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM16, _dialNum.DialNum / 10000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM17, _dialNum.DialNum / 1000 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM18, _dialNum.DialNum / 100 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM19, _dialNum.DialNum / 10 % 10, 0);
        GUI_LCDSetNumInt(LCD_NUM20, _dialNum.DialNum % 10, 0);
      }
      else
      {
        GUI_LCDSetNumInt(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumInt(LCD_NUM20, LCD_DISPNULL, 0);
      }
      break;
  }
}


Widget* dialDialNumCreate(void)
{
  WG_Create((Widget*)&_dialNum, &dialWin, dialNumCallback);
  dialDialNum = (Widget*)&_dialNum;
  return dialDialNum;
}

void dialDialNumSetVal(uint32_t DialNum)
{
  _dialNum.DialNum = DialNum;
}

uint32_t dialDialNumGetVal(void)
{
  return _dialNum.DialNum;
}

