#include "LcdControl.h"
#include "UIMessage.h"
#include "dlg.h"
#include "groupDialWinDlg.h"
#include "GUI.h"
#include "GUI_LCD.h"
#include "system_config.h"
#include "group_chat.h"
#include "GUI_Timer.h"

typedef struct WG_GDialEditPassword{
  Widget Widget;
  char Password[6];
  uint8_t Activate;
}WG_GDialEditPassword;

WG_GDialEditPassword _gdial_editpassword = {0};
Widget* gdialEditPassword;

static void gdialEditPasswordCallback(WM_Message* pMsg)
{
  WM_Message Msg;
  uint8_t Length;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
      GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
      gdialEditPasswordSetActive(GDIAL_EDIT_UNACTIVETE);
      gdialEditGroupIDSetActive(GDIAL_EDIT_UNACTIVETE);
      gdialEditPasswordSetUnderline();
      gdialEditGroupIDSetUnderline();
      WG_SetFocus(gdialEditGroupID);
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &gdialWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      {
        GUI_RestartTimer(&gdialWin, GROUP_EDIT_TIMER, EDIT_TIMEROUT_LENGTH);
        if(_gdial_editpassword.Activate == GDIAL_EDIT_ACTIVETE)
        {
          switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
          {
            case GUI_KEY_PICKUP:
              GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
              gdialEditGroupIDSetUnderline();
              gdialEditPasswordSetUnderline();
              gdialEditGroupIDSetActive(GDIAL_EDIT_UNACTIVETE);
              gdialEditPasswordSetActive(GDIAL_EDIT_UNACTIVETE);
              WG_Paint(gdialEditGroupID);
              WG_Paint(gdialEditPassword);
              WG_SetFocus(gdialEditGroupID);
              WM_SetFocus(&editDialNumWin);
              WM_BringToTop(&editDialNumWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_STAR:
              gdialEditPasswordSetUnderline();
              gdialEditGroupIDSetUnderline();
              WG_SetFocus(gdialEditGroupID);
              WG_Paint(gdialEditPassword);
              WG_Paint(gdialEditGroupID);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_DEL:
//              GUI_CharToUint32(_gdial_editpassword.Password, &Length);
//              if(Length != GDIAL_GROUPID_LENGTH)
//              {
                GUI_SetCharUnderLine(_gdial_editpassword.Password, GDIAL_PASSWORD_LENGTH);
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
//              }
//              else
//              {
//                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
//              }
//              break;
            case GUI_KEY_BACK:
              GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
              gdialEditGroupIDSetUnderline();
              gdialEditPasswordSetUnderline();
              gdialEditGroupIDSetActive(GDIAL_EDIT_UNACTIVETE);
              gdialEditPasswordSetActive(GDIAL_EDIT_UNACTIVETE);
              WG_Paint(gdialEditGroupID);
              WG_Paint(gdialEditPassword);
              WG_SetFocus(gdialEditGroupID);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_UP:
            case GUI_KEY_DOWN:
              if(gdialEditPasswordGetUnderlineNum())
              {
                gdialEditPasswordSetUnderline();
                WG_Paint(gdialEditPassword);
              }
              WG_SetFocus(gdialEditGroupID);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_ENTER:
              if(!gdialEditGroupIDGetUnderLineNum() && !gdialEditPasswordGetUnderlineNum() && gdialEditGroupIDGetVal() && gdialEditPasswordGetVal())
              {
                SysCfg sysCfg;
                GUI_DestroyTimer(&gdialWin, GROUP_EDIT_TIMER);
                SysCfg_GetConfig(&sysCfg);
                sysCfg.groupNumber = gdialEditGroupIDGetVal();
                sysCfg.groupPwd = gdialEditPasswordGetVal();
                SysCfg_SetConfig(&sysCfg);
                gdialGroupIDSetValue(sysCfg.groupNumber);
                WG_SetInVisible(gdialEditGroupID);
                WG_Paint(gdialEditGroupID);
                WG_SetInVisible(gdialEditPassword);
                WG_Paint(gdialEditPassword);
                WG_SetVisible(gdialGroupID);
                WG_Paint(gdialGroupID);
                WG_SetFocus(gdialChannel);
                WG_Paint(gdialChannel);
                Grp_Enter();
                AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                break;
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            case GUI_KEY_0_CALLSHIP:
              Length = GUI_AddAChar(_gdial_editpassword.Password, '0', GDIAL_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_PASSWORD_LENGTH)
              {
                if(gdialEditGroupIDGetUnderLineNum())
                {
                  WG_Paint(gdialEditPassword);
                  WG_SetFocus(gdialEditGroupID);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gdialEditPassword);
              break;
            case GUI_KEY_1_GROUP:
              Length = GUI_AddAChar(_gdial_editpassword.Password, '1', GDIAL_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_PASSWORD_LENGTH)
              {
                if(gdialEditGroupIDGetUnderLineNum())
                {
                  WG_Paint(gdialEditPassword);
                  WG_SetFocus(gdialEditGroupID);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gdialEditPassword);
              break;
            case GUI_KEY_2_DIGIT:
              Length = GUI_AddAChar(_gdial_editpassword.Password, '2', GDIAL_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_PASSWORD_LENGTH)
              {
                if(gdialEditGroupIDGetUnderLineNum())
                {
                  WG_Paint(gdialEditPassword);
                  WG_SetFocus(gdialEditGroupID);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gdialEditPassword);
              break;
            case GUI_KEY_3_SIMULATE:
              Length = GUI_AddAChar(_gdial_editpassword.Password, '3', GDIAL_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_PASSWORD_LENGTH)
              {
                if(gdialEditGroupIDGetUnderLineNum())
                {
                  WG_Paint(gdialEditPassword);
                  WG_SetFocus(gdialEditGroupID);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gdialEditPassword);
              break;
            case GUI_KEY_4_LOG:
              Length = GUI_AddAChar(_gdial_editpassword.Password, '4', GDIAL_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_PASSWORD_LENGTH)
              {
                if(gdialEditGroupIDGetUnderLineNum())
                {
                  WG_Paint(gdialEditPassword);
                  WG_SetFocus(gdialEditGroupID);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gdialEditPassword);
              break;
            case GUI_KEY_5_DIRECTION:
              Length = GUI_AddAChar(_gdial_editpassword.Password, '5', GDIAL_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_PASSWORD_LENGTH)
              {
                if(gdialEditGroupIDGetUnderLineNum())
                {
                  WG_Paint(gdialEditPassword);
                  WG_SetFocus(gdialEditGroupID);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gdialEditPassword);
              break;
            case GUI_KEY_6_REMEMBER:       
              Length = GUI_AddAChar(_gdial_editpassword.Password, '6', GDIAL_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_PASSWORD_LENGTH)
              {
                if(gdialEditGroupIDGetUnderLineNum())
                {
                  WG_Paint(gdialEditPassword);
                  WG_SetFocus(gdialEditGroupID);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gdialEditPassword);
              break;
            case GUI_KEY_7_SIGN: 
              Length = GUI_AddAChar(_gdial_editpassword.Password, '7', GDIAL_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_PASSWORD_LENGTH)
              {
                if(gdialEditGroupIDGetUnderLineNum())
                {
                  WG_Paint(gdialEditPassword);
                  WG_SetFocus(gdialEditGroupID);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gdialEditPassword);
              break;
            case GUI_KEY_8_WEATHER:        
              Length = GUI_AddAChar(_gdial_editpassword.Password, '8', GDIAL_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_PASSWORD_LENGTH)
              {
                if(gdialEditGroupIDGetUnderLineNum())
                {
                  WG_Paint(gdialEditPassword);
                  WG_SetFocus(gdialEditGroupID);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gdialEditPassword);
              break;
            case GUI_KEY_9_SCAN:         
              Length = GUI_AddAChar(_gdial_editpassword.Password, '9', GDIAL_PASSWORD_LENGTH);
              if(!Length)
              {
                AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
                break;
              }
              if(Length == GDIAL_PASSWORD_LENGTH)
              {
                if(gdialEditGroupIDGetUnderLineNum())
                {
                  WG_Paint(gdialEditPassword);
                  WG_SetFocus(gdialEditGroupID);
                  AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
                  break;
                }
              }
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              WG_Paint(gdialEditPassword);
              break;
            default:
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
          }
        }
        else
        {
          switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
          {
            case GUI_KEY_STAR:
              gdialEditGroupIDSetActive(GDIAL_EDIT_ACTIVETE);
              gdialEditPasswordSetActive(GDIAL_EDIT_ACTIVETE);
              WG_Paint(gdialEditPassword);
              WG_Paint(gdialEditGroupID);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_0_CALLSHIP:
              WM_SetFocus(&callShipNumWin);
              WM_BringToTop(&callShipNumWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_2_DIGIT:
              WM_SetFocus(&digitWin);
              WM_BringToTop(&digitWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_3_SIMULATE:
              WM_BringToTop(&analogWin);
              WM_SetFocus(&analogWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_4_LOG:
              WM_SetFocus(&calogWin);
              WM_BringToTop(&calogWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_5_DIRECTION:
              WM_SetFocus(&contactsWin);
              WM_BringToTop(&contactsWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_6_REMEMBER:
              WM_SetFocus(&rememberChWin);       
              WM_BringToTop(&rememberChWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            case GUI_KEY_8_WEATHER:
              WM_SetFocus(&weatherWin);        
              WM_BringToTop(&weatherWin);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            default:
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
          }
        }
      }
      break;
    case WM_CREATE:
      GUI_SetCharUnderLine(_gdial_editpassword.Password, GDIAL_PASSWORD_LENGTH);
      break;
    case WM_FOCUS:
      if(pMsg->data_v)
      {
        GUI_SetCharUnderLine(_gdial_editpassword.Password, GDIAL_PASSWORD_LENGTH);
        WG_Paint(gdialEditPassword);
      }
      break;
    case WM_PAINT:
      if(_gdial_editpassword.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        uint8_t flash = 1;
        GUI_LCDSetS(LCD_S29, 1);
        GUI_LCDSetS(LCD_S26, 1);
        GUI_LCDSetS(LCD_S28, 1);
        if(_gdial_editpassword.Activate == GDIAL_EDIT_ACTIVETE)
        {
          if(flash && _gdial_editpassword.Password[0] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM15, _gdial_editpassword.Password[0], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM15, _gdial_editpassword.Password[0], 0);
          }
          
          if(flash && _gdial_editpassword.Password[1] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM16, _gdial_editpassword.Password[1], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM16, _gdial_editpassword.Password[1], 0);
          }
          
          if(flash && _gdial_editpassword.Password[2] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM17, _gdial_editpassword.Password[2], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM17, _gdial_editpassword.Password[2], 0);
          }
          
          if(flash && _gdial_editpassword.Password[3] == '_')
          {
            GUI_LCDSetNumChar(LCD_NUM18, _gdial_editpassword.Password[3], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM18, _gdial_editpassword.Password[3], 0);
          }
          
          if(flash)
          {
            GUI_LCDSetNumChar(LCD_NUM19, _gdial_editpassword.Password[4], 1);
            flash = 0;
          }
          else
          {
            GUI_LCDSetNumChar(LCD_NUM19, _gdial_editpassword.Password[4], 0);
          }
        }
        else
        {
          GUI_LCDSetNumChar(LCD_NUM15, '0', 0);
          GUI_LCDSetNumChar(LCD_NUM16, '0', 0);
          GUI_LCDSetNumChar(LCD_NUM17, '0', 0);
          GUI_LCDSetNumChar(LCD_NUM18, '0', 0);
          GUI_LCDSetNumChar(LCD_NUM19, '0', 0);
        }
      }
      else
      {
        GUI_LCDSetS(LCD_S29, 0);
        GUI_LCDSetS(LCD_S26, 0);
        GUI_LCDSetS(LCD_S28, 0);
        GUI_LCDSetNumChar(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM19, LCD_DISPNULL, 0);
      }
      break;
    case WM_TIMER:
      gdialEditGroupIDSetUnderline();
      gdialEditPasswordSetUnderline();
      gdialEditGroupIDSetActive(GDIAL_EDIT_UNACTIVETE);
      gdialEditPasswordSetActive(GDIAL_EDIT_UNACTIVETE);
      WG_Paint(gdialEditGroupID);
      WG_Paint(gdialEditPassword);
      WG_SetFocus(gdialEditGroupID);
      break;
  }
  return;
}


Widget* gdialEditPasswordCreate(void)
{
  WG_Create((Widget*)&_gdial_editpassword, &gdialWin, gdialEditPasswordCallback);
  gdialEditPassword = (Widget*)&_gdial_editpassword;
  return gdialEditPassword;
}

void gdialEditPasswordSetUnderline(void)
{
  GUI_SetCharUnderLine(_gdial_editpassword.Password, GDIAL_PASSWORD_LENGTH);
}

uint8_t gdialEditPasswordGetUnderlineNum(void)
{
  uint8_t length = 0;
  GUI_CharToUint32(_gdial_editpassword.Password, &length);
  return length;
}

uint8_t gdialEditPasswordSetActive(uint8_t Val)
{
  return _gdial_editpassword.Activate = Val;
}

uint32_t  gdialEditPasswordGetVal(void)
{
  return GUI_CharToUint32(_gdial_editpassword.Password, 0);
}
