#include "analogWinDlg.h"
#include "UIMessage.h"
#include "dlg.h"
#include "GUI.h"
#include "lcdControl.h"
#include "GUI_LCD.h"
#include "analog_channel.h"
#include "channel480.h"
#include "analog_channel.h"
#include "GUI_Timer.h"

typedef struct WG_AnaEditChannel{
  Widget Widget;
  char Channel[5];
  uint8_t Length;
  uint32_t Max;
}WG_AnaEditChannel;

WG_AnaEditChannel _ana_editchannel = {0};
Widget* anaEditChannel;

static void anaEditChannelCallBack(WM_Message* pMsg)
{
  WM_Message Msg;
  switch(pMsg->msgType)
  {
    case USER_MSG_CALL_IN:
    {
      GUI_DestroyTimer(&analogWin, ANA_EDIT_TIMER);
      WG_SetFocus(anaChannel);
      WG_SetInVisible(anaEditChannel);
      WG_Paint(anaEditChannel);
      WG_Paint(anaChannel);
      Msg.pTarget = &beCalledWin;
      Msg.pSource = &analogWin;
      Msg.msgType = USER_MSG_BECALLED;
      Msg.data_v = pMsg->data_v;
      Msg.data_p = pMsg->data_p;
      WM_PostMessage(&Msg);
    }
      break;
    case WM_KEY:
      if(((WM_KEY_INFO*)pMsg->data_p)->Pressed == KEY_DOWN)
      { 
        GUI_RestartTimer(&analogWin, ANA_EDIT_TIMER, EDIT_TIMEROUT_LENGTH);
        switch(((WM_KEY_INFO*)pMsg->data_p)->Key)
        {
          case GUI_KEY_0_CALLSHIP:
            if(!GUI_ChnIsAvailable480(_ana_editchannel.Channel, 0))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(GUI_AddAChar(_ana_editchannel.Channel, '0', _ana_editchannel.Length) == _ana_editchannel.Length)
            {
              GUI_DestroyTimer(&analogWin, ANA_EDIT_TIMER);
              anaChannelSetValue(GUI_CharToUint32(_ana_editchannel.Channel, 0));
              WG_SetFocus(anaChannel);
              WG_SetInVisible(anaEditChannel);
              WG_Paint(anaEditChannel);
              WG_Paint(anaChannel);
              EnterAnalog(ACH_GetCurrChannel());
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(anaEditChannel);
            break;
          case GUI_KEY_1_GROUP:
            if(!GUI_ChnIsAvailable480(_ana_editchannel.Channel, 1))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(GUI_AddAChar(_ana_editchannel.Channel, '1', _ana_editchannel.Length) == _ana_editchannel.Length)
            {
              GUI_DestroyTimer(&analogWin, ANA_EDIT_TIMER);
              anaChannelSetValue(GUI_CharToUint32(_ana_editchannel.Channel, 0));
              WG_SetFocus(anaChannel);
              WG_SetInVisible(anaEditChannel);
              WG_Paint(anaEditChannel);
              WG_Paint(anaChannel);
              EnterAnalog(ACH_GetCurrChannel());
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            WG_Paint(anaEditChannel);
            break;
          case GUI_KEY_2_DIGIT:
            if(!GUI_ChnIsAvailable480(_ana_editchannel.Channel, 2))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(GUI_AddAChar(_ana_editchannel.Channel, '2', _ana_editchannel.Length) == _ana_editchannel.Length)
            {
              GUI_DestroyTimer(&analogWin, ANA_EDIT_TIMER);
              anaChannelSetValue(GUI_CharToUint32(_ana_editchannel.Channel, 0));
              WG_SetFocus(anaChannel);
              WG_SetInVisible(anaEditChannel);
              WG_Paint(anaEditChannel);
              WG_Paint(anaChannel);
              EnterAnalog(ACH_GetCurrChannel());
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(anaEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_3_SIMULATE:
            if(!GUI_ChnIsAvailable480(_ana_editchannel.Channel, 3))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(GUI_AddAChar(_ana_editchannel.Channel, '3', _ana_editchannel.Length) == _ana_editchannel.Length)
            {
              GUI_DestroyTimer(&analogWin, ANA_EDIT_TIMER);
              anaChannelSetValue(GUI_CharToUint32(_ana_editchannel.Channel, 0));
              WG_SetFocus(anaChannel);
              WG_SetInVisible(anaEditChannel);
              WG_Paint(anaEditChannel);
              WG_Paint(anaChannel);
              EnterAnalog(ACH_GetCurrChannel());
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(anaEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_4_LOG:
            if(!GUI_ChnIsAvailable480(_ana_editchannel.Channel, 4))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(GUI_AddAChar(_ana_editchannel.Channel, '4', _ana_editchannel.Length) == _ana_editchannel.Length)
            {
              GUI_DestroyTimer(&analogWin, ANA_EDIT_TIMER);
              anaChannelSetValue(GUI_CharToUint32(_ana_editchannel.Channel, 0));
              WG_SetFocus(anaChannel);
              WG_SetInVisible(anaEditChannel);
              WG_Paint(anaEditChannel);
              WG_Paint(anaChannel);
              EnterAnalog(ACH_GetCurrChannel());
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(anaEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_5_DIRECTION:
            if(!GUI_ChnIsAvailable480(_ana_editchannel.Channel, 5))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(GUI_AddAChar(_ana_editchannel.Channel, '5', _ana_editchannel.Length) == _ana_editchannel.Length)
            {
              GUI_DestroyTimer(&analogWin, ANA_EDIT_TIMER);
              anaChannelSetValue(GUI_CharToUint32(_ana_editchannel.Channel, 0));
              WG_SetFocus(anaChannel);
              WG_SetInVisible(anaEditChannel);
              WG_Paint(anaEditChannel);
              WG_Paint(anaChannel);
              EnterAnalog(ACH_GetCurrChannel());
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(anaEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_6_REMEMBER:     
            if(!GUI_ChnIsAvailable480(_ana_editchannel.Channel, 6))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }        
            if(GUI_AddAChar(_ana_editchannel.Channel, '6', _ana_editchannel.Length) == _ana_editchannel.Length)
            {
              GUI_DestroyTimer(&analogWin, ANA_EDIT_TIMER);
              anaChannelSetValue(GUI_CharToUint32(_ana_editchannel.Channel, 0));
              WG_SetFocus(anaChannel);
              WG_SetInVisible(anaEditChannel);
              WG_Paint(anaEditChannel);
              WG_Paint(anaChannel);
              EnterAnalog(ACH_GetCurrChannel());
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(anaEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_7_SIGN: 
            if(!GUI_ChnIsAvailable480(_ana_editchannel.Channel, 7))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(GUI_AddAChar(_ana_editchannel.Channel, '7', _ana_editchannel.Length) == _ana_editchannel.Length)
            {
              GUI_DestroyTimer(&analogWin, ANA_EDIT_TIMER);
              anaChannelSetValue(GUI_CharToUint32(_ana_editchannel.Channel, 0));
              WG_SetFocus(anaChannel);
              WG_SetInVisible(anaEditChannel);
              WG_Paint(anaEditChannel);
              WG_Paint(anaChannel);
              EnterAnalog(ACH_GetCurrChannel());
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(anaEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_8_WEATHER: 
            if(!GUI_ChnIsAvailable480(_ana_editchannel.Channel, 8))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }           
            if(GUI_AddAChar(_ana_editchannel.Channel, '8', _ana_editchannel.Length) == _ana_editchannel.Length)
            {
              GUI_DestroyTimer(&analogWin, ANA_EDIT_TIMER);
              anaChannelSetValue(GUI_CharToUint32(_ana_editchannel.Channel, 0));
              WG_SetFocus(anaChannel);
              WG_SetInVisible(anaEditChannel);
              WG_Paint(anaEditChannel);
              WG_Paint(anaChannel);
              EnterAnalog(ACH_GetCurrChannel());
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(anaEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_9_SCAN:
            if(!GUI_ChnIsAvailable480(_ana_editchannel.Channel, 9))
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
              break;
            }
            if(GUI_AddAChar(_ana_editchannel.Channel, '9', _ana_editchannel.Length) == _ana_editchannel.Length)
            {
              GUI_DestroyTimer(&analogWin, ANA_EDIT_TIMER);
              anaChannelSetValue(GUI_CharToUint32(_ana_editchannel.Channel, 0));
              WG_SetFocus(anaChannel);
              WG_SetInVisible(anaEditChannel);
              WG_Paint(anaEditChannel);
              WG_Paint(anaChannel);
              EnterAnalog(ACH_GetCurrChannel());
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
              break;
            }
            WG_Paint(anaEditChannel);
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          case GUI_KEY_DEL:
          case GUI_KEY_STAR:
          {
            uint8_t Length;
            GUI_CharToUint32(_ana_editchannel.Channel, &Length);
            if(Length != _ana_editchannel.Length)
            {
              GUI_SetCharUnderLine(_ana_editchannel.Channel, _ana_editchannel.Length);
              WG_Paint(anaEditChannel);
              AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            }
            else
            {
              AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            }
          }
            break;
          case GUI_KEY_BACK:
            GUI_DestroyTimer(&analogWin, ANA_EDIT_TIMER);
            WG_SetFocus(anaChannel);
            WG_SetInVisible(anaEditChannel);
            WG_Paint(anaEditChannel);
            WG_Paint(anaChannel);
            EnterAnalog(ACH_GetCurrChannel());
            AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL,AUDIO_PLAY_TIMES_KEY_NORMAL);
            break;
          default:
            AudioPlayCnt(AUDIO_TYPE_KEY_UNNORMAL,AUDIO_PLAY_TIMES_KEY_UNNORMAL);
            break;
        }
      }
      break;
    case WM_CREATE:
      _ana_editchannel.Length = 3;
      _ana_editchannel.Max = 480;
      GUI_SetCharUnderLine(_ana_editchannel.Channel, _ana_editchannel.Length);
      break;
    case WM_FOCUS:
      GUI_SetCharUnderLine(_ana_editchannel.Channel, _ana_editchannel.Length);
      break;
    case WM_PAINT:
    {
      uint8_t flash = 1;
      if(_ana_editchannel.Widget.States >= WG_VISIBLE && pMsg->data_v)
      {
        GUI_LCDSetNumChar(LCD_NUM14, 'C', 0);
        GUI_LCDSetNumChar(LCD_NUM15, 'H', 0);
        GUI_LCDSetNumChar(LCD_NUM16, '-', 0);
        GUI_LCDSetNumChar(LCD_NUM17, LCD_DISPNULL, 0);
        if(flash && _ana_editchannel.Channel[0] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM18, _ana_editchannel.Channel[0], 1);
          flash = 0;
        }
        else
          GUI_LCDSetNumChar(LCD_NUM18, _ana_editchannel.Channel[0], 0);
        
        if(flash && _ana_editchannel.Channel[1] == '_')
        {
          GUI_LCDSetNumChar(LCD_NUM19, _ana_editchannel.Channel[1], 1);
          flash = 0;
        }
        else
          GUI_LCDSetNumChar(LCD_NUM19, _ana_editchannel.Channel[1], 0);
        
        if(flash)
        {
          GUI_LCDSetNumChar(LCD_NUM20, _ana_editchannel.Channel[2], 1);
          flash = 0;
        }
        else
          GUI_LCDSetNumChar(LCD_NUM20, _ana_editchannel.Channel[2], 0);
        
      }
      else
      {
        GUI_LCDDisableNumFlash();
        GUI_LCDSetNumChar(LCD_NUM14, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM15, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM16, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM17, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM18, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM19, LCD_DISPNULL, 0);
        GUI_LCDSetNumChar(LCD_NUM20, LCD_DISPNULL, 0);
      }  
    case WM_TIMER:
      if(pMsg->data_v == ANA_EDIT_TIMER)
      {
        WG_SetFocus(anaChannel);
        WG_SetInVisible(anaEditChannel);
        WG_Paint(anaEditChannel);
        WG_Paint(anaChannel);
        EnterAnalog(ACH_GetCurrChannel());
      }
      break;
    }      
      break;
  }
}


Widget* anaEditChannelCreate(void)
{
  WG_Create((Widget*)&_ana_editchannel, &analogWin, anaEditChannelCallBack);
  anaEditChannel = (Widget*)&_ana_editchannel;
  return anaEditChannel;
}