#include "UIMessage.h"
#include "dlg.h"
#include "mainWinDlg.h"
#include "GUI_LCD.h"

typedef struct WG_Menu{
  Widget widget;
  uint16_t value;
}WG_Menu;

WG_Menu _menu = {0};

Widget *menu;

static void menuCallback(WM_Message *pMsg)
{
  switch(pMsg->msgType)
  {
    case WM_CREATE:
      Menu_SetValue(LCD_S70);
      break;
    case WM_FOCUS:
      break;
    case WM_PAINT:
      if(_menu.widget.States == WG_VISIBLE)
      {
        GUI_LCDSetS(_menu.value, 1);
      }
      else
      {
        GUI_LCDSetS(_menu.value, 0);
      }
      break;
  }
  return;
}


Widget* menuCreate(void)
{
  WG_Create((Widget*)&_menu, &mainWin, menuCallback);
  menu = (Widget*)&_menu;
  return menu;
}

void Menu_SetValue(uint16_t value)
{
  _menu.value = value;
}




