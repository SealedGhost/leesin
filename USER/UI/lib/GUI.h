#ifndef _GUI_H_
#include "stdio.h"
#include "stdint.h"

#define GUI_KEY_NUM 10

#define GUI_KEY_1_GROUP             0x20
#define GUI_KEY_2_DIGIT             0x21
#define GUI_KEY_3_SIMULATE          0x22
#define GUI_KEY_4_LOG               0x23
#define GUI_KEY_5_DIRECTION         0x24
#define GUI_KEY_6_REMEMBER          0x25
#define GUI_KEY_7_SIGN              0x26
#define GUI_KEY_8_WEATHER           0x27
#define GUI_KEY_9_SCAN              0x28
#define GUI_KEY_0_CALLSHIP          0x29
#define GUI_KEY_STAR                0x30
#define GUI_KEY_DEL                 0x31
#define GUI_KEY_PICKUP              0x32
#define GUI_KEY_HANGUP              0x33
#define GUI_KEY_BACK                0x34
#define GUI_KEY_ENTER               0x35
#define GUI_KEY_UP                  0x36
#define GUI_KEY_DOWN                0x37
#define GUI_KEY_NEW                 0x38
#define GUI_KEY_PTTDOWN             0x39
#define GUI_KEY_PTTUP               0x40
#define GUI_KEY_BKLINGHT						0x41


enum KEY_STATE{
	KEY_DOWN = 2,
	KEY_UP = 1
};


typedef struct WM_KEY_INFO{
  int Key;
  int Pressed;
}WM_KEY_INFO;

void GUI_KeyInit(void);
void GUI_StoreKeyMsg(uint8_t Key, uint8_t Pressed);
void GUI_SendKeyMsg(void);
void GUI_CleanKey(void);

uint8_t GUI_CharToUint8(char *ch, uint8_t* UnderLine);
void GUI_Uint8ToChar(uint8_t num, char *p, uint8_t length);

uint16_t GUI_CharToUint16(char *ch, uint8_t* UnderLine);
void GUI_Uint16ToChar(uint16_t num, char *p, uint8_t length);

uint32_t GUI_CharToUint32(char *ch, uint8_t* UnderLine);
void GUI_Uint32ToChar(uint32_t num, char *p, uint8_t length);

uint8_t GUI_AddAChar(char *pchhead, char ch, uint8_t length);

void GUI_SetCharUnderLine(char *Ch, uint8_t Length);

uint8_t GUI_ChnIsAvailable480(char* Ch, uint8_t Add);
#endif

