#ifndef _WINDOWMANAGER_H_
#define _WINDOWMANAGER_H_
#include "Window.h"
#include "stdio.h"

void WM_Init(void);
void WM_Create(Window* pWindow, CallBackfun callback);
void WM_AddMyWidget(Window* pWindow, WidgetList* pWidgetList, Widget** pWidget, uint8_t num);
void WM_BringToTop(Window* pWindow);
void WM_BringToBottom(Window* pWindow);
void WM_SetFocus(Window* pWindow);
Window* WM_GetFocus(void);
void WM_Paint(Window* pWindow, uint8_t val);

void WM_CleanScreen(void);
#endif
