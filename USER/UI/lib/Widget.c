#include "Widget.h"
#include "UIMessage.h"

/** 
 * @brief 初始化窗口的控件链表
 * @param pWidgetList 链表的头指针
 * @param pWidget 窗口的控件头指针
 * @return 返回链表的头指针
 */
WidgetList* WG_InitList(WidgetList *pWidgetList, Widget** pWidget,uint8_t num)
{
  if(!pWidgetList)
  {
    return 0;
  }
  if(num > 1)
  {
    for(int i = 0; i < num-1; i++)
    {
      pWidgetList[i].pNext = &pWidgetList[i+1];
      pWidgetList[i].pWidget = pWidget[i];
    }
    pWidgetList[num-1].pNext = NULL;
    pWidgetList[num-1].pWidget = pWidget[num-1];
    
    for(int i = num-1; i > 0; i--)
    {
      pWidgetList[i].pPrev = &pWidgetList[i-1];
    }
    pWidgetList[0].pPrev = 0;
  }
  else
  {
    pWidgetList->pNext = NULL;
    pWidgetList->pPrev = NULL;
    pWidgetList->pWidget = *pWidget;
  }
  return pWidgetList;
}

/** 
 * @brief 返回控件的链表指针
 * @param pWidget 控件指针
 * @return 控件的链表指针
 * @note 调用之前请初始化，并指定其父窗口
 */
WidgetList* WL_FindWidget(Widget* pWidget)
{
  WidgetList *p = pWidget->Parent->pMyWidgetList;
  while(p)
  {
    if(p->pWidget == pWidget)
    {
      break;
    }
    p = p->pNext;
  }
  return p;
}

/** 
 * @brief 把控件调到其父窗口控件链表的头
 * @param 控件指针
 * @note  得到焦点时会操作
 */
void WL_CarryToHead(Widget* pWidge)
{
  WidgetList* p = WL_FindWidget(pWidge);
  
  if(!p)
  {
    return;
  }
  
  if(!p->pPrev)
    return;
  
  if(p->pNext && p->pPrev)
  {
    p->pNext->pPrev = p->pPrev;
    p->pPrev->pNext = p->pNext;
  }
  
  if(!p->pNext && p->pPrev)
  {
    p->pPrev->pNext = NULL;
  }
  
  p->pWidget->Parent->pMyWidgetList->pPrev = p;
  p->pNext = p->pWidget->Parent->pMyWidgetList;
  p->pWidget->Parent->pMyWidgetList = p;
  p->pPrev = 0;
  return;
}

void WL_CarryAToBPrev(Widget *pWidgetA, Widget *pWidgetB)
{
  WidgetList* pA = WL_FindWidget(pWidgetA);
  WidgetList* pB = WL_FindWidget(pWidgetB);
  WidgetList* p = pA->pPrev;
  
  if(!pA && !pB)
  {
    return;
  }
  
  if(pWidgetA->Parent->pMyWidgetList->pWidget == pWidgetB || pWidgetA->Parent->pMyWidgetList->pWidget == pWidgetA)
    return;
  
  while(p)
  {
    if(p->pWidget == pWidgetB)
    {
      if(pA->pNext)
      {
        pA->pNext->pPrev = pA->pPrev;
        pA->pPrev->pNext = pA->pNext;
      }
      else
      {
        p->pPrev->pNext = NULL;
      }
      pA->pNext = pB;
      pA->pPrev = pB->pPrev;
      
      pB->pPrev->pNext = pA;
      pB->pPrev = pA;
      return;
    }
    p = p->pPrev;
  }
  return;
}

uint8_t WL_CarryToButtom(Widget *pWidget)
{
  WidgetList* q = WL_FindWidget(pWidget);
  WidgetList* p = q;
  
  if(!q)
  {
    return 0;
  }
  
  if(q->pNext && !q->pPrev) 
  {
    q->pNext->pPrev = NULL;
    q->pWidget->Parent->pMyWidgetList = q->pNext;
  } 
  
  if(q->pNext && q->pPrev)
  {
    q->pNext->pPrev = q->pPrev;
    q->pPrev->pNext = q->pNext;
  }
  
  while(p)
  {
    p = p->pNext;
  }
  
  q->pPrev = p;
  p->pNext = q;

  q->pNext = NULL;
  return 1;
}

/** 
 * @brief 创建窗口中的控件，设置控件的父窗口和回调，发送WM_CREATE消息
 * @param pWidget 控件的指针
 * @param pWindow 父窗口的指针
 * @param callback 控件的回调
 * @return 成功返回1，失败返回0
 */
uint8_t WG_Create(Widget* pWidget, Window* pWindow, CallBackfun callback)
{
  WM_Message msg;
  
  if(!pWidget) 
  {
    return 0;
  }
  pWidget->CallBack = callback;
  pWidget->Parent = pWindow;
  
  msg.msgType = WM_CREATE;
  msg.data_v = 1;
  msg.data_p = 0;
  callback(&msg);
  return 1;
}

/** 
 * @brief 设置控件的状态
 * @param pWidget 控件的指针
 * @param states 控件的状态
 * @return 成功返回1，失败返回0
 */
uint8_t WG_SetStates(Widget* pWidget, uint8_t states)
{
  if(!pWidget) 
  {
    printf("pWidget == null");
    return 0;
  }
  pWidget->States = states;
  return 1;
}

/** 
 * @brief 设置控件可见
 * @param pWidget 控件的指针
 * @return 成功返回1，失败返回0
 */
uint8_t WG_SetVisible(Widget *pWidget)
{
  
  if(WG_SetStates(pWidget, WG_VISIBLE))
  {
    return 1;
  }
  return 0;
}

/** 
 * @brief 设置控件不可见
 * @param pWidget 控件的指针
 * @return 成功返回1，失败返回0
 */
uint8_t WG_SetInVisible(Widget *pWidget)
{
  WM_Message msg;
  if(WG_SetStates(pWidget, WG_INVISIBLE))
  {
    return 1;
  }
  return 0;
}

/** 
 * @brief 设置控件得到焦点
 *
 * @param pWidget 控件指针
 * @return 成功返回1，失败返回0
 */
uint8_t WG_SetFocus(Widget* pWidget)
{
  WM_Message msg;
  WidgetList* pWidgetList = pWidget->Parent->pMyWidgetList;
  
  if(pWidgetList->pWidget == pWidget && pWidget->States == WG_FOCUSED)
    return 1;
  
  if(pWidgetList->pWidget != pWidget)
  {
    WL_CarryToHead(pWidget);
    if(pWidgetList->pWidget->States == WG_FOCUSED)
    {
      WM_Message msg;
      msg.msgType = WM_FOCUS;
      msg.data_v = 0;
      msg.data_p = 0;
      WG_SetVisible(pWidgetList->pWidget);
      pWidgetList->pWidget->CallBack(&msg);
    }
    
  }
  
  if(WG_SetStates(pWidget, WG_FOCUSED))
  {
    msg.msgType = WM_FOCUS;
    msg.data_v = 1;
    msg.data_p = 0;
    pWidget->CallBack(&msg);
    return 1;
  }
  return 0;
}

/** 
 * @brief 设置当前焦点的控件的下个控件得到焦点
 *
 * @param pWidget 控件指针
 * @return 成功返回1，失败返回0
 */
uint8_t WG_SetNextFocus(Window* pWindow)
{
  if(pWindow->pMyWidgetList && pWindow->pMyWidgetList->pNext)
  {
    Widget *pWidget =  pWindow->pMyWidgetList->pNext->pWidget;
    WG_SetFocus(pWidget);
    return 1;
  }
  return 0;
}
/** 
 * @brief 重绘控件
 * @param pWidget 控件指针
 * @return 成功返回1，失败返回0
 */
uint8_t WG_Paint(Widget *pWidget)
{
  WM_Message msg;
  
  if(!pWidget)
  {
    return 0;
  }
  
  msg.msgType = WM_PAINT;
  msg.data_v = 1;
  
  pWidget->CallBack(&msg);
  return 1;
}

uint8_t WG_GetStates(Widget *pWidget)
{
  return pWidget->States;
}

void WG_CarryAToBPrev(Widget* pWidgetA, Widget* pWidgetB)
{
  WL_CarryAToBPrev(pWidgetA, pWidgetB);
  return;
}
