#include "UIMessage.h"


MessageList messageList[MESSAGE_NUM];

MessageList* pAdd;
MessageList* pDec;

/** 
 * @brief 消息链表的初始化
 * @note  在UI任务开始时必须初始化
 */
void MessageInit(void)
{
  for(int i = 0; i < MESSAGE_NUM-1; i++)
  {
    messageList[i].pNext = &messageList[i+1];
  }
  messageList[MESSAGE_NUM-1].pNext = messageList;
  pDec = messageList;
  pAdd = pDec->pNext;
}

/** 
 * @brief   从消息链表中取出一个消息，没有消息就返回空
 * @return  返回一个消息类型的指针WM_Message*
 */
WM_Message* PeekMessage(void)
{
  MessageList *p;
  if(pDec->pNext != pAdd) 
  {
    p = pDec->pNext;
    pDec = pDec->pNext;
    return &p->message;
  }
  return 0;
}

/** 
 * @brief   发送消息，通过消息队列传消息
 * @param   pMsg 要发送消息结构体的指针
 * @return  发送成功返回1，发送失败返回0
 */
uint8_t WM_PostMessage(WM_Message* pMsg)
{
  if(pAdd->pNext != pDec)
  {
    pAdd->message.data_p = pMsg->data_p;
    pAdd->message.data_v = pMsg->data_v;
    pAdd->message.msgType = pMsg->msgType;
    pAdd->message.pTarget = pMsg->pTarget;
    pAdd->message.pSource = pMsg->pSource;
    pAdd = pAdd->pNext;
    return 1;
  }
  return 0;
}

/** 
 * @brief   发送消息，直接调用回调
 * @param   pMsg 发送消息的结构体指针
 * @note    相当于函数调用
 */
void WM_SendMessage(WM_Message* pMsg)
{
  if(pMsg->pTarget && pMsg->pTarget->CallBack)
    pMsg->pTarget->CallBack(pMsg);
  return;
}

/** 
 * @brief 消息的默认处理
 * @param pMsg 需要处理的消息
 * @note  将发送给窗口的消息，传给其控件
 */
void WM_Defaultproc(WM_Message* pMsg)
{
  WidgetList* pWidgetList;
  WidgetList* pWidgetListTail;
  
  if(!pMsg->pTarget)
    return;
  if(!pMsg->pTarget->pMyWidgetList)
    return;
  
  switch(pMsg->msgType)
  {
    case WM_PAINT:
      pWidgetList = pMsg->pTarget->pMyWidgetList;
      while(pWidgetList)
      {
        if(pWidgetList->pNext == NULL)
        {
          pWidgetListTail = pWidgetList;
          break;
        }
        pWidgetList = pWidgetList->pNext;
      }
      while(pWidgetListTail)
      {
        if(pWidgetListTail->pWidget->States >= WG_VISIBLE)
          pWidgetListTail->pWidget->CallBack(pMsg);
        pWidgetListTail = pWidgetListTail->pPrev;
      }
      break;
    case WM_KEY:
      if(pMsg->pTarget->pMyWidgetList->pWidget->States == WG_FOCUSED)
        pMsg->pTarget->pMyWidgetList->pWidget->CallBack(pMsg);
      break;
    default:
      if(pMsg->pTarget->pMyWidgetList->pWidget->States == WG_FOCUSED)
        pMsg->pTarget->pMyWidgetList->pWidget->CallBack(pMsg);
      break;
  }
  return;
}

