#ifndef _WM_H_
#define _WM_H_

#define WM_CREATE         1
#define WM_PAINT          2
#define WM_FOCUS          3
#define WM_KEY            4
#define WM_TIMER          5


#define WM_USER           100

#define USER_MSG_MENUNUM          (WM_USER + 0X01)
#define USER_MSG_LOGTOCONTACTS    (WM_USER + 0X02)
#define USER_MSG_MAINTIMEVISIBLE  (WM_USER + 0X03)
#define USER_MSG_DIAL             (WM_USER + 0X04)
#define USER_MSG_DIALTALKING      (WM_USER + 0X05)
#define USER_MSG_BECALLED         (WM_USER + 0X06) //except bacallwin to becallwin
#define USER_MSG_SLSETSHIPNUM     (WM_USER + 0X07)

/** 来电消息 */
#define USER_MSG_CALL_IN          (WM_USER + 0x10)

//#define USER_MSG_CALL_IN_SUBSEQUENT (WM_USER + 0x11)

/** 对方响铃消息 */
#define USER_MSG_RING             (WM_USER + 0x12)

/** 对方接听消息 */
#define USER_MSG_CONNECT          (WM_USER + 0x13)

/** 对方挂断消息 */
#define USER_MSG_HUNG_UP          (WM_USER + 0x14)

/** 对方信道忙消息 */
#define USER_MSG_TELL_BUSY        (WM_USER + 0x15)

/** 呼叫失败消息 */
#define USER_MSG_CALL_FAIL        (WM_USER + 0x16)

/** 群组有群消息 */
#define USER_MSG_GRP_SING         (WM_USER + 0x20)

/** 群组静默 */
#define USER_MSG_GRP_MUTE         (WM_USER + 0x21)

/** 群频道推荐 */
#define USER_MSG_CHAN_RECOMMEND   (WM_USER + 0x22)

/** 船呼应答 */
#define USER_MSG_BOAT_INFO_RESPONSE   (WM_USER + 0x40)


/** 系统时间秒滴答消息 */
#define USER_MSG_SECOND_TICK          (WM_USER + 0x50)


/** RF 接收消息 */
#define USER_MSG_NOTIFY_RFRX          (WM_USER + 0x60)
/** RF 发射消息 */
#define USER_MSG_NOTIFY_RFTX          (WM_USER + 0x61)

/** 接收的RSSI强度更新 */
#define USER_MSG_NOTIFY_RSSI_UPDATE   (WM_USER + 0x62)

#endif

