#include "Window.h"

/** 
 * @brief 设置窗口的回调函数
 * @param pWindow 窗口的指针
 * @param callback 窗口的回调指针
 * @return 成功返回1，失败返回0
 */
uint8_t WD_SetCallback(Window *pWindow, CallBackfun callback)
{
  if(!pWindow)
  {
    return 0;
  }
  pWindow->CallBack = callback;
  return 1;
}

/** 
 * @brief 设置窗口的控件链表
 * @param pWindow 窗口的指针
 * @param pWidgetList 控件的链表头指针
 * @return 成功返回1，失败返回0
 */
uint8_t WD_SetMyWidgetList(Window* pWindow, WidgetList* pWidgetList)
{
  if(!pWindow)
  {
    return 0;
  }
  pWindow->pMyWidgetList = pWidgetList;
  return 1;
}

//uint8_t WD_SetStates(Window* pWindow, uint8_t states)
//{
//  if(!pWindow)
//  {
//    printf("pWindow == null");
//    return 0;
//  }
//  pWindow->states = states;
//  return 1;
//}

  
