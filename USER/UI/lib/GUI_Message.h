/*************************************************************
 * @file               GUI_Message.h       
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	
	#ifndef _GUI_MESSAGE_H
	#define _GUI_MESSAGE_H
	
	#include <stdint.h>
	#include <stdbool.h>
	
	/** @addtogroup   GUI消息
 *   
 *   @{
 */


/** GUI消息的最大数量  */
 #define GUI_MESSAGE_MAX_NUMBER           20

	
	/** 按键消息 */
	#define GUI_MESSAGE_KEY                  0x01  
	
	/** 定时器消息 */
	#define GUI_MESSAGE_TIMER                0x02
	
	/** 任务间通信消息 */
	#define GUI_MESSAGE_IPC                  0x03
	
	/**   系统时间消息 */
	#define GUI_MESSAGE_SYSTEM_TIME           0x04               
	
	

	/** GUI消息结构体 */
	typedef struct{
		 uint16_t  type;  /**<消息类型  */
		 uint16_t  id;    /**<消息ID */
		 int       v;     /**<消息参数v */		
		 uintptr_t p;     /**<消息参数p */
	}GUI_Message;
	
	
	
	
	#ifdef __cplusplus
  extern "C" {
	#endif
			

	void GUI_PostMessage(uint16_t type, uint16_t id, int v, uintptr_t p);
 bool GUI_PeekMessage(GUI_Message* pMsg);

	#ifdef __cplusplus
		}
	#endif




/** @} */
	


	
	
	
	#endif
	