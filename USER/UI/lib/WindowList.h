#ifndef _WINDOWLIST_H_
#include "Window.h"
#include "stdio.h"
#include "stdint.h"

#define WINDOW_NUM           20

typedef struct WindowList
{
  Window* pWindow;
  struct WindowList* pNext;
  struct WindowList* pPrev;
}WindowList;

extern WindowList windowList[WINDOW_NUM];
extern WindowList *pWindowHead;
extern WindowList *pWindowTail;

void initWindowList(void);


uint8_t addWindowToHead(Window *p);
uint8_t carryToHead(Window *p);
uint8_t carryToBottom(Window *p);

uint8_t isWindowHead(Window *p);
uint8_t isWindowTail(Window *p);

WindowList* FindWindow(Window *p);
uint8_t getWindowNum(void);

Window* getWindowHead(void);
Window* getWindowTail(void);
#endif
