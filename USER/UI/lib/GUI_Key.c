#include "GUI.h"
#include "UIMessage.h"
#include "WindowManager.h"

typedef struct KeyList{
  WM_KEY_INFO KeyBuffer;
  struct KeyList* pNext;
}KeyList;


KeyList *keyListAdd, *keyListDec;
KeyList keylist[GUI_KEY_NUM];

void KeyListInit(void)
{

  for(uint8_t i = 0; i < GUI_KEY_NUM - 1; i++)
  {
    keylist[i].pNext = &keylist[i + 1];
  }
  keylist[GUI_KEY_NUM-1].pNext = &keylist[0];
  
  keyListDec = keylist;
  keyListAdd = keyListDec->pNext;
  return;
}

void KeyListAdd(uint8_t Key, uint8_t Pressed)
{
  if(keyListAdd->pNext != keyListDec)
  {
    keyListAdd->KeyBuffer.Key = Key;
    keyListAdd->KeyBuffer.Pressed = Pressed;
    keyListAdd = keyListAdd->pNext;
    return;
  }
  return;
}

WM_KEY_INFO* KeyListGetKey(void)
{
  KeyList *p;
  
  if(keyListDec->pNext != keyListAdd)
  {
    p = keyListDec;
    keyListDec = keyListDec->pNext;
    return &p->pNext->KeyBuffer;
  }
  return 0;
}

void GUI_KeyInit(void)
{
  KeyListInit();
  return;
}

void GUI_StoreKeyMsg(uint8_t Key, uint8_t Pressed)
{
  KeyListAdd(Key, Pressed);
  return;
}

void GUI_SendKeyMsg(void)
{
  WM_KEY_INFO* p = KeyListGetKey();
  WM_Message Msg;
  
  while(p)
  {
    Msg.pTarget = WM_GetFocus();
    Msg.msgType = WM_KEY;
    Msg.data_p = (uintptr_t*)p;
    Msg.data_v = 0;
    WM_PostMessage(&Msg);
    p = KeyListGetKey();
  }
  return;
}

void GUI_CleanKey()
{
	keyListAdd = keyListDec->pNext;
	return;
}




















