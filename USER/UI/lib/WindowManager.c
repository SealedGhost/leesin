#include "WindowManager.h"
#include "WindowList.h"
#include "UIMessage.h"

#include "lcdControl.h"

Window* pWindowFocused = NULL;

/** 
 * @brief 窗口管理器初始化，初始化了窗口的链表
 * @see  initWindowList()
 */
void WM_Init()
{ 
 initWindowList();
}

/** 
 * @brief 窗口创建函数
 * @param pWindow 窗口结构体的指针
 * @param callback 回调函数指针
 */
void WM_Create(Window* pWindow, CallBackfun callback)
{
  WM_Message msg;

  pWindow->CallBack = callback;
  addWindowToHead(pWindow);
  
  msg.msgType = WM_CREATE;
  msg.pTarget = pWindow;
  
  callback(&msg);
}

/** 
 * @brief 添加窗口的控件
 * @param pWindow 窗口指针
 * @param pWidgetList 控件链表的头指针
 * @param pWidget 控件的头指针
 * @param num 控件的数量
 * @see  WidgetList* WG_InitList(WidgetList *pWidgetList, Widget* pWidget,uint8_t num) uint8_t WD_SetMyWidgetList(Window* pWindow, WidgetList* pWidgetList)
 */
void WM_AddMyWidget(Window* pWindow, WidgetList* pWidgetList, Widget** pWidget, uint8_t num)
{
  WG_InitList(pWidgetList, pWidget, num);
  WD_SetMyWidgetList(pWindow, pWidgetList);
}

/** 
 * @brief 将窗口调到最前面
 * @param pWindow 窗口的指针
 */
void WM_BringToTop(Window* pWindow)
{ 
  if(getWindowHead())
  {
    WM_Paint(getWindowHead(), 0);
  }
  if(carryToHead(pWindow))
  {
    WM_Paint(pWindow, 1);
  }
  return;
}

/** 
 * @brief 将窗口调到最后面
 * @param pWindow 窗口的指针
 */
void WM_BringToBottom(Window* pWindow)
{
  //WM_CleanScreen();
  if(carryToBottom(pWindow))
  {
    WM_Paint(pWindow, 0);
  }
  if(getWindowHead())
  {
    WM_Paint(getWindowHead(), 1);
  }
  return;
}

/** 
 * @brief使谴翱诘玫浇沟�
 * @param pWindow 窗口的指针
 */
void WM_SetFocus(Window* pWindow)
{
  WM_Message msg;
  
  if(pWindowFocused)
  {
    pWindowFocused->states = WM_NULL;
    msg.pTarget = pWindowFocused;
    msg.msgType = WM_FOCUS;
    msg.data_v = 0;
    WM_SendMessage(&msg);
  }
  pWindow->states = WM_FOCUSED;
  msg.pTarget = pWindow;
  msg.msgType = WM_FOCUS;
  msg.data_v = 1;
  WM_SendMessage(&msg);
  pWindowFocused = pWindow;
  return;
}

/** 
 * @brief 获得得到焦点的窗口
 * @return 返回获得焦点的窗口指针 没有就返回空
 */
Window* WM_GetFocus(void)
{
  return pWindowFocused;
}

/** 
 * @brief 重绘窗口
 * @param pWindow 窗口的指针
 */
void WM_Paint(Window* pWindow, uint8_t val)
{
  WM_Message msg;
  msg.pTarget = pWindow;
  msg.msgType = WM_PAINT;
  msg.data_v = val;
  msg.pTarget->CallBack(&msg);
  return;
}

void WM_CleanScreen()
{
  LCDCleanScreen();
}
