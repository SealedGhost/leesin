#ifndef _WIDGET_H_
#define _WIDGET_H_
#include "stdio.h"
#include "stdint.h"
#include "callback.h"

#define WG_INVISIBLE 0x00
#define WG_VISIBLE   0x01
#define WG_FOCUSED   0x02

typedef struct Window Window;

typedef struct Widget{
  CallBackfun CallBack;
  Window* Parent;
  uint8_t States;
}Widget;

typedef struct WidgetList{
  Widget* pWidget;
  struct WidgetList* pNext;
  struct WidgetList* pPrev;
}WidgetList;

WidgetList* WG_InitList(WidgetList *pWidgetList, Widget** pWidget, uint8_t num);

uint8_t WG_Create(Widget* pWidget, Window* pWindow, CallBackfun callback);

uint8_t WG_SetVisible(Widget *pWidget);
uint8_t WG_SetInVisible(Widget *pWidget);
uint8_t WG_SetFocus(Widget* pWidget);
uint8_t WG_Paint(Widget *pWidget);

uint8_t WG_SetNextFocus(Window* pWindow);

uint8_t WG_GetStates(Widget *pWidget);

void WG_CarryAToBPrev(Widget* pWidgetA, Widget* pWidgetB);
#endif
