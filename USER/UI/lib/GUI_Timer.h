/*************************************************************
 * @file  timer.h
 * @brief
 *
 *
 * @version
 * @author
 * @data
 *************************************************************/
 
 #ifndef _LXY_TIMER_H
 #define _LXY_TIMER_H
 
 #include <stdint.h>
 
 #include "callback.h"
#include "Window.h"
  
#define OS_TICK_PER_GUI_PERIOD   50

#define SCAN_TIMER_LENGTH 3
#define EDIT_TIMEROUT_LENGTH (45 * 1000 / 50)

typedef void (*GUITimerCallback)(uint8_t id);
 
 
 typedef struct{
	 uint8_t       id;    /**<  定时器ID*/
	 uint8_t       state; /**<  定时器状态 : 0表示休眠，非0表示激活*/
	 uint16_t      span;  /**<  定时时间，单位为GUI的一个Tick*/
   Window*     Window;
 }Timer;
 
/**@brief GUI的每个Tick执行一次
 * 
 *
 * @return void
 * @note
 */
void GUI_OnTick(void);
	
int GUI_CreateTimer(Window* pWindow, uint16_t id,  uint16_t span);
void GUI_DestroyTimer(Window* pWindow, uint16_t id);
int GUI_RestartTimer(Window* pWindow, uint16_t id, uint16_t span);
 
 
 
 
 #endif
 
 