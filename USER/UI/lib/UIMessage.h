#ifndef _UIMESSAGE_H_
#define _UIMESSAGE_H_
#include "stdint.h"
#include "window.h"
#include "WM.h"

#define MESSAGE_NUM           16

typedef struct WM_Message{
  Window*       pSource;
  Window*       pTarget;
  uint32_t      msgType;
  int           data_v;
  uintptr_t*    data_p;
}WM_Message;

typedef struct MessageList{
  WM_Message message;
  struct MessageList* pNext;
}MessageList;

void MessageInit(void);
WM_Message* PeekMessage(void);
uint8_t WM_PostMessage(WM_Message* pMsg);
void WM_SendMessage(WM_Message* pMsg);

void WM_Defaultproc(WM_Message* pMsg);

#endif



