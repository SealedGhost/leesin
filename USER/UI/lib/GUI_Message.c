/*************************************************************
 * @file                   GUI_Message.c       
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	
	#include "GUI_message.h"
	#include "string.h"
    #include <stdio.h>
    #include "dac.h"
    #include "audio_mgr.h"
	
#include <uCOS-II\Source\ucos_ii.h>
	
	/** @addtogroup      GUI消息
 *   
 *   @{
 */



static GUI_Message GUIMsgQueue[GUI_MESSAGE_MAX_NUMBER]  = {0};
static uint8_t inCursor  = 0;
static uint8_t outCursor  = 0;


/**@brief   向GUI的消息队列中加入一条消息
  * 
  * @param type 消息类型
  * @param id   消息id
  * @param v    消息v参数
  * @param p    消息p参数
  * @return void
  * @note
  */
void GUI_PostMessage(uint16_t type, uint16_t id, int v, uintptr_t p)
{

#ifdef USE_CRITICAL_WHEN_POST    
    
#if OS_CRITICAL_METHOD == 3u	 
	 OS_CPU_SR cpu_sr  = 0u;
#endif	

	OS_ENTER_CRITICAL();
#endif
	
	GUIMsgQueue[inCursor].type  = type;
	GUIMsgQueue[inCursor].id    = id;
	GUIMsgQueue[inCursor].v     = v;
	GUIMsgQueue[inCursor].p     = p;
	inCursor  = (inCursor + 1) % GUI_MESSAGE_MAX_NUMBER;
    
    if(inCursor == outCursor){
       Audio_TurnOn(SpkOnwer_Tone);
       DAC_UpdateValue(LPC_DAC,1000);
    }

#ifdef USE_CRITICAL_WHEN_POST 	
	OS_EXIT_CRITICAL();

#endif	
	
	
//	 static uint8_t Ingest  = 0;
//	 static uint8_t CursorOffset  = 0;
	

	 
//	
//	 uint8_t tmpCursor;
//	
//	 Ingest++;
//	 tmpCursor  = (inCursor + Ingest - 1) % GUI_MESSAGE_MAX_NUMBER;
//	 
//	 GUIMsgQueue[tmpCursor].type  = type;
//	 GUIMsgQueue[tmpCursor].id    = id;
//	 GUIMsgQueue[tmpCursor].v     = v;
//	 GUIMsgQueue[tmpCursor].p     = p;
//	
//  if(Ingest > CursorOffset) {
//			 CursorOffset  = Ingest;
//		}
//		
//		
//		
//		if(Ingest == 1){
//			 inCursor  = (inCursor + CursorOffset) % GUI_MESSAGE_MAX_NUMBER;
//			 CursorOffset  = 0;
//		}
//	
//	 Ingest--;
	// inCursor  = (inCursor +1) % GUI_MESSAGE_MAX_NUMBER;
}


/**@brief   窥探GUI消息队列中是否存在消息
  * 
  * 
  * @return 存在消息返回true，否则返回false
  * @note
  */
bool GUI_PeekMessage(GUI_Message* pMsg)
{
	 if(outCursor != inCursor){
			 memcpy(pMsg,&(GUIMsgQueue[outCursor]), sizeof(GUI_Message));
			 outCursor  = (outCursor +1) % GUI_MESSAGE_MAX_NUMBER;
#ifndef NO_PEEK_PROC         
			 return true;
#endif         
		}
		
		return false;
}




/** @} */