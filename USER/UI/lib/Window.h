#ifndef _WINDOW_H_
#define _WINDOW_H_
#include "stdint.h"
#include "callback.h"
#include "Widget.h"

#define WM_NULL       0x00
#define WM_FOCUSED    0x01

typedef struct Window{
  CallBackfun CallBack;
  WidgetList* pMyWidgetList;
  uint8_t states;
}Window;



uint8_t WD_SetCallback(Window *pWindow, CallBackfun callback);
uint8_t WD_SetMyWidgetList(Window* pWindow, WidgetList* pWidgetList);
//uint8_t WD_SetStates(Window* pWindow, uint8_t states);

#endif

 

