#include "GUI.h"
#include "channel480.h"

void addnext8(uint8_t* base, char* ch, uint8_t* UnderLine);
void addnext16(uint16_t* base, char *ch, uint8_t* UnderLine);
void addnext32(uint32_t* base, char *ch, uint8_t* UnderLine);

void addnext8(uint8_t* base, char* ch, uint8_t* UnderLine)
{
  if(*ch >= '0' && *ch <= '9')
  {
    *base = *base*10 + *ch - '0';
    addnext8(base, ch+1, UnderLine);
  }
  if(UnderLine && *ch == '_')
  {
    (*UnderLine)++;
    addnext8(base, ch+1, UnderLine);
  }
  return;
}

void addnext16(uint16_t* base, char *ch, uint8_t* UnderLine)
{
  if(*ch >= '0' && *ch <= '9')
  {
    *base = *base*10 + *ch - '0';
    addnext16(base, ch+1, UnderLine);
  }
  if(UnderLine && *ch == '_')
  {
    (*UnderLine)++;
    addnext16(base, ch+1, UnderLine);
  }
  return;
}

void addnext32(uint32_t* base, char *ch, uint8_t* UnderLine)
{
  if(*ch >= '0' && *ch <= '9')
  {
    *base = *base*10 + *ch - '0';
    addnext32(base, ch+1, UnderLine);
  }
  if(UnderLine && *ch == '_')
  {
    (*UnderLine)++;
    addnext32(base, ch+1, UnderLine);
  }
  return;
}


uint8_t GUI_CharToUint8(char *ch, uint8_t* UnderLine)
{
  uint8_t num = 0;
  if(UnderLine)
    *UnderLine = 0;
  addnext8(&num, ch, UnderLine);
  return num;
}

void GUI_Uint8ToChar(uint8_t num, char *p, uint8_t length)
{
  for(uint8_t i = 0; i < length+1; i++)
  {
    p[i] = '\0';
  }
  switch(length)
  {
    case 1:
      sprintf(p, "%01d", num);
      break;
    case 2:
      sprintf(p, "%02d", num);
      break;
    case 3:
      sprintf(p, "%03d", num);
      break;
  }
  return;
}

uint16_t GUI_CharToUint16(char *ch, uint8_t* UnderLine)
{
  uint16_t num = 0;
  if(UnderLine)
    *UnderLine = 0;
  addnext16(&num, ch, UnderLine);
  return num;
}

void GUI_Uint16ToChar(uint16_t num, char *p, uint8_t length)
{
  for(uint8_t i = 0; i < length+1; i++)
  {
    p[i] = '\0';
  }
  switch(length)
  {
    case 1:
      sprintf(p, "%01d", num);
      break;
    case 2:
      sprintf(p, "%02d", num);
      break;
    case 3:
      sprintf(p, "%03d", num);
      break;
    case 4:
      sprintf(p, "%04d", num);
      break;
    case 5:
      sprintf(p, "%05d", num);
      break;
  }
  return;
}

uint32_t GUI_CharToUint32(char *ch, uint8_t* UnderLine)
{
  uint32_t num = 0;
  if(UnderLine)
    *UnderLine = 0;
  addnext32(&num, ch, UnderLine);
  return num;
}

void GUI_Uint32ToChar(uint32_t num, char *p, uint8_t length)
{
  for(uint8_t i = 0; i < length+1; i++)
  {
    p[i] = '\0';
  }
  switch(length)
  {
    case 1:
      sprintf(p, "%01d", num);
      break;
    case 2:
      sprintf(p, "%02d", num);
      break;
    case 3:
      sprintf(p, "%03d", num);
      break;
    case 4:
      sprintf(p, "%04d", num);
      break;
    case 5:
      sprintf(p, "%05d", num);
      break;
    case 6:
      sprintf(p, "%06d", num);
      break;
    case 7:
      sprintf(p, "%07d", num);
      break;
    case 8:
      sprintf(p, "%08d", num);
      break;
    case 9:
      sprintf(p, "%09d", num);
      break;
    case 10:
      sprintf(p, "%010d", num);
      break;
  }
  return;
}

uint8_t GUI_AddAChar(char *pchhead, char ch, uint8_t length)
{
  for(int i = 0; i < length; i++)
  {
    if(*(pchhead + i) == '_')
    {
      *(pchhead + i) = ch;
      return i + 1;
    }
  }
  return 0;
}

void GUI_SetCharUnderLine(char *Ch, uint8_t Length)
{
  for(uint8_t i = 0; i < Length; i++)
  {
    *(Ch + i) = '_';
  }
}

uint8_t GUI_ChnIsAvailable480(char* Ch, uint8_t Add)
{ 
  uint8_t length = 0;
  uint32_t Channel = GUI_CharToUint32(Ch, &length);
  uint32_t tmpChn = Channel;
  uint32_t  tmpAdd = Add;
  uint8_t tmplength = length;
  
  if(Add == 0 && length == 1 && Channel == 0)
    return 0;
  
  while(tmplength)
  {
    tmpChn *= 10;
    tmplength--;
  }
  
  tmplength = length;
  while(tmplength - 1 > 0)
  {
    tmpAdd *= 10;
    tmplength--;
  }
  
  if((tmpChn+tmpAdd) > 480)
    return 0;
  
  if(length == 1 && !IsChnAvailable(tmpChn+Add))
    return 0;
  return 1;
}  

