/*************************************************************
 * @file     timer.c
 * @brief
 *
 *
 * @version
 * @author
 * @data
 *************************************************************/
 
 #include "GUI_Timer.h"
 #include <string.h>
 #include "UIMessage.h"
 
	/**  允许同时存在的最大定时器数目
	 *
  */
 #define TIMER_EVENT_MAX_NUM        10
 
 static Timer timerEventSpace[TIMER_EVENT_MAX_NUM]  = {0};
 static uint8_t TimerNumber  = 0;
 
 
/**@brief   创建并启动一个定时器
 * 
 *
 * @return void
 * @note
 */ 
 int GUI_CreateTimer(Window* pWindow, uint16_t id,  uint16_t span)
 {
	  int i  = 0;
	 
	 
	  if(TimerNumber <= TIMER_EVENT_MAX_NUM){
		  /// 看是否有已存在的Timer
		  for(; i < TIMER_EVENT_MAX_NUM; i++){
			  if(timerEventSpace[i].id == id && timerEventSpace[i].Window == pWindow){
            timerEventSpace[i].Window = pWindow;
						timerEventSpace[i].span  = span;
						timerEventSpace[i].state  = 1;
				  return 1;
			  }
		  }
		 
				for(i=0; i < TIMER_EVENT_MAX_NUM; i++){
						if(timerEventSpace[i].Window == NULL){
								timerEventSpace[i].id  = id;
								timerEventSpace[i].Window = pWindow;
								timerEventSpace[i].span = span;
							 timerEventSpace[i].state  = 1;
								TimerNumber++;
								return 1;
						}
				}		 
	 }
	 else{
		  return 0;
	 }

	 
	 return 0;
 }
 
 
 /**@brief  销毁指定定时器
 * 
 *
 * @return void
 * @note
 */
 void GUI_DestroyTimer(Window* pWindow, uint16_t id)
 {
	  int i  = 0;
		
		 if(TimerNumber){
					for(; i < TIMER_EVENT_MAX_NUM; i++){
							if(timerEventSpace[i].id  ==  id && timerEventSpace[i].Window == pWindow){
									memset(&timerEventSpace[i], 0, sizeof(Timer));
									TimerNumber--;
							}
					}
			}
 }
 
 
 /**@brief   重启指定定时器
 * 
 *
 * @return void
 * @note
 */
 int GUI_RestartTimer(Window* pWindow, uint16_t id, uint16_t span)
 {
	  if(TimerNumber){
				 int i  = 0;
				 for(; i < TIMER_EVENT_MAX_NUM; i++){
						 if(timerEventSpace[i].id  == id && timerEventSpace[i].Window == pWindow){
								 timerEventSpace[i].span  = span;
								 timerEventSpace[i].state  = 1;
							 	return 1;
							}
					}
			}
			
			return 0;
 }
 
 
	
	/**@brief
 * 
 *
 * @return void
 * @note
 */
void GUI_OnTick()
{
	int i  = 0;
	 
	if(TimerNumber){ 
		for(; i < TIMER_EVENT_MAX_NUM; i++ ){
			if(timerEventSpace[i].id){
				if(timerEventSpace[i].state){
					if(timerEventSpace[i].span){
						timerEventSpace[i].span--;
					}
					else{
						timerEventSpace[i].state  = 0;			

						if(timerEventSpace[i].Window){
							WM_Message Msg;
							Msg.pTarget = timerEventSpace[i].Window;
							Msg.msgType = WM_TIMER;
							Msg.data_v = timerEventSpace[i].id;
							timerEventSpace[i].Window->CallBack(&Msg);	
						}												 
					}
				}
			}
		}
	}
}
 
	
 