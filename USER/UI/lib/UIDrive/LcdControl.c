#include "lcdControl.h"
#include "usart.h"
#include "utils.h" 

static int invalid_cnt=0;

static uint16_t Digtal_dec[10]={0x1F80,0x0300,0x2D80,0x2780,0x3300,0x3680,0X3E80,0x0380,0x3F80,0x3780}; // 十位 0~9
static uint8_t Digtal_unit[10]={0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F};  // 个位 0~9

int test;

Disp lcdDisplay;
Indicator invalid_data;

/**
 *	@brief lcd 初始化
 *
 */
void lcdInit(void)
{
//	invalid_data.size = 0;
//  invalid_data.end = 0;
//	invalid_data.start = 0;	
//	
//	LcdMenuSet(1,FUNTION_CALL|FUNTION_CONTRCT);
//	LcdTimeSet(1,5,25,12,12,12);
//	LcdOrderSet(1,10);
//	LcdSmallGroupIdSet(1,1245);
//	LcdSmallPhoneIdSet(1,1234567);
//	LcdDateSet(1,2017,5,25);
//	LcdSmallShipIdSet(1,12354);
//	LcdSmallCalllogState(1,NUM1_NOTES_MISSED|NUM1_NOTES_CALLIN);
//	LcdBigPhoneIdSet(1,1235678);
//	LcdPasswordSet(1,13456);
//	LcdBigGroupIdSet(1,2234);
//	LcdChannelFlag(1,NUM2_NOTES_SCAN);
//	LcdTalkStateSet(1,TALK_STATE_CALLING);
//	LcdSignalStateSet(1,SIGNAL_STATE_SEND,5);
//	
  LCDCleanScreen();
	//NumberSet(&lcdDisplay.time_month,5);
	//NumberSet(&lcdDisplay.time_day,25);
	
	//LcdWrite(0,sizeof(lcdDisplay.menu),(uint8_t*)&lcdDisplay);
	
	//LcdWrite(TIME_MONTH_BASE_ADD,sizeof(lcdDisplay.time_month)+sizeof(lcdDisplay.time_day),((uint8_t*)(&lcdDisplay))+(TIME_MONTH_BASE_ADD/8));
}

void LCDCleanScreen(void)
{
  LcdMenuSet(0,0);
  LcdTimeSet(0, 0,0,0, 0, 0);
  LcdOrderSet(0,0);
  LcdSmallPhoneIdSet(0, 0);
  LcdSmallGroupIdSet(0, 0);
  LcdDateSet(0,0,0, 0);
  LcdSmallShipIdSet(0, 0);
  LcdRecommendSet(0, 0);
  LcdChannelSet(0, 0);
  LcdSmallCalllogState(0, 0);

  LcdBigPhoneIdSet(0, 0);
  LcdBigShipIdSet(0, 0);
  LcdPasswordSet(0, 0);
  LcdBigGroupIdSet(0, 0);
  LcdChannelFlag(0, 0);
  LcdTalkStateSet(0, 0);
  LcdSignalStateSet(0,0, 0);
  Invalid();	
}

/**
 *	@brief 将内存的数据写到LCD中
 *	@param	add 数据块的开始地址
 *  @param  len	数据的长度
 *  @param	buf	数据
 */
void LcdWrite(uint16_t add,uint16_t len,uint8_t* buf)
{
	uint8_t address[2];
	uint8_t lenth[2];
	address[0] = add>>8;
	address[1] = add&0xff;
	lenth[0] = len>>8;
	lenth[1] = len&0xff;
	LCDUartSend(address,2); //地址
  LCDUartSend(lenth,2); //数据长度
	LCDUartSend(buf,len); //数据
}

/**
 *	@brief LCD 数字显示设置
 *	@param address 设置的目标地址
 *  @param num 设置的值
 */
void DoubelNumSet(uint16_t* address, int num)
{
   *address = 0;
//   if(num>9){
//       *address|=Digtal_dec[num/10]; //十位
//       *address|=Digtal_unit[num%10];//个位
//   }
//   else{
//       *address|=Digtal_dec[0]; //十位
//       *address|=Digtal_unit[num];//个位
//   }
  
     if(num>9){
       *address|=Digtal_dec[num/10]; //十位
       *address|=Digtal_unit[num%10];//个位
   }
   else{
       *address|=Digtal_dec[0]; //十位
       *address|=Digtal_unit[num];//个位
   }
}

void UnitNumSet(uint8_t* address, int num)
{
    *address = 0;
    *address |= Digtal_unit[num];
}

/**
 *	@brief 无效化数据，将内存中更改的数据写到LCD
 *	
 */
void Invalid()
{
  if(invalid_data.size)
    LcdWrite(invalid_data.start,invalid_data.size,((uint8_t*)&(lcdDisplay)+(invalid_data.start/8)));
	invalid_data.start = 0;
  invalid_data.end = 0;
	invalid_data.size = 0;
}
 
 /**
  *	@brief	无效化数据管理
  *	
  *	@param	start 开始地址
  *	@param	size  数据大小
  */
void InvalidManage(uint16_t start,uint16_t size)
{
  
  uint16_t end=0;;
	end = start+size*8; 
  
  if(invalid_data.size==0){
    invalid_data.start = start;
    invalid_data.end = end;
  }
  else{
    if(start<invalid_data.start)
      invalid_data.start = start;
    if(end>invalid_data.end)
      invalid_data.end = end;
  }
    
  
	
	invalid_data.size = (invalid_data.end-invalid_data.start)/8;
}


/**
 *	@brief 菜单显示设置
 *  @param val 设置的值
 *
 */
void LcdMenuSet(uint8_t funtionEnable,uint16_t val)
{
	if(funtionEnable)
		lcdDisplay.menu = val;
	else
		lcdDisplay.menu = 0;
		
	InvalidManage(MENU_BASE_ADD,sizeof(lcdDisplay.menu));
}

/**
 *	@brief	时间设置
 *
 *	@param 	month:月
 *	@param	day:日
 *	@param 	hour:时
 *  @param	min:分
 *	@param	sec:秒
 */
void LcdTimeSet(uint8_t funtionEnable,uint8_t month,uint8_t day,uint8_t hour, uint8_t min, uint8_t sec)
{
 int size;
 if(funtionEnable){
	 DoubelNumSet(&lcdDisplay.time_month,month);
	 DoubelNumSet(&lcdDisplay.time_day,day);
	 DoubelNumSet(&lcdDisplay.time_hour,hour);
	 DoubelNumSet(&lcdDisplay.time_min,min);
	 DoubelNumSet(&lcdDisplay.time_sec,sec);
	 

 }
 else{
	lcdDisplay.time_month = 0;
	lcdDisplay.time_day = 0;
	lcdDisplay.time_hour = 0;
	lcdDisplay.time_min = 0;
	lcdDisplay.time_sec = 0;
 }
 size = sizeof(lcdDisplay.time_month)+sizeof(lcdDisplay.time_day)
    + sizeof(lcdDisplay.time_min) + sizeof(lcdDisplay.time_sec)
    + sizeof(lcdDisplay.time_hour);
 InvalidManage(TIME_MONTH_BASE_ADD,size);
}

/**
 *	@brief	序号显示设置
 *
 *	@param	funtionEnable 状态使能/失能
 *	@param	val	设置的值
 */
void LcdOrderSet(uint8_t funtionEnable,uint8_t val)
{
	int size;
	if(funtionEnable){
		UnitNumSet(&lcdDisplay.small_lcdnum[0],val/10);
		UnitNumSet(&lcdDisplay.small_lcdnum[1],val%10);
		
		lcdDisplay.smallnum_notes[0] |= (NUM1_LINE_LEFT_1|NUM1_LINE_LEFT_4);
		lcdDisplay.smallnum_notes[1] |= NUM1_NOTES_IDX;
	}
	else{
		lcdDisplay.small_lcdnum[0] = 0;
		lcdDisplay.small_lcdnum[1] = 0;
		lcdDisplay.smallnum_notes[0] = 0;
		lcdDisplay.smallnum_notes[1] = 0;
	}
	size = sizeof(lcdDisplay.small_lcdnum[0])+sizeof(lcdDisplay.small_lcdnum[1]);
	
	InvalidManage(SMALL_NUM_BASE_ADD,size);
	InvalidManage(NUM1_NOTES_BASE_ADD,sizeof(lcdDisplay.smallnum_notes[0])
					+ sizeof(lcdDisplay.smallnum_notes[1]));								
}

/**
 *	@brief	 呼号设值 (小)
 *
 *	@param	funtionEnable	状态失能/使能
 *	@param	val	显示的值
 */
void LcdSmallPhoneIdSet(uint8_t funtionEnable, uint32_t val)
{
	uint8_t i=0;
	
	if(funtionEnable){
		uint8_t num[LEN_PHONE_ID];
		CutNumber(val,num,LEN_PHONE_ID);
		for(;i<LEN_PHONE_ID;i++)
			UnitNumSet(&lcdDisplay.small_lcdnum[i],num[i]);
    
    lcdDisplay.smallnum_notes[0] |= (NUM1_LINE_LEFT_1|NUM1_LINE_LEFT_2|NUM1_LINE_LEFT_3);
		lcdDisplay.smallnum_notes[1] |= NUM1_NOTES_PHONEID;
	}
	else{
		for(;i<LEN_PHONE_ID;i++)
				UnitNumSet(&lcdDisplay.small_lcdnum[i],0);
    
    lcdDisplay.smallnum_notes[0] |= (NUM1_LINE_LEFT_1|NUM1_LINE_LEFT_2|NUM1_LINE_LEFT_3);
		lcdDisplay.smallnum_notes[1] |= NUM1_NOTES_PHONEID;
	}
	
	InvalidManage(SMALL_NUM_BASE_ADD,sizeof(lcdDisplay.small_lcdnum[0])*LEN_PHONE_ID);
}

/**
 *	@brief	群号设置 (小)
 *
 *	@param	funtionEnable	状态失能/使能
 *	@param	val	值
 */
 void LcdSmallGroupIdSet(uint8_t funtionEnable, uint16_t val)
 {
	uint8_t i=0;
	
	if(funtionEnable){
		uint8_t num[LEN_GROUP_ID];
		CutNumber(val,num,LEN_GROUP_ID);
		
		for(;i<LEN_GROUP_ID;i++)
			UnitNumSet(&lcdDisplay.small_lcdnum[i+3],num[i]);
			
		lcdDisplay.smallnum_notes[0] |= (NUM1_LINE_LEFT_3|NUM1_LINE_LEFT_5);
		lcdDisplay.smallnum_notes[1] |= NUM1_NOTES_GROUPID;
	}
	else{
		for(;i<LEN_GROUP_ID;i++)
			UnitNumSet(&lcdDisplay.small_lcdnum[i+3],0);
			
			lcdDisplay.smallnum_notes[0] &= ~(NUM1_LINE_LEFT_3|NUM1_LINE_LEFT_5);
			lcdDisplay.smallnum_notes[1] &= ~NUM1_NOTES_GROUPID;
	}
		
	InvalidManage(SMALL_NUM_BASE_ADD+3*8
					,sizeof(lcdDisplay.smallnum_notes[0])*LEN_GROUP_ID);
		
	
	InvalidManage(NUM1_NOTES_BASE_ADD
					,sizeof(lcdDisplay.smallnum_notes[0])
					+ sizeof(lcdDisplay.smallnum_notes[1]));
 }
 
 
 /**
  *	@brief	日期设置
  *	
  *	@param	funtionEnable	状态失能/使能
  *
  * @param	year 年
  * @param	month 月
  * @param  day	日
  */
 void LcdDateSet(uint8_t funtionEnable,uint16_t year,uint8_t month, uint8_t day)
 {
	uint8_t i=0;
	uint8_t num[4];
	
	if(funtionEnable)
	{
		//year
		CutNumber(year,num,4);
		for(;i<4;i++)
			UnitNumSet(&lcdDisplay.small_lcdnum[i+3],num[i]);
		
		//month 
		num[0] = month/10;
		num[1] = month%10;
		UnitNumSet(&lcdDisplay.small_lcdnum[8],num[0]);
		UnitNumSet(&lcdDisplay.small_lcdnum[9],num[1]);
		
		//day
		num[0] = day/10;
		num[1] = day%10;
		UnitNumSet(&lcdDisplay.small_lcdnum[11],num[0]);
		UnitNumSet(&lcdDisplay.small_lcdnum[12],num[1]);
			
						
		lcdDisplay.smallnum_notes[0] |= (NUM1_LINE_LEFT_3|NUM1_LINE_LEFT_5|NUM1_LINE_RIGHT_1);
		lcdDisplay.smallnum_notes[1] |= (NUM1_NOTES_YEAR_M_D
										|NUM1_LINE_RIGHT_3
										|NUM1_LINE_RIGHT_4
										|NUM1_LINE_RIGHT_5);
	}
	else{
		for(;i<4;i++)
			UnitNumSet(&lcdDisplay.small_lcdnum[i+3],0);
			
		UnitNumSet(&lcdDisplay.small_lcdnum[8],0);
		UnitNumSet(&lcdDisplay.small_lcdnum[9],0);
		
		UnitNumSet(&lcdDisplay.small_lcdnum[11],0);
		UnitNumSet(&lcdDisplay.small_lcdnum[12],0);
		
		lcdDisplay.smallnum_notes[0] &= ~(NUM1_LINE_LEFT_3|NUM1_LINE_LEFT_5|NUM1_LINE_RIGHT_1);
		lcdDisplay.smallnum_notes[1] &= ~(NUM1_NOTES_YEAR_M_D
										|NUM1_LINE_RIGHT_3
										|NUM1_LINE_RIGHT_4
										|NUM1_LINE_RIGHT_5);
	}
	
	//flag
	InvalidManage(SMALL_NUM_BASE_ADD+3*8
					,sizeof(lcdDisplay.smallnum_notes[0])*10);
	InvalidManage(NUM1_NOTES_BASE_ADD,sizeof(lcdDisplay.smallnum_notes[0])*3);
 }
 
 
 /**
  *	@brief	船号设置 (小)
  *
  *	@param	funtionEnable	状态失能/使能
  *	@param	val	显示的值
  */
 void LcdSmallShipIdSet(uint8_t funtionEnable, uint32_t val)
 {
	uint8_t i=0;
	if(funtionEnable){
		uint8_t num[LEN_SHIP_ID];
		CutNumber(val,num,LEN_SHIP_ID);
		
		for(;i<LEN_SHIP_ID;i++)
			UnitNumSet(&lcdDisplay.small_lcdnum[i+8],num[i]);	
		lcdDisplay.smallnum_notes[0] |= NUM1_LINE_RIGHT_1;
		
		lcdDisplay.smallnum_notes[1] |= (NUM1_LINE_RIGHT_2|NUM1_LINE_RIGHT_3);
		lcdDisplay.smallnum_notes[2] |= NUM1_NOTES_SHIPID;
	}
	else{
		for(;i<LEN_SHIP_ID;i++)
			UnitNumSet(&lcdDisplay.small_lcdnum[i+8],0);	
			
		lcdDisplay.smallnum_notes[0] &= ~NUM1_LINE_RIGHT_1;
		
		lcdDisplay.smallnum_notes[1] &= ~(NUM1_LINE_RIGHT_2|NUM1_LINE_RIGHT_3);
		lcdDisplay.smallnum_notes[2] &= ~NUM1_NOTES_SHIPID;
	}
	
	
	InvalidManage(SMALL_NUM_BASE_ADD+8*8
					,sizeof(lcdDisplay.smallnum_notes[0])*LEN_SHIP_ID);
		
	InvalidManage(NUM1_NOTES_BASE_ADD,sizeof(lcdDisplay.smallnum_notes[0])*3);
 }
 
 /**
  *	@brief	推荐频道设置
  *
  *	@param	funtionEnable	状态失能/使能
  *	@param	val	显示的值
  */
 void LcdRecommendSet(uint8_t funtionEnable, uint32_t val)
 {
	if(funtionEnable)
  {
    lcdDisplay.small_lcdnum[10] = Digtal_unit[val/100];
    lcdDisplay.small_lcdnum[11] = Digtal_unit[val/10%10];
    lcdDisplay.small_lcdnum[12] = Digtal_unit[val%10];
    
    lcdDisplay.smallnum_notes[1] |= NUM1_LINE_RIGHT_2|NUM1_LINE_RIGHT_3|NUM1_LINE_RIGHT_4;
    lcdDisplay.smallnum_notes[2] |= NUM1_NOTES_RECOMMEND | NUM1_NOTES_CHANNEL;
  }
  else
  {
    lcdDisplay.small_lcdnum[10] = Digtal_unit[0];
    lcdDisplay.small_lcdnum[11] = Digtal_unit[0];
    lcdDisplay.small_lcdnum[12] = Digtal_unit[0];
    
    lcdDisplay.smallnum_notes[1] &= ~NUM1_LINE_RIGHT_2;
    lcdDisplay.smallnum_notes[1] &= ~NUM1_LINE_RIGHT_3;
    lcdDisplay.smallnum_notes[1] &= ~NUM1_LINE_RIGHT_4;
    lcdDisplay.smallnum_notes[2] &= ~NUM1_NOTES_RECOMMEND;
    lcdDisplay.smallnum_notes[2] &= ~NUM1_NOTES_CHANNEL;
  }
  InvalidManage(SMALL_NUM_BASE_ADD+10, 3);
  InvalidManage(NUM1_NOTES_BASE_ADD,
					3);
 }
 
 /**
  *	@brief	频道设置
  *
  */
 void LcdChannelSet(uint8_t funtuonEnable, uint32_t val)
 {
  if(funtuonEnable)
  {
    lcdDisplay.big_lcdnum[3] = Digtal_unit[val/1000];
    lcdDisplay.big_lcdnum[4] = Digtal_unit[val/100%10];
    lcdDisplay.big_lcdnum[5] = Digtal_unit[val/10%10];
    lcdDisplay.big_lcdnum[6] = Digtal_unit[val%10];
   }
  else
  {
    lcdDisplay.big_lcdnum[3] = Digtal_unit[0];
    lcdDisplay.big_lcdnum[4] = Digtal_unit[0];
    lcdDisplay.big_lcdnum[5] = Digtal_unit[0];
    lcdDisplay.big_lcdnum[6] = Digtal_unit[0];
  }
  InvalidManage(BIG_NUM_BASE_ADD,
					7);
 } 
 
 /**
  *	@brief	通话记录状态
  *	
  *	@param	funtionEnable	状态失能/使能
  *	@param	val 值
  */
 void LcdSmallCalllogState(uint8_t funtionEnable, uint8_t val)
 {
	if(funtionEnable){
		lcdDisplay.smallnum_notes[2] |= val;
	}
	else{
		//lcdDisplay.smallnum_notes[2] &= ~val;
    lcdDisplay.smallnum_notes[2] = 0;
	}
	InvalidManage(NUM1_NOTES_BASE_ADD+2*8,
					sizeof(lcdDisplay.smallnum_notes[0]));
 } 
 
 
 /**
  *	@brief	呼号设置	(大)
  *
  *	@param	funtionEnable	状态失能/使能
  *	@param	val	显示的值
  */
 void LcdBigPhoneIdSet(uint8_t funtionEnable, uint32_t val)
 {
	uint8_t i=0;
	if(funtionEnable){
		uint8_t num[LEN_PHONE_ID];
		CutNumber(val,num,LEN_PHONE_ID);
		
		for(;i<7;i++)
			UnitNumSet(&lcdDisplay.big_lcdnum[i],num[i]);
			
		lcdDisplay.bignum_notes |= NUM2_NOTES_PHONEID;
	}
	else{
		uint8_t num[LEN_PHONE_ID];
		CutNumber(val,num,LEN_PHONE_ID);
		
		for(;i<7;i++)
			UnitNumSet(&lcdDisplay.big_lcdnum[i],0);
			
			lcdDisplay.bignum_notes &= ~NUM2_NOTES_PHONEID;
	}
	
	InvalidManage(BIG_NUM_BASE_ADD,sizeof(lcdDisplay.big_lcdnum[0])*LEN_PHONE_ID);
	InvalidManage(NUM2_NOTES_BASE_ADD,sizeof(lcdDisplay.bignum_notes));
 }
 
 /**
  *	@brief	船号设置(大)
  *
  *	@param	funtionEnable	状态失能/使能
  *	@param	val	显示的值
  */
 void LcdBigShipIdSet(uint8_t funtionEnable, uint32_t val)
 {
	uint8_t i=0;
	
	if(funtionEnable){
		
		uint8_t num[LEN_SHIP_ID];
		CutNumber(val,num,LEN_SHIP_ID);
		
		for(;i<LEN_SHIP_ID;i++)
			UnitNumSet(&lcdDisplay.big_lcdnum[i+1],num[i]);
			
		lcdDisplay.bignum_notes |= (NUM2_LINE_1|NUM2_LINE_2);
		lcdDisplay.bignum_notes |= NUM2_NOTES_SHIPID;
	}
	else{
		for(;i<LEN_SHIP_ID;i++)
			UnitNumSet(&lcdDisplay.big_lcdnum[i+1],0);
			
		lcdDisplay.bignum_notes &= ~(NUM2_LINE_1|NUM2_LINE_2);
		lcdDisplay.bignum_notes &= ~NUM2_NOTES_SHIPID;
	}
		
	InvalidManage(BIG_NUM_BASE_ADD+8*1,sizeof(lcdDisplay.big_lcdnum[0])*LEN_SHIP_ID);
	InvalidManage(NUM2_NOTES_BASE_ADD,sizeof(lcdDisplay.bignum_notes));
 }
 
 /**
  *	@brief	密码设置
  *
  *	@param	funtionEnable	状态失能/使能
  *	@param	val	值
  */
 void LcdPasswordSet(uint8_t funtionEnable, uint32_t val)
 {
	uint8_t i=0;
	
	if(funtionEnable){
		uint8_t num[LEN_PASSWORD] = {0};
		CutNumber(val,num,LEN_PASSWORD);
		
		for(;i<LEN_PASSWORD;i++)
			UnitNumSet(&lcdDisplay.big_lcdnum[i+1],num[i]);
			
		lcdDisplay.bignum_notes |= (NUM2_LINE_1|NUM2_LINE_2);
		lcdDisplay.bignum_notes |= NUM2_NOTES_CODE;
	}
	else{
		for(;i<LEN_PASSWORD;i++)
			UnitNumSet(&lcdDisplay.big_lcdnum[i+1],0);
			
		lcdDisplay.bignum_notes &= ~(NUM2_LINE_1|NUM2_LINE_2);
		lcdDisplay.bignum_notes &= ~NUM2_NOTES_CODE;
	}
		
	InvalidManage(BIG_NUM_BASE_ADD+8*1,sizeof(lcdDisplay.big_lcdnum[0])*LEN_PASSWORD);
	InvalidManage(NUM2_NOTES_BASE_ADD,sizeof(lcdDisplay.bignum_notes));
 } 
 
 /**
  *	@brief	群号设置(大)
  *
  */
 void LcdBigGroupIdSet(uint8_t funtionEnable, uint16_t val)
 {
	uint8_t i=0;
	
	if(funtionEnable){
	
		uint8_t num[LEN_GROUP_ID];
		CutNumber(val,num,LEN_GROUP_ID);

		for(;i<LEN_GROUP_ID;i++)
			UnitNumSet(&lcdDisplay.big_lcdnum[i+2],num[i]);
			
		lcdDisplay.bignum_notes |= (NUM2_LINE_2|NUM2_LINE_3);
		lcdDisplay.bignum_notes |= NUM2_NOTES_GROUPID;
	}
	else{
		for(;i<LEN_GROUP_ID;i++)
			UnitNumSet(&lcdDisplay.big_lcdnum[i+2],0);
			
		lcdDisplay.bignum_notes &= ~(NUM2_LINE_2|NUM2_LINE_3);
		lcdDisplay.bignum_notes &= ~NUM2_NOTES_GROUPID;
	}
	
	InvalidManage(BIG_NUM_BASE_ADD+8*2,sizeof(lcdDisplay.big_lcdnum[0])*LEN_GROUP_ID);
	InvalidManage(NUM2_NOTES_BASE_ADD,sizeof(lcdDisplay.bignum_notes));
 }
 
 /**
  *	@brief	频道标签显示 (扫描、记忆)
  *
  *	@param	funtionEnable	状态失能/使能
  *	@param	val	值
  */
 void LcdChannelFlag(uint8_t funtionEnable, uint8_t val)
 {
	if(funtionEnable){
		lcdDisplay.bignum_notes |= val;
	}
	else{
		lcdDisplay.bignum_notes = 0;
	}
	InvalidManage(NUM2_NOTES_BASE_ADD,sizeof(lcdDisplay.bignum_notes));
 } 
 
 /**
  *	@brief	通话状态设置
  *
  *	@param	funtionEnable	状态失能/使能
  *	@param	val 值
  */
 void LcdTalkStateSet(uint8_t funtionEnable, uint8_t val)
 {
	if(funtionEnable){
		lcdDisplay.talk_states |= val;
	}
	else{
		lcdDisplay.talk_states = 0;
	}
	InvalidManage(TALK_STATES_BASE_ADD,sizeof(lcdDisplay.bignum_notes));
 } 
 
 /**
  *	@breif	信号状态设置
  *
  *	@param	funtionEnable	状态失能/使能
  *	@param	state	信号状态 发送/接收
  *	@param	val	信号强度值
  */
 void LcdSignalStateSet(uint8_t funtionEnable,uint8_t state, uint8_t val)
 {
	if(funtionEnable){
		lcdDisplay.signal_indicate |= state;
		lcdDisplay.signal_indicate |= val;
	}
	else{
		lcdDisplay.signal_indicate = 0;
	}
	InvalidManage(SIGNAL_STATES_BASE_ADD,sizeof(lcdDisplay.signal_indicate));
 } 
 
 
 void LcdBigTimeSet(uint8_t funtionEnable, uint8_t hour, uint8_t min, uint8_t sec)
 {
  if(funtionEnable)
  {
    uint8_t num[6];
		CutNumber(hour*10000+min*100+sec,num,6);

		for(int i = 0; i<6; i++)
			UnitNumSet(&lcdDisplay.big_lcdnum[i+1],num[i]);
  }
  else
  {
    for(int i = 0;i<6;i++)
			UnitNumSet(&lcdDisplay.big_lcdnum[i+1],0);
  }
  InvalidManage(BIG_NUM_BASE_ADD+8,sizeof(lcdDisplay.big_lcdnum[0])*6);
 }