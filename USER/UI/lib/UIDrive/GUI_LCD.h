#ifndef GUI_LCD_H_

#include "ssp.h"

#define LCD_DISPNULL      0xff

#define LCD_FLASH_SPEED   10

#define LCD_S1           0420
#define LCD_S2           0421
#define LCD_S3           0422
#define LCD_S4           0423
#define LCD_S5           0424
#define LCD_S6           0425
#define LCD_S7           0426
#define LCD_S8           0427
#define LCD_S9           0416
#define LCD_S10          0417
#define LCD_S11          07
#define LCD_S12          0415
#define LCD_S13          0414
#define LCD_S14          0413
#define LCD_S15          0412
#define LCD_S16          0411
#define LCD_S17          0410
#define LCD_S18          017
#define LCD_S19          027
#define LCD_S20          037
#define LCD_S21          047
#define LCD_S22          057
#define LCD_S23          067
#define LCD_S24          077
#define LCD_S25          076
#define LCD_S26          075
#define LCD_S27          074
#define LCD_S28          073
#define LCD_S29          072
#define LCD_S30          071
#define LCD_S31          070
#define LCD_S32          0400
#define LCD_S33          0401
#define LCD_S34          0402
#define LCD_S35          0403
#define LCD_S36          0100
#define LCD_S37          0110
#define LCD_S38          0120
#define LCD_S39          0130
#define LCD_S40          0140
#define LCD_S41          0170
#define LCD_S42          0200
#define LCD_S43          0210
#define LCD_S44          0220
#define LCD_S45          0230
#define LCD_S46          0240
#define LCD_S47          0160
#define LCD_S48          0150
#define LCD_S49          0361
#define LCD_S50          0362
#define LCD_S51          0363
#define LCD_S52          0373
#define LCD_S53          0372
#define LCD_S54          0404
#define LCD_S55          0304
#define LCD_S56          0344
#define LCD_S57          0264
#define LCD_S58          0324
#define LCD_S59          0353
#define LCD_S60          0364
#define LCD_S61          0352
#define LCD_S62          0351
#define LCD_S63          0350
#define LCD_S64          0261
#define LCD_S65          0260
#define LCD_S66          0340
#define LCD_S67          0320
#define LCD_S68          0300
#define LCD_S69          0262
#define LCD_S70          0263




#define LCD_NUM1         0241
#define LCD_NUM2         0231
#define LCD_NUM3         0221
#define LCD_NUM4         0211
#define LCD_NUM5         0201
#define LCD_NUM6         0171
#define LCD_NUM7         0161
#define LCD_NUM8         0151
#define LCD_NUM9         0141
#define LCD_NUM10        0131
#define LCD_NUM11        0121
#define LCD_NUM12        0111
#define LCD_NUM13        0101
#define LCD_NUM14        060
#define LCD_NUM15        050
#define LCD_NUM16        040
#define LCD_NUM17        030
#define LCD_NUM18        020
#define LCD_NUM19        010
#define LCD_NUM20        0
#define LCD_NUM21        0330
#define LCD_NUM22        0310
#define LCD_NUM23        0270
#define LCD_NUM24        0374
#define LCD_NUM25        0354
#define LCD_NUM26        0334
#define LCD_NUM27        0314
#define LCD_NUM28        0274
#define LCD_NUM29        0254



void GUI_LCDLightLoop(void);
void GUI_LCDInit(void);
void GUI_LCDWriteDate(uint8_t addr, uint8_t data);

void GUI_LCDSetS(uint16_t LCD_S, uint8_t val);
void GUI_LCDSetNumInt(uint16_t LCD_NUM, uint8_t val, uint8_t EnableFlash);
void GUI_LCDSetNumChar(uint16_t LCD_NUM, uint8_t val, uint8_t EnableFlash);
void GUI_LCDUpdate(void);
void GUI_LCDDisableNumFlash();
void GUI_LCDClean(void);
#endif












