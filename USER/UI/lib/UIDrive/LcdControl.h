#ifndef __LCDCONTROL_H
#define __LCDCONTROL_H

#include "stdint.h"

//length of number
#define LEN_PHONE_ID		7
#define LEN_GROUP_ID		4
#define LEN_YEAR			4
#define LEN_MONTH			2
#define LEN_DAY				2
#define LEN_SHIP_ID			5
#define LEN_CHANNEL			7
#define LEN_PASSWORD		5


#define FUNTION_SIMULATE     ((uint16_t)1<<9)   
#define FUNTION_GROUP        ((uint16_t)1<<8)   
#define FUNTION_CONTRCT      ((uint16_t)1<<7)   
#define FUNTION_RECORD       ((uint16_t)1<<6)   
#define FUNTION_DIGTAL       ((uint16_t)1<<5)   
#define FUNTION_MEMORY       ((uint16_t)1<<4)   
#define FUNTION_CALL         ((uint16_t)1<<3)   
#define FUNTION_WEATHER      ((uint16_t)1<<2)   
#define FUNTION_SHIPNUMCALL  ((uint16_t)1<<1)   
#define FUNTION_RECEPTION   ((uint16_t)1<<0)   

/*******************************************
 * 数字显示
 *******************************************/
//十位
#define DIGTAL_DEC_0    ((uint16_t)0x1F80)
#define DIGTAL_DEC_1    ((uint16_t)0x0300)
#define DIGTAL_DEC_2    ((uint16_t)0x2D80)
#define DIGTAL_DEC_3    ((uint16_t)0x2780)
#define DIGTAL_DEC_4    ((uint16_t)0x3300)
#define DIGTAL_DEC_5    ((uint16_t)0x3680)
#define DIGTAL_DEC_6    ((uint16_t)0X3E80)
#define DIGTAL_DEC_7    ((uint16_t)0x0380)
#define DIGTAL_DEC_8    ((uint16_t)0x3F80)
#define DIGTAL_DEC_9    ((uint16_t)0x3780)

//个位
#define DIGTAL_UNIT_0   ((uint8_t)0x3F)
#define DIGTAL_UNIT_1   ((uint8_t)0x06)
#define DIGTAL_UNIT_2   ((uint8_t)0x5B)
#define DIGTAL_UNIT_3   ((uint8_t)0x4F)
#define DIGTAL_UNIT_4   ((uint8_t)0x66)
#define DIGTAL_UNIT_5   ((uint8_t)0x6D)
#define DIGTAL_UNIT_6   ((uint8_t)0x7D)
#define DIGTAL_UNIT_7   ((uint8_t)0x07)
#define DIGTAL_UNIT_8   ((uint8_t)0x7F)
#define DIGTAL_UNIT_9   ((uint8_t)0x6F)


/*******************************************
 * num1_notes define
 *******************************************/
#define NUM1_LINE_LEFT_1        ((uint8_t)1<<5)  //左线1
#define NUM1_LINE_LEFT_2        ((uint8_t)1<<4)  //左线2
#define NUM1_LINE_LEFT_3        ((uint8_t)1<<3)  //左线3
#define NUM1_LINE_LEFT_4        ((uint8_t)1<<2)  //左线4
#define NUM1_LINE_LEFT_5        ((uint8_t)1<<1)  //左线5
                                  
#define NUM1_LINE_RIGHT_1       ((uint8_t)1<<0)  //右线1


#define NUM1_LINE_RIGHT_2       ((uint8_t)1<<7)  //右线2
#define NUM1_LINE_RIGHT_3       ((uint8_t)1<<6)  //右线3
#define NUM1_LINE_RIGHT_4       ((uint8_t)1<<5)  //右线4
#define NUM1_LINE_RIGHT_5       ((uint8_t)1<<4)  //右线5
                                  
#define NUM1_NOTES_IDX          ((uint8_t)1<<3)   //序号
#define NUM1_NOTES_PHONEID      ((uint8_t)1<<2)   //对讲机号
#define NUM1_NOTES_GROUPID      ((uint8_t)1<<1)    //群号
#define NUM1_NOTES_YEAR_M_D     ((uint8_t)1<<0)    //年,月,日
//#define NUM1_NOTES_MONTH        ((uint8_t)1<<7)    //月


#define NUM1_NOTES_SHIPID       ((uint8_t)1<<6)    //船号
#define NUM1_NOTES_RECOMMEND    ((uint8_t)1<<5)    //推荐
//#define NUM1_NOTES_DAY          ((quint8)1<<4)    //天
#define NUM1_NOTES_CHANNEL      ((uint8_t)1<<3)    //频道
#define NUM1_NOTES_MISSED       ((uint8_t)1<<2)    //未接
#define NUM1_NOTES_CALLIN       ((uint8_t)1<<1)    //呼入
#define NUM1_NOTES_CALLOUT      ((uint8_t)1<<0)    //呼出

/*******************************************
 * num2_notes define
 *******************************************/
#define NUM2_NOTES_POINT1       ((uint16_t)1<<14)   //小数点1
#define NUM2_NOTES_COLON1       ((uint16_t)1<<13)   //冒号1
#define NUM2_NOTES_POINT2       ((uint16_t)1<<12)   //小数点2
#define NUM2_NOTES_COLON2       ((uint16_t)1<<11)   //冒号2

#define NUM2_LINE_1             ((uint16_t)1<<10)   //线1
#define NUM2_LINE_2             ((uint16_t)1<<9)    //线2
#define NUM2_LINE_3             ((uint16_t)1<<8)    //线3

#define NUM2_NOTES_PHONEID      ((uint16_t)1<<7)    //对讲机号
#define NUM2_NOTES_SHIPID       ((uint16_t)1<<6)    //船号
#define NUM2_NOTES_CODE         ((uint16_t)1<<5)    //密码
#define NUM2_NOTES_GROUPID      ((uint16_t)1<<4)    //群号
#define NUM2_NOTES_UPDATE       ((uint16_t)1<<3)    //升级
#define NUM2_NOTES_SUCESS       ((uint16_t)1<<2)    //成功
#define NUM2_NOTES_SCAN         ((uint16_t)1<<1)    //扫描
#define NUM2_NOTES_MEMORY       ((uint16_t)1<<0)    //记忆

/*******************************************
 * talk state define
 *******************************************/
#define TALK_STATE_CALLING      ((uint8_t)1<<4)     //正在呼叫
#define TALK_STATE_FAIL         ((uint8_t)1<<3)     //失败
#define TALK_STATE_TALKING      ((uint8_t)1<<2)     //正在通话
#define TALK_STATE_END          ((uint8_t)1<<1)     //结束
#define TALK_STATE_TALKING_FLAG ((uint8_t)1<<0)     //通话标志

/*******************************************
 * signal state define
 *******************************************/
#define SIGNAL_STATE_RECEIVE    ((uint8_t)1<<5)
#define SIGNAL_STATE_SEND       ((uint8_t)1<<4)
#define SIGNAL_STATE_INTENSITY  ((uint8_t)0x0F)

/*******************************************
 * dispControl Base Address
 *******************************************/
#define MENU_BASE_ADD            	0x0000
#define TIME_MONTH_BASE_ADD         0x0010
#define TIME_DAY_BASE_ADD           0x0020
#define TIME_HOUR_BASE_ADD          0x0030
#define TIME_MIN_BASE_ADD           0x0040
#define TIME_SEC_BASE_ADD           0x0050
#define SMALL_NUM_BASE_ADD          0x0060
#define NUM1_NOTES_BASE_ADD         0x00C8
#define BIG_NUM_BASE_ADD            0x00E0
#define NUM2_NOTES_BASE_ADD         0x0118
#define TALK_STATES_BASE_ADD        0x0128
#define SIGNAL_STATES_BASE_ADD      0x0130

typedef struct _Disp{
	uint16_t menu;
	uint16_t time_month;
	uint16_t time_day;
	uint16_t time_hour;
	uint16_t time_min;
	uint16_t time_sec;
	
	uint8_t small_lcdnum[13];
	uint8_t smallnum_notes[3];
	
	uint8_t big_lcdnum[7];
	uint16_t bignum_notes;
	
	uint8_t talk_states;
	uint8_t	signal_indicate;
	
}Disp;

typedef struct _Indicator{
	uint16_t start;	//无效化数据开始地址
  uint16_t end;
	uint16_t size;	//无效化数据大小
}Indicator;


void lcdInit(void);
void LcdWrite(uint16_t add,uint16_t len,uint8_t* buf);
void DoubelNumSet(uint16_t* address, int num);
void UnitNumSet(uint8_t* address, int num);
void Invalid(void);
void InvalidManage(uint16_t start,uint16_t size);

void LcdMenuSet(uint8_t funtionEnable,uint16_t val);
void LcdTimeSet(uint8_t funtionEnable, uint8_t month,uint8_t day,uint8_t hour, uint8_t min, uint8_t sec);
void LcdOrderSet(uint8_t funtionEnable,uint8_t val);
void LcdSmallPhoneIdSet(uint8_t funtionEnable, uint32_t val);
void LcdSmallGroupIdSet(uint8_t funtionEnable, uint16_t val);
void LcdDateSet(uint8_t funtionEnable,uint16_t year,uint8_t month, uint8_t day);
void LcdSmallShipIdSet(uint8_t funtionEnable, uint32_t val);
void LcdRecommendSet(uint8_t funtionEnable, uint32_t val);
void LcdChannelSet(uint8_t funtuonEnable, uint32_t val);
void LcdSmallCalllogState(uint8_t funtionEnable, uint8_t val);
void LcdBigTimeSet(uint8_t funtionEnable, uint8_t hour, uint8_t min, uint8_t sec);

void LcdBigPhoneIdSet(uint8_t funtionEnable, uint32_t val);
void LcdBigShipIdSet(uint8_t funtionEnable, uint32_t val);
void LcdPasswordSet(uint8_t funtionEnable, uint32_t val);
void LcdBigGroupIdSet(uint8_t funtionEnable, uint16_t val);
void LcdChannelFlag(uint8_t funtionEnable, uint8_t val);
void LcdTalkStateSet(uint8_t funtionEnable, uint8_t val);
void LcdSignalStateSet(uint8_t funtionEnable,uint8_t state, uint8_t val);

void LCDCleanScreen(void);

#endif

