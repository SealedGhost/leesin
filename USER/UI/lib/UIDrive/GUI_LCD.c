#include "GUI_LCD.h"
#include <uCOS-II\Source\ucos_ii.h>
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "pwm.h"
#include "string.h"

enum {
  INTMODE = 0,
  CHARMODE = 1,
};

typedef struct NumFlash{
  uint16_t LCD_NUM;
  uint8_t Val;
  uint8_t Enable;
  uint8_t DataType;
}NumFlash;

static NumFlash numFlash = {0};

static uint8_t invalid_start = 0, invalid_end = 34;
uint8_t LCDData[35] = {0};
void LCDSetBit(uint8_t th, uint8_t com, uint8_t val);
void LCDInvalidSet(uint8_t th);

void GUI_LCDLightLoop(void)
{
  static uint8_t Light0_3 = 0;
  Light0_3++;
  if(Light0_3 > 3) Light0_3 = 0;
	PWMSetDutyCycle(Light0_3 * 33);
}

void GUI_LCDInit(void)
{
	/*Light PWM*/
	PWMInit();
	/*LCD DRIVER Init*/
  SSP1_Set12Bit();
  SSP_SendData(LPC_SSP1, 0x802);
  OSTimeDlyHMSM(0,0,0,10);
  SSP_SendData(LPC_SSP1, 0x806);
  OSTimeDlyHMSM(0,0,0,10);
  SSP1_Set14Bit();
  OSTimeDlyHMSM(0,0,0,10);
}

void LCDWriteDate(uint8_t addr, uint8_t data)
{
  uint16_t tmp;
  
  if(addr > 72)
    return;
  if(data > 16)
    return;
  tmp = (0x5 << 11) | (addr << 4) | data;
  
  SSP_SendData(LPC_SSP1, tmp);
  OSTimeDlyHMSM(0,0,0,2);
}


void LCDSetBit(uint8_t th, uint8_t com, uint8_t val)
{
  if(val)
  {
    LCDData[th] |= 1<<(com);
  }
  else
  {
    LCDData[th] &= ~(1<<(com));
  }
  LCDInvalidSet(th);
}

void GUI_LCDSetS(uint16_t LCD_S, uint8_t val)
{
  uint8_t th = LCD_S / 8;
  uint8_t com = LCD_S % 8;
  
  LCDSetBit(th, com, val);
}


void LCDSetNumBit(uint16_t LCD_NUM, char NUM_COM, uint8_t val)
{
  uint8_t th = 0,com = 0;
  if(LCD_NUM <= LCD_NUM1)
  {
    switch(NUM_COM)
    {
      case 'A':
      case 'a':
        th = (LCD_NUM + 0) / 8;
        com = (LCD_NUM + 0) % 8;
        break;
      case 'B':
      case 'b':
        th = (LCD_NUM + 1) / 8;
        com = (LCD_NUM + 1) % 8;
        break;
      case 'C':
      case 'c':
        th = (LCD_NUM + 4) / 8;
        com = (LCD_NUM + 4) % 8;
        break;
      case 'D':
      case 'd':
        th = (LCD_NUM + 6) / 8;
        com = (LCD_NUM + 6) % 8;
        break;
      case 'E':
      case 'e':
        th = (LCD_NUM + 5) / 8;
        com = (LCD_NUM + 5) % 8;
        break;
      case 'F':
      case 'f':
        th = (LCD_NUM + 2) / 8;
        com = (LCD_NUM + 2) % 8;
        break;
      case 'G':
      case 'g':
        th = (LCD_NUM + 3) / 8;
        com = (LCD_NUM + 3) % 8;
        break;
    }
  }
  else
  {
    switch(NUM_COM)
    {
      case 'A':
      case 'a':
        th = (LCD_NUM + 0) / 8;
        com = (LCD_NUM + 0) % 8;
        break;
      case 'B':
      case 'b':
        th = (LCD_NUM + 1) / 8;
        com = (LCD_NUM + 1) % 8;
        break;
      case 'C':
      case 'c':
        th = (LCD_NUM + 2) / 8;
        com = (LCD_NUM + 2) % 8;
        break;
      case 'D':
      case 'd':
        th = (LCD_NUM + 3) / 8;
        com = (LCD_NUM + 3) % 8;
        break;
      case 'E':
      case 'e':
        th = (LCD_NUM + 11) / 8;
        com = (LCD_NUM + 11) % 8;
        break;
      case 'F':
      case 'f':
        th = (LCD_NUM + 9) / 8;
        com = (LCD_NUM + 9) % 8;
        break;
      case 'G':
      case 'g':
        th = (LCD_NUM + 10) / 8;
        com = (LCD_NUM + 10) % 8;
        break;
    }
  }
  LCDSetBit(th, com, val);
}

void GUI_LCDSetNum(uint16_t LCD_NUM, uint8_t val, uint8_t ValMode)
{
  if(ValMode == INTMODE)
  {
    switch(val)
    {
      case 0:
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 0);
        break;
      case 1:
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 0);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 0);
        LCDSetNumBit(LCD_NUM, 'G', 0);
        break;
      case 2:
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 0);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 0);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 3:
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 0);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 4:
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 0);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 5:
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 6:
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 7:
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 0);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 0);
        LCDSetNumBit(LCD_NUM, 'G', 0);
        break;
      case 8:
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 9:
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 'A':
      case 'a':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 0);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 'B':
      case 'b':
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 'C':
      case 'c':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 0);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 0);
        break;
      case 'D':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 0);
        break;
      case 'd':
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 0);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 'E':
      case 'e':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 0);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 'F':
      case 'f':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 0);
        LCDSetNumBit(LCD_NUM, 'D', 0);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 'G':
      case 'g':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 'H':
      case 'h':
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 0);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 'L':
      case 'l':
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 0);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 0);
        break;
      case '-':
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 0);
        LCDSetNumBit(LCD_NUM, 'D', 0);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 0);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case '_':
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 0);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 0);
        LCDSetNumBit(LCD_NUM, 'G', 0);
        break;
      case LCD_DISPNULL:
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 0);
        LCDSetNumBit(LCD_NUM, 'D', 0);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 0);
        LCDSetNumBit(LCD_NUM, 'G', 0);
        break;
    }
  }
  else if(ValMode == CHARMODE)
  {
    switch(val)
    {
      case '0':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 0);
        break;
      case '1':
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 0);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 0);
        LCDSetNumBit(LCD_NUM, 'G', 0);
        break;
      case '2':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 0);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 0);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case '3':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 0);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case '4':
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 0);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case '5':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case '6':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case '7':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 0);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 0);
        LCDSetNumBit(LCD_NUM, 'G', 0);
        break;
      case '8':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case '9':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 'A':
      case 'a':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 0);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 'B':
      case 'b':
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 'C':
      case 'c':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 0);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 0);
        break;
      case 'D':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 0);
        break;
      case 'd':
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 0);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 'E':
      case 'e':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 0);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 'F':
      case 'f':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 0);
        LCDSetNumBit(LCD_NUM, 'D', 0);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 'G':
      case 'g':
        LCDSetNumBit(LCD_NUM, 'A', 1);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 'H':
      case 'h':
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 1);
        LCDSetNumBit(LCD_NUM, 'C', 1);
        LCDSetNumBit(LCD_NUM, 'D', 0);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case 'L':
      case 'l':
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 0);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 1);
        LCDSetNumBit(LCD_NUM, 'F', 1);
        LCDSetNumBit(LCD_NUM, 'G', 0);
        break;
      case '-':
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 0);
        LCDSetNumBit(LCD_NUM, 'D', 0);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 0);
        LCDSetNumBit(LCD_NUM, 'G', 1);
        break;
      case '_':
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 0);
        LCDSetNumBit(LCD_NUM, 'D', 1);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 0);
        LCDSetNumBit(LCD_NUM, 'G', 0);
        break;
      case LCD_DISPNULL:
        LCDSetNumBit(LCD_NUM, 'A', 0);
        LCDSetNumBit(LCD_NUM, 'B', 0);
        LCDSetNumBit(LCD_NUM, 'C', 0);
        LCDSetNumBit(LCD_NUM, 'D', 0);
        LCDSetNumBit(LCD_NUM, 'E', 0);
        LCDSetNumBit(LCD_NUM, 'F', 0);
        LCDSetNumBit(LCD_NUM, 'G', 0);
        break;
    }
  }
}


void GUI_LCDSetNumChar(uint16_t LCD_NUM, uint8_t val, uint8_t EnableFlash)
{
  if(EnableFlash)
  {
    if(numFlash.Enable)
    {
      if(numFlash.LCD_NUM == LCD_NUM)
      {
        numFlash.Val = val;
        numFlash.DataType = CHARMODE;
        return;
      }
      else
      {
        if(numFlash.DataType == CHARMODE)
          GUI_LCDSetNum(numFlash.LCD_NUM, numFlash.Val, CHARMODE);
        else
          GUI_LCDSetNum(numFlash.LCD_NUM, numFlash.Val, INTMODE);
        numFlash.LCD_NUM = LCD_NUM;
        numFlash.Val = val;
        numFlash.DataType = CHARMODE;
        return;
      }
    }
    else
    {
      numFlash.DataType = CHARMODE;
      numFlash.Enable = 1;
      numFlash.LCD_NUM = LCD_NUM;
      numFlash.Val = val;
    }
  }
  else
  {
    if(numFlash.Enable)
    {
      if(numFlash.LCD_NUM == LCD_NUM)
      {
        numFlash.Enable = 0;
        numFlash.LCD_NUM = 0XFFFF;
        GUI_LCDSetNum(LCD_NUM, val, CHARMODE);
        return;
      }
    }
    GUI_LCDSetNum(LCD_NUM, val, CHARMODE);
    return;
  }
}

void GUI_LCDSetNumInt(uint16_t LCD_NUM, uint8_t val, uint8_t EnableFlash)
{
  if(EnableFlash)
  {
    if(numFlash.Enable)
    {
      if(numFlash.LCD_NUM == LCD_NUM)
      {
        numFlash.Val = val;
        numFlash.DataType = CHARMODE;
        return;
      }
      else
      {
        if(numFlash.DataType == CHARMODE)
          GUI_LCDSetNum(numFlash.LCD_NUM, numFlash.Val, CHARMODE);
        else
          GUI_LCDSetNum(numFlash.LCD_NUM, numFlash.Val, INTMODE);
        numFlash.LCD_NUM = LCD_NUM;
        numFlash.Val = val;
        numFlash.DataType = INTMODE;
        return;
      }
    }
    else
    {
      numFlash.DataType = INTMODE;
      numFlash.Enable = 1;
      numFlash.LCD_NUM = LCD_NUM;
      numFlash.Val = val;
    }
  }
  else
  {
    if(numFlash.Enable)
    {
      if(numFlash.LCD_NUM == LCD_NUM)
      {
        numFlash.Enable = 0;
        numFlash.LCD_NUM = 0XFFFF;
        GUI_LCDSetNum(LCD_NUM, val, INTMODE);
        return;
      }
    }
    GUI_LCDSetNum(LCD_NUM, val, INTMODE);
    return;
  }
  
  
}

void LCDInvalidSet(uint8_t th)
{
  if(th >= 35)
    return;
  if(invalid_start > th)
    invalid_start = th;
  if(invalid_end < th)
    invalid_end = th;
}


void GUI_LCDUpdate(void)
{
  static uint8_t flashSpeed = LCD_FLASH_SPEED*2; 
  
  if(flashSpeed)
    flashSpeed--;
  else
    flashSpeed = LCD_FLASH_SPEED*2;
  
  if(numFlash.Enable)
  {
    if(flashSpeed > LCD_FLASH_SPEED)
    {
      GUI_LCDSetNum(numFlash.LCD_NUM, 0xff, INTMODE);
    }
    else
    {
      if(numFlash.DataType == INTMODE)
        GUI_LCDSetNum(numFlash.LCD_NUM, numFlash.Val, INTMODE);
      else
        GUI_LCDSetNum(numFlash.LCD_NUM, numFlash.Val, CHARMODE);
    }
  }
  
  if(invalid_start > invalid_end)
    return;
  
  for(int i = invalid_start; i <= invalid_end; i++)
  {
    LCDWriteDate(2*i, LCDData[i] >> 4);
    LCDWriteDate(2*i+1, LCDData[i] & 0x0f);
  }
  
  invalid_start = 34;
  invalid_end = 0;
}


void GUI_LCDDisableNumFlash()
{
  numFlash.Enable = 0;
  numFlash.LCD_NUM = 0xffff;
}

void GUI_LCDClean()
{
  memset(LCDData, 0, 35);
  invalid_start = 0;
  invalid_end = 34;
  GUI_LCDUpdate();
}