/*************************************************************
 * @file             channel480.c            
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	
#include "common.h"
		
		
		
		
		
		
		
/** @addtogroup     480模式
 *   
 *   @{
 */
 
 
 #define WEATHER_CHANNEL  225



#ifdef __cplusplus
  extern "C" {
#endif


uint16_t  GetNextChannel(uint16_t chn);
uint16_t  GetPrevChannel(uint16_t chn);
uint32_t  GetFrequency(uint16_t chn);
bool      IsChnAvailable(uint16_t chn);
			
void      ConfigChannel(uint16_t chn);
			
void      EnterAnalog(uint16_t chn);
void      ExitAnalog(void);
	  
void      EnterWeather(uint16_t chn);
void      ExitWeather(void);	 

#ifdef __cplusplus
 }
#endif










/** @} */