/*************************************************************
 * @file             
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	
	#ifndef _DIGITAL_CHANNEL_H
	#define _DIGITAL_CHANNEL_H
	
	#include <stdint.h>
	#include <stdbool.h>
	
	

	
	
	
	
	
	/** @addtogroup   数字频道
 *   
 *   @{
 */


#ifdef __cplusplus
  extern "C" {
#endif

	uint16_t DCH_GetNextChannel(void);
	uint16_t DCH_GetPrevChannel(void);
 uint16_t DCH_GetCurrChannel(void);
	void DCH_SetCurrChannel(uint16_t chn);
			
	void DCH_Enter();
	void DCH_Exit();

			
#ifdef __cplusplus
 }
#endif






/** @} */
	
	
	
	
	
	
	#endif
	