
/*************************************************************
 * @file        group_chat.c     
 * @brief       群组通话数据结构和方法实现
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
#include "group_chat.h"
#include "channel480.h"

#include "ccl.h"



/** @addtogroup  群组通话
*   
*   @{
*/


static uint16_t CurrentGroupChannel  = 1;
static uint16_t RecommandChannel  = 1;



//	/**@brief   得到群号
//  * 
//  *
//  * @return 群号
//  * @note
//  */
//	uint16_t Grp_GetID(void)
//	{
//				SysCfg config;
//				SysCfg_GetConfig(&config);
//				return config.groupNumber;
//	}



//	
//	
//	/**@brief 得到群密码
//  * 
//  *
//  * @return 系统密码
//  * @note   
//  */
//		uint32_t  Grp_GetPwd()
//		{
//				SysCfg config;
//				SysCfg_GetConfig(&config);
//				return config.groupPwd;
//	 }
		
		

/**@brief      群组频道切换到后一个
  *   
  *
  * @return    切换后的频道号
  * @note
  */		
 inline uint16_t Grp_GetNextChannel(void)
	{
		 return (CurrentGroupChannel = GetNextChannel(CurrentGroupChannel));
	}
	
	
	
/**@brief 群组频道切换到前一个
  * 
  *
  * @return 切换后的频道
  * @note
  */	
inline uint16_t Grp_GetPrevChannel(void)
{
	 return (CurrentGroupChannel = GetPrevChannel(CurrentGroupChannel));
}
		
		
	
 /**@brief      得到群组推荐频道
  * 
  *
  * @return      推荐频道
  * @note
  */
uint16_t Grp_RecommendChannel(void)
{
	  Grp_SetChannel(RecommandChannel);
	  return CurrentGroupChannel;  
}
	
	
	
	
void Grp_RequestRecommendChannel(void)
{
	 CCL_RequestAvailableChannel();
}
	
	
	
 /**@brief         设置群组的频道号
  * 
  * @param chn 要设置的群组频道号
  * @return    设置是否成功
  * @note
  */
void Grp_SetChannel(uint16_t chn)
{
	 if(chn > 0  &&  chn  <= 480){
			 CurrentGroupChannel  = GetNextChannel(chn);			
			 CurrentGroupChannel  = GetPrevChannel(CurrentGroupChannel);
		}
}
	
	
 /**@brief       得到群组的频道号
  * 
  *
  * @return void
  * @note
  */
inline uint16_t Grp_GetChannel()
{
	 return CurrentGroupChannel;
}



void Grp_UpdateRecommendChannel(uint16_t chn)
{
	  RecommandChannel  = chn;
}

	
	
void Grp_Enter()
{
printf("In group :%u -- %u Hz\n", CurrentGroupChannel, GetFrequency(CurrentGroupChannel));		
	 CCL_InGroup(GetFrequency(CurrentGroupChannel));
}




void Grp_Exit()
{
	 CCL_OutGroup();
}


/** @} */


	