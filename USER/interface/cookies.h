/**@file      cookies.h 
 * @brief
 * 
 * @author SealedGhost
 */
 
#include "common.h" 

 /** @addtogroup      Cookie
 *   
 *   @{
 */

/** Cookie项 */
typedef struct{
	uint16_t analogChan;  	/**< 模拟频道号 */
	uint16_t digitalChan;	/**< 数字频道号 */
	uint16_t groupChan;		/**< 群组频道号 */
}ChanCookie;


/** @} */#ifdef __cplusplus
  extern "C" {
#endif

void Coki_Store(void);
void Coki_Load(void);

void Coki_SetAnalogChan(uint16_t chn);
void Coki_SetDigitalChan(uint16_t chn);
void Coki_SetGroupChan(uint16_t chn);

uint16_t Coki_GetAnalogChan(void);
uint16_t Coki_GetDigitalChan(void);
uint16_t Coki_GetGroupChan(void);

#ifdef __cplusplus
 }
#endif

