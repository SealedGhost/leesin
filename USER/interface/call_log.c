/*************************************************************
 * @file           call_log.c
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/


#include "call_log.h"



#include "system_time.h"
#include "linkman.h"

#include <string.h>


#include "common.h"

#include "flash.h"


/** @addtogroup    通话记录
 *
 *   @{
 */



	#define CALL_LOG_MAX_NUMBER     30



typedef struct _CallLogListItem CallLogListItem;
struct _CallLogListItem{
	  uint16_t        pos;
		 CallLogItem     logItem;
		 CallLogListItem* pNext;
	};






static CallLogItem CallLogVector[CALL_LOG_MAX_NUMBER]  = {0};
static uint8_t  CallLogNumber  = 0;
static uint8_t  CurrLogItemPos  = 0;



 /**@brief  添加通话记录项
  *
  *
  * @return void
  * @note
  */
//static inline void CallLog_AddItem(CallLogItem* newItem)
//{
//	 if(CallLogNumber < CALL_LOG_MAX_NUMBER){
//			 memcpy( &(CallLogVector[CallLogNumber]), newItem, sizeof(CallLogItem) );

//Debug("Add call log item.%u,%u call %s,%s", CallLogVector[CallLogNumber].who.phoneNumber, CallLogVector[CallLogNumber].who.shipNumber,
//			                                CallLogVector[CallLogNumber].dir == CALL_DIR_IN ? "in" : "out", 
//			                                CallLogVector[CallLogNumber].isMiss == CALL_CONNECT ? "conect" : "miss");		
//         Debug("Call log time:%02d:%02d:%02d", CallLogVector[CallLogNumber].when.hour,
//                                               CallLogVector[CallLogNumber].when.minute,
//                                               CallLogVector[CallLogNumber].when.second);         
//			
//			 CallLogNumber++;
//			 if(CallLogNumber == 1){
//					 CurrLogItemPos  = 1;
//				}
//				
//				CallLog_Store();
//		}
//}





 /**@brief  添加一项通话记录
  *
  *
  * @return void
  * @note 如果通话记录满了，则从最早的一项覆盖
  */
void CallLog_Add(CallLogItem* newItem)
{
    int i = 0;
	
	if(newItem == NULL  ||  newItem->who.phoneNumber == 0)
	{
		return ;
	}
    
    if(CallLogNumber > 0){
        if(CallLogNumber == CALL_LOG_MAX_NUMBER){
            i  = CALL_LOG_MAX_NUMBER -1;
            CallLogNumber--;
        }
        else{
            i  = CallLogNumber;
        }
        
        for( ; i > 0; i--){
            memcpy(&CallLogVector[i], &CallLogVector[i-1], sizeof(CallLogItem));
        }
    }
    
    memcpy(&CallLogVector[0], newItem, sizeof(CallLogItem));
    CallLogNumber++;
    
    if(CallLogNumber == 1){
      CurrLogItemPos  = 1;   
    }
    
    CallLog_Store();
}



	/**@brief  删除指定序号的通话记录
  *
  * @param  所要删除的通话记录的位置
  * @note
  */
static inline void  _CallLog_Delete(uint16_t pos)
{
	if(CallLogNumber > 0  &&  pos <= CallLogNumber){
		/** 删除的不是最后一个 */
		if(pos < CallLogNumber) {
			// memcpy(&(CallLogVector[pos-1]),  &(CallLogVector[pos]), sizeof(CallLogItem) *(CallLogNumber-pos));
			int i  = 0;
			for(; i < CallLogNumber-pos; i++)
			{
				memcpy(&(CallLogVector[pos -1 +i]), &(CallLogVector[pos +i]), sizeof(CallLogItem));
			}
		}


		CallLogNumber--;	

		if(CurrLogItemPos > CallLogNumber)
		{
			CurrLogItemPos  = CallLogNumber;
		}
	}
}




	/**@brief  删除指定序号的通话记录
  *
  * @param  所要删除的通话记录的位置
  * @note
  */
 void  CallLog_Delete(uint16_t pos)
	{
		  if(CallLogNumber > 0){
						_CallLog_Delete(pos);		  
						CallLog_Store();					
				}
	}



/**@brief  第一个设置为当前项
  *
  *
  * @return void
  * @note
  */
void CallLog_GoHome(void)
{
	 if(CallLogNumber > 0){
			 CurrLogItemPos  = 1;
		}
		else{
			 CurrLogItemPos  = 0;
		}
}



 /**@brief    得到后一个的通话记录项
  *
  * @return   后一个通话记录项
  * @note
  */
CallLogItem* CallLog_GetNextLog()
{
	 if(CallLogNumber > 0){
			 if(CurrLogItemPos < CallLogNumber){
					 CurrLogItemPos++;
				}
				else{
					 CurrLogItemPos  = 1;
				}

				return &(CallLogVector[CurrLogItemPos-1]);
		}

		return 0;
}



 /**@brief    得到前一个的通话记录项
  *
  * @return   前一个通话记录项
  * @note
  */
CallLogItem* CallLog_GetPrevLog()
{
	 if(CallLogNumber > 0){
			 if(CurrLogItemPos > 1){
					 CurrLogItemPos--;
				}
				else{
					 CurrLogItemPos  = CallLogNumber;
				}

				return &(CallLogVector[CurrLogItemPos-1]);
		}

		return 0;
}



 /**@brief    得到当前的通话记录项
  *
  * @return  当前的通话记录项
  * @note
  */
CallLogItem* CallLog_GetCurrLog()
{
	 if(CallLogNumber > 0){
			 return &(CallLogVector[CurrLogItemPos-1]);
		}

		return 0;
}




/**@brief  得到当前位置
  * 
  *
  * @return 当前位置
  * @note
  */
inline uint8_t CallLog_GetCurrPos()
{
	 return CurrLogItemPos;
}




void CallLog_Store()
{
   /** 将没有用到的位置清空 */ 
	if(CallLogNumber < CALL_LOG_MAX_NUMBER){
		memset(&CallLogVector[CallLogNumber],  0, sizeof(CallLogItem) *(CALL_LOG_MAX_NUMBER - CallLogNumber));
	}
	  
	SPIFlashSectionWrite((uint8_t*)CallLogVector, FLASH_ADDRESS_CALL_LOG /0x1000, \
	      sizeof(CallLogItem) *(CallLogNumber < CALL_LOG_MAX_NUMBER ? CallLogNumber+1 : CALL_LOG_MAX_NUMBER));
}


void CallLog_Load()
{	 
	int  i  = 0;
	CallLogNumber  = 0;
	
	SPIFlashSectionRead((uint8_t*)CallLogVector,FLASH_ADDRESS_CALL_LOG / 0x1000, sizeof(CallLogItem)* CALL_LOG_MAX_NUMBER );
	  
	for(; i < CALL_LOG_MAX_NUMBER; i++){				
		 if(CallLogVector[i].who.phoneNumber > 0  &&  CallLogVector[i].who.phoneNumber != UINT32_MAX){
				  CallLogNumber++;
			}
			else{
				 break;
			}
	}
	
	if(CallLogNumber > 0){
		 CurrLogItemPos  = 1;
		  
		 if(CallLogNumber > CALL_LOG_MAX_NUMBER){
				 CallLogNumber  = CALL_LOG_MAX_NUMBER;
			} 
	}
}



#if 0

void CallLog_Dump()
{
  int i  = 0;
  CallLogItem* pItem; 
	
  for(; i < CallLogNumber; i++){
    pItem  = &CallLogVector[i];
			
		 	Debug("%2d - Call %s %s, phone id:%u, boat number:%u", i+1,  pItem->dir == CALL_DIR_IN ? "in" : "out",
			                                              pItem->isMiss == CALL_CONNECT ? "connect" : "miss",
                                                 pItem->who.phoneNumber, pItem->who.shipNumber);
  }
}

#endif



///**@brief      删除通话记录列表中最早的一条记录
//  *
//  * @note
//  */
//static void CallLogList_Delete(CallLogListItem* pIterm)
//{
//	 CallLogListItem* pIter  = pIterm->pNext;
//	 memset(pIterm, 0, sizeof(CallLogListItem));
//
//	 while(pIter){
//			 pIter->pos--;
//			 pIter  = pIter->pNext;
//		}
//	// TODO :eraser mem in flash
//}



// /**@brief    相通话记录列表中添加一条通话记录
//  *
//  * @param    pItem   新增通话记录的地址
//  * @return   添加是否成功
//  * @note
//  */
//bool  CallLogList_Add(CallLogItem* pItem)
//{
//	 if(pItem->who.phoneNumber == 0){
//			 return false;
//		}
//
//  if(CallLogListSize == CALL_LOG_MAX_NUMBER){
////			 CallLogList_DeleteOldest();
//		}
//
//		if(CallLogListSize){
//			 CallLogListItem* pNewItem  = (CallLogListItem*)Alloc(CallLogList, CALL_LOG_MAX_NUMBER, sizeof(CallLogListItem),  (size_t)&((CallLogListItem*)0)->logItem.who.phoneNumber);
//			 if(pNewItem){
//					 memcpy(pNewItem, pItem, sizeof(CallLogItem));
//					 CallLogListSize++;
//					 pNewItem->pos  = CallLogListSize;
//
//				 	pTailer->pNext  = pNewItem;
//					 pTailer  = pNewItem;
//
//
//					// TODO: Add in flash
//					 return true;
//				}
//				else{
//					 Debug("Call log list item alloc gg. Now there are %d item", CallLogListSize);
//					 return false;
//				}
//		}
//		else{
//			 CallLogListItem* pNewItem  = (CallLogListItem*)Alloc(CallLogList, CALL_LOG_MAX_NUMBER, sizeof(CallLogListItem),  (size_t)&((CallLogListItem*)0)->logItem.who.phoneNumber);
//			 if(pNewItem){
//					 memcpy(pNewItem, pItem, sizeof(CallLogItem));
//				 	CallLogListSize++;
//					 pNewItem->pos  = CallLogListSize;
//
//					 pHeader  = pNewItem;
//					 pTailer  = pNewItem;
//
//					// TODO: Add in flash
//					 return true;
//				}
//				else{
//				 	Debug("Call log list item alloc gg. Now there are %d item", CallLogListSize);
//					 return false;
//				}
//		}
//}



///**@brief  删除指定位置的通话记录
//  *
//  *
//  * @return void
//  * @note
//  */
//	uint16_t CallLogList_DeleteAt(uint16_t pos)
//	{
//			CallLogListItem* pIter  = pHeader;
//		 CallLogListItem* pBackUp  = pIter;
//
//
//		 if(pHeader->pos == pos){
//				 pBackUp  = pHeader;
//			 	pHeader  = pHeader->pNext;
//
//				 CallLogList_Delete(pBackUp);
//				 return 0;
//			}
//
//			pIter  = pHeader->pNext;
//
//		 while(pIter){
//				 if(pIter->pos  == pos){
//						 pBackUp->pNext  = pIter->pNext;
//						 CallLogList_Delete(pIter);
//						 return 0;
//					}
//					else{
//						pBackUp  = pIter;
//						pIter  = pIter->pNext;
//					}
//			}
//		 return 0;
//	}

//
//	/**@brief   将整个Call log list 写入Flash中
//  *
//  *
//  * @note
//  */
// void       CallLogList_Store(void)
//	{
//		 int i  = 0;
//		 CallLogListItem* pIter  = pHeader;
//
//		/// TODO: Write to flash
//		 while(pIter){
//				 // Flash to Write (addr + i*sizeof(CallLogItem), &pIter->logItem, sizeof(CallLogItem));
//				 i++;
//			}
//	}




/** @} */







