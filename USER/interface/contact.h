/*************************************************************
 * @file        contact.h        
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	
	#ifndef _CONTACT_H
	#define _CONTACT_H
	
	
	#include "linkman.h"
	
	/** @addtogroup      联系人列表
 *   
 *   @{
 */



#ifdef __cplusplus
  extern "C" {
#endif
			
			
bool Contact_Add(uint32_t shipNumber, uint32_t phoneId);
void Contact_Delete(uint16_t pos);
void     Contact_GoHome(void);
LinkMan* Contact_GetNextLinkMan(void);
LinkMan* Contact_GetPrevLinkMan(void);
LinkMan* Contact_GetCurrLinkMan(void);
LinkMan* Contact_FindByPhoneId(uint32_t phoneId);
uint16_t Contact_GetPosOfLinkMan(LinkMan* man);
uint16_t Contact_GetLinkManNumber(void);
			
void Contact_Store(void);
void Contact_Load(void);
			
			
#ifdef DEBUG
 void     Contact_Dump(void);
#endif			

#ifdef __cplusplus
 }
#endif



	
	
	
	

/** @} */
	
	
	
	
	
	
	
	
	
	
	#endif
	