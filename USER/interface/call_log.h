/*************************************************************
 * @file            call_log.h      
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	#ifndef _CALL_LOG_H
	#define _CALL_LOG_H
	
	#include "linkman.h"
	#include "system_time.h"
 #include "common.h"	
	
	

	/** @addtogroup  通话记录
 *   
 *   @{
 */



/** 通话记录项
	*
	*
	*/
	typedef struct{
		 LinkMan         who;    /**< 通话的联系人 */
		 Time            when;   /**< 通话产生的时间 */
		 uint8_t         dir;    /**< 通话方向，呼出或者呼入*/
		 uint8_t         isMiss; /**< 通话是否未接通 */
	}CallLogItem;
	
	
	#ifdef __cplusplus
  extern "C" {
#endif

	void         CallLog_Add(CallLogItem* newItem);
	void         CallLog_Delete(uint16_t pos);
	void         CallLog_GoHome(void);
	CallLogItem* CallLog_GetNextLog(void);
	CallLogItem* CallLog_GetPrevLog(void);
	CallLogItem* CallLog_GetCurrLog(void);
	uint8_t     CallLog_GetCurrPos(void);
			
			
//	bool       CallLogList_Add(CallLogItem* pItem);
// uint16_t   CallLogList_DeleteAt(uint16_t pos);
			
 void       CallLog_Store(void);
	void       CallLog_Load(void);
			
#ifdef DEBUG
 void       CallLog_Dump(void);
#endif			

#ifdef __cplusplus
 }
#endif

	

	




/** @} */
	
	
	
	
	#endif

	


	