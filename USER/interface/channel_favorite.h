/*************************************************************
 * @file               channel_favorite.h        
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	
	#ifndef _CHANNEL_FAVORITE_H
	#define _CHANNEL_FAVORITE_H
	
	
	#include "common.h"
	
	
	
	/** @addtogroup       频道收藏
 *   
 *   @{
 */



#ifdef __cplusplus
  extern "C" {
#endif

bool ChnFavorite_Add(uint16_t chn);
void ChnFavorite_DeleteAt(uint16_t pos);
void ChnFavorite_DeleteByChn(uint16_t chn);
uint16_t ChnFavorite_LookFor(uint16_t);
uint16_t ChnFavorite_GetNextChannel(void);
uint16_t ChnFavorite_GetPrevChannel(void);
uint16_t ChnFavorite_GetCurrChannel(void);
uint8_t  ChnFavorite_GetCurrPos(void);
void     ChnFavorite_GoHome(void);			
void ChnFavorite_Store(void);
void ChnFavorite_Load(void);
			
			
#ifdef DEBUG
void ChnFavorite_Dump(void);
#endif			
			

#ifdef __cplusplus
 }
#endif




/** @} */
	
	
	
	
	
	
	
	
	
	#endif
	