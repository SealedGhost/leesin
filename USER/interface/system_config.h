/*************************************************************
 * @file         system_config.h        
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	
#include "common.h"




/** @addtogroup        系统配置
 *   
 *   @{
 */
	
	/** 模拟频道模式枚举 
	 *
		*
		*/
	typedef enum{
		 AnalogChan480  = 1,  /**< 正常480模式 */
		 AnalogChan3200,      /**< xyorca 3200模式 */
		 AnalogChan5000       /**< 三星 5000模式  */
	}AnalogMode;
	
	
	
	/** 系统配置结构体 */
typedef struct{	 
	 uint32_t groupPwd;     /**< 群密码 */
	 uint32_t boatNumber;   /**< 船号 */
	 uint16_t groupNumber;  /**< 群号  */
	 uint8_t  analogMode;   /**< 模拟频道模式 */
}SysCfg;



#ifdef __cplusplus
  extern "C" {
#endif

 void SysCfg_SetConfig(SysCfg* newConfig);
	void SysCfg_GetConfig(SysCfg* config);
	void SysCfg_Load(void);
	void SysCfg_Store(void);

#ifdef __cplusplus
 }
#endif







/** @} */