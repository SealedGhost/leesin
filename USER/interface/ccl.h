/*************************************************************
 * @file              ccl.h        
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	#include "common.h"
	
	
	#ifndef _MY_CCL_H
	#define _MY_CCL_H
	
	
	
	/** @addtogroup    通信接口
 *   
 *   @{
 */


	
	/** CCL层与应用层消息类型枚举 */
	typedef enum{		
		 None  = 0,   /**< 空 */
		 CallIn,      /**< 来电 */  
		 CallOut,     /**< 主叫发起 */
		 Ring,        /**< 响铃 */
		 Connect,     /**< 接通 */
		 HungUp,      /**< 挂断 */
		 TellBusy,    /**< 忙 */
		 GrpIn,       /**< 群切入 */
		 GrpOut,      /**< 群切出 */
		 GrpMove,     /**< 群信道切换 */
		 GrpSing,     /**< 群组有声音 */
		 ChnSing,     /**< 信道有声音 */
		 ListenChn,   /**< 监听信道 */
		 IgnoreChn,   /**< 停止监听信道 */
		 BoatCall     /**< 船呼 */
	}CCLMsgType;   /**<  */
	

	
 /** CCL层与应用层交互的消息结构体  */
typedef struct{
		CCLMsgType  action;
	 int         v;
	 uintptr_t   p;
}CCLMsg;
	
	
	#ifdef __cplusplus
  extern "C" {
#endif

void  CCL_HandleOgle(CCLMsgType type, int v, uintptr_t p);
			
void  CCL_Call(uint32_t phoneId, uint32_t boatNumber);
void  CCL_HangUp(uint32_t phoneId);
void  CCL_Connect(uint32_t phoneId);
void  CCL_InGroup(uint32_t fre);
void  CCL_OutGroup();
void  CCL_InAnalog(uint32_t fre);
void  CCL_OutAnalog();
void  CCL_InWeather(uint32_t fre);
void  CCL_OutWeather();
void  CCL_InDigital(uint32_t fre);
void  CCL_OutDigital();		
void  CCL_RequestAvailableChannel();			
void  CCL_RequestBoatInfo(uint32_t boatNumber);
void  CCL_ConfigSystemParam(uint32_t phoineId, uint32_t boatNumber, uint32_t groupNumber, uint32_t groupPwd);
void  CCL_ConfigFrequency(uint32_t fre);
void  CCL_StopRx(void);
#ifdef __cplusplus
 }
#endif

	
	

/** @} */
	
	
	
	#endif
	