/*************************************************************
 * @file                       contact.c
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	

#include "contact.h"
#include "common.h"
#include "flash.h"


#define CONTACT_LIST_MAX_SIZE    30


typedef LinkMan ContactItem;



static ContactItem   ContactVector[CONTACT_LIST_MAX_SIZE]  = {0};
static uint8_t       ContactItemNumber  = 0;
static uint8_t       CurrentContactItemPos  = 0;

	
	
static inline bool Contact_AddLinkMan(LinkMan* newGay)
{
	 if(ContactItemNumber < CONTACT_LIST_MAX_SIZE){
			 ContactVector[ContactItemNumber].phoneNumber  = newGay->phoneNumber;
			 ContactVector[ContactItemNumber++].shipNumber  = newGay->shipNumber;
			 if(ContactItemNumber == 1){
					  CurrentContactItemPos  = 1;
				}
				
				Contact_Store();
			 return true;
	}
	else{
		return false;
	}
}


/** @addtogroup   联系人列表
 *   
 *   @{
 */


	
	
 /**@brief    添加联系人
  * 
  * @param   shipNumber  新加联系人船号
  * @param   phoneId     新加联系人呼号
  * @return  void
  * @note
  */
bool Contact_Add(uint32_t shipNumber, uint32_t phoneId)
{
	int  i  = 0;
	LinkMan man;
		
	for(; i < ContactItemNumber; i++){
		if(ContactVector[i].phoneNumber == phoneId){
			ContactVector[i].shipNumber  = shipNumber;
			Contact_Store();
			return true;
		}
	}
			
	man.phoneNumber  = phoneId;
    man.shipNumber   = shipNumber;
   
			
	return Contact_AddLinkMan(&man);			
}
	
	
	
 /**@brief  删除指定序号的联系人
  * 
  * @param  所要删除的联系人的序号
  * @note
  */
void Contact_Delete(uint16_t pos)
{
	 uint16_t idx  = pos -1;
	
	 if(ContactItemNumber > 0  &&  pos <= ContactItemNumber){
			/** 删除的不是最后一个 */
			if(pos < ContactItemNumber){
				//memcpy( &ContactVector[pos-1], &ContactVector[pos], sizeof(ContactItem)* (ContactItemNumber-pos) );
				int i  = 0;
				for(; i < ContactItemNumber - pos; i++)
				{
				 memcpy( &(ContactVector[pos -1 +i]),  &ContactVector[pos +i], sizeof(ContactItem));
				}
			}

			
			ContactItemNumber--;
			
			if(CurrentContactItemPos > ContactItemNumber)
			{
				CurrentContactItemPos  = ContactItemNumber;
			}
			
			Contact_Store();
		}
}
	
	
	
	
 /**@brief  得到下一条联系人记录
  * 
  *
  * @return 下一条联系人
  * @note
  */
LinkMan*  Contact_GetNextLinkMan()
{
	 if(ContactItemNumber > 0){
			 if(CurrentContactItemPos < ContactItemNumber){
					 CurrentContactItemPos++;
				}
				else{
					 CurrentContactItemPos  = 1;
				}
				
				return &(ContactVector[CurrentContactItemPos-1]);
		}
		 return 0;
}
	

	
	
	
 /**@brief    得到前一条联系人记录
  * 
  *
  * @return 前一条联系人
  * @note
  */
LinkMan* Contact_GetPrevLinkMan()
{
	 if(ContactItemNumber > 0){
			 if(CurrentContactItemPos > 1){
					  CurrentContactItemPos--;
				}
				else{
					  CurrentContactItemPos  = ContactItemNumber;
				}
				return &(ContactVector[CurrentContactItemPos-1]);
		}
		
		return 0;
}
	
	
	
 /**@brief  得到当前联系人
  * 
  *
  * @return 当前联系人
  * @note
  */
LinkMan* Contact_GetCurrLinkMan()
{
	 if(ContactItemNumber > 0){				
					return &(ContactVector[CurrentContactItemPos-1]);
		}
		
		return 0;
}
	
	
	
 /**@brief  第一个设为当前
  * 
  *
  * @return void
  * @note
  */
void Contact_GoHome()
{
	 if(ContactItemNumber > 0){
			 CurrentContactItemPos  = 1;
		}
		else{
			 CurrentContactItemPos  = 0;
		}
}
	
	
	
	
 /**@brief     根据呼号查询联系人
  * 
  *
  * @return void
  * @note
  */
LinkMan* Contact_FindByPhoneId(uint32_t phoneId)
{
	 int i  = 0;
	 
	 for(; i < ContactItemNumber; i++){
			 if(ContactVector[i].phoneNumber  ==  phoneId){
					  return &(ContactVector[i]);
				}
		}
		
		return 0;
}
	
	
	
 /**@brief    根据联系人得到其在联系人向量中的位置
  * 
  *
  * @return void
  * @note
 */
uint16_t Contact_GetPosOfLinkMan(LinkMan* man)
{
	 if(man >= ContactVector  &&  man < &ContactVector[ContactItemNumber]){
			 return (man - ContactVector + 1);
		}
		
		return 0;
}

	
	
	
inline uint16_t Contact_GetLinkManNumber()
{
		return ContactItemNumber;
}
	
	
	
	
 /**@brief  存储联系人列表
  * 
  *
  * @return void
  * @note
  */
void Contact_Store()
{
	 /// TODO: Write to flash
	 /** 将没有用到的位置清空 */ 		
	 memset(&ContactVector[ContactItemNumber], 0, (CONTACT_LIST_MAX_SIZE - ContactItemNumber) *sizeof(ContactItem));
	 SPIFlashSectionWrite((uint8_t*)ContactVector, FLASH_ADDRESS_CONTACT / 0x1000, \
			   sizeof(ContactItem) *(ContactItemNumber < CONTACT_LIST_MAX_SIZE ? ContactItemNumber + 1: CONTACT_LIST_MAX_SIZE));
}
	
	
	
 /**@brief  从flash中载入联系人
  * 
  *
  * @return void
  * @note
  */
void Contact_Load()
{
	/// TODO: Read from flash
	 int i  = 0;
	
	 ContactItemNumber  = 0;
	
	 SPIFlashSectionRead((uint8_t*)ContactVector, FLASH_ADDRESS_CONTACT /0x1000, sizeof(ContactItem) *CONTACT_LIST_MAX_SIZE);
	
	 for(; i < CONTACT_LIST_MAX_SIZE; i++){
			 if(ContactVector[i].phoneNumber > 0  &&  ContactVector[i].phoneNumber != UINT32_MAX){
					  ContactItemNumber++;
				}
				else{
					  break;
				}
		}
		
		if(ContactItemNumber > 0 ){
			  CurrentContactItemPos  = 1;
			  
			  if(ContactItemNumber > CONTACT_LIST_MAX_SIZE){
						  ContactItemNumber  = CONTACT_LIST_MAX_SIZE;
					}
		}
}
	
	
	
#if 0

void Contact_Dump()
{
	 int i  = 0;
	 
}

#endif


/** @} */
	
	
	
	
	