/*************************************************************
 * @file             
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	
#include "system_time.h"
#include "string.h"

#include "rtc.h"
	
	
	/** @addtogroup  系统时间
 *   
 *   @{
 */


static Time SystemTime  = {0};

void SysTime_Set(uint32_t year, uint32_t month, uint32_t day, uint32_t hour, uint32_t minute, uint32_t second)
{
	 SystemTime.year     = year;
	 SystemTime.month    = month;
	 SystemTime.day      = day;
	 SystemTime.hour     = hour;
	 SystemTime.minute   = minute;
	 SystemTime.second   = second;
	
	 SetRtcTime(SystemTime.year, SystemTime.month, SystemTime.day,
	            SystemTime.hour, SystemTime.minute, SystemTime.second);
}


void SysTime_Get(Time* time)
{
	 if(time){
		   memcpy(time, &SystemTime, sizeof(Time))	;
	 }	 
}



void SysTime_Update(uint32_t year, uint32_t month, uint32_t day, uint32_t hour, uint32_t minute, uint32_t second)
{
	SystemTime.year     = year;
	SystemTime.month    = month;
	SystemTime.day      = day;
	SystemTime.hour     = hour;
	SystemTime.minute   = minute;
	SystemTime.second   = second;
}




/** @} */