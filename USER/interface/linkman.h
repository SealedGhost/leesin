/*************************************************************
 * @file            linkman.h          
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	
	#ifndef _LINK_MAN_H
	#define _LINK_MAN_H
	
	#include <stdint.h>
	#include <stdbool.h>
	
	
	
	/** @addtogroup 联系人
 *   
 *   @{
 */


/** 联系人结构体
 *
	*
	*
	*/
	typedef struct{
		 uint32_t shipNumber;  /**< 联系人船号 */
		 uint32_t phoneNumber; /**< 联系人呼号 */
	}LinkMan;





/** @} */
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	#endif
	
	
	