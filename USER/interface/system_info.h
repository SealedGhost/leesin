/*************************************************************
 * @file         system_info.h     
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	
	
	#include "common.h"
	
	/** @addtogroup
 *   
 *   @{
 */

	#define PHONE_ADDRESS       0x3000
	#define VERSION_ADDRESS     0x3006
  
	#define SYS_PWD             59118
	
	
	/** 版本状态：内部测试版本 */
	#define VER_STATE_ALPHA      0 
	
	/** 版本状态：公开测试版 */
	#define VER_STATE_BETA       1
	
	/** 版本状态：候选版本(Release Candidate) */
	#define VER_STATE_RC         2
	
	/** 版本状态：稳定版 */
	#define VER_STATE_STABLE     3
	
	
	
	
	
	/** 版本信息结构体 */
	typedef struct{
			uint8_t major;        /**< 主版本号   */
		 uint8_t minor;        /**< 次版本号   */
		 uint8_t revise;       /**< 修订版本号 */
	 	uint8_t state;        /**< 版本状态 {alpha, beta, rc, stable} */
		 uint8_t day;          /**< 版本日期--日 */
		 uint8_t month;        /**< 版本日期--月 */
		 uint16_t year;        /**< 版本日期--年 */
	}VersionInfo;

	

	/** 系统信息结构体 */
		typedef struct{
			VersionInfo version;  /**< 版本信息 */
		 uint32_t    phoneId;  /**< 对讲机号 */
		 uint16_t    pwd;      /**< 系统密码 */
	}SysInfo;
		
	
	
	uint32_t SysInfo_GetId(void);
	uint16_t SysInfo_GetPassword(void);
	void     SysInfo_GetVersion(VersionInfo* ver);
	void     SysInfo_Load(void);
	void     SysInfo_Store(void);
		
/** @} */	
	
	
	
	
	
	
	
	
	
	