/*************************************************************
 * @file          ccl.c     
 * @brief      CCL层接口实现
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	
	
#include "ccl.h"
#include "macService.h"
#include "call_mgr.h"
#include "group_chat.h"
#include "boat_search.h"

#include "WindowManager.h"
#include "dlg.h"
#include "UIMessage.h"
#include "audio_mgr.h"
#include "ipc.h"
#include "gpio.h"
#include "adc.h"

#include "rt_indicator.h"
#include "GUI_Timer.h"
#include "contact.h"	
	
	/** @addtogroup  通信接口
 *   
 *   @{
 */

extern Window VirtualWindow;


void  CCL_HandleOgle(CCLMsgType type, int v, uintptr_t p)
{

    switch(type & UINT16_MAX)
    {
         /** 单呼 */
         case IndCallTCRingAction:{
                      WM_Message msg;
                      LinkMan * man  = OnCallIn(v, p);
                
           Debug("CallIn phone id:%u", v);           
           
                      if(man){
                           msg.data_p  = (uintptr_t*)(man->shipNumber);
           Debug("Have matched contact. boat number :%u", man->shipNumber);                        
                      }
                      else{
                           msg.data_p  = (uintptr_t*)p;
                      }
                      msg.pTarget  = WM_GetFocus();
                      msg.msgType  = USER_MSG_CALL_IN;
                      msg.data_v   = v;

                      WM_SendMessage(&msg);
                 }
          break;
     
            /** 对方接通 */
          case IndCallConnectAction:{
                    WM_Message  msg;
                
                    if(OnConnect(v, p)){
                        msg.pTarget  = WM_GetFocus();
                        msg.msgType  = USER_MSG_CONNECT;
                        msg.data_v   = v;
                        msg.data_p   = (uintptr_t*)p;
                        WM_SendMessage(&msg);
                    }
               }	    
          break;
            
         /** 对方挂断 */
        case IndCallHangUpAction:{
                 WM_Message  msg;
            
                if( OnHangUp(v, p)) {
                    msg.pTarget  = WM_GetFocus();
                    msg.msgType  = USER_MSG_HUNG_UP;
                    msg.data_v   = v;
                    msg.data_p   = (uintptr_t*)p;
                    WM_SendMessage(&msg);
                }
             }
                
         break;
                            
        /** 对方响铃 */					
        case IndCallOCRingAction:{
                  WM_Message  msg;
                  LinkMan* man  = OnRing(v, p);						
                  
                  if(man){
                       msg.data_p  = (uintptr_t*)(man->shipNumber);
                  }
                  else{
                     msg.data_p  = (uintptr_t*)p;
                  }
                  msg.pTarget  = WM_GetFocus();
                  msg.msgType  = USER_MSG_RING;
                  msg.data_v   = v;
                  WM_SendMessage(&msg);
             }
                
             break;
            
        /** 对方忙 */
        case IndCallTcBusyAction:{
                 WM_Message msg;
                
                 if( OnTellBusy(v, p) ){
                   msg.pTarget  = WM_GetFocus();
                    msg.msgType  = USER_MSG_TELL_BUSY;
                    msg.data_v   = v;
                    msg.data_p   = (uintptr_t*)p;
                    WM_SendMessage(&msg);
                 }
             }						   
        break;
            
        /** 呼叫失败  */
        case IndCallFailAction:{
                WM_Message msg;
            
                if(OnCallFail(v, p) ){ 
                     msg.pTarget  = WM_GetFocus();
                     msg.msgType  = USER_MSG_CALL_FAIL;
                     msg.data_v   = v;
                     msg.data_p   = (uintptr_t*)p;
                     WM_SendMessage(&msg);                                
                }
             }	
         break;
                       
            
        /** 群呼状态时，MAC层通知应用层说话人信息指令 */
        case GroupCallTalkerStartNotifyAction:{
                WM_Message msg;
				/** 当前讲话的人在通讯录中存在 */
				LinkMan*  man  = Contact_FindByPhoneId(v);
				if(man != NULL)
				{
					msg.data_p  =  (uintptr_t*)(man->shipNumber);
				}			
				else
				{
					msg.data_p  = (uintptr_t*)p;
				}

                msg.pTarget  = WM_GetFocus();
                msg.msgType  = USER_MSG_GRP_SING;
                msg.data_v   = v;
                WM_SendMessage(&msg);		
             }
        break;
                                
        /** 群呼状态时，MAC层通知应用层说话人信息指令 */
        case GroupCallTalkerOverNotifyAction:{
                WM_Message msg;
			


                msg.pTarget  = WM_GetFocus();
                msg.msgType  = USER_MSG_GRP_MUTE;
                msg.data_v   = 0;
                msg.data_p   = 0;
                WM_SendMessage(&msg);		
             }
        break;										
            
        /** 群呼状态信令上收到消息推送应用层，应用层收到消息后亮灯 */
        case GroupCallMsgNotifyAction:{
//                 printf("  GroupCallMsgNotifyAction\n\n");
				 GUI_CreateTimer(&VirtualWindow, 126,  20);
				
                 LedYellowOn();
             }
        break;
            
        /** 船号选呼应答 */					
        case ShipInfoResponseAction:  
			if(BoatSch_AddResult(v))
			{
                WM_Message msg;

Debug("OnShipResponse:%ld", v);				
				msg.pTarget  = WM_GetFocus();
				msg.msgType  = USER_MSG_BOAT_INFO_RESPONSE;
				msg.data_v   = v;
				msg.data_p   = (uintptr_t*)p;
				WM_SendMessage(&msg);
			}     
        break;
              
        /** 推送可用信道号  */
        case PushAvailableChanNumAction:{
                   WM_Message msg;
                   
                   msg.pTarget  = WM_GetFocus();
                   msg.msgType  = USER_MSG_CHAN_RECOMMEND;
                   msg.data_v   = v;
                   msg.data_p   = 0;
                   WM_SendMessage(&msg);
             }
        break;
            
        /** 开始发射 */
        case TxStartAction:{
             WM_Message msg;                
            Debug("     ccl ---  ptt push \n");
                  RTState_StartTx();
            
                  msg.msgType   = USER_MSG_NOTIFY_RFTX;
                  msg.data_v    = 1;
                 
                  if( (WM_GetFocus() == &analogWin)  ||  (WM_GetFocus() == &rememberChWin)){
                      msg.pTarget  = WM_GetFocus();
                      WM_SendMessage(&msg);
                  }
            
                  msg.pTarget   = &mainWin;
                  WM_SendMessage(&msg);
             }
         break;
            
        /** 结束发射 */
        case TxOverAction:{
                  WM_Message msg;
Debug("     ccl ---  ptt release \n");

                  RTState_StopTx();
             
                  msg.pTarget   = &mainWin;
                  msg.msgType   = USER_MSG_NOTIFY_RFTX;
                  msg.data_v    = 0;
                  WM_SendMessage(&msg);	


                  if(RTState_IsRxing())	{
                     msg.msgType  = USER_MSG_NOTIFY_RFRX;
                     msg.data_v    = getChannelRssi(CclChannelIndicator);
                     msg.data_v  = msg.data_v /(ADC_VALUE_MAX /10);
                     if(msg.data_v == 0){
                        msg.data_v  = 1;
                     }
                     WM_SendMessage(&msg);
                  }								  
             }
        break;
                                                    
    }
}
	
	
	
 /**@brief  CCL呼叫
  * 
  * @param phoneId         呼叫号码
	 * @param boatNumber      本机船号
  * @return void
  * @note
  */
inline void CCL_Call(uint32_t phoneId, uint32_t boatNumber)
{

    #ifdef SCH_LOCK_WHEN_POST
    OSSchedLock();
    #endif  
    
    CCL_PostMessage(IPC_MSG_GUI, IndCallDialAction & UINT32_MAX, phoneId, boatNumber);
    
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedUnlock();
    #endif     

}
	
	
	
 /**@brief  CCL 挂断  
  * 
  * @param phoneId    要挂断的号码
  * @return void
  * @note
  */
inline void CCL_HangUp(uint32_t phoneId)
{

    #ifdef SCH_LOCK_WHEN_POST
    OSSchedLock();
    #endif     
    
    CCL_PostMessage(IPC_MSG_GUI, IndCallHangUpAction, phoneId, 0);
    
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedUnlock();
    #endif     

}
	
	
	
 /**@brief CCL 接通
  * 
  * @param 要接通的号码
  * @return void
  * @note
  */
inline void CCL_Connect(uint32_t phoneId)
{

    #ifdef SCH_LOCK_WHEN_POST
    OSSchedLock();
    #endif     
    CCL_PostMessage(IPC_MSG_GUI, IndCallConnectAction, phoneId, 0);
    
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedUnlock();
    #endif     

}
	
	
	
 /**@brief  CCL 进入群组
  * 
  * @param groupNumber  群号
	 * @param fre          群所在的信道频率
  * @return void
  * @note
  */
inline void CCL_InGroup(uint32_t fre)
{

    #ifdef SCH_LOCK_WHEN_POST
    OSSchedLock();
    #endif     
    CCL_PostMessage(IPC_MSG_GUI, GroupCallStartAction, fre, 0);
    
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedUnlock();
    #endif     

}
    
	
	
	
 /**@brief  CCL 离开群组
  * 
  * @param groupNumber 群号
  * @return void
  * @note
  */
inline void CCL_OutGroup()
{

    #ifdef SCH_LOCK_WHEN_POST
    OSSchedLock();
    #endif         
    
    CCL_PostMessage(IPC_MSG_GUI, GroupCallOverAction, 0, 0);
    
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedUnlock();
    #endif     

}
	
	
	
	/**@brief  CCL 进入模拟
  * 
  * @param 模拟信道频率
  * @return void
  * @note
  */
inline void CCL_InAnalog(uint32_t fre)
{
        //printf("In analog, %d, %d\n", IPC_MSG_GUI, AnalogCallStartAction);
    
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedLock();
    #endif     
    
    CCL_PostMessage(IPC_MSG_GUI, AnalogCallStartAction, fre, 0);
    
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedUnlock();
    #endif     
   
}
	
	
	
	/**@brief CCL 退出模拟
  * 
  *
  * @return void
  * @note
  */
inline void CCL_OutAnalog()
{
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedLock();
    #endif     
    
    CCL_PostMessage(IPC_MSG_GUI, AnalogCallOverAction, 0, 0);
    
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedUnlock();
    #endif        
}


/**@brief   CCL 进入天气
  * 
  * @param fre 天气频道的频率
  * @return void
  * @note
  */	
inline void CCL_InWeather(uint32_t fre)	
{
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedLock();
    #endif     
    
	CCL_PostMessage(IPC_MSG_GUI, WeatherStartAction, fre, 0);
    
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedUnlock();
    #endif      
}


/**@brief  CCL 退出天气
  * 
  *
  * @return void
  * @note
  */
inline void CCL_OutWeather()
{ 
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedLock();
    #endif     
    
	CCL_PostMessage(IPC_MSG_GUI, WeatherOverAction, 0, 0);
    
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedUnlock();
    #endif        
}
	
	
 /**@brief CCL 进入数字
  * 
  * @param  数字信道频率
  * @return void
  * @note
  */
inline void CCL_InDigital(uint32_t fre)
{
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedLock();
    #endif     

    CCL_PostMessage(IPC_MSG_GUI, DigitalCallStartAction, fre, 0);

    #ifdef SCH_LOCK_WHEN_POST
    OSSchedUnlock();
    #endif        
}
	
	
	
	/**@brief CCL 退出数字
  * 
  *
  * @return void
  * @note
  */
inline void CCL_OutDigital()
{
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedLock();
    #endif 
    
    CCL_PostMessage(IPC_MSG_GUI, DigitalCallOverAction, 0, 0);
    
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedUnlock();
    #endif     
    
}




void CCL_RequestAvailableChannel()
{
    
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedLock();
    #endif     
    
    CCL_PostMessage(IPC_MSG_GUI, RequestAvailableChanNumber, 0, 0);
    
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedUnlock();
    #endif     
    
}
	
	
	
	/**@brief CCL 船呼请求
  * 
  * @param boatNumber 所请求的船的船号
  * @return void
  * @note
  */	
inline void CCL_RequestBoatInfo(uint32_t boatNumber)
{
    
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedLock();
    #endif 
    
    CCL_PostMessage(IPC_MSG_GUI, ShipInfoRequestAction, boatNumber, 0);
    
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedUnlock();
    #endif     
    
}
	
	
	
	/**@brief CCL 系统参数配置
  * 
  * @param phoneId   
	 * @param param       
  * @return void
  * @note
  */
inline void CCL_ConfigSystemParam(uint32_t phoineId, uint32_t boatNumber, uint32_t groupNumber, uint32_t groupPwd )
{
    static ShipInformation Info  = {0};
//		  CCL_PostMessage(IPC_MSG_GUI, ConfigParaAction, size, param);
    Info.shipId  = phoineId;
    Info.shipNumber    = boatNumber;
    Info.groupNumber  = groupNumber;
    Info.grouppwd   = groupPwd;

    #ifdef SCH_LOCK_WHEN_POST
    OSSchedLock();
    #endif         
     
     CCL_PostMessage(IPC_MSG_GUI, ConfigParaAction,sizeof(Info),(uintptr_t)&Info);
     
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedUnlock();
    #endif          
     
}

	
	
 /**@brief  CCL  频率配置
  * 
  * @param  fre  频率(单位为Hz)
  * @return void
  * @note
  */
inline void CCL_ConfigFrequency(uint32_t fre)
{		  
    
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedLock();
    #endif         
    
    CCL_PostMessage(IPC_MSG_GUI, ConfigVoiceServiceFreqAction, fre, 0);
            
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedUnlock();
    #endif         
    
}
    
    
inline void CCL_StopRx()
{
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedLock();
    #endif    

    CCL_PostMessage(IPC_MSG_GUI, RxOverAction, 0, 0);
    
    #ifdef SCH_LOCK_WHEN_POST
    OSSchedUnlock();
    #endif    
    
}    
	
	/** @} */
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	