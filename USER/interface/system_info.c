/*************************************************************
 * @file                        system_info.c        
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	

#include  "system_info.h"

#include "flash.h"


/** @addtogroup     系统信息
*   
*   @{
*/
	
	


	
	
	
static SysInfo SystemInfo  = {0};
	
	
	
	
	/**@brief  得到本机呼号
  * 
  *
  * @return void
  * @note
  */
 inline  uint32_t SysInfo_GetId()
	{
		 return SystemInfo.phoneId;
	}

	
	
	/**@brief 得到本机密码
  * 
  *
  * @return void
  * @note
  */
inline  uint16_t SysInfo_GetPassword()
{
	 return SystemInfo.pwd;
}
	
	
	
	/**@brief  得到版本信息
  * 
  *
  * @return void
  * @note
  */
void SysInfo_GetVersion(VersionInfo* ver)
{
	 if(ver){
			 memcpy(ver, &SystemInfo.version, sizeof(VersionInfo));
		}
}



	
	/**@brief  从Flash中载入系统信息
  * 
  *
  * @return void
  * @note
  */

void SysInfo_Load()
{
	 SPIFlashSectionRead((uint8_t*)&SystemInfo, FLASH_ADDRESS_SYS_INFO / 0x1000, sizeof(SystemInfo));
	
	 if(SystemInfo.phoneId == 0  ||  SystemInfo.phoneId == UINT32_MAX || SystemInfo.pwd != 59118){
			 SystemInfo.phoneId  = *(uint8_t*)(PHONE_ADDRESS)<<24 | *(uint8_t*)(PHONE_ADDRESS+1)<<16 | *(uint8_t*)(PHONE_ADDRESS+2)<<8 | *(uint8_t*)(PHONE_ADDRESS+3);
			 SystemInfo.pwd      = 59118;
			 
			 /// 版本号 1.0.0
			 SystemInfo.version.major  = *(uint8_t*)(VERSION_ADDRESS); 			
			 SystemInfo.version.minor  = *(uint8_t*)(VERSION_ADDRESS + 1);
			 SystemInfo.version.revise = *(uint8_t*)(VERSION_ADDRESS + 2);
			 SystemInfo.version.day  = 18;
			 SystemInfo.version.month = 7;
			 SystemInfo.version.year  = 2017;
			 SystemInfo.version.state  = *(uint8_t*)(VERSION_ADDRESS + 3);
			 SysInfo_Store();				
	}
	else{
//            printf("sysInfo Load success!\n");
}
	
    
	SystemInfo.phoneId  = *(uint8_t*)(PHONE_ADDRESS)<<24 | *(uint8_t*)(PHONE_ADDRESS+1)<<16 | *(uint8_t*)(PHONE_ADDRESS+2)<<8 | *(uint8_t*)(PHONE_ADDRESS+3);

	printf("load system info phoneid %d\n\n", SystemInfo.phoneId);
}
	
	
	
	
	/**@brief  系统信息存储到Flash
  * 
  *
  * @return void
  * @note
  */
void SysInfo_Store()
{
	  SPIFlashSectionWrite((uint8_t*)&SystemInfo, FLASH_ADDRESS_SYS_INFO / 0x1000, sizeof(SystemInfo));
}

/** @} */


	