/*************************************************************
 * @file             call_mgr.h 
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	
	#ifndef _CALL_MGR_H
	#define _CALL_MGR_H
	
	
	#include "common.h"
	
	#include "call_log.h"
    
    #include "system_time.h"
	
	
	
	/** @addtogroup        通话管理
 *   
 *   @{
 */


 /** 表示一个通话事务 */
typedef struct{
	 uint8_t  type;     /**< 本次事务的通话类型 */
	 uint8_t  state;    /**< 本事务的当前通话状态 */
	 uint32_t id;       /**< 唯一标识，通话的对讲机号 */
	 uint32_t val;      /**< 用于放群号、船号等*/
     Time     time;     /**< 生成的时间戳*/
}CallTransaction;



#ifdef __cplusplus
 extern "C" {
#endif


void CallMgr_DeleteCallTransaction(uint32_t id);
CallTransaction* CallMgr_GetCallTransaction(int i);
bool CallMgr_AddCallTransaction(CallTransaction* pTransaction);
uint8_t CallMgr_GetCallTransactionNumber(void);
void    CallMgr_HangUp(uint32_t phoneId);
void    CallMgr_Connect(uint32_t phoneId);
uint32_t  CallMgr_Call(uint32_t phoneId);
	
		
LinkMan* OnCallIn(uint32_t phoneId, uint32_t boatNumber);
LinkMan* OnRing(uint32_t phoneId, uint32_t boatNumber);
bool OnConnect(uint32_t phoneId, uint32_t boatNumber);
bool OnHangUp(uint32_t phoneId, uint32_t boatNumber);		
bool OnTellBusy(uint32_t phoneId, uint32_t boatNumber);
bool OnCallFail(uint32_t phoneId, uint32_t boatNumber);
		
#ifdef DEBUG
 void CallMgr_DumpTransaction(void);
#endif
		
#ifdef __cplusplus
	}
#endif		
		
		

/** @} */
	
	
	
	
	
	
	
	
	#endif
	