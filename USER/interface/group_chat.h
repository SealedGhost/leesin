/*************************************************************
 * @file          grout_char.h
 * @brief        群组通话相关数据结构和方法
 * 
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	

	#ifndef _GROUP_CHAT_H
	#define _GROUP_CHAT_H
	
	#include <stdint.h>
	#include <stdbool.h>
	
	
	/** @addtogroup  群组通话
 *   
 *   @{
 */


#ifdef __cplusplus
  extern "C" {
#endif

//	uint16_t Grp_GetID(void);

//	
//	uint32_t  Grp_GetPwd(void);


 uint16_t  Grp_GetNextChannel(void);
	uint16_t  Grp_GetPrevChannel(void);
	void      Grp_SetChannel(uint16_t chn);
	uint16_t  Grp_GetChannel(void);
	void      Grp_UpdateRecommendChannel(uint16_t chn);
	uint16_t  Grp_RecommendChannel(void);
	void      Grp_RequestRecommendChannel(void);
			
 void      Grp_Enter();
	void      Grp_Exit();
			 

#ifdef __cplusplus
 }
#endif


	



/** @} */
	
	


	
	

	
	
	
	
	
	
	
	
	#endif
	