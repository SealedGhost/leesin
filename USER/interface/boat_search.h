/*************************************************************
 * @file           boat_search.h       
 * @brief        船舶信息搜索头文件
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	
	
	#ifndef _BOAT_SEARCH_H
	#define _BOAT_SEARCH_H
	
	
	
	#include "common.h"
	
	
	#define BOAT_SEARCH_MAX_NUMBER   5
	
	
	/** @addtogroup  船呼
 *   
 *   @{
 */





 
	#ifdef __cplusplus
  extern "C" {
#endif

void BoatSch_DoSearch(uint32_t boatNumber);
bool BoatSch_AddResult(uint32_t newPhoneId);
uint8_t  BoatSch_GetCurrPos();	
uint32_t BoatSch_GetCurrResult();
uint32_t BoatSch_GetNextResult();		
uint32_t BoatSch_GetPrevResult();
			

			
	void BoatSch_GoHome();
	
	
	

#ifdef __cplusplus
 }
#endif

 
	



/** @} */
	
	
	
	

	
	
	
	
	#endif
	