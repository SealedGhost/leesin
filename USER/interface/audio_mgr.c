/*************************************************************
 * @file    audio_mgr.c          
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	
#include "audio_mgr.h"
#include "gpio.h"


static uint8_t AudioPermission  = 1;
static uint8_t AudioState  = 0;


inline void Audio_Enable()
{
	 AudioPermission  = 1;
	if(AudioState > 0)
	{
		SpeakerOn();
	}
}
	
	
inline void Audio_Disable()
{
	 AudioPermission  = 0;
	
	 if(AudioState > 0){
			 SpeakerOff(); 
			 //printf("Speaker off\n");
//				 AudioState  = 0;
		}
}
	
	
	
void Audio_TurnOn(SpeakerOwner owner)
{
	AudioState  |= (0x01 << owner);
	
	
Debug("Ask turn on by %s when permission %s", owner == SpkOnwer_Tone ? "Tone" : "Rx",
									AudioPermission > 0 ? "Enabel" : "Disable");
	
	 if(AudioPermission){
			 SpeakerOn();
			 //printf("Speaker on\n");
		}
	 else{
//			 printf("Speaker on but permission denied\n");
		 
	 }
}
	
	
	
void Audio_TurnOff(SpeakerOwner owner)
{
Debug("Ask turn off by %s when permission %s", owner == SpkOnwer_Tone ? "Tone" : "Rx",
									AudioPermission > 0 ? "Enabel" : "Disable");
	 AudioState  &= ~(0x01 << owner);
	
	 if(AudioState == 0){
			 SpeakerOff();
			 //printf("Speaker off\n");
	}
	else{
Debug("But there are someone using it");			
	}
}
	
	
	