/*************************************************************
 * @file        rt_indicator.c    
 * @brief     
 *
 *
 * @version   
 * @author     
 * @data       
 *************************************************************/
 
 
 
 #include "rt_indicator.h"
 
 #include "audio_mgr.h"
 #include "gpio.h"

uint8_t RTState  = RTState_Idle;
 

void RTState_StartTx(void)
{
Debug("Start Tx.So disable audio and turn led on");	
	RTState  |= RTState_Txing;
	
	Audio_Disable();
	LedTxOn();
	
	if(RTState & RTState_Rxing){
		LedRxOff();
Debug("Sorry, Rx led need to be turned off");		
	}
}




void RTState_StopTx()
{
Debug("Stop Tx. So enable audio and turn led off");	
	RTState  &= ~RTState_Txing;	
    Audio_Enable();
	
    LedTxOff();
	
	if(RTState & RTState_Rxing){		
//		Audio_TurnOn(SpkOwner_Rx);
		LedRxOn();
Debug("Ops, Rx is still here,let's turn it on");		
	}
}



void RTState_StartRx()
{
Debug("Start rx");	
	RTState  |= RTState_Rxing;
	
	if( !(RTState & RTState_Txing) ){
		LedRxOn();
		Audio_TurnOn(SpkOwner_Rx);
Debug("Not Tx,So we can turn led rx on and turn rx audio on");		
	}
	else
	{
Debug("But is Tx...");		
	}
}


void RTState_StopRx()
{
Debug("Stop rx");	
	RTState  &= ~RTState_Rxing;

	LedRxOff();
	Audio_TurnOff(SpkOwner_Rx);
}