/*************************************************************
 * @file             channel_favorite.c
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/


#include "channel_favorite.h"

#include "flash.h"




 /** @addtogroup          频道收藏
*
*   @{
*/




/** 存储收藏的频道的变量类型 */
typedef uint16_t FavoriteItem;



static  FavoriteItem FavoriteVector[CHN_FAVORITE_MAX_NUMBER] = { 0 };
static  uint8_t FavoriteItemNumber = 0;
static  uint8_t CurrentFavoritePos = 0;



 /**@brief       向收藏夹添加一个频道
  * 
  *
  * @return  是否成功
  * @note
  */
bool static ChnFavorite_AddFavoriteItem(uint16_t chn)
{
	if (FavoriteItemNumber < CHN_FAVORITE_MAX_NUMBER) {
	 	FavoriteVector[FavoriteItemNumber++] = chn;
		 if(FavoriteItemNumber == 1){
				  CurrentFavoritePos  = 1;
			}
			
			ChnFavorite_Store();
	 	return true;
	}
	else {
		return false;
	}
}




/**@brief      添加一个频道
  *
  * @param  要添加的频道号
  * @return 是否添加成功
  * @note
  */
bool ChnFavorite_Add(uint16_t chn)
{
	int  i = 0;

	for (; i < FavoriteItemNumber; i++) {
		if (FavoriteVector[i] == chn) {
			return true;
		}
	}
	return ChnFavorite_AddFavoriteItem(chn);
}



/**@brief      删除指定位置的频道
 *
 * @param      要删除的频道在收藏夹中的位置
 * @note
 */
void ChnFavorite_DeleteAt(uint16_t pos)
{
	uint16_t idx = pos - 1;
Debug("Favorite at %u", pos);    

	if (pos > 0) {
		if (FavoriteItemNumber > 0) {		
			/** 删除的不是最后一个 */
			if (idx < FavoriteItemNumber - 1) {
				//memcpy(&(FavoriteVector[idx]), &(FavoriteVector[idx + 1]), sizeof(FavoriteItem)* (FavoriteItemNumber - idx - 1));
				int i  = 0;
				for(; i < FavoriteItemNumber - pos; i++)
				{
					memcpy( &(FavoriteVector[idx+i]), &(FavoriteVector[pos+i]), sizeof(FavoriteItem) );
				}
			}

			FavoriteItemNumber--;
			
			if(CurrentFavoritePos > FavoriteItemNumber)
			{
				CurrentFavoritePos  = FavoriteItemNumber;
			}
			
			ChnFavorite_Store();
		}
	}
}



 /**@brief  删除指定的频道
  *
  * @param  要删除的频道号
  * @note
  */
void ChnFavorite_DeleteByChn(uint16_t chn)
{
	uint16_t pos ;
    
    
    pos    = ChnFavorite_LookFor(chn);
	
    Debug("Favorite delete %u, find it at %u", chn, pos);	
    
	if (pos > 0) {
		ChnFavorite_DeleteAt(pos);
	}
}



/**@brief      寻找指定的频道号
 *
 * @param      寻找的频道号
 * @return     找到的位置
 * @note
 */
uint16_t ChnFavorite_LookFor(uint16_t chn)
{
	int i  = 0;
	for(; i < FavoriteItemNumber; i++){
        if(FavoriteVector[i] == chn){
            return i+1;
        }
	}
		
	return 0;
}





/**@brief 得到收藏夹中下一个频道
  *
  *
  * @return 下一频道号
  * @note
  */
uint16_t ChnFavorite_GetNextChannel(void)
{
	if (FavoriteItemNumber > 0) {
		if (CurrentFavoritePos < FavoriteItemNumber) {
			CurrentFavoritePos++;
		}
		else {
			CurrentFavoritePos = 1;
		}
		return  FavoriteVector[CurrentFavoritePos-1];
	}

	return 0;

}




/** @brief  得到收藏夹前一频道
  *
  *
  * @return 前一频道号
  * @note
  */
uint16_t ChnFavorite_GetPrevChannel(void)
{
	if (FavoriteItemNumber > 0) {
		if (CurrentFavoritePos > 1) {
			CurrentFavoritePos--;
		}
		else {
			CurrentFavoritePos = FavoriteItemNumber;
		}

		return FavoriteVector[CurrentFavoritePos - 1];
	}

	return 0;
}



/**@brief 得到向前所选的频道
  *
  *
  * @return  所选频道
  * @note
  */
uint16_t ChnFavorite_GetCurrChannel(void)
{
	if (CurrentFavoritePos > 0 && FavoriteItemNumber > 0) {
		return FavoriteVector[CurrentFavoritePos - 1];
	}

	return 0;
}




/**@brief   得到当前位置
  * 
  *
  * @return 当前位置
  * @note
  */
inline uint8_t ChnFavorite_GetCurrPos()
{
	 return CurrentFavoritePos;
}




/**@brief  第一个设为当前
  * 
  *
  * @return void
  * @note
  */
void ChnFavorite_GoHome()
{
	 if(FavoriteItemNumber > 0){
		 	CurrentFavoritePos  = 1;
		}
		else{
			 CurrentFavoritePos  = 0;
		}
}




 /**@brief    将收藏夹存入Flash中 
  * 
  *
  * @note
  */
void ChnFavorite_Store()
{
	 /// TODO:  Write to flash
	 /** 将没有用到的位置清空 */ 
	 memset(&(FavoriteVector[FavoriteItemNumber]),  0, sizeof(FavoriteItem) *(CHN_FAVORITE_MAX_NUMBER - FavoriteItemNumber));
	 SPIFlashSectionWrite((uint8_t*)FavoriteVector, FLASH_ADDRESS_FAVORITE / 0x1000, \
	sizeof(FavoriteItem) *(FavoriteItemNumber < CHN_FAVORITE_MAX_NUMBER ? FavoriteItemNumber+1 : CHN_FAVORITE_MAX_NUMBER));
	 
}



/**@brief    从Flash中载入收藏夹
  * 
  *
  * @return 
  * @note
  */
void ChnFavorite_Load()
{
	/// TODO: Read from flash
	 int i  = 0;
	 FavoriteItemNumber  = 0;
    
     
	
	 SPIFlashSectionRead((uint8_t*)FavoriteVector, FLASH_ADDRESS_FAVORITE / 0x1000, sizeof(FavoriteItem) *CHN_FAVORITE_MAX_NUMBER);
	 
	 for(; i < CHN_FAVORITE_MAX_NUMBER; i++){			
			 if(FavoriteVector[i] > 0  &&  FavoriteVector[i] != UINT16_MAX){                 
					 FavoriteItemNumber++;
				}
				else{
					 break;
				}
		}
		
		if(FavoriteItemNumber > 0){
			 CurrentFavoritePos  = 1;
			
			 if(FavoriteItemNumber > CHN_FAVORITE_MAX_NUMBER){
					 FavoriteItemNumber  = CHN_FAVORITE_MAX_NUMBER;
				}
		}
}



#if 0
void ChnFavorite_Dump()
{
	 int i  = 0;
	
	 Debug("Favorite:"); 
	
	 for(; i < FavoriteItemNumber; i++){
			 Debug("%2d - %u", i+1, FavoriteVector[i]);
		}
		
		Debug("Curr: %d / %d", CurrentFavoritePos, FavoriteItemNumber);
}
#endif



/** @} */