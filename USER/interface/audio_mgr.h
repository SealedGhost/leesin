/*************************************************************
 * @file             audio_mgr.h          
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	#ifndef _AUDIO_MGR_H
	#define _AUDIO_MGR_H
	
	
	#include "common.h"
	
	
	/** @addtogroup   
 *   
 *   @{
 */


typedef enum{
	 SpkOnwer_Tone  = 1,
	 SpkOwner_Rx
}SpeakerOwner;


#ifdef __cplusplus
  extern "C" {
#endif

 void Audio_Enable();
	void Audio_Disable();
	void Audio_TurnOn(SpeakerOwner owner);
	void Audio_TurnOff(SpeakerOwner owner);

#ifdef __cplusplus
 }
#endif





/** @} */
	
	
	
	
	
	
	
	
	
	
	#endif
	