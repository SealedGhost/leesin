/*************************************************************
 * @file       analog_channel.h
 * @brief      模拟频道的头文件
 *
 *
 * @version    Version 0.0
 * @author     SealedGhost
 * @data       2017.06.01
 *************************************************************/
	
	#ifndef _ANALOG_CHANNEL_H
	#define _ANALOG_CHANNEL_H
	
	
	#include <stdint.h>
	#include <stdbool.h>
	
	
	/** @addtogroup 模拟频道
 *   
 *   @{
 */



#ifdef __cplusplus
  extern "C" {
#endif

//	bool ACH_PutIntoFavorite(uint16_t chn);
//	bool ACH_RemoveFromFavorite(uint16_t chn);
//	bool ACH_IsInFavorite(uint16_t chn);
	uint16_t ACH_GetNextChannel(void);
	uint16_t ACH_GetPrevChannel(void);	
	uint16_t ACH_GetCurrChannel(void);
	void ACH_SetCurrentChannel(uint16_t chn);
			

#ifdef __cplusplus
 }
#endif



 /** @} */


	
	

	
	
	
	
	#endif

