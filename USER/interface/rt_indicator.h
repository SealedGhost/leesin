/*************************************************************
 * @file       rt_indicator.h 
 * @brief     
 *
 *
 * @version   
 * @author     
 * @data       
 *************************************************************/
 
 
 
 #ifndef _RT_INDICATOR_H
 #define _RT_INDICATOR_H
 
 
 
 #include "common.h"
 
 
 #define RTState_Rxing   0x01
 #define RTState_Txing   0x02
 #define RTState_Idle    0x00
 

 extern uint8_t RTState;
 
 

 #define RTState_Clean()     RTState  = RTState_Idle
 
 
 #define RTState_IsRxing()    ( (RTState  & RTState_Rxing) > 0)
 #define RTState_IsTxing()    ( (RTState & RTState_Txing) > 0)
 
 
 
 void RTState_StartTx(void);
 void RTState_StopTx(void);
 void RTState_StartRx(void);
 void RTState_StopRx(void);
 
 
 
 
 
 #endif
 