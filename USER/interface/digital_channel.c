/*************************************************************
 * @file             
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
#include "digital_channel.h"
#include "channel480.h"

#include "ccl.h"

static uint16_t CurrentDigitalChannel  = 1;
	
	
/** @addtogroup 数字频道
 *   
 *   @{
 */

/**@brief    得到下一个数字频道号
  * 
  *
  * @return 下一个频道号
  * @note
  */
		
		
uint16_t DCH_GetNextChannel(void)
{
  return (CurrentDigitalChannel  =  GetNextChannel(CurrentDigitalChannel));
}

	
 /**@brief  得到前一个数字频道号
  * 
  *
  * @return 前一个频道号
  * @note
  */
uint16_t DCH_GetPrevChannel(void)
{
	 return (CurrentDigitalChannel  =  GetPrevChannel(CurrentDigitalChannel));
}
	
	
	
/**@brief  得到当前数字频道号
* 
*
* @return 当前频道号
* @note
*/
inline uint16_t DCH_GetCurrChannel(void)
{
   return CurrentDigitalChannel;		
}
	
	


	
/**@brief   设置数字频道的当前频道
* 
* @param   chn 设置频道对应的频道号 
* @note    频道号只允许在480频道范围内
*/
void DCH_SetCurrChannel(uint16_t chn)
{
	 if(chn > 0  &&  chn  <= 480){
			 CurrentDigitalChannel  = GetNextChannel(chn);
			 CurrentDigitalChannel  = GetPrevChannel(CurrentDigitalChannel);
		}
}



void DCH_Enter()
{
	 CCL_InDigital(GetFrequency(CurrentDigitalChannel));
}




void DCH_Exit()
{
	  CCL_OutDigital();
}


/** @} */