/*************************************************************
 * @file               ipc.h       
 * @brief   IPC通信消息类型定义
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	#ifndef _IPC_H
	#define _IPC_H
    

#ifndef USE_CRITICAL_WHEN_POST
    #define USE_CRITICAL_WHEN_POST
#endif


//#ifndef SCH_LOCK_WHEN_POST
//#define SCH_LOCK_WHEN_POST
//#endif

	

/**  消息类型定义 */

/** 表示IPC消息来自于UI任务 */
#define IPC_MSG_GUI        0x01

/** 表示IPC消息来自于串口 */
#define IPC_MSG_UART       0x02

/** 表示IPC消息来自于DLL*/
#define IPC_MSG_DLL        0x03

/** 表示IPC消息来自于CCL */
#define IPC_MSG_CCL        0x04

/** 表示IPC消息来自于RTC */
#define IPC_MSG_RTC        0x05


/** 表示IPC消息来自于IO */
#define IPC_MSG_IO          0x06

/** 表示IPC消息来自于AD采集 */
#define IPC_MSG_ADC         0x07


/**  消息ID定义 */


/**  串口消息id */
#define UART_MSG_RX0          0x10
#define UART_MSG_RX1          0x11
#define UART_MSG_RX2          0x12
#define UART_MSG_RX3          0x13


/**  IO消息id */
#define IO_MSG_RF_RX          0x01



/** ADC消息id */
#define ADC_MSG_CCL_RSSI        0x01


/** RTC_MSG_SECOND_TICK */
#define RTC_MSG_SECOND_TICK   0x01





#endif