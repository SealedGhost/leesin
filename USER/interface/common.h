/*************************************************************
 * @file              common.h         
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	
	
	#include <stdio.h>
	#include <stdint.h>
	#include <stdbool.h>
	#include <assert.h>
	#include <string.h>
	
	
	
	
	/** @addtogroup        Common
 *   
 *   @{
 */

/**@def Debug(format,...)
 * 改造过的受宏开关控制的打印语句
	*
	*/
	//#define DEBUG
	
	#ifdef DEBUG
	#define Debug(format,...)   //printf(format"\n",##__VA_ARGS__)
	#else
	#define Debug(format,...)
	#endif
	
	
	
	
	
	
	/** 表示呼入 */
	#define    CALL_DIR_IN      0x01   
	
	/** 表示呼出 */
	#define    CALL_DIR_OUT     0x02


 /** 表示未接 */
	#define    CALL_MISS        0x01
	
 /** 表示挂断  */
	#define    CALL_HUNG_UP     0x01
	
	/** 表示响铃  */
	#define  CALL_RING          0x02
	
	/** 表示接通 */
	#define CALL_CONNECT        0x03





/** 同时保持的通话事务最大数量 */
#define CALL_TRANSACTION_MAX_NUMBER    5

/** 频道收藏夹的大小 */
#define CHN_FAVORITE_MAX_NUMBER       50   


/** @} */
	
	

	