/*************************************************************
 * @file         boat_search.h     
 * @brief      船舶信息搜索源文件
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	
	#include "boat_search.h"
	#include "ccl.h"
	
	/** @addtogroup  船呼
 *   
 *   @{
 */


uint32_t SearchResult[BOAT_SEARCH_MAX_NUMBER]  = {0};
static uint8_t SearchResultNumber  = 0;
static uint8_t CurrentPos  = 0;
	
	
	
	/**@brief 执行呼号查询
  * 
  * @param 要查询的船号
  * @return void
  * @note
  */
void BoatSch_DoSearch(uint32_t boatNumber)
{
	 SearchResultNumber  = 0;
	 CurrentPos          = 0;		
	 memset(SearchResult, 0, sizeof(SearchResult[0]) * BOAT_SEARCH_MAX_NUMBER);
	
	 /// CCL_SearchBoat
	 CCL_RequestBoatInfo(boatNumber);
}

	
	
 /**	
  *
  *
  */
inline static bool BoatSch_HasInResult(uint32_t phoneId)
{
	int i  = 0;
	
	
	
	for(; i < SearchResultNumber; i++){
		if(SearchResult[i]  ==  phoneId){
			return true;
		}
	}
	
	return false;
}
	
	
	
	
 /**@brief  添加查询结果
  *  
  * @param  查询到的呼号
  * @return void
  * @note
  */
bool BoatSch_AddResult(uint32_t newPhoneId)
{
	if(newPhoneId == 0){
		return false;;
	}

	if(SearchResultNumber < BOAT_SEARCH_MAX_NUMBER){
		if( BoatSch_HasInResult(newPhoneId) == false){
			SearchResult[SearchResultNumber++]  = newPhoneId;	 
		}		
		else
		{
			return false;
		}
	}
	
	if(SearchResultNumber == 1){
		 CurrentPos  = 1;
	}
	
	return true;
}
		

	
 /**@brief  得到当前位置
  * 
  * 
  * @return 当前位置
  * @note
  */	
uint8_t BoatSch_GetCurrPos()
{
	return CurrentPos;
}
	
	

/**@brief 得到当前搜索结果
  * 
  *
  * @return 当前结果
  * @note
  */	
uint32_t BoatSch_GetCurrResult()
{
	if(CurrentPos > 0  &&  CurrentPos <= SearchResultNumber){
		return SearchResult[CurrentPos-1];
	}
	else 
	{
		return 0;
	}
}

	

/**@brief  得到下一个搜索结果
* 
*
* @return 下一个结果
* @note
*/
uint32_t BoatSch_GetNextResult()
{
	 if(SearchResultNumber > 0){
			 if(CurrentPos  < SearchResultNumber){
					  CurrentPos++;			 
				}
				else{
					 CurrentPos  = 1;
				}
				
				return SearchResult[CurrentPos-1];
		}
		else{
			  return 0;
		}
}

	
	
/**@brief  得到前一个搜索结果
* 
*
* @return 前一个结果
* @note
*/	
uint32_t BoatSch_GetPrevResult()
{
	 if(SearchResultNumber > 0){
			 if(CurrentPos > 1){
					 CurrentPos--;
				}
				else{
					 CurrentPos  = SearchResultNumber;
				}
				
				return SearchResult[CurrentPos-1];
		}
		else{
			 return 0;
		}
}
	
	
	
/**@brief  显示cursor复位
* 
*
* @return void
* @note
*/	
void BoatSch_GoHome()
{
	  if(SearchResultNumber > 0){
				 CurrentPos  = 1;
			}
}

/** @} */