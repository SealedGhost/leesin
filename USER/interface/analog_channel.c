/*************************************************************
 * @file       analog_channel.c
 * @brief      模拟频道数据结构和方法实现
 *
 *
 * @version    Version 0.0
 * @author     SealedGhost
 * @data       2017.06.01
 *************************************************************/
#include "analog_channel.h"
#include "channel480.h"

#include "ccl.h"

/** @addtogroup  模拟频道
 *  @{
	*/
		
static uint16_t CurrentAnalogChannel  = 1;
		
//	/**@brief 将指定的频道放入收藏夹中
//  * 
//  * @param chn 所收藏的频道
//  * @return    放入收藏夹是否成功
//  * @note
//  */
//	bool  ACH_PutIntoFavorite(uint16_t chn)
//	{
//		 return false;
//	}
//	

//	/**@brief 从收藏夹中移除指定频道
//  * 
//  * @param  chn  所要移除的频道
//  * @return 频道是否在收藏夹中
//  * @note
//  */
//	bool ACH_RemoveFromFavorite(uint16_t chn)
//	{
//		 return false;
//	}
	
	
// /**@brief 判断指定频道是否存在于收藏夹中
//  * 
//  * @param  chn 要寻找的频道
//  * @return 是否存在
//  * @note
//  */	
//	bool ACH_IsInFavorite(uint16_t chn)
//	{
//		 return false;
//	}
	
	
 /**@brief 得到模拟频道列表的后一个频道
  * 
  *
  * @return 后一个频道
  * @note
  */
uint16_t ACH_GetNextChannel()
{
	 return (CurrentAnalogChannel = GetNextChannel(CurrentAnalogChannel));
}
	
	
/**@brief 得到模拟频道列表的前一个频道
* 
*
* @return  前一个频道
* @note
*/
uint16_t ACH_GetPrevChannel()
{
	 return (CurrentAnalogChannel = GetPrevChannel(CurrentAnalogChannel));
}
	
	
	
/**@brief  得到当前的模拟频道号
  * 
  *
  * @return 当前频道号
  * @note
  */
inline uint16_t ACH_GetCurrChannel()
{
	  return CurrentAnalogChannel;
}
	
	
	
/**@brief 设置模拟频道列表的当前频道
* 
* @param chn 当前频道
* @note
*/
void ACH_SetCurrentChannel(uint16_t chn)
{ 
	CurrentAnalogChannel  = GetNextChannel(chn);
	CurrentAnalogChannel  = GetPrevChannel(CurrentAnalogChannel);
}
	
	

	
	
	
	
		
/** @} */


	