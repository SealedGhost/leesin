/*************************************************************
 * @file            system_config.c         
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	
	
#include "system_config.h"
#include "system_info.h"
#include "ccl.h"
#include "flash.h"



/** @addtogroup       系统配置
*   
*   @{
*/





static SysCfg   SystemConfig  = {0};


/**@brief
  * 
  *
  * @return void
  * @note
  */
inline void SysCfg_SetConfig(SysCfg* newConfig)
{
	 if(newConfig){
			 memcpy(&SystemConfig, newConfig, sizeof(SysCfg));
			 SysCfg_Store();
		}
}



/**@brief
  * 
  *
  * @return void
  * @note
  */
inline void SysCfg_GetConfig(SysCfg* config)
{
	 if(config){
			 memcpy(config, &SystemConfig, sizeof(SysCfg));
			 //CCL_ConfigSystemParam(SysInfo_GetId(), SystemConfig.boatNumber,  SystemConfig.groupNumber, SystemConfig.groupPwd);
		}
}


static bool isValidConfig()
{
	 if(SystemConfig.analogMode < AnalogChan480  ||  SystemConfig.analogMode > AnalogChan5000){
			 return false;
		}
		
		if(SystemConfig.boatNumber == UINT32_MAX){
			 return false;
		}
		
		if(SystemConfig.groupNumber == UINT16_MAX) {
			 return false;
		}
		
		if(SystemConfig.groupPwd  == UINT16_MAX){
			 return false;
		}
		
		
		return true;
}



/**@brief
  * 
  *
  * @return void
  * @note
  */
void SysCfg_Load()
{
	 SPIFlashSectionRead((uint8_t*)&SystemConfig, FLASH_ADDRESS_SYS_CONFIG /0x1000, sizeof(SysCfg));
	
	 if(! isValidConfig()){
			 SystemConfig.analogMode  = AnalogChan480;
			 SystemConfig.boatNumber  = 0;
			 SystemConfig.groupNumber  = 0;
			 SystemConfig.groupPwd   = 0;
			
			 SysCfg_Store();
		}
}



/**@brief
  * 
  *
  * @return void
  * @note
  */
void SysCfg_Store()
{
	 SPIFlashSectionWrite((uint8_t*)&SystemConfig, FLASH_ADDRESS_SYS_CONFIG /0x1000, sizeof(SysCfg));
}




/** @} */