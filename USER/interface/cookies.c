/**@file      cookies.c 
 * @brief
 * 
 * @author SealedGhost
 */
 
#include "cookies.h" 
#include "flash.h"
#include "analog_channel.h"
#include "digital_channel.h"
#include "group_chat.h"
 
 /** @addtogroup    Cookie
 *   
 *   @{
 */


static ChanCookie CookiedChan  = {1, 1, 1};



/**@brief
 * 
 *
 * @param
 * @return
 * @note
 */
inline uint16_t Coki_GetAnalogChan()
{
	return CookiedChan.analogChan;
}



/**@brief
 * 
 *
 * @param
 * @return
 * @note
 */
inline uint16_t Coki_GetDigitalChan()
{
	return CookiedChan.digitalChan;
}



/**@brief
 * 
 *
 * @param
 * @return
 * @note
 */
inline uint16_t Coki_GetGroupChan()
{
	return CookiedChan.groupChan;
}



/**@brief
 * 
 *
 * @param
 * @return
 * @note
 */
void Coki_SetAnalogChan(uint16_t chn)
{
	CookiedChan.analogChan  = chn;
	Coki_Store();
}



/**@brief
 * 
 *
 * @param
 * @return
 * @note
 */
void Coki_SetDigitalChan(uint16_t chn)
{
	CookiedChan.digitalChan  = chn;
	Coki_Store();
}



/**@brief
 * 
 *
 * @param
 * @return
 * @note
 */
void Coki_SetGroupChan(uint16_t chn)
{
	CookiedChan.groupChan  = chn;
	Coki_Store();
}



void Coki_Load()
{
	int flag  = 0;
	
	SPIFlashSectionRead((uint8_t*)&CookiedChan, FLASH_ADDRESS_COOKIES /0x1000, sizeof(CookiedChan));
	
	if(CookiedChan.analogChan == 0  ||  CookiedChan.analogChan > 480)
	{
		flag++;
	}
		
	if(CookiedChan.digitalChan == 0  ||  CookiedChan.digitalChan > 480)
	{
		flag++;
	}
	
	if(CookiedChan.groupChan == 0  ||  CookiedChan.groupChan > 480)
	{
		flag++;
	}

	/** 任一频道不正确，则将所有频道置为频道1 */
	if(flag > 0)
	{
		CookiedChan.analogChan  = 1;
		CookiedChan.digitalChan  = 1;
		CookiedChan.groupChan   = 1;
		Coki_Store();
	}
	
	
	ACH_SetCurrentChannel(CookiedChan.analogChan);
	DCH_SetCurrChannel(CookiedChan.digitalChan);
	Grp_SetChannel(CookiedChan.groupChan);
}




void Coki_Store()
{
	SPIFlashSectionWrite((uint8_t*)&CookiedChan, FLASH_ADDRESS_COOKIES /0x1000, sizeof(CookiedChan));
}


/** @} */