/*************************************************************
 * @file             
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	#ifndef _SYSTEM_TIME_H
	#define _SYSTEM_TIME_H
	
	
	#include <stdint.h>
/** @addtogroup  系统时间
 *   
 *   @{
 */




	/** 时间类型结构体
	 *
		*
		*/
	typedef struct{
		uint16_t year;   /**< 年 */
	 uint8_t  month;  /**< 月 */
		uint8_t  day;    /**< 日*/
		uint8_t  hour;   /**< 时 */
		uint8_t  minute; /**< 分 */
		uint8_t  second; /**< 秒 */
	}Time;
	

	
	#ifdef __cplusplus
  extern "C" {
#endif

void SysTime_Set(uint32_t year, uint32_t month, uint32_t day, uint32_t hour, uint32_t minute, uint32_t second);
void SysTime_Update(uint32_t year, uint32_t month, uint32_t day, uint32_t hour, uint32_t minute, uint32_t second);
void SysTime_Get(Time* time);

#ifdef __cplusplus
 }
#endif




/** @} */
	
	
	
	
	
	
	
	#endif


	