/*************************************************************
 * @file            channel480.c       
 * @brief
 *
 *
 * @version   Version
 * @author    SealedGhost
 * @data
 *************************************************************/
	
	#include "channel480.h"
	#include "ccl.h"
	
	
/** @addtogroup    480模式
 *   
 *   @{
 */


	
	
	
/**@brief   得到指定频道的下一个频道号
 * 
 *
 * @return 下一个频道号
 * @note   会跳过 221 223 225 231 236 238 号频道
 * @see    GetPrevChannel()
 */
inline	uint16_t GetNextChannel(uint16_t chn)
	{
	  if(chn <= 480){				
		   chn++;
     
     switch(chn){
						case 221: chn++; break;
						case 223: chn++; break;
						case 225: chn++; break;
						case 231: chn++; break;
						case 236: chn++; break;
						case 238: chn++; break;
						case 481: chn = 1;break;
					}
					
					return chn;
			}
			else{
				return 0;
			}
	}

	
	
	
	/**@brief   得到指定频道的前一个频道号
 * 
 *
 * @return 前一个频道号
 * @note 会跳过 221 223 225 231 236 238 号频道
	* @see  GetNextChannel()
 */
	inline uint16_t GetPrevChannel(uint16_t chn)
	{
		
	  if(chn <= 480 && chn > 0){				
		   chn--;
     
     switch(chn){
						case 221: chn--; break;
						case 223: chn--; break;
						case 225: chn--; break;
						case 231: chn--; break;
						case 236: chn--; break;
						case 238: chn--; break;
						case 0: chn = 480;break;
					}
					
					return chn;
			}
			else{
				return 0;
			}
	}
	
	
	
	/**@brief   频道2频率
  * 
  * @param chn 频道	 
  * @return 频率
  * @note
  */
	inline uint32_t GetFrequency(uint16_t chn)
	{
		 return (27500000 + (chn-1) * 25000);
	}

	
	
 /**@brief  判断指定的频道号是否是有效想480模式下的频道号
  * 
  *
  * @return void
  * @note
  */	
	bool IsChnAvailable(uint16_t chn)
	{
		 if(chn > 0  &&  chn <= 480){
				 switch(chn){
							case 221:
							case 223:
							case 225:
							case 231:
							case 236:
							case 238:
								return false;
								break;
							default:
								return true;
							 break;
					}
			}
			else{
				 return false;
			}
	}
	
	
 /**@brief  通知底层配置新的频率
  * 
  *
  * @return void
  * @note
  */	
	void ConfigChannel(uint16_t chn)
	{
		  //if(chn < 0  &&  chn <= 480){
		Debug("Config channel:%u", chn);
				   CCL_ConfigFrequency(GetFrequency(chn));	
				//}
		  
	}
	
	
	
 /**@brief  进入模拟
  * 
  * @note
  */	
	void EnterAnalog(uint16_t chn)
	{
    CCL_InAnalog(GetFrequency(chn));
	}		
	
	
	
 /**@brief 退出模拟
  * 
  * @note
  */	
	void ExitAnalog()
	{
		  CCL_OutAnalog();
	}
	

	
/** 进入天气
  *
  *
  */
	
void EnterWeather(uint16_t chn)
{
	CCL_InWeather(GetFrequency(chn));
}



/** 退出天气
 *
 *
 */
void ExitWeather()
{
	CCL_OutWeather();
}

	

/** @} */
	
	