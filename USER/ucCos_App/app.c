#include "app.h"
#include  <stdio.h>
#include  <string.h>
#include  <ctype.h>
#include  <stdlib.h>
#include  <stdarg.h>
#include  <math.h>

#include  <uCOS-II\Ports\ARM-Cortex-M3\RealView\os_cpu.h>
#include  <uC-CPU\ARM-Cortex-M3\RealView\cpu.h>

#if (OS_VIEW_MODULE == DEF_ENABLED)
#include    <uCOS-VIEW\Ports\ARM-Cortex-M3\Realview\os_viewc.h>
#include    <uCOS-VIEW\Source\os_view.h>
#endif
#include  "LPC17xx.h"
#include  "lpc17xx_uart.h"

#include  "MainTask.h"
#include  "key.h"
#include  "dac.h"

#include "peripheralTest.h"
#include "mac_working.h"
#include "tones.h"

#include "storeChannel.h"


static  OS_STK         App_TaskStartStk[APP_TASK_START_STK_SIZE];
static  void  App_OK			(void		*p_arg);

static OS_STK          UI_TaskStk[UI_TASK_STK_SIZE];

static OS_STK			     KEY_TaskStk[KEY_TAST_STK_SIZE];

static OS_STK					 AUDIO_TaskSrk[AUDIO_TAST_STK_SIZE];

static OS_STK					 PERIPHERALTEST_TaskSrk [PERITEST_TAST_STK_SIZE];

static OS_STK					 DLL_TaskSrk[DLL_TASK_STK_SIZE];
static OS_STK					 CCL_TaskSrk[CCL_TASK_STK_SIZE];

static OS_STK          STRCH_TaskStk[STRCH_TASK_STK_SIZE];

//OS_EVENT *keyResetSemp;	//按键复位信号量
//OS_EVENT *tonesSemp;		//��ʾ�������ź���


void App_TaskStart(void)
{
	  
  OSInit();   
	
	//while(1);
	
  OS_CPU_SysTickInit(SystemCoreClock / OS_TICKS_PER_SEC - 1);	
  

  OSTaskCreateExt((void (*)(void *)) UI_Task,  
                           (void          * ) 0,
                           (OS_STK        * )&UI_TaskStk[UI_TASK_STK_SIZE - 1],
                           (INT8U           ) UI_TASK_PRIO,
                           (INT16U          ) UI_TASK_PRIO,
                           (OS_STK        * )&UI_TaskStk[0],
                           (INT32U          ) UI_TASK_STK_SIZE,
                           (void          * )0,
                           (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));      
  OSTaskCreateExt((void (*)(void *)) KeyScan,  
                           (void          * ) 0,
                           (OS_STK        * )&KEY_TaskStk[KEY_TAST_STK_SIZE - 1],
                           (INT8U           ) KEY_TASK_PRIO,
                           (INT16U          ) KEY_TASK_PRIO,
                           (OS_STK        * )&KEY_TaskStk[0],
                           (INT32U          ) KEY_TAST_STK_SIZE,
                           (void          * )0,
                           (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));  													 
  OSTaskCreateExt((void (*)(void *)) TonesTask,  
                           (void          * ) 0,
                           (OS_STK        * )&AUDIO_TaskSrk[AUDIO_TAST_STK_SIZE - 1],
                           (INT8U           ) TONES_TASK_PRIO,
                           (INT16U          ) TONES_TASK_PRIO,
                           (OS_STK        * )&AUDIO_TaskSrk[0],
                           (INT32U          ) AUDIO_TAST_STK_SIZE,
                           (void          * )0,
                           (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK));  

	OSTaskCreateExt((void (*)(void *)) DLLTask,  
                           (void          * ) 0,
                           (OS_STK        * )&DLL_TaskSrk[DLL_TASK_STK_SIZE - 1],
                           (INT8U           ) DLL_TASK_PRIO,
                           (INT16U          ) DLL_TASK_PRIO,
                           (OS_STK        * )&DLL_TaskSrk[0],
                           (INT32U          ) DLL_TASK_STK_SIZE,
                           (void          * )0,
                           (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK)); 

	OSTaskCreateExt((void (*)(void *)) CCLTask,  
                           (void          * ) 0,
                           (OS_STK        * )&CCL_TaskSrk[CCL_TASK_STK_SIZE - 1],
                           (INT8U           ) CCL_TASK_PRIO,
                           (INT16U          ) CCL_TASK_PRIO,
                           (OS_STK        * )&CCL_TaskSrk[0],
                           (INT32U          ) CCL_TASK_STK_SIZE,
                           (void          * )0,
                           (INT16U          )(OS_TASK_OPT_STK_CLR | OS_TASK_OPT_STK_CHK)); 		
													 
	OSStart();  
  
}  

/** 
 * @brief  OK
 * @param  p_arg �������β�
 */

static  void  App_OK (void *p_arg)
{   
	(void)p_arg;
	
	/***************  Init hardware ***************/

#if (OS_TASK_STAT_EN > 0)
    OSStatInit();                                            /* Determine CPU capacity.                              */
#endif
                                       /* Create application tasks.                            */
    uint32_t  i  = 0;
	 for(;;)
   	{
        
        //AudioPlayCnt(AUDIO_TYPE_KEY_NORMAL + i);
		printf("Task running...\n");
		
      	OSTimeDly(OS_TICKS_PER_SEC*10);							 /* Delay One minute */
    }	
}

