#include "storeChannel.h"
#include <uCOS-II\Ports\ARM-Cortex-M3\RealView\os_cpu.h>
#include "analog_channel.h"
#include "digital_channel.h"
#include "group_chat.h"
#include "cookies.h"

uint32_t anaCh = 0;
uint16_t grpCh = 0;
uint16_t DgtCh = 0;
void InitStroeChannel()
{
  anaCh = ACH_GetCurrChannel();
  grpCh = Grp_GetChannel();
  DgtCh = DCH_GetCurrChannel();
}

void StoreChannel()
//void StoreChannel_Task(void *p_arg)
{
  static uint8_t num = 0;
//  OS_CPU_SR cpu_sr;
//  OS_ENTER_CRITICAL();
//  anaCh = ACH_GetCurrChannel();
//  grpCh = Grp_GetChannel();
//  DgtCh = DCH_GetCurrChannel();
//  OS_EXIT_CRITICAL();
  num++;
  if(num == 200)
  {
    num = 0;
    if(anaCh != ACH_GetCurrChannel())
    {
     // OS_ENTER_CRITICAL();
      anaCh = ACH_GetCurrChannel();
      Coki_SetAnalogChan(anaCh);
     // OS_EXIT_CRITICAL();
    }
    if(grpCh != Grp_GetChannel())
    {
     // OS_ENTER_CRITICAL();
      grpCh = Grp_GetChannel();
      Coki_SetGroupChan(grpCh);
     // OS_EXIT_CRITICAL();
    }
    if(DgtCh != DCH_GetCurrChannel())
    {
     // OS_ENTER_CRITICAL();
      DgtCh = DCH_GetCurrChannel();
      Coki_SetDigitalChan(DgtCh);
     // OS_EXIT_CRITICAL();
    }
//    
//    OSTimeDlyHMSM(0,0,1,0);
  }
}
