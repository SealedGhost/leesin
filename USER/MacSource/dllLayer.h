
//
// dllLayer.h
//
//      Copyright (c) Sandemarine Corporation. All rights reserved.
//
//      Created by ZuoDahua --- 2017/05/24
//		dhzuo@sandemarine.com
// 定义剑鱼DLL层的头文件
//



#ifndef DLLLAYER_H
#define DLLLAYER_H

#include<stdint.h>
#include<stdbool.h>
#include "cclMsgParser.h"


// -----------------------------------------------------------------------------------
// FPGA接收数据处理
// -----------------------------------------------------------------------------------

#define DLL_PHY_FRAME_HEADER	0xAD	// DLL和FPGA之间数据帧的帧头字节

#define NUM_FPGA_REC_BYTE_PER_FRAME 163			// FPGA上传的每帧的字节长度
#define NUM_BUFFER_FPGA_REC_FRAMES  20			// MCU缓存的FPGA接收帧数
#define NUM_LDPC_MSG_BITS			648			// LDPC消息位比特数
#define NUM_LDPC_CODE_BITS			1296		// LDPC编码后的比特数

#define NUM_LDPC_MSG_BYTES			(NUM_LDPC_MSG_BITS / 8)			// 编码消息字节数
#define NUM_LDPC_CODE_BYTES			(NUM_LDPC_CODE_BITS / 8)		// 编码后的字节数


// ----------------------
// 接收通道区分
// ----------------------
#define CCL_SIGNAL					0x00
#define VOICE_SIGNAL				0X02


/* 通道指示 */
typedef enum
{
	CclChannelIndicator = 0,
	DataChannelIndicator = 1
} ChannelIndicator;


typedef struct
{
	uint8_t dataBuffer[NUM_CCL_MSG_BYTES];			// 接收到的数据，本机将只缓存CCL要处理的数据
	ChannelIndicator channel;						// 通道指示
	bool isDealed;									// 指示本数据是否被处理过
} FpgaRxData;


/* 缓存FPGA上传的接收数据 */
extern FpgaRxData fpgaRxDataBuffer[NUM_BUFFER_FPGA_REC_FRAMES];

/* 初始化DLL层 */
void initDllLayer();

//************************************
// Method:    	recFpgaData
// FullName:  	recFpgaData
// Access:   	public 
// Parameter: 	uint8_t * recBuffer  ---  接收到的原始数据缓冲区
// Parameter: 	int startIdx         ---  缓冲区起始的字节索引
// Returns:  	int 									--- 如果正确接收到一个帧，则返回当前写入的索引，否则返回-1
// Description: 处理来自FPGA的接收数据，并将结果进行保存
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
int recFpgaData(uint8_t *recBuffer, int startIdx);


/** 获取指定索引数据缓冲区的地址 */
uintptr_t getRxDataBufferAddr(int bufferIndex);


extern int rxFpgaDataWrIndex;		// FPGA接收数据的写入索引
extern int rxFpgaDataRdIndex;		// FPGA接收数据的读出索引


// -----------------------------------------------------------------------------------
// FPGA发射控制接口部分
// -----------------------------------------------------------------------------------

#define NUM_PHY_FREQ_BYTES	4			// PHY层频率控制字的字节数


//************************************
// Method:    	sendNewMsg
// Access:   	public 
// Parameter: 	uint8_t * txBuffer		--- 数据发射缓冲区
// Parameter: 	int numTxBytes			--- 发射字节数
// Parameter: 	int maxReTxTimes		--- 最大重发次数
// Parameter:	int txFreq				--- 本次发射的频率，单位为Hz
// Parameter:   bool isAvoid            --- 指示本次是否避让
// Returns:  	void
// Description: DLL层提供的数据发射接口，该接口只负责将数据进行缓存，不立即进行发射。
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
void sendNewMsg(uint8_t *txBuffer, int numTxBytes, int maxReTxTimes, int txFreq, bool isAvoid);


/**  物理层向DLL请求待发送数据 */
void phyNotifyDllForTxData(uint8_t info);


typedef struct 
{
	uint8_t txBuffer[NUM_LDPC_MSG_BYTES];			// 消息字节内容
    bool isAvailable;                               // 指示是否可用
	int txTimes;									// 本条消息可以发送的最大次数
	bool isAvoid;									// 消息是否避让
	int freq;										// 频率单位为Hz
} DllTxData;


#define NUM_TO_SEND_MSG_BUFFER_SIZE			20					// 数据发射缓冲区大小


#define PHY_TX_CCL_MSG_BUFFER_RDY   0x55        // 指示当前FPGA可以接收新的CCL信令数据
#define PHY_TX_CCL_MSG_BUFFER_BUSY  0xAA        // 指示当前FPGA不能接收新的CCL信令数据

/* DLL层将数据发送给物理层的接口 */
void txMsgToPhy(int bufferIndex);




#endif // !DLLLAYER_H


