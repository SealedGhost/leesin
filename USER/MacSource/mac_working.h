/*************************************************************
 * @file       mac_working.h
 * @brief      MAC 任务和接口
 *
 *
 * @version   v0.0
 * @author    xieyoub
 * @data      2017/06/09
 *************************************************************/
 
 #ifndef	__MAC_WORKING_H
 #define  __MAC_WORKING_H
 
 #include  <uCOS-II\Source\ucos_ii.h>
 #include "lpc17xx_uart.h"
 #include "lpc17xx.h"
 #include "string.h"
 #include <stdint.h>
 #include <stdbool.h>

 
	

	
// 
 /** @addtogroup  MAC层任务
  * 
  *  @{
  */
	
	
	
	/**< 消息最大数量    */
 #define MESSAGE_MAX_NUMBER	20	
 
 	/** 消息结构体 */
	typedef struct{
		 uint16_t  type;  /**<消息类型  */
		 uint16_t  id;    /**<消息ID */
		 int       v;     /**<消息参数v */		
		 uintptr_t p;     /**<消息参数p */
	}Message;
	
	
	/**< 消息类型     */
	#define		USER_MSG_UART_RX	0x01		//串口接收消息
	#define		USER_MSG_UART_TX	0x02		//串口发送消息
	
	/**< 消息ID     */
	#define		MSG_ID_UART0		0x01	
	#define		MSG_ID_UART1		0x02
	#define		MSG_ID_UART2		0x03
	#define		MSG_ID_UART3		0x04
	


  void DLLTask(void *p_arg);
	void CCLTask(void *p_arg);
	
	void DLL_PostMessage(uint16_t type, uint16_t id, int v, uintptr_t p);
	bool DLL_PeekMessage(Message* pMsg);
	
	void CCL_PostMessage(uint16_t type, uint16_t id, int v, uintptr_t p);
	bool CCL_PeekMessage(Message* pMsg);
    
    
    
    
	
/** @} */


 #endif
 