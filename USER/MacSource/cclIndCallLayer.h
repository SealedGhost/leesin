/************************************************************************/
// Created By ZuoDahua  ---  2017/06/17
// dhzuo@sandemarine.com
// Team: Research and Development Department of Sandemarine
// Brief: MAC中管理单呼的模块
// Copyright (C) Nanjing Sandemarine Electric Co., Ltd
// All Rights Reserved
/************************************************************************/

#ifndef CCLINDCALLLAYER_H

#define CCLINDCALLLAYER_H

#include "macService.h"
#include "cclMsgParser.h"
#include<stdint.h>


// 单呼的状态
typedef enum
{
    IndCallWaitingForCallResponse,  // 单呼主叫等待单呼应答状态
    IndCallWaitingForPickup,        // 单呼等待摘机状态
    IndCallTalking,                 // 单呼通话中
    IndCallHangUp,                  // 单呼挂起
    IndCallIdle                     // 单呼空闲
} IndCallState;


// 单呼过程中的角色
typedef enum
{
    IndividualCaller,                      // 单呼主叫
    IndividualCallee                       // 单呼被叫
} IndCallRole;



#define         IndCallFailTimeInMilliseconds           5000            // 单呼呼叫失败时间，单位为毫秒，即在发出呼叫请求后，等待该时间未有收到回应，则上报“失败”

#define         IndCallWaitForPickUpTimeInMilliseconds  30000           // 单呼等待摘机时间，单位为毫秒

#define         IndCallRequestSpaceTimeInMilliseconds   2000             // 单呼请求重传请求时间间隔

#define			IndCallRequestTxTimes					2				// 每次单呼请求要尝试发送的次数

#define         IndCallRequestMaxTxTimes       (IndCallFailTimeInMilliseconds / IndCallRequestSpaceTimeInMilliseconds)      // 单呼请求的最大发射次数


#define         IndCallResponseMsgTxTimes               3
#define         IndCallConnectMsgTxTimes                6               // 单呼信道建立帧发的次数，没有重传，需要多尝试几次
#define         IndCallPickUpMsgTxTimes                 2
#define         IndCallHangUpMsgTxTimes                 2



void indCallHandleDial(uint32_t dstId);

void indCallHandleConnectAction();

void indCallHandleHangupAction();

void indCallSendRequestMsg(int numChannels);

void indCallSendPickUpMsg(uint16_t txChannelNumber, uint8_t txTimes);
void indCallSendHangUpMsg(uint16_t commCode, uint32_t dstId, uint16_t chanNumber, uint16_t txChannelNumber, uint8_t txTimes);


//--------------------------------------------------------
// 接收处理DLL消息的接口方法
//--------------------------------------------------------

void handleIndCallMsgFromDll(MsgIdSet msgId, void * msg);


void handleIndCallRequestMsg(IndCallConnInfoMsg *msg);
void handleIndCallResponseMsg(IndCallConnInfoMsg *msg);
void handleIndCallConnectMsg(IndCallConnInfoMsg *msg);
void handleIndCallPickUpMsg(IndCallSwitchInfoMsg *msg);
void handleIndCallHangupMsg(IndCallSwitchInfoMsg *msg);


//--------------------------------------------------------
// 单呼模块相关定时器操作接口
//--------------------------------------------------------


void updateIndCallTimer(uint32_t periodInMilliseconds);


void checkIsToReTxIndCallRequestMsg(uint32_t periodInMilliseconds);


void checkIsToHangUpForTimeOut(uint32_t periodInMilliseconds);


/** 重新发出单呼请求 */
void reDial();


/** 返回当前被叫是否处于响铃状态 */
bool isCalleeRinging();




#endif


