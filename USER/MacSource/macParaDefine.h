
/************************************************************************/
// Created By ZuoDahua  ---  2017/06/02
// dhzuo@sandemarine.com
// Team: Research and Development Department of Sandemarine
// Brief: 定义一些MAC层共用的参数
// Copyright (C) Nanjing Sandemarine Electric Co., Ltd
// All Rights Reserved
/************************************************************************/

#ifndef MACPARADEFINE_H
#define MACPARADEFINE_H

#include<stdint.h>
#include<stdbool.h>



#define MAC_VERSION			0x01				// 通信协议栈版本号


#define NUM_DIGITAL_CHANNELS	480				// 数字可用信道数量

#define CHANNEL_START_FREQ		27500000		// 信道起始频率，单位Hz

#define CHANNEL_FREQ_SPACE		25000			// 信道频率间隔，单位Hz


#define NUM_CCL_DEAL_MSGS	12					// CCL要处理的消息数量

// 指示消息是否要处理
extern bool CCL_HANDLE_MSG_TYPE[NUM_CCL_DEAL_MSGS];


#define DIGITAL_CALL_COMM_CODE		0x0000		// 数字对频点呼叫业务的通信代码

extern uint32_t thisCallSign;					                 // 本船的呼号
extern uint32_t thisShipNum;					                 // 本船的船号

extern uint32_t currentPartnerCallSign;                // 当前通话中的对方呼号
extern uint32_t currentPartnerShipNum;                 // 当前通话中的对方船号
extern uint16_t currentCommCode;                       // 当前通话采用的通信代码
extern uint16_t currentVoiceChanNum;			             // 当前的话音信道号

extern uint32_t potentialPartnerCallSign;              // 第三方的呼号
extern uint32_t potentialPartnerShipNum;               // 第三方的船号
extern uint16_t potentialCommCode;                     // 第三方的通信代码
extern uint16_t potentialVoiceChanNum; 			           // 第三方的语音信道号

extern uint8_t isSuspend;                              // 设备是否处于挂起状态
extern uint8_t isOC;                                   // 单呼通话中的本机角色--0：被叫；1：主叫

/* 初始化本船信息 */
void setThisShipInfo(uint32_t callSign, uint32_t shipNum);

/** 设置本船相关的群号和密码，同时根据群号和密码，生成对应的通信代码 */
void setThisGroupInfo(uint32_t groupNumber, uint32_t groupPwd);

/** 获取本船群的通信代码 */
uint16_t getThisGroupCommCode();

uint32_t getShipGroupNumber();
uint32_t getShipGroupPassword();

void setThisCallSign(uint32_t callSign);
uint32_t getThisCallSign();

uint32_t getThisShipNumber();

void setCurrentPartnerCallSign(uint32_t callSign);
uint32_t getCurrentPartnerCallSign();

void setCurrentPartnerShipNumber(uint32_t shipNumber);
uint32_t getCurrentPartnerShipNumber();


void setCurrentCommCode(uint16_t commCode);
uint16_t getCurrentCommCode();

void setCurrentVoiceChanNum(uint16_t chanNum);
uint16_t getCurrentVoiceChanNum();


#define CCL_TX_FREQ				33000000		// CCL的信令发射频率
#define CCL_TX_CHAN_NUM			220				// CCL的发射信道号


#define DDS_REF_FREQ        350000000		// DDS的参考频率，350MHz

#define DDS_RX_FREQ_FREQ    10700000        // DDS作为接收本振时的参考频率





#endif // !MACPARADEFINE_H


