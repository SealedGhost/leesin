/************************************************************************/
// Created By ZuoDahua  ---  2017/06/12
// dhzuo@sandemarine.com
// Team: Research and Development Department of Sandemarine
// Brief: 维护MAC层的业务状态
//        MAC层的业务共有两个：语音呼叫业务；船舶信息搜索业务
// Copyright (C) Nanjing Sandemarine Electric Co., Ltd
// All Rights Reserved
/************************************************************************/

#include "macService.h"
#include "macParaDefine.h"
#include "macUtils.h"
#include "cclMsgParser.h"
#include "dllLayer.h"
#include "cclChanListener.h"
#include "cclShipInfoRequest.h"
#include "GUI_Message.h"
#include "cclIndCallLayer.h"
#include "cclChanListener.h"
#include "cclGroupCallLayer.h"
#include "cclIndCallLayer.h"
#include "pttController.h"
#include "mac_working.h"
#include "ipc.h"
#include "gpio.h"
#include "stdio.h"
 #include <uCOS-II\Source\ucos_ii.h>


static MacServiceType macServiceState = ServiceIdle;


static bool isVoiceService = false;


// 配置物理层的参数静态变量
static PhyConfigParam phyConfigParam;

static uint8_t configPhyDataBuffer[NUM_PHY_CONFIG_BYTES];


// FPGA是否初始化标志
static bool isFpgaInited = false;




//********************************************
// @brief       获取当前MAC的服务状态类型
// @author zuodahua
//********************************************
inline MacServiceType getMacServiceType()
{
    return macServiceState;
}

inline bool isSystemVoiceService()
{
    return ((macServiceState == ServiceIdle || macServiceState == IndCallConnecting) ? false : true);
}


//********************************************
/* 
  * @brief    指示系统是否可以进行信道扫描
  * @return    true --- 可以做信道扫描， false --- 停止信道扫描 
  * @note
  */
//********************************************
inline bool isChanScanAvailable()
{
    return (macServiceState == ServiceIdle);
}


//********************************************
// @brief       当前系统是否话音避让，如果当前系统是在单呼、群呼状态，则避让，否则则不避让 
// @author zuodahua
//********************************************
bool isAvoidVoicePtt()
{
    if(macServiceState == IndCall || macServiceState == GroupCall)
    {
        return true;
    }
    else
    {
        return false;
    }
}




//********************************************
// @brief   MAC层处理来自App的消息
// @param   msg                --- Message类型消息数据地址
// @author zuodahua
//********************************************
void macPeekAppMsg(Message * msg)
{
	//printf(" mac rec msg ,   msg.id = %d\n", msg->id);
    switch(msg->id)
    {
        case AnalogCallStartAction:
            macServiceState = AnalogCall;
			//printf("mac rec analog in msg \n");
            setVoiceServiceParam(AnalogCallStartAction, msg->v);
            break;
        case AnalogCallOverAction:
            macServiceState = ServiceIdle;
            setVoiceServiceParam(AnalogCallOverAction, 0);
            break;
        case DigitalCallStartAction:
            macServiceState = DigitalCall;
            setCurrentCommCode(DIGITAL_CALL_COMM_CODE);
            setVoiceServiceParam(DigitalCallStartAction, msg->v);
            break;
        case DigitalCallOverAction:
            macServiceState = ServiceIdle;
            setVoiceServiceParam(DigitalCallOverAction, 0);
            break;
        case GroupCallStartAction:
//            printf(" group call , freq = %d\n\n", msg->v);
            macServiceState = GroupCall;
            setCurrentCommCode(getThisGroupCommCode());
            setVoiceServiceParam(GroupCallStartAction, msg->v);
            break;
        case GroupCallOverAction:
            macServiceState = ServiceIdle;
//            printf("  group call over \n\n");
            setVoiceServiceParam(GroupCallOverAction, 0);
            break;
        case ShipInfoRequestAction:
            requestShipInfo(msg->v);
            break;
        
        case IndCallDialAction:
            macServiceState = IndCallConnecting;       // 单呼拨号时，应该为话音状态，避免PTT在通信连接过程中按下
            indCallHandleDial(msg->v);                 // 单呼拨号请求，应用层要给出具体的呼号
            break;
        case RequestAvailableChanNumber:
            handleChanNumberRequest();
            break;
        case IndCallConnectAction:                      // 单呼接通指令
            macServiceState = IndCall;
            // APP给MAC层的接通指令，需要及时对物理层进行配置
            indCallHandleConnectAction();
            break;
        case IndCallHangUpAction:                       // 单呼挂断指令
            appHangupIndCallUpdateState();
//			printf("   app hang up ... \n\n");
            indCallHandleHangupAction();            
            break;
        case ConfigParaAction:
            configShipInformation((ShipInformation *)msg->p);
            break;
        case ConfigVoiceServiceFreqAction:
            setVoiceServiceParam(ConfigVoiceServiceFreqAction, msg->v);
            break;
        
        // 接收机话音接收信号
        case RxOverAction:
            pttDealRxVoiceOverMsg();
            groupCallDealRxVoiceOverInfo();
            break;
        
        // 天气频道相关
        case WeatherStartAction:
            macServiceState = ServiceIdle;
            setVoiceServiceParam(WeatherStartAction, msg->v);
            break;
        case WeatherOverAction:
            macServiceState = ServiceIdle;
            setVoiceServiceParam(WeatherOverAction, 0);
            break;
        default:
            return;
    }    
}



//********************************************
// @brief   MAC层将消息推送给APP层
// @param macAction            --- 具体的指令类型
// @author zuodahua
//********************************************
void macPostMsgToApp(MacServiceAction macAction, uint32_t shipId, uint32_t shipNumber)
{
    switch(macAction)
    {
        case GroupCallTalkerStartNotifyAction:
            GUI_PostMessage(IPC_MSG_CCL, GroupCallTalkerStartNotifyAction, shipId, shipNumber);
            break;
        case GroupCallTalkerOverNotifyAction:
            GUI_PostMessage(IPC_MSG_CCL, GroupCallTalkerOverNotifyAction, 0, 0);
            break;
        case GroupCallMsgNotifyAction:
//            printf("  mac post group msg \n");
            GUI_PostMessage(IPC_MSG_CCL, GroupCallMsgNotifyAction, 0, 0);
            break;
        case ShipInfoResponseAction:
            GUI_PostMessage(IPC_MSG_CCL, ShipInfoResponseAction, shipId, shipNumber);
            break;
        case IndCallTCRingAction:
//            macServiceState = IndCallConnecting;            // 单呼被叫响铃，实际进入单呼业务
//			printf("     mac post ring to app ... \n\n");
            GUI_PostMessage(IPC_MSG_CCL, IndCallTCRingAction, shipId, shipNumber);
            break;
        case IndCallOCRingAction:
            macServiceState = IndCallConnecting;           // 单呼主叫响铃，实际进入单呼业务
            GUI_PostMessage(IPC_MSG_CCL, IndCallOCRingAction, shipId, shipNumber);
            break;
        case IndCallTcBusyAction:
            macServiceState = ServiceIdle;                 // 单呼“对方忙”，离开单呼业务
            GUI_PostMessage(IPC_MSG_CCL, IndCallTcBusyAction, shipId, 0);
            break;
        case IndCallFailAction:
            macServiceState = ServiceIdle;                  // 单呼“呼叫失败”，离开单呼业务
            GUI_PostMessage(IPC_MSG_CCL, IndCallFailAction, shipId, 0);
            break;
        case IndCallConnectAction:
            // 单呼接通指令
            printf("  indcal connect msg, phone id = %d ...\n\n", shipId);
            macServiceState = IndCall;
            GUI_PostMessage(IPC_MSG_CCL, IndCallConnectAction, shipId, 0);
            break;
        case IndCallHangUpAction:
            printf("  indcall hangup msg, phone id = %d ... \n\n", shipId);
            // 单呼挂断指令
            macHandleIndCallHangupUpdateState();
            GUI_PostMessage(IPC_MSG_CCL, IndCallHangUpAction, shipId, 0);
            break;
        case PushAvailableChanNumAction:
            GUI_PostMessage(IPC_MSG_CCL, PushAvailableChanNumAction, shipId, 0);
            break;
        case TxStartAction:
            GUI_PostMessage(IPC_MSG_CCL, TxStartAction, 0, 0);
            break;
        case TxOverAction:
            GUI_PostMessage(IPC_MSG_CCL, TxOverAction, 0, 0);
            break;
		case FpgaInitDone:
			GUI_PostMessage(IPC_MSG_CCL, FpgaInitDone, 0, 0);
        default:
            return;
    }
    
}


//********************************************
// @brief   CCL调用相关DLL的消息post接口，将待发送消息发送出去
// @param   msg         --- 待发送给DLL层的消息
// @author zuodahua
//********************************************
void cclPostMsgToDll(CclPostToDllMsg * msg)
{
    // 将消息推送到DLL的消息队列中
    DLL_PostMessage(IPC_MSG_CCL, CCL_TO_DLL_MSG_ID, 0, (uintptr_t) msg);
//	printf("    ccl post msg to dll,   postMsgId = %d\n", CCL_TO_DLL_MSG_ID);
}



//********************************************
// @brief   DLL将接收到的消息推送给CCL层 
// @author zuodahua
//********************************************
void dllPostMsgToCcl(uint8_t * dataBuffer, uint32_t numBytes)
{
    DLL_PostMessage(MAC_MESSAGE_TYPE, DLL_TO_CCL_MSG_ID, numBytes, ((uintptr_t )dataBuffer));
}



//********************************************
// @brief   设置本机的MAC层服务状态 
// @author zuodahua
//********************************************
void setMacServiceState(MacServiceType serviceType)
{
    macServiceState = serviceType;
}


//********************************************
// @brief   应用层挂断机器，MAC层更新状态  
// @author zuodahua
//********************************************
void appHangupIndCallUpdateState()
{
    if(isCalleeRinging() == false)
    {
        // 如果此时在通话状态，同时收到第三方来电，则直接挂断当前来电，而不改变当前的业务状态
        // 如果此时不在通话状态，接收到来电后挂断，则直接将状态置为空闲即可
        macServiceState = ServiceIdle;
    }    
}


/** MAC层收到底层挂断消息后，MAC层更新状态接口 */
void macHandleIndCallHangupUpdateState()
{
    if(macServiceState == IndCall || macServiceState == GroupCall)
    {
        return;
    }
    else
    {
        macServiceState = ServiceIdle;
    }
}



//********************************************
// @brief   处理来自应用层的信道推荐请求
// @author zuodahua
//********************************************
void handleChanNumberRequest()
{
    uint16_t chanNumber = getOneAvailableChannel();
    macPostMsgToApp(PushAvailableChanNumAction, chanNumber, 0);
}




//********************************************
// @brief   MAC层配置物理层相关参数
// @param   物理层所需参数结构体
// @author zuodahua
//********************************************
void macConfigPhyParam(PhyConfigParam param)
{
    configPhyDataBuffer[0] = DLL_PHY_FRAME_HEADER;
    configPhyDataBuffer[1] = param.isVoiceService ? 0x01 : 0x00;
    configPhyDataBuffer[1] |= (param.workMode == ANALOG_WORK_MODE ? 0x02 : 0x00);
    configPhyDataBuffer[2] = param.macVersion;
    setIntNumberToHalfBytes(param.msgId, configPhyDataBuffer, 3, 2);
    setIntNumberToHalfBytes(param.commCode, configPhyDataBuffer, 5, 4);
    setIntNumberToHalfBytes(param.shipId, configPhyDataBuffer, 9, 6);
    setIntNumberToHalfBytes(param.shipNumber, configPhyDataBuffer, 15, 5);
    setIntNumberToHalfBytes(param.startCrc, configPhyDataBuffer, 20, 4); 
    setIntNumberToHalfBytes(param.overCrc, configPhyDataBuffer, 24, 4);
    setIntNumberToHalfBytes(param.ddsRxFreqWord, configPhyDataBuffer, 28, 8);
    setIntNumberToHalfBytes(param.ddsTxFreqWord, configPhyDataBuffer, 36, 8);
    
    // 通过串口3向FPGA发送配置信息
    UART_Send(LPC_UART3, configPhyDataBuffer, NUM_PHY_CONFIG_BYTES, BLOCKING);
	
	//printf("    uart3 sending the config msg ... \n");
	//printf("          config - workMode = %d\n", param.workMode);
	//printf("          config - isVoiceSerivce = %d\n", param.isVoiceService);
}

//********************************************
// @brief       根据当前输入的业务类型，设置话音业务所需的参数
// @param   serviceType            --- 业务类型
// @param   txFreq                 --- 发射频率
// @author zuodahua
//********************************************
void setVoiceServiceParam(MacServiceAction serviceAction, uint32_t txFreq)
{
//	printf("  setVoice service param,  type = %d\n\n", serviceAction);
    phyConfigParam.macVersion = MAC_VERSION;
    phyConfigParam.shipId = getThisCallSign();
    phyConfigParam.shipNumber = getThisShipNumber();
    phyConfigParam.ddsRxFreqWord = calDdsRxFreqCtrWord(calDdsTxFreq(CCL_TX_CHAN_NUM));    // 配置dds的接收频率（信令频道）
    if(serviceAction == AnalogCallStartAction || serviceAction == DigitalCallStartAction || serviceAction == GroupCallStartAction || serviceAction == IndCallConnectAction)
    {
        phyConfigParam.isVoiceService = true;
        phyConfigParam.msgId = TalkVoiceMsgId;
        phyConfigParam.commCode = getCurrentCommCode();
        
        calVoiceInfoFrameCrc(&phyConfigParam);
        phyConfigParam.ddsTxFreqWord = calDdsFreqCtrWord(txFreq);
//		phyConfigParam.ddsRxFreqWord = calDdsRxFreqCtrWord(txFreq);		// 将dds接收作为话音频道
        
        if(serviceAction == AnalogCallStartAction)
        {
            phyConfigParam.workMode = ANALOG_WORK_MODE;
        }
        else
        {
            phyConfigParam.workMode = DIGITAL_WORK_MODE;
        }
        setVoiceChannelFreq(txFreq);
    }
    else if(serviceAction == AnalogCallOverAction || serviceAction == DigitalCallOverAction || serviceAction == GroupCallOverAction || serviceAction == IndCallHangUpAction)
    {
        // 当系统不处于话音业务状态时，只需直接将物理层置于非通话状态即可，其他参数不用变
        phyConfigParam.isVoiceService = false;
        phyConfigParam.msgId = 0;
		
		if(serviceAction == AnalogCallOverAction)
		{
			phyConfigParam.workMode = DIGITAL_WORK_MODE;
		}
    }
    else if(serviceAction == ConfigVoiceServiceFreqAction)
    {
        phyConfigParam.ddsTxFreqWord = calDdsFreqCtrWord(txFreq);
        setVoiceChannelFreq(txFreq);
    }
    else if(serviceAction == WeatherStartAction)
    {
        phyConfigParam.isVoiceService = true;
        phyConfigParam.workMode = ANALOG_WORK_MODE;
        setVoiceChannelFreq(txFreq);
    }
    else if(serviceAction == WeatherOverAction)
    {
        phyConfigParam.isVoiceService = false;
    }
    
    // 生成具体的配置消息格式，并进行数据传输
    macConfigPhyParam(phyConfigParam);
}

//********************************************
// @brief       计算话音业务的起始和结束帧所需的crc参数，并设置在configParam指定的参数中
// @param   configParam ---  物理层配置参数指针
// @author zuodahua
//********************************************
void calVoiceInfoFrameCrc(PhyConfigParam * configParam)
{
    uint8_t tmp[NUM_CCL_MSG_BYTES] = {0};
    tmp[0] = MAC_VERSION << 2;
	tmp[0] |= ((VoiceStartMsgId >> 6) & 0x03);
	tmp[1] = VoiceStartMsgId << 2;
	tmp[1] |= (CCL_MSG_SLOT_NUM >> 3) & 0x03;
	tmp[2] = (CCL_MSG_SLOT_NUM << 5) & 0xFF;
    setBitsToBuffer(tmp, 19, configParam->commCode, 16);
    setBitsToBuffer(tmp, 35, configParam->shipId, 24);
    setBitsToBuffer(tmp, 59, configParam->shipNumber, 17);
	
    configParam->startCrc = crc16UseTable(tmp, 0, NUM_CCL_MSG_BYTES-2);
    
    tmp[0] = (tmp[0] & 0xFC) | ((VoiceOverMsgId >> 6) & 0x03);
    tmp[1] = (tmp[1] & 0x03) | ((VoiceOverMsgId << 2) & 0xFC);
    configParam->overCrc = crc16UseTable(tmp, 0, NUM_CCL_MSG_BYTES-2);
}


//********************************************
// @brief   设置本船相关信息
// @param   info        --- 本船信息结构体指针
// @author zuodahua
//********************************************
void configShipInformation(ShipInformation * info)
{
    printf( "srcId = %d, shipNumber = %d\n\n", info->shipId, info->shipNumber );
    setThisShipInfo(info->shipId, info->shipNumber);
    setThisGroupInfo(info->groupNumber, info->grouppwd);
}



//********************************************
// @brief   ccl层的定时器任务，输入的是本次任务循环所消耗的时间
// @param   periodInMilliseconds    --- 本次任务循环所消耗的时间，单位为毫秒
// @author zuodahua
//********************************************
void cclTimerTask(uint32_t periodInMilliseconds)
{
    // 信道状态更新模块
    channelStateUpdate(periodInMilliseconds);
    // 信道扫描
    periodScanChannel(periodInMilliseconds);
    
    // 群呼定期广播消息定时器
    updateGroupInfoTimer(periodInMilliseconds);
    
    // PTT状态更新
    updatePttTalkTimer(periodInMilliseconds);
    
    // 更新单呼
    updateIndCallTimer(periodInMilliseconds);
}



/** 对CCL层进行初始化操纵 */
void initCclLayer()
{
    // 初始化信道监听模块的参数
    initChannelStateInfos();    
}


/** 初始化FPGA */
void initFpga()
{
	if(isFpgaInited == false)
	{
//		printf("  start to initialize the fpga .... \n\n");
		FPGAResetEnable();
		FPGAResetDisable();
		isFpgaInited = true;
		macPostMsgToApp(FpgaInitDone, 0, 0);
        
        OSTimeDlyHMSM(0, 0, 0, 500);
        
        // 配置FPGA硬件参数
        setVoiceServiceParam(ConfigVoiceServiceFreqAction, CHANNEL_START_FREQ);
	}
}




