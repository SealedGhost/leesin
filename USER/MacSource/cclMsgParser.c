#include "cclMsgParser.h"
#include "dllLayer.h"
#include "macParaDefine.h"
#include "macUtils.h"
#include "macService.h"
#include "cclChanListener.h"
#include "pttController.h"
#include "cclGroupCallLayer.h"
#include "cclShipInfoRequest.h"
#include "cclIndCallLayer.h"
#include<stdint.h>
#include <stdio.h>


//------------------------------------------
// 定义一些变量
//------------------------------------------

ChannelIndicator channel;


// 接收消息缓存变量定义
IndCallConnInfoMsg indCallConnInfoMsg;
IndCallSwitchInfoMsg indCallSwitchInfoMsg;
VoiceCallStartMsg voiceCallStartMsg;
VoiceCallOverMsg voiceCallOverMsg;
ShipInfoReqMsg shipInfoReqMsg;
ShipInfoRespMsg shipInfoRespMsg;
GroupInfoBroadcastMsg groupInfoBroadcastMsg;


// 本文件私有变量
// 待发送消息的字节缓冲区，用来缓存将要发到DLL层的数据
uint8_t toSendCCLMsgBuffer[NUM_CCL_MSG_BYTES];

static CclPostToDllMsg cclPostToDllMsg;

// CCL推送消息缓冲区队列相关变量
static CclPostToDllMsg cclPostToDllMsgBuffer[CCL_TO_DLL_MSG_BUFFER_SIZE];
static uint32_t cclPostMsgIndex = 0;



//------------------------------------------
// 函数定义
//------------------------------------------

// 打包消息固定部分，包括版本号、消息ID、时隙号
void packageFixedMsg(uint8_t msgId) {
	toSendCCLMsgBuffer[0] = MAC_VERSION << 2;
	toSendCCLMsgBuffer[0] |= ((msgId >> 6) & 0x03);
	toSendCCLMsgBuffer[1] = msgId << 2;
	toSendCCLMsgBuffer[1] |= (CCL_MSG_SLOT_NUM >> 3) & 0x03;
	toSendCCLMsgBuffer[2] = (CCL_MSG_SLOT_NUM << 5) & 0xFF;
}

//********************************************
// @brief       计算输入数据流的16位CRC结果，并设置在指定的偏移地址上
// @param   srcBuffer   --- 数据缓冲区
// @param   numBytes    --- 缓冲区中要计算crc的字节数量
// @param   byteOffset  --- 缓冲区中放crc结果的起始地址
// @author zuodahua
//********************************************
void setCrcValueToMsg(uint8_t *srcBuffer, int numBytes, int byteOffset) {
	uint16_t crc = crc16UseTable(srcBuffer, 0, numBytes);
	srcBuffer[byteOffset] = crc >> 8;
	srcBuffer[byteOffset + 1] = crc;
}


/**
*		@brief 解析从DLL接收到的消息，并返回解析后的消息ID，如果消息不是系统要解析的消息类型，则直接返回0，否则返回解析到的消息ID
*		@author zuodahua
*		@return uint8_t msgId,  0 --- 无效消息ID
*/
uint8_t parseRxMsg(uint8_t * toParseMsgBuffer)
{
	uint8_t msgId;
	uint8_t version;
		
	version = getBitsToIntValue(toParseMsgBuffer, 0, 6);
	if (version != MAC_VERSION)
	{
		return 0;
	}
	msgId = getBitsToIntValue(toParseMsgBuffer, 6, 8);
	if (CCL_HANDLE_MSG_TYPE[msgId - 1] == false)
	{
		return 0;
	}

	switch (msgId)
	{

	case IndCallRequestMsgId: case IndCallResponseMsgId: case IndCallConnectMsgId:
		parseIndCallConnInfoMsg(msgId, toParseMsgBuffer);
		break;
	case IndCallPickUpMsgId: case IndCallHangUpMsgId: case IndCallSuspendMsgId:
		parseIndCallSwitchInfoMsg(msgId, toParseMsgBuffer);
		break;
	case VoiceStartMsgId:
		parseVoiceCallStartMsg(toParseMsgBuffer);
		break;
	case VoiceOverMsgId:
		parseVoiceCallOverMsg(toParseMsgBuffer);
		break;
	case ShipInfoRequestMsgId:
		parseShipInfoReqMsg(toParseMsgBuffer);
		break;
	case ShipInfoResponseMsgId:
		parseShipInfoRespMsg(toParseMsgBuffer);
		break;
	case GroupInfoBroadcastMsgId:
		parseGroupInfoBroadcastMsg(toParseMsgBuffer);
		break;
	default:
		return 0;
	}
	
	return msgId;
}


void getParsedMsg(CclMsgType msgType, void *msg){
	if(msgType == NullMsg){
		return;
	}
	switch(msgType)
	{
		case IndCallConnInfoMsgType:
			
			break;
		case IndCallSwitchInfoMsgType:
			break;
		
		case VoiceCallStartMsgType : case VoiceCallOverMsgType:
			
		default:
			break;
	}		
}


//************************************
// Method:    	parseIndCallConnInfoMsg
// Access:   	public 
// Returns:  	void
// Description: 解析消息1 - 3
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
void parseIndCallConnInfoMsg(uint8_t msgId, uint8_t * toParseMsgBuffer)
{
	uint8_t idxChan;
	uint16_t idxBit;
    
	indCallConnInfoMsg.msgId			= msgId;
	indCallConnInfoMsg.commCode		    = getBitsToIntValue(toParseMsgBuffer, 19, 16);
	indCallConnInfoMsg.srcId			= getBitsToIntValue(toParseMsgBuffer, 35, 24);
	indCallConnInfoMsg.dstId			= getBitsToIntValue(toParseMsgBuffer, 59, 24);
	indCallConnInfoMsg.srcShipNum		= getBitsToIntValue(toParseMsgBuffer, 83, 17);
	indCallConnInfoMsg.numChannel		= getBitsToIntValue(toParseMsgBuffer, 100, 3);
    
    if(indCallConnInfoMsg.numChannel > NUM_AVAILABEL_CHANNEL)
    {
        return;
    }
    
#ifdef TEST_CCL_MSG_PARSE_INDCONNMSG
	//printf("msgId = %d, commCode = %4x, srcId = %6x, dstId = %6x, srcShipNum = %5x, numChans = %d \n",
		indCallConnInfoMsg.msgId, indCallConnInfoMsg.commCode, indCallConnInfoMsg.srcId, indCallConnInfoMsg.dstId, indCallConnInfoMsg.srcShipNum, indCallConnInfoMsg.numChannel);
#endif // TEST_CCL_MSG_PARSE_INDCONNMSG
	
	idxBit = 103;
	for (idxChan = 0; idxChan < indCallConnInfoMsg.numChannel; idxChan++)
	{
		indCallConnInfoMsg.chanNum[idxChan] = getBitsToIntValue(toParseMsgBuffer, idxBit, 9);
		idxBit += 9;
#ifdef TEST_CCL_MSG_PARSE_INDCONNMSG
		//printf("chanNum[%d] = %d\n", idxChan, indCallConnInfoMsg.chanNum[idxChan]);
#endif // TEST_CCL_MSG_PARSE_INDCONNMSG
	}
	
	//printf("   msgParser rec msg , msgId = %u, dstId = %u\n", msgId, indCallConnInfoMsg.dstId);
    handleIndCallMsgFromDll(indCallConnInfoMsg.msgId, &indCallConnInfoMsg);
}

//************************************
// Method:    	parseIndCallSwitchInfoMsg
// Access:   	public 
// Returns:  	void
// Description: 解析消息4 - 6
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
void parseIndCallSwitchInfoMsg(uint8_t msgId, uint8_t * toParseMsgBuffer)
{
    
	indCallSwitchInfoMsg.msgId = msgId;
	indCallSwitchInfoMsg.commCode = getBitsToIntValue(toParseMsgBuffer, 19, 16);
	indCallSwitchInfoMsg.chanNum = getBitsToIntValue(toParseMsgBuffer, 35, 9);
	indCallSwitchInfoMsg.srcId = getBitsToIntValue(toParseMsgBuffer, 44, 24);
	indCallSwitchInfoMsg.dstId = getBitsToIntValue(toParseMsgBuffer, 68, 24);
    
    // 处理单呼
    handleIndCallMsgFromDll(indCallSwitchInfoMsg.msgId, &indCallSwitchInfoMsg);
    
    // 处理信道占用
    dealChannelOcupyInfo(IndCallSwitchInfoMsgType, &indCallSwitchInfoMsg);
}

//************************************
// Method:    	parseVoiceCallStartMsg
// Access:   	public 
// Returns:  	void
// Description: 解析消息8
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
void parseVoiceCallStartMsg(uint8_t * toParseMsgBuffer)
{
	voiceCallStartMsg.commCode = getBitsToIntValue(toParseMsgBuffer, 19, 16);
	voiceCallStartMsg.srcShipId = getBitsToIntValue(toParseMsgBuffer, 35, 24);
	voiceCallStartMsg.srcShipNum = getBitsToIntValue(toParseMsgBuffer, 59, 17);
    
    dealVoiceTalkMsg(VoiceCallStartMsgType, &voiceCallStartMsg);
    dealGroupCallInfo(VoiceCallStartMsgType, &voiceCallStartMsg);
    
    dealChannelOcupyInfo(VoiceCallStartMsgType, &voiceCallStartMsg);
}

//************************************
// Method:    	parseVoiceCallOverMsg
// Access:   	public 
// Returns:  	void
// Description: 解析消息9
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
void parseVoiceCallOverMsg(uint8_t * toParseMsgBuffer)
{
	voiceCallOverMsg.commCode = getBitsToIntValue(toParseMsgBuffer, 19, 16);
    
    dealVoiceTalkMsg(VoiceCallOverMsgType, &voiceCallOverMsg);
    dealGroupCallInfo(VoiceCallOverMsgType, &voiceCallOverMsg);
    
    dealChannelOcupyInfo(VoiceCallOverMsgType, &voiceCallOverMsg);
}

//********************************************
// @brief   解析并处理船舶信息请求消息
// @param
// @author zuodahua
//********************************************
void parseShipInfoReqMsg(uint8_t * toParseMsgBuffer)
{
	shipInfoReqMsg.srcId = getBitsToIntValue(toParseMsgBuffer, 19, 24);
	shipInfoReqMsg.dstShipNum = getBitsToIntValue(toParseMsgBuffer, 43, 17);
    
    responseShipInfoRequest(shipInfoReqMsg.srcId, shipInfoReqMsg.dstShipNum);    
}

//********************************************
// @brief   解析并处理船舶信息响应消息，如果是本船的消息，则要通知应用层
// @param
// @author zuodahua
//********************************************
void parseShipInfoRespMsg(uint8_t * toParseMsgBuffer)
{
	shipInfoRespMsg.dstId = getBitsToIntValue(toParseMsgBuffer, 19, 24);
	shipInfoRespMsg.srcId = getBitsToIntValue(toParseMsgBuffer, 43, 24);
	shipInfoRespMsg.shipNum = getBitsToIntValue(toParseMsgBuffer, 67, 17);
    
    dealRxShipInfoResponse(&shipInfoRespMsg);
}


void parseGroupInfoBroadcastMsg(uint8_t * toParseMsgBuffer) {
	groupInfoBroadcastMsg.commCode = getBitsToIntValue(toParseMsgBuffer, 19, 16);
	groupInfoBroadcastMsg.chanNum = getBitsToIntValue(toParseMsgBuffer, 35, 9);
	
	groupInfoBroadcastMsg.groupNumber = getBitsToIntValue(toParseMsgBuffer, 44, 14);
	groupInfoBroadcastMsg.groupPwd = getBitsToIntValue(toParseMsgBuffer, 58, 18);
    
    dealGroupBroadcastMsg(&groupInfoBroadcastMsg);
    
    dealChannelOcupyInfo(GroupInfoBroadcastMsgType, &groupInfoBroadcastMsg);
}



//********************************************
// @brief   清空发送数据缓冲区
// @author zuodahua
//********************************************
void clearBufferBeforePackage()
{
	uint16_t idx;
	for (idx = 0; idx < NUM_CCL_MSG_BYTES; idx++)
	{
		toSendCCLMsgBuffer[idx] = 0x00;
	}
}

//************************************
// Method:    	packageToSendMsg
// Access:   	public 
// Parameter: 	CclMsgType msgType	--- 消息类型
// Parameter: 	void * msg			--- 消息指针
// Returns:  	void
// Description: 封装待发送的信令消息，并向DLL层进行推送
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
void packageToSendMsg(CclMsgType msgType, void * msg)
{
	clearBufferBeforePackage();
	switch (msgType)
	{
	case IndCallConnInfoMsgType:
		packageIndCallConnInfoMsg(msg);
		break;
	case IndCallSwitchInfoMsgType:
		packageIndCallSwitchInfoMsg(msg);
		break;
//	case VoiceCallStartMsgType:
//		packageVoiceCallStartMsg(msg);
//		break;
//	case VoiceCallOverMsgType:
//		packageVoiceCallOverMsg(msg);
//		break;
	case ShipInfoReqMsgType:
		packageShipInfoReqMsg(msg);
		break;
	case ShipInfoRespMsgType:
		packageShipInfoRespMsg(msg);
		break;
	case GroupInfoBroadcastMsgType:
		packageGroupInfoBroadcastMsg(msg);
		break;
	default:
        return;
	}
    
    // 向DLL推送待发送信令消息
    cclPostMsgToDll(&cclPostToDllMsgBuffer[cclPostMsgIndex]);
    cclPostMsgIndex = (cclPostMsgIndex + 1) % CCL_TO_DLL_MSG_BUFFER_SIZE;
}

void packageIndCallConnInfoMsg(IndCallConnInfoMsg * msg)
{
	uint8_t idxChan;
	uint8_t bitOffset;
    uint8_t idxByte;
    
    if(msg->numChannel > NUM_AVAILABEL_CHANNEL)
    {
        return;
    }
    
	//printf("  package indCallConnInfoMsg , msgId = %u, txTimes = %d\n", msg->msgId, msg->txTimes);
	packageFixedMsg(msg->msgId);
	setBitsToBuffer(toSendCCLMsgBuffer, 19, msg->commCode, 16);
	setBitsToBuffer(toSendCCLMsgBuffer, 35, msg->srcId, 24);
	setBitsToBuffer(toSendCCLMsgBuffer, 59, msg->dstId, 24);
	setBitsToBuffer(toSendCCLMsgBuffer, 83, msg->srcShipNum, 17);
	setBitsToBuffer(toSendCCLMsgBuffer, 100, msg->numChannel, 3);
	bitOffset = 103;  
    
	for (idxChan = 0; idxChan < msg->numChannel; idxChan++)
	{
		setBitsToBuffer(toSendCCLMsgBuffer, bitOffset, msg->chanNum[idxChan], 9);
		bitOffset += 9;
	}     
    
	setCrcValueToMsg(toSendCCLMsgBuffer, NUM_CCL_MSG_BYTES - 2, NUM_CCL_MSG_BYTES - 2);      
    
    cclPostToDllMsgBuffer[cclPostMsgIndex].isAvoid = msg->isAvoid;
    cclPostToDllMsgBuffer[cclPostMsgIndex].txFreq = calDdsTxFreq(msg->txChanNumber);
    cclPostToDllMsgBuffer[cclPostMsgIndex].txTimes = msg->txTimes;    
	
    duplicate3Times(toSendCCLMsgBuffer, NUM_CCL_MSG_BYTES, cclPostToDllMsgBuffer[cclPostMsgIndex].dataBuffer, 0);
	
	//printf("done\n");
}

void packageIndCallSwitchInfoMsg(IndCallSwitchInfoMsg * msg)
{
	packageFixedMsg(msg->msgId);
	setBitsToBuffer(toSendCCLMsgBuffer, 19, msg->commCode, 16);
	setBitsToBuffer(toSendCCLMsgBuffer, 35, msg->chanNum, 9);
	setBitsToBuffer(toSendCCLMsgBuffer, 44, msg->srcId, 24);
	setBitsToBuffer(toSendCCLMsgBuffer, 68, msg->dstId, 24);
	setCrcValueToMsg(toSendCCLMsgBuffer, NUM_CCL_MSG_BYTES - 2, NUM_CCL_MSG_BYTES - 2);
    
    cclPostToDllMsgBuffer[cclPostMsgIndex].isAvoid = msg->isAvoid;
    cclPostToDllMsgBuffer[cclPostMsgIndex].txFreq = calDdsTxFreq(msg->txChanNumber);
    cclPostToDllMsgBuffer[cclPostMsgIndex].txTimes = msg->txTimes;
	
	//printf("  package IndCallSwitchInfoMsg , msgId = %u, dstId = %u\n", msg->msgId, msg->dstId);
	
	duplicate3Times(toSendCCLMsgBuffer, NUM_CCL_MSG_BYTES, cclPostToDllMsgBuffer[cclPostMsgIndex].dataBuffer, 0);
}


// 封装语音起始帧的任务将由FPGA来做，本机要做一个计算CRC的任务
void packageVoiceCallStartMsg(VoiceCallStartMsg * msg)
{
	packageFixedMsg(VoiceStartMsgId);
	setBitsToBuffer(toSendCCLMsgBuffer, 19, msg->commCode, 16);
	setBitsToBuffer(toSendCCLMsgBuffer, 35, msg->srcShipId, 24);
	setBitsToBuffer(toSendCCLMsgBuffer, 59, msg->srcShipNum, 17);
}

// 封装语音结束帧的任务由FPGA来做，
void packageVoiceCallOverMsg(VoiceCallOverMsg * msg)
{

}

//************************************
// Method:    	packageShipInfoReqMsg
// Access:   	public 
// Parameter: 	ShipInfoReqMsg * msg
// Returns:  	void
// Description: 封装船舶信息请求帧
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
void packageShipInfoReqMsg(ShipInfoReqMsg * msg)
{
	packageFixedMsg(ShipInfoRequestMsgId);
	setBitsToBuffer(toSendCCLMsgBuffer, 19, msg->srcId, 24);
	setBitsToBuffer(toSendCCLMsgBuffer, 43, msg->dstShipNum, 17);
	setCrcValueToMsg(toSendCCLMsgBuffer, NUM_CCL_MSG_BYTES - 2, NUM_CCL_MSG_BYTES - 2);
    
    cclPostToDllMsgBuffer[cclPostMsgIndex].isAvoid = msg->isAvoid;
    cclPostToDllMsgBuffer[cclPostMsgIndex].txFreq = calDdsTxFreq(msg->txChanNumber);
    cclPostToDllMsgBuffer[cclPostMsgIndex].txTimes = msg->txTimes;
    
	//printf("  package ShipInfoReqMsg , txTimes = %d\n", msg->txTimes);
	
    duplicate3Times(toSendCCLMsgBuffer, NUM_CCL_MSG_BYTES, cclPostToDllMsgBuffer[cclPostMsgIndex].dataBuffer, 0);
}

//************************************
// Method:    	packageShipInfoRespMsg
// Access:   	public 
// Parameter: 	ShipInfoRespMsg * msg
// Returns:  	void
// Description: 封装船舶信息请求应答帧
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
void packageShipInfoRespMsg(ShipInfoRespMsg * msg)
{
	packageFixedMsg(ShipInfoResponseMsgId);
	setBitsToBuffer(toSendCCLMsgBuffer, 19, msg->dstId, 24);
	setBitsToBuffer(toSendCCLMsgBuffer, 43, msg->srcId, 24);
	setCrcValueToMsg(toSendCCLMsgBuffer, NUM_CCL_MSG_BYTES - 2, NUM_CCL_MSG_BYTES - 2);
    
    cclPostToDllMsgBuffer[cclPostMsgIndex].isAvoid = msg->isAvoid;
    cclPostToDllMsgBuffer[cclPostMsgIndex].txFreq = calDdsTxFreq(msg->txChanNumber);
    cclPostToDllMsgBuffer[cclPostMsgIndex].txTimes = msg->txTimes;
    
	//printf("  package ShipInfoRespMsg , txTimes = %d\n", msg->txTimes);
	
    duplicate3Times(toSendCCLMsgBuffer, NUM_CCL_MSG_BYTES, cclPostToDllMsgBuffer[cclPostMsgIndex].dataBuffer, 0);
}

void packageGroupInfoBroadcastMsg(GroupInfoBroadcastMsg * msg)
{
	packageFixedMsg(GroupInfoBroadcastMsgId);
	setBitsToBuffer(toSendCCLMsgBuffer, 19, msg->commCode, 16);
	setBitsToBuffer(toSendCCLMsgBuffer, 35, msg->chanNum, 9);
	setBitsToBuffer(toSendCCLMsgBuffer, 44, msg->groupNumber, 14);
	setBitsToBuffer(toSendCCLMsgBuffer, 58, msg->groupPwd, 18);
	
	setCrcValueToMsg(toSendCCLMsgBuffer, NUM_CCL_MSG_BYTES - 2, NUM_CCL_MSG_BYTES - 2);
    
    cclPostToDllMsgBuffer[cclPostMsgIndex].isAvoid = msg->isAvoid;
    cclPostToDllMsgBuffer[cclPostMsgIndex].txFreq = calDdsTxFreq(msg->txChanNumber);
    cclPostToDllMsgBuffer[cclPostMsgIndex].txTimes = msg->txTimes;
    
    duplicate3Times(toSendCCLMsgBuffer, NUM_CCL_MSG_BYTES, cclPostToDllMsgBuffer[cclPostMsgIndex].dataBuffer, 0);
}


void getPackagedMsg(uint8_t * dstBuffer)
{
	int idx;
	for (idx = 0; idx < NUM_CCL_MSG_BYTES; idx++)
	{
		*(dstBuffer++) = toSendCCLMsgBuffer[idx];
	}
}



