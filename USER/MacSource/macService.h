/************************************************************************/
// Created By ZuoDahua  ---  2017/06/12
// dhzuo@sandemarine.com
// Team: Research and Development Department of Sandemarine
// Brief: 维护MAC层的业务状态
//        MAC层的业务共有两个：语音呼叫业务；船舶信息搜索业务
// Copyright (C) Nanjing Sandemarine Electric Co., Ltd
// All Rights Reserved
/************************************************************************/

#ifndef MACSERVICE_H
#define MACSERVICE_H

#include<stdint.h>
#include "cclMsgParser.h"
#include "dllLayer.h"
#include "mac_working.h"

/** MAC层的业务类型，主要有数字对频点呼叫业务、单呼、群呼以及船舶信息搜索业务
*   这四个业务只能处于一种状态
*/
typedef enum{
    AnalogCall         = 0,
    DigitalCall,
    IndCall,                    // 单呼通话状态
    IndCallConnecting,          // 单呼连接状态
    GroupCall,
    ServiceIdle
} MacServiceType;

MacServiceType getMacServiceType();

/** 获取系统当前是否处于语音业务状态 */
bool isSystemVoiceService();

/** 当前系统是否话音避让，如果当前系统是在单呼、群呼状态，则避让，否则则不避让 */
bool isAvoidVoicePtt();

/** 指示系统是否可以进行信道扫描 */
bool isChanScanAvailable();


typedef enum{
    AnalogCallStartAction                   = 0,
    AnalogCallOverAction,
    DigitalCallStartAction,
    DigitalCallOverAction,
    GroupCallStartAction,
    GroupCallOverAction,
    
    
    GroupCallTalkerStartNotifyAction,               // 群呼状态时，MAC层通知应用层说话人信息指令
    GroupCallTalkerOverNotifyAction,                // 群呼状态时，MAC层通知应用层当前说话人结束说话
    GroupCallMsgNotifyAction,                       // 群呼状态信令上收到消息推送应用层，应用层收到消息后亮灯
    ShipInfoRequestAction,                          // 船号选呼指令，应用层通知MAC层
    ShipInfoResponseAction,                         // 船号选呼应答消息，MAC层通知应用层
    
    WeatherStartAction,                             // 进入天气状态
    WeatherOverAction,                              // 离开天气状态
    
    // 应用层发的单呼业务相关指令
    IndCallDialAction,                              // 单呼拨号指令 --- 单呼发起指令
    RequestAvailableChanNumber,                     // 请求可用信道号
    
    // MAC层发的单呼相关指令
    IndCallTCRingAction,                            // 通知被叫单呼响铃指令
    IndCallOCRingAction,                            // 通知主叫单呼响铃指令
    IndCallTcBusyAction,                            // 对方忙指令，响铃之后没接通
    IndCallFailAction,                              // 呼叫失败，没响铃呼叫失败
    
    // 应用层和MAC层共用的指令
    IndCallConnectAction,                           // 接通指令
    IndCallHangUpAction,                            // 挂断指令
    IndCallSuspendAction,                           // 挂起指令
		
    // MAC层向应用层推送可用信道号
    PushAvailableChanNumAction,                     // 推送可用信道号
    
    TxStartAction,                                  // 发射开始，点亮发射指示灯，显示发射信号强度
    TxOverAction,                                   // 发射结束
    
    RxOverAction,                                   // 接收结束指示
    
    // 应用层配置MAC层相关参数接口
    ConfigParaAction,                               // 配置MAC层相关参数，包括本机船号、呼号、群号、群密码
    ConfigVoiceServiceFreqAction,                   // 配置话音业务的频率
	
	// 发送给应用层的FPGA初始化完成消息
	FpgaInitDone									// FPGA配置完毕
    
} MacServiceAction;


/** 群信息结构体 */
typedef struct
{
    uint32_t shipId;                // 呼号
    uint32_t shipNumber;            // 船号
    uint32_t groupNumber;           // 群号
    uint32_t grouppwd;              // 群密码
} ShipInformation;


/** CCL发送给DLL层的消息结构体 */
typedef struct
{
    uint8_t dataBuffer[NUM_LDPC_MSG_BYTES];
    uint8_t txTimes;
    uint32_t txFreq;
    bool isAvoid;
} CclPostToDllMsg;


/** 处理来自应用层的信道推荐请求 */
void handleChanNumberRequest();


/** MAC层处理来自App的消息 */
void macPeekAppMsg(Message * msg);

/** MAC层将消息推送给APP层 */
void macPostMsgToApp(MacServiceAction macAction, uint32_t shipId, uint32_t shipNumber);


/** CCL调用相关DLL的消息post接口，将待发送消息发送出去 */
void cclPostMsgToDll(CclPostToDllMsg * msg);

/** CCL接收到来自DLL推送的消息，并进行消息分发处理 */
void cclPeekDllMsg(CclMsgType msgType, void * msg);

/** DLL将接收到的消息推送给CCL层 */
void dllPostMsgToCcl(uint8_t * dataBuffer, uint32_t numBytes);

/** DLL接收来自CCL推送的消息，并进行缓存等待发送 */
void dllPeekCclMsg(CclMsgType msgType, void * msg);

/** 设置本机的MAC层服务状态 */
void setMacServiceState(MacServiceType serviceType);


/** 应用层挂断消息，MAC更新状态 */
void appHangupIndCallUpdateState();


/** MAC层收到底层挂断消息后，MAC层更新状态接口 */
void macHandleIndCallHangupUpdateState();


//-----------------------------------------------------------------

// 物理层的参数配置结构体
typedef struct
{
    uint8_t      macVersion;
    uint8_t      workMode;              // 0 --- 数字， 1 --- 模拟
    bool         isVoiceService;        // 是否是语音业务状态
    uint8_t      msgId;                 // 消息Id号，没有话音业务则直接置零即可
    uint16_t     commCode;
    uint32_t     shipId;                // 本机呼号
    uint32_t     shipNumber;            // 本机船号
    uint16_t     startCrc;              // 语音起始CRC
    uint16_t     overCrc;               // 语音结束CRC
    uint32_t     ddsRxFreqWord;         // dds接收频率控制字
    uint32_t     ddsTxFreqWord;         // dds发射频率控制字
} PhyConfigParam;


#define DIGITAL_WORK_MODE 0x00          // 数字工作模式
#define ANALOG_WORK_MODE  0X01          // 模拟工作模式


/** 呼叫者信息 */
typedef struct
{
    uint32_t callerId;
    uint32_t callerShipNum;
} CallerInfo;


/** 设置话音业务的参数配置，当MAC层进入话音业务后，即对物理层进行相关参数配置 */
void setVoiceServiceParam(MacServiceAction serviceAction, uint32_t txFreq);
    

/** MAC层配置物理层参数的接口 */
void macConfigPhyParam(PhyConfigParam param);


void calVoiceInfoFrameCrc(PhyConfigParam * configParam);


void configShipInformation(ShipInformation * info);


/** ccl层的定时器任务，输入的是本次任务循环所消耗的时间 */
void cclTimerTask(uint32_t periodInMilliseconds);


/** 对CCL层进行初始化操纵 */
void initCclLayer();



#define NUM_PHY_CONFIG_BYTES    44          // 物理层参数配置帧格式字节数

#define MAC_MESSAGE_TYPE        0x001a      // MAC层中进行消息通信的类型

#define CCL_TO_DLL_MSG_ID       0x0001      // CCL推送给DLL层的消息ID
#define DLL_TO_CCL_MSG_ID       0x0002      // DLL推送消息给CCL层的消息ID


/** 初始化FPGA */
void initFpga();




#endif



