/************************************************************************/
// Created By ZuoDahua  ---  2017/06/06
// dhzuo@sandemarine.com
// Team: Research and Development Department of Sandemarine
// Brief: CCL信道监听功能模块，包括监听信道的使用状况，以及通信代码信息
// Copyright (C) Nanjing Sandemarine Electric Co., Ltd
// All Rights Reserved
/************************************************************************/

#include "cclChanListener.h"
#include "macParaDefine.h"
#include "macUtils.h"
#include "si4463.h"
#include "adc.h"
#include "macService.h"
#include "system_time.h"
#include<stdint.h>
#include<stdbool.h>
#include<stdlib.h>


// 不可用信道列表，列表编号从1开始，是协议上的正常编号
static const bool isChanNumberAvailable[480] = {
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                        false,  true, false,  true, false,  true,  true,  true,  true,  true, 
                        false,  true,  true,  true,  true, false,  true, false,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true, 
                         true,  true,  true,  true,  true,  true,  true,  true,  true,  true
                     };


// 获取的可用频道号列表
uint16_t availabelChans[NUM_AVAILABEL_CHANNEL];


/* 信道状态信息 */
static ChannelOcupyInfo ChannelStateInfos[NUM_DIGITAL_CHANNELS];


/** 当前扫描的信道号 */
static uint16_t currentScanChanNum = 0;

/** 指示当前系统是否处于信道监听状态 */
static bool isChanListening = false;


// 信道列表状态更新的计时器，以毫秒为单位，当达到给定的时间后，即开始启动一次
// 信道更新，系统查看一遍当前维护的信道列表，如果有超时未更新的信道，即做“可用”处理
static uint32_t chanUpdateTimerInMilliseconds = 0;

// 信道扫描计时器，以毫秒为单位，当系统计时到给定时间后，即启动一次信道监听，
// 将当前的话音信道切换出去，监听接收rssi值，判断该信道是否可用
static uint32_t chanScanTimerInMilliseconds = 0;


// 当前时间结构体
static Time macSystemTime;

//************************************
// Method:    	getAvailableChans
// Access:   	public 
// Returns:  	int
// Description: 获取可用信道列表，并返回获取到的信道数
//              在生成信道过程中引入随机化，给定一个随机起点
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
int getAvailableChans()
{
	int numGetChans = 0;
    int numVisitedChans = 0;
	int idxChan = 0;
    int channelIdxOffset;
    
    SysTime_Get(&macSystemTime);
    srand(macSystemTime.second);
    channelIdxOffset = rand() % NUM_DIGITAL_CHANNELS;
    
	while (numGetChans < NUM_AVAILABEL_CHANNEL && numVisitedChans < NUM_DIGITAL_CHANNELS)
	{
        idxChan = (numVisitedChans + channelIdxOffset) % NUM_DIGITAL_CHANNELS;
		if (isChanNumberAvailable[idxChan] && ((ChannelStateInfos[idxChan].indicator & 0x0FFF) == 0))
		{
			availabelChans[numGetChans++] = idxChan;
		}
		else
		{
			//printf(" line = %u, channel[%u] = %04x\n\n", __LINE__, idxChan, ChannelStateInfos[idxChan].indicator);
		}
		numVisitedChans++;
	}
	return numGetChans;
}



//********************************************
// @brief   获取一个可用信道，如果有则直接返回一个信道号，如果没有，则直接返回一个 1 - 220 之间的一个随机信道号
// @author zuodahua
//********************************************
uint16_t getOneAvailableChannel()
{
    int idxChan = 0;
    int chanNumber;
    SysTime_Get(&macSystemTime);
    srand(macSystemTime.second);
    chanNumber = rand() % NUM_DIGITAL_CHANNELS;
    for(; idxChan < NUM_DIGITAL_CHANNELS; idxChan++)
    {
        chanNumber = (chanNumber + idxChan) % NUM_DIGITAL_CHANNELS;
        if(isChanNumberAvailable[chanNumber] && ((ChannelStateInfos[chanNumber].indicator & 0x0FFF) == 0))
        {
            return (chanNumber+1);
        }
    }
    // 如果没有可用信道，则直接随机生成一个信道号
    return ((rand() % 220) + 1);
}



//************************************
// Method:    	getIndCallCommCode
// Access:   	public 
// Parameter: 	uint32_t srcId --- 呼号，24位
// Parameter: 	uint32_t dstId
// Returns:  	uint16_t
// Description: 获取单呼的通信代码，输入源呼号Id和目标呼号Id
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
uint16_t getIndCallCommCode(uint32_t srcId, uint32_t dstId)
{
	return getValidCommCode(srcId, dstId);
}

//************************************
// Method:    	getGroupCallCommCode
// Access:   	public 
// Parameter: 	uint32_t groupNum
// Parameter: 	uint32_t groupPwd
// Returns:  	uint16_t
// Description: 获取群呼的通信代码，输入群号和密码
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
uint16_t getGroupCallCommCode(uint32_t groupNum, uint32_t groupPwd)
{
	return getValidCommCode(groupNum, groupPwd);
}

//************************************
// Method:    	getValidCommCode
// Access:   	public 
// Parameter: 	uint32_t num1
// Parameter: 	uint32_t num2
// Returns:  	uint16_t
// Description: 根据两个输入数，返回有效的通信代码
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
uint16_t getValidCommCode(uint32_t num1, uint32_t num2)
{
	uint8_t buffer[8];
	uint8_t idxByte;
	for (idxByte = 0; idxByte < 4; idxByte++)
	{
		buffer[idxByte] = (num1 >> ((3 - idxByte) << 3) & 0x000000FF);
	}
	for (idxByte = 0; idxByte < 4; idxByte++)
	{
		buffer[idxByte+4] = (num2 >> ((3 - idxByte) << 3) & 0x000000FF);
	}
	return crc16UseTable(buffer, 0, 8);
}


//********************************************
// @brief       在本地搜索源信道列表是否可用，不可用直接返回-1，否则返回信道号,信道号范围 0 - 479
// @param   srcChannels      --- 信道号列表
// @param   srcNumChans      --- 信道号数量
// @return  源信道号列表中可用的信道号，没有则返回-1
// @author zuodahua
//********************************************
int findOneAvailableChan(uint16_t * srcChannels, uint16_t srcNumChans)
{
    uint16_t idxChan;
    for(idxChan = 0; idxChan < srcNumChans; idxChan++)
    {
        if((ChannelStateInfos[srcChannels[idxChan]].indicator & 0x0FFF) == 0x0000)
        {
            return srcChannels[idxChan];
        }
    }
    return -1;    
}




//************************************
// Method:    	setVoiceChannelFreqWithChanNumberWithChanNumber
// Access:   	public 
// Parameter: 	uint16_t chanNumber  --- 信道号， 1 -- 480
// Returns:  	void
// Description: 设置话音信道的频率，直接通过SPI配置话音信道的本振4463
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
void setVoiceChannelFreqWithChanNumber(uint16_t chanNumber)
{
	uint32_t freq = 0;
    
	freq = CHANNEL_START_FREQ + CHANNEL_FREQ_SPACE * chanNumber;

    // 调用4463频率配置模块进行频率更改
	setVoiceChannelFreq(freq);
}


//********************************************
// @brief       设置话音频道的频率，直接输入要调制的频率
// @param       可用频率，单位为Hz
// @author zuodahua
//********************************************
void setVoiceChannelFreq(uint32_t freq)
{
	//printf("       set Voice Freq = %u\n", freq);
    changeSi4463FrequencyAtHz(((freq + 49950000) << 1));
}



//********************************************
// @brief   根据给定的信道号，返回具体的频率，单位为Hz
// @param   信道号 --- 0 - 479
// @author zuodahua
//********************************************
inline uint32_t calDdsTxFreq(uint16_t chanNumber)
{
    return CHANNEL_START_FREQ + CHANNEL_FREQ_SPACE * chanNumber;    
}



//************************************
// Method:    	dealChannelOcupyInfo
// Access:   	public 
// Parameter: 	CclMsgType cclMsgType
// Parameter: 	void * msg
// Returns:  	void
// Description: 处理信道占用相关消息
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
void dealChannelOcupyInfo(CclMsgType cclMsgType, void * msg)
{
	uint16_t commCode;
	bool isOcupy;
	uint8_t msgId;
	uint16_t chanNum;
	switch (cclMsgType)
	{
	case IndCallSwitchInfoMsgType:
		msgId = ((IndCallSwitchInfoMsg *)msg)->msgId;
		if (msgId == IndCallPickUpMsgId)
		{
			isOcupy = true;
		}
		else
		{
			isOcupy = false;
		}
        chanNum = ((IndCallSwitchInfoMsg *)msg)->chanNum;
        commCode = ((IndCallSwitchInfoMsg *)msg)->commCode;
		break;
	case VoiceCallStartMsgType:
		isOcupy = !isSystemVoiceService();        // 如果是话音状态，则默认为不占用，话音开始帧是在话音信道上接收到的，话音业务上不应该处理此消息
		commCode = ((VoiceCallOverMsg *)msg)->commCode;
		chanNum = getCurrentVoiceChanNum();
		break;
	case VoiceCallOverMsgType:
		isOcupy = !isSystemVoiceService();        // 如果是话音状态，则默认为不占用，话音结束帧是在话音信道上接收到的，话音业务上不应该处理此消息
		commCode = ((VoiceCallOverMsg *)msg)->commCode;
		chanNum = getCurrentVoiceChanNum();
		break;
	case GroupInfoBroadcastMsgType:
		isOcupy = true;
		chanNum = ((GroupInfoBroadcastMsg *)msg)->chanNum;
		commCode = ((GroupInfoBroadcastMsg *)msg)->commCode;
	default:
		return;
	}
    
	ChannelStateInfos[chanNum].commCode = commCode;
	if (isOcupy)
	{
		ChannelStateInfos[chanNum].indicator = (0x8000) | (MAX_CHANNEL_SILENT_TIME & 0x0FFF);
	}
	else
	{
		ChannelStateInfos[chanNum].indicator = 0x0000;
	}
}



//********************************************
// @brief       信道监听计时器更新，定时检查当前的信道列表，如果发现有超时未有
//              信号更新的信道，将标记为可用
// @param   periodInSeconds --- 以毫秒为单位的时间间隔
// @author zuodahua
//********************************************
void channelStateUpdate(uint32_t periodInMilliseconds)
{
    int idxChan;
    uint16_t time;
    
    chanUpdateTimerInMilliseconds += periodInMilliseconds;
    if(chanUpdateTimerInMilliseconds < CHAN_UPDATE_MILLISECONDS)
    {
        return;
    }
    
    chanUpdateTimerInMilliseconds %= CHAN_UPDATE_MILLISECONDS;
    
    for(idxChan = 0; idxChan < NUM_DIGITAL_CHANNELS; idxChan++)
    {
        time = ChannelStateInfos[idxChan].indicator & 0x0FFF;
        if(time > CHAN_UPDATE_SECONDS)
        {
            time -= CHAN_UPDATE_SECONDS;
        }
        else
        {
            time = 0;
            ChannelStateInfos[idxChan].indicator = 0x0000;
        }
    }
}



//********************************************
// @brief       周期性地扫描频道 
// @param   periodInMilliseoncds --- 以毫秒为单位的时间间隔
// @author zuodahua
//********************************************
void periodScanChannel(uint32_t periodInMilliseoncds)
{
    
    // 如果正在话音业务状态，则直接返回
    if(isChanScanAvailable() == false)
    {
        isChanListening = false;
        chanScanTimerInMilliseconds = 0;
        return;
    }
    
    chanScanTimerInMilliseconds += periodInMilliseoncds;
    
    if(chanScanTimerInMilliseconds < CHAN_LISTEN_MILLISECONDS)
    {
        return;
    }
    
    chanScanTimerInMilliseconds %= CHAN_LISTEN_MILLISECONDS;
    
    if(isChanListening == false)
    {
        // 如果上一次计时循环不是处于监听状态，则本次只设置状态，不监听
        isChanListening = true;
        setVoiceChannelFreqWithChanNumber(currentScanChanNum);
        setCurrentVoiceChanNum(currentScanChanNum);
        return;
    }
    
    // 标记当前的信道可用状态
    if(isChannelBusy(DataChannelIndicator))
    {
		//printf(" line = %u, channel listener , scan channel[%d] is occypied ! ... \n\n", __LINE__, currentScanChanNum);
        ChannelStateInfos[currentScanChanNum++].indicator = MAX_CHANNEL_SILENT_TIME & 0x0FFF;
    }
    else
    {
        ChannelStateInfos[currentScanChanNum++].indicator = 0x0000;
    }
    
    currentScanChanNum %= NUM_DIGITAL_CHANNELS;
    setVoiceChannelFreqWithChanNumber(currentScanChanNum);
    setCurrentVoiceChanNum(currentScanChanNum);
}



//********************************************
// @brief       初始化信道状态信息 ，将所有信道都标记为可访问
// @author zuodahua
//********************************************
void initChannelStateInfos()
{
    uint16_t idxChan;
    
    for(idxChan = 0; idxChan < NUM_DIGITAL_CHANNELS; idxChan++)
    {
        ChannelStateInfos[idxChan].commCode  = 0x0000;
        ChannelStateInfos[idxChan].indicator = 0x0000;
    }    
}





