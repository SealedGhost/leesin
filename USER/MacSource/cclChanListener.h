/************************************************************************/
// Created By ZuoDahua  ---  2017/06/06
// dhzuo@sandemarine.com
// Team: Research and Development Department of Sandemarine
// Brief: CCL信道监听功能模块，包括监听信道的使用状况，以及通信代码信息
// Copyright (C) Nanjing Sandemarine Electric Co., Ltd
// All Rights Reserved
/************************************************************************/
#ifndef CCLCHANLISNTENER_H

#define CCLCHANLISNTENER_H

#include<stdint.h>
#include "cclMsgParser.h"
#include "macParaDefine.h"

#define NUM_AVAILABEL_CHANNEL	7					// 信道用三个比特表示，最大只能有7个可用信道

// 获取的可用频道号列表
extern uint16_t availabelChans[NUM_AVAILABEL_CHANNEL];

// 获取可用信道，并返回当前可用的信道数量
int getAvailableChans();

// 获取一个可用信道，如果没有，则直接返回一个 1 - 220 之间的一个随机信道号
uint16_t getOneAvailableChannel();


// ------------------------------------------
// 通信代码相关
// ------------------------------------------


// 话音业务类型
typedef enum 
{
	IndCallService			= 0,					// 单呼呼叫业务
	GroupCallService		= 1,					// 群呼呼叫业务
	DigitalCallService		= 2						// 数字对频点呼叫业务
} VoiceCallServiceType;


//************************************
// Method:    	getIndCallCommCode
// Access:   	public 
// Parameter: 	uint32_t srcId		--- 源id，id即呼号，有效24位
// Parameter: 	uint32_t dstId		--- 目标id（呼号），有效24位
// Returns:  	uint16_t
// Description: 获取单呼的通信代码
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
uint16_t getIndCallCommCode(uint32_t srcId, uint32_t dstId);


//************************************
// Method:    	getGroupCallCommCode
// Access:   	public 
// Parameter: 	uint32_t groupNum	--- 
// Parameter: 	uint32_t groupPwd
// Returns:  	uint16_t
// Description: 获取群呼的通信代码
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
uint16_t getGroupCallCommCode(uint32_t groupNum, uint32_t groupPwd);

uint16_t getValidCommCode(uint32_t num1, uint32_t num2);


/** 从源信道列表中选出一个本机可用的信道列表，如果本机都不可用，则返回-1，否则返回信道号 */
int findOneAvailableChan(uint16_t * srcChannels, uint16_t srcNumChans);



//************************************
// Method:    	setVoiceChannelFreqWithChanNumber
// Access:   	public 
// Parameter: 	uint16_t chanNumber	--- 指定的信道号，1 - 480
// Returns:  	void
// Description: 设置话音频道的频率，直接通过配置4463进行频率设置
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
void setVoiceChannelFreqWithChanNumber(uint16_t chanNumber);

/** 设置话音频道的频率，直接输入要调制的频率 */
void setVoiceChannelFreq(uint32_t freq);


/** 根据给定的信道号，返回具体的频率 */
uint32_t calDdsTxFreq(uint16_t chanNumber);



#define MAX_CHANNEL_SILENT_TIME			3600			// 信道静默的最大时间，单位为秒

/* 信道占用信息指示 */
typedef struct 
{
	uint16_t commCode;				// 通信代码
	uint16_t indicator;				// 相关信息指示, 第一个比特指代通信代码是否有效，(1 - 有效, 0 - 无效), 后12个比特为静默持续时间，单位为秒
} ChannelOcupyInfo;


/* 处理信道占用消息，查看从DLL处接收到的消息，并将信道相关的消息选出来并进行处理 */
void dealChannelOcupyInfo(CclMsgType cclMsgType, void *msg);


#define CHAN_UPDATE_SECONDS         30                              // 信道刷新的时间周期，单位为秒，即多长时间来刷新一次信道状态
#define CHAN_UPDATE_MILLISECONDS    (CHAN_UPDATE_SECONDS*1000)      // 信道刷新的时间周期，单位为毫秒，即多长时间来刷新一次信道状态
#define CHAN_LISTEN_MILLISECONDS    300                             // 信道监听时长，单位为毫秒，即信道监听模块的调用周期


/** 信道状态更新 */
void channelStateUpdate(uint32_t periodInMilliseconds);


/** 周期性地扫描频道 */
void periodScanChannel(uint32_t periodInMilliseoncds);


/** 初始化信道状态信息 */
void initChannelStateInfos();



#endif


