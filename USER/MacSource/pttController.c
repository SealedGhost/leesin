/************************************************************************/
// Created By ZuoDahua  ---  2017/06/06
// dhzuo@sandemarine.com
// Team: Research and Development Department of Sandemarine
// Brief: MAC中管理PTT的模块
// Copyright (C) Nanjing Sandemarine Electric Co., Ltd
// All Rights Reserved
/************************************************************************/

#include "pttController.h"
#include "cclGroupCallLayer.h"
#include "macParaDefine.h"
#include "macService.h"
#include "adc.h"
#include "gpio.h"

// PTT按讲的计时器，以毫秒为单位进行计时
static uint32_t pttTalkTimer = 0;

// 指示当前是否有人在讲话，
// 当收到语音的起始帧的时候，将该值置为 true
// 当收到语音结束帧或者超时时，将该值设为false
static bool isVoiceTalking = false;


static bool isThisShipTalking = false;


/** 指示当前本机是否正在接收话音消息，从接收到话音起始消息开始，
 // 到接收到话音结束帧结束（或者接收到RxOverAction，即FPGA给出
 // 话音接收断开
*/
static bool isCurrentRxVoiceSignal = false;


//********************************************
// @brief 处理PTT按下操作
// @author zuodahua
//********************************************
bool handlePttPush()
{
//	printf("    handle ptt push\n");
    // 如果当前系统不处于话音业务状态，按下PTT应无任何反应
    if(isSystemVoiceService() == false)
    {
        return false;
    }
    
    if(isAvoidVoicePtt()){
        // 系统采用避让策略，当前有人讲话或者信道忙，则直接退出，不处理
        if(isVoiceTalking || isChannelBusy(DataChannelIndicator)){ 
//			printf("   ptt pushed,    isChannelBusy = %s\n\n", (isChannelBusy(DataChannelIndicator) ? "true" : "false"));
            return false;
        }
    }
    
    // 系统能按下PTT
    pttTalkTimer = 0;
    isVoiceTalking = true;
    // 打开语音使能
    turnOnVoiceSend();
    
    return true;
}



//********************************************
// @brief 处理PTT松开
// @author zuodahua
//********************************************
void handlePttRelease()
{
    if(isThisShipTalking)
    {
        // 如果当前按下PTT有效，才将话音讲话状态清除
        isVoiceTalking = false;
    }
    turnOffVoiceSend();
    // 尝试一次群信息广播
    broadcastGroupInfo();
}


//********************************************
// @brief 更新PTT对单次按讲的计时器，当正在通话为true时，该计时器就 要进行计时
// @author zuodahua
//********************************************
void updatePttTalkTimer(uint32_t periodInMilliseconds)
{
    if(isVoiceTalking)
    {
        pttTalkTimer += periodInMilliseconds;
        
        if(pttTalkTimer >= MaxDurationPerTalkInMilliseconds)
        {
            isVoiceTalking = false;
            isCurrentRxVoiceSignal = false;
            pttTalkTimer = 0;
            // 关闭语音使能
            turnOffVoiceSend();
			// 通知群显示关闭
			clearGroupTalkerInfoForTimeout();
//			printf("   ptt released, for timeout .... \n\n\n");
        }
    }
    else
    {
        pttTalkTimer = 0;
    }
}


//********************************************
// @brief   处理语音通话状态消息，即语音开始和语音讲话结束
// @param   msgType           --- 消息类型
// @param   msg               --- 空类型的消息指针
// @author zuodahua
//********************************************
void dealVoiceTalkMsg(CclMsgType msgType, void *msg){
    
    if(isAvoidVoicePtt() == false)
    {
        // 如果当前不是话音避让模式，则不处理话音开始和结束消息
        return;
    }
    
    switch(msgType)
    {
        case VoiceCallStartMsgType:
            pttDealVoiceStartMsg(msg);
            break;
        case VoiceCallOverMsgType:
            pttDealVoiceOverMsg(msg);
            break;
        default:
            break;
    }
}


//********************************************
// @brief  处理语音数据开始消息信号
// @author zuodahua
//********************************************
void pttDealVoiceStartMsg(VoiceCallStartMsg * msg)
{
    if(msg->commCode == getCurrentCommCode() && msg->srcShipId != getThisCallSign())
    {
        if(isThisShipTalking == false)
		{
			isVoiceTalking = true;
            isCurrentRxVoiceSignal = true;
			pttTalkTimer = 0;
		}
//		printf("    rec voice start msg, commCode = %u\n", msg->commCode);
    }
}


//********************************************
// @brief  处理语音数据结束消息信号
// @author zuodahua
//********************************************
void pttDealVoiceOverMsg(VoiceCallOverMsg * msg)
{
    if(msg->commCode == getCurrentCommCode())
    {
        if(isThisShipTalking == false)
		{
			isVoiceTalking = false;
            isCurrentRxVoiceSignal = false;
			pttTalkTimer = 0;
		}		
//		printf("    rec voice over msg, commCode = %u\n", msg->commCode);
    }
}


//********************************************
// @brief  处理接收话音信号结束消息
// @author zuodahua
//********************************************
void pttDealRxVoiceOverMsg()
{
    if(isThisShipTalking == false)
    {
        isVoiceTalking = false;
        isCurrentRxVoiceSignal = false;
        pttTalkTimer = 0;
    }
}


//********************************************
// @brief  打开语音发射使能，打开对FPGA的PTT使能IO，并通知应用层
// @author zuodahua
//********************************************
void turnOnVoiceSend()
{
//	printf("      turn on voice send  \n");
    isThisShipTalking = true;
    RFSendEnable();
    macPostMsgToApp(TxStartAction, 0, 0);
}



//********************************************
// @brief  关闭语音发射使能，关闭对FPGA的PTT使能IO，并通知应用层
// @author zuodahua
//********************************************
void turnOffVoiceSend()
{
    if(isThisShipTalking)
    {
        macPostMsgToApp(TxOverAction, 0, 0);
        isThisShipTalking = false;
    }
    RFSendDisable();
}



//********************************************
// @brief  清空PTT的状态 
// @author zuodahua
//********************************************
void clearPttState()
{
	isVoiceTalking = false;
	isThisShipTalking = false;
    pttTalkTimer = 0;
}


inline bool isThisShipCurrentTalking()
{
    return isThisShipTalking;
}


/** 指示当前本机是否正在接收话音消息，从接收到话音起始消息开始，
 // 到接收到话音结束帧结束（或者接收到RxOverAction，即FPGA给出
 // 话音接收断开
*/
inline bool isThisRxVoiceSignal()
{
    return isCurrentRxVoiceSignal;
}



