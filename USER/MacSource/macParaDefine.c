#include "macParaDefine.h"
#include "cclChanListener.h"


bool CCL_HANDLE_MSG_TYPE[NUM_CCL_DEAL_MSGS] = { true, true, true, true, true, true,
									false, true, true, true, true, true};


uint32_t thisCallSign = 0;
uint32_t thisShipNum = 0;

uint32_t currentPartnerCallSign = 0;
uint32_t currentPartnerShipNum = 0;
uint16_t currentCommCode = 0;
uint16_t currentVoiceChanNum = 0;

uint32_t potentialPartnerCallSign;
uint32_t potentialPartnerShipNum;
uint16_t potentialCommCode;
uint16_t potentialVoiceChanNum;

uint8_t isSuspend = 0;
uint8_t isOC = 0;
                                    
                                   
static uint32_t thisShipGroupNumber = 0;        // 本船的群号
static uint32_t thisShipGroupPassword = 0;      // 本船对应群的密码                       
static uint16_t thisShipGroupCommCode = 0;      // 本船群对应的通信代码
                               
//********************************************
// @brief   设置本船相关的群号和密码，同时根据群号和密码，生成对应的通信代码
// @param   groupNumber         --- 群号
// @param   groupPwd            --- 群密码
// @author zuodahua
//********************************************                                   
void setThisGroupInfo(uint32_t groupNumber, uint32_t groupPwd)
{
    thisShipGroupNumber = groupNumber;
    thisShipGroupPassword = groupPwd;
    thisShipGroupCommCode = getGroupCallCommCode(thisShipGroupNumber, thisShipGroupPassword);
}


//********************************************
// @brief   获取本船群的通信代码
// @return  本群呼的通信代码
// @author zuodahua
//********************************************
inline uint16_t getThisGroupCommCode()
{
    return thisShipGroupCommCode;
}

inline uint32_t getShipGroupNumber()
{
    return thisShipGroupNumber;
}

inline uint32_t getShipGroupPassword()
{
    return thisShipGroupPassword;
}


//************************************
// Method:    	setThisShipInfo
// Access:   	public 
// Parameter: 	uint32_t callSign		-- 呼号
// Parameter: 	uint32_t shipNum		-- 船号
// Returns:  	void
// Description: 设置本船的相关信息，包括呼号和船号
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
void setThisShipInfo(uint32_t callSign, uint32_t shipNum)
{
	thisCallSign = callSign;
	thisShipNum = shipNum;
}

inline void setThisCallSign(uint32_t callSign){
    thisCallSign = callSign;
}

inline uint32_t getThisCallSign(){
    return thisCallSign;
}

inline uint32_t getThisShipNumber()
{
    return thisShipNum;
}


inline void setCurrentPartnerCallSign(uint32_t callSign){
    currentPartnerCallSign = callSign;
}

inline uint32_t getCurrentPartnerCallSign(){
    return currentPartnerCallSign;
}

inline void setCurrentPartnerShipNumber(uint32_t shipNumber)
{
    currentPartnerShipNum = shipNumber;
}

inline uint32_t getCurrentPartnerShipNumber()
{
    return currentPartnerShipNum;
}


inline void setCurrentCommCode(uint16_t commCode){
    currentCommCode = commCode;
}

inline uint16_t getCurrentCommCode(){
    return currentCommCode;
}

inline void setCurrentVoiceChanNum(uint16_t chanNum){
    currentVoiceChanNum = chanNum;
}

inline uint16_t getCurrentVoiceChanNum(){
    return currentVoiceChanNum;
}
