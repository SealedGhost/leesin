#include "macUtils.h"
#include "macParaDefine.h"
#include<stdint.h>
#include<stdio.h>
#include<stdbool.h>
#include<string.h>


//---------------------------------------------------------------------------
// Define some parameters
//---------------------------------------------------------------------------
static uint64_t DDS_FREQ_WORD = ((uint64_t)1) << 32;



static const uint16_t crc16tab[256] = {
	0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
	0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
	0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
	0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
	0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
	0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
	0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
	0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
	0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
	0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
	0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
	0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
	0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
	0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
	0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
	0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
	0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
	0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
	0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
	0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
	0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
	0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
	0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
	0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
	0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
	0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
	0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
	0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
	0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
	0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
	0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
	0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0
};



void getInfoFromHalfBytes(uint8_t * srcBuffer, int srcOffset, uint8_t * dstBuffer, int dstOffset, int numBytes)
{
	int idx;
	if (numBytes <= 0)
	{
		return;
	}
	srcBuffer += srcOffset;
	dstBuffer += dstOffset;
	for (idx = 0; idx < numBytes; idx++)
	{
		*(dstBuffer++) = (*(srcBuffer) << 4) | (*(srcBuffer + 1) & 0x0F);
		srcBuffer += 2;
	}
}

void setInfoToHalfBytes(uint8_t * srcBuffer, int numBytes, uint8_t * dstBuffer, int dstOffset)
{
	int idx;
	dstBuffer += dstOffset;
	for (idx = 0; idx < numBytes; idx++)
	{
		*(dstBuffer++) = (srcBuffer[idx] >> 4) & 0x0F;
		*(dstBuffer++) = srcBuffer[idx] & 0x0F;
	}
}


void setIntInfoToHalfBytes(uint32_t number, uint8_t * dstBuffer, int dstOffset)
{
	int idx;
	for (idx = 0; idx < 8; idx++)
	{
		dstBuffer[dstOffset + idx] = (number >> (28 - (idx << 2))) & 0x0F;
	}
}

//********************************************
// @brief   将指定的半字节数填入目标数据缓冲区
// @param   number             --- 最大32位的数字
// @param   dstBuffer          --- 目标数据缓冲区
// @param   dstOffset          --- 缓冲区的字节偏移量
// @param   numHalfBytes       --- 要填入的半字节数
// @author zuodahua
//********************************************
void setIntNumberToHalfBytes(uint32_t number, uint8_t * dstBuffer, int dstOffset, int numHalfBytes)
{
    int idx;
    dstBuffer += dstOffset;
    for(idx = 0; idx < numHalfBytes; idx++)
    {
        *(dstBuffer++) = 0x0F & (number >> ((numHalfBytes-1-idx) << 2));
    }
}


void setShortInfoToHalfBytes(uint16_t number, uint8_t * dstBuffer, int dstOffset)
{
	int idx;
	for (idx = 0; idx < 4; idx++)
	{
		dstBuffer[dstOffset + idx] = (number >> (12 - (idx << 2))) & 0x0F;
	}
}


uint16_t crc16Cal(uint8_t * srcBuffer, int offSet, int numBytes)
{
	uint16_t crc = CRC_INITIAL_VALUE;
	int i;

	for (; numBytes > 0; numBytes--)               /* Step through bytes in memory */
	{
		crc = crc ^ (*(srcBuffer++ + offSet) << 8);      /* Fetch byte from memory, XOR into CRC top byte*/
		for (i = 0; i < 8; i++)              /* Prepare to rotate 8 bits */
		{
			if (crc & 0x8000)
			{
				crc = (crc << 1) ^ CRC16_GEN_POLY;
			}
			else {
				crc <<= 1;
			}
		}                              /* Loop for 8 bits */
		crc &= 0xFFFF;
	}

	return crc;
}

uint16_t crc16UseTable(const uint8_t * srcBuffer, int offSet, int numBytes)
{
	uint16_t crc = CRC_INITIAL_VALUE;
	int idxByte;
	uint8_t byte;

	for (idxByte = 0; idxByte < numBytes; idxByte++)
	{
		byte = (crc >> 8) ^ (srcBuffer[idxByte + offSet]);
		crc = CRC_16_TABLE[byte] ^ (crc << 8);
	}
	return (crc ^ CRC_FINAL_XOR_VALUE);
}


void createCrc16Table() {
	FILE *file;
	if (file = fopen("crc16table.txt", "wt"))
	{
//		printf("文件打开成功!\n");
	}
	else
	{
//		printf("文件打开失败!\n");
		return;
	}

	uint16_t remainder, dividend;
	int bit;

	for (dividend = 0; dividend < 256; dividend++)
	{
		remainder = dividend << 8;
		for (bit = 0; bit < 8; bit++)
		{
			if (remainder & 0x8000)
			{
				remainder = (remainder << 1) ^ CRC16_GEN_POLY;
			}
			else
			{
				remainder <<= 1;
			}
		}
		if (dividend != 0 && dividend % 8 == 0)
		{
			fprintf(file, "\n");
		}
		fprintf(file, "0x%2x, ", remainder);
	}

	fclose(file);
}


//************************************
// Method:    	decodeDupCode
// Access:   	public 
// Parameter: 	uint8_t * srcBuffer
// Parameter: 	int numBytes					--- 有效消息字节数
// Parameter: 	uint8_t * dstBuffer
// Returns:  	bool
// Description: 3次重复码的解码，并进行CRC校验，返回最终的校验结果，true --- 正确的帧， fals --- 错误的帧
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
bool decodeDup3Code(uint8_t * srcBuffer, int numBytes, uint8_t * dstBuffer)
{
	int idxByte, idxBit;
	uint8_t byte1, byte2, byte3, byte;
	uint8_t tmp;
	int shiftBits;
	
	uint16_t crc1, crc2;

	for (idxByte = 0; idxByte < numBytes; idxByte++)
	{
		byte1 = *(srcBuffer + idxByte);
		byte2 = *(srcBuffer + idxByte + numBytes);
		byte3 = *(srcBuffer + idxByte + (numBytes << 1));
		for (idxBit = 0; idxBit < 8; idxBit++)
		{
			shiftBits = 7 - idxBit;
			tmp = ((byte1 >> shiftBits) & 0x01) + ((byte2 >> shiftBits) & 0x01) + ((byte3 >> shiftBits) & 0x01);
			if (tmp > 1)
			{
				byte |= (0x80 >> idxBit);
			}
			else
			{
//				printf("%1x\n", (~(0x80 >> idxBit)));
				byte &= (~(0x80 >> idxBit));
			}
//			printf("%1x, ", byte);
		}
		dstBuffer[idxByte] = byte;
	}

	crc1 = crc16UseTable(dstBuffer, 0, numBytes-2);
	crc2 = (dstBuffer[numBytes - 2] << 8) | (dstBuffer[numBytes - 1]);
	
	return (crc1 == crc2);
}

//************************************
// Method:    	duplicate3Times
// Access:   	public 
// Parameter: 	uint8_t * srcBuffer
// Parameter: 	int numBytes
// Parameter: 	uint8_t * dstBuffer
// Parameter: 	int dstOffset
// Returns:  	void
// Description: 
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
void duplicate3Times(uint8_t * srcBuffer, int numBytes, uint8_t * dstBuffer, int dstOffset)
{
//    int idxTimes;
//    for(idxTimes = 0; idxTimes < 3; idxTimes++)
//    {
//        dstBuffer += dstOffset;
//        memcpy(dstBuffer, srcBuffer, numBytes * sizeof(uint8_t));
//    }
    
	int idxByte;        
	for (idxByte = 0; idxByte < numBytes; idxByte++)
	{
		dstBuffer[idxByte + dstOffset] = srcBuffer[idxByte];
		dstBuffer[idxByte + dstOffset + numBytes] = srcBuffer[idxByte];
		dstBuffer[idxByte + dstOffset + (numBytes << 1)] = srcBuffer[idxByte];
	}
}

//************************************
// Method:    	getBitsToIntValue
// Access:   	public 
// Parameter: 	uint8_t * srcBuffer
// Parameter: 	uint32_t bitOffset
// Parameter: 	uint8_t numBitsToGet
// Returns:  	int
// Description: 从指定的数据缓冲区中获取指定比特数的信息，平凑成一个int数
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
uint32_t getBitsToIntValue(uint8_t * srcBuffer, uint32_t bitOffset, uint8_t numBitsToGet)
{
	uint32_t value = 0;
	uint8_t valueShift = 0;
	uint32_t idxByte;
	uint8_t numBitsLeft;
	while (numBitsToGet > 0)
	{
		idxByte = bitOffset >> 3;
		numBitsLeft = 8 - bitOffset % 8;
		if (numBitsLeft >= numBitsToGet)
		{
			value = (value << numBitsToGet) | (((*(srcBuffer + idxByte) << (bitOffset % 8)) & 0x000000FF) >> (8 - numBitsToGet));
			break;
		}
		else
		{
			value = (value << numBitsLeft) | (((*(srcBuffer + idxByte) << (bitOffset % 8)) & 0x000000FF) >> (8 - numBitsLeft));
			bitOffset += numBitsLeft;
			numBitsToGet -= numBitsLeft;
		}
	}
	return value;
}


//************************************
// Method:    	setBitsToBuffer
// Access:   	public 
// Parameter: 	uint8_t * srcBuffer
// Parameter: 	int bitOffset
// Parameter: 	int value
// Parameter: 	int numBits
// Returns:  	void
// Description: 将数据设置进给定的数据缓冲区
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
void setBitsToBuffer(uint8_t * srcBuffer, int bitOffset, uint32_t value, int numBits)
{
	int numLeftBits;
	int idxByte;
	int valueOffset = 0;
	uint8_t byte;
	while (numBits > 0)
	{
		idxByte = bitOffset >> 3;
		numLeftBits = 8 - bitOffset % 8;
		if (numLeftBits >= numBits)
		{
			byte = (value << valueOffset) >> valueOffset;
			byte <<= (numLeftBits - numBits);
			srcBuffer[idxByte] |= byte;
			return;
		}
		else
		{
			byte = (((value << valueOffset) >> valueOffset) >> (numBits - numLeftBits));
			srcBuffer[idxByte] |= byte;
			numBits -= numLeftBits;
			valueOffset += numLeftBits;
			bitOffset += numLeftBits;
		}
	}
}


//************************************
// Method:    	calDdsFreqCtrWord
// Access:   	public 
// Parameter: 	int freq --- 输入频率
// Returns:  	int
// Description: 
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
uint32_t calDdsFreqCtrWord(uint32_t freq)
{
	return freq * DDS_FREQ_WORD / DDS_REF_FREQ;
}


//********************************************
// @brief      计算DDS作为本振时的频率控制字
//            计算公式 --- f = fc - f_ref
// @param
// @author zuodahua
//********************************************
uint32_t calDdsRxFreqCtrWord(uint32_t freq)
{
    return (freq - DDS_RX_FREQ_FREQ) * DDS_FREQ_WORD / DDS_REF_FREQ;
}

