#include<stdint.h>
#include<stdio.h>
#include "dllLayer.h"
#include "macUtils.h"
#include "macParaDefine.h"
#include "macService.h"
#include <stdio.h>
#include<string.h>


// 定义 一些变量
uint8_t recFrameBuffer[NUM_LDPC_MSG_BYTES*2];				// 缓存FPGA接收到的一帧数据信号
uint8_t dupDecodeFrameBuffer[NUM_CCL_MSG_BYTES];			// 缓存信令消息的数据，字节长度为信令消息的长度



// DLL从串口发给FPGA的数据缓冲区
static uint8_t uartMsgBuffer[(NUM_LDPC_MSG_BYTES + NUM_PHY_FREQ_BYTES) * 2 + 2];


// ------------------------------------------------------------------------------------------------------
// 此部分函数都在头文件中声明，对于外部而言，可以直接看到并使用
// ------------------------------------------------------------------------------------------------------

FpgaRxData fpgaRxDataBuffer[NUM_BUFFER_FPGA_REC_FRAMES];
int rxFpgaDataWrIndex = -1;
int rxFpgaDataRdIndex = -1;


static DllTxData toSendMsgBuffer[NUM_TO_SEND_MSG_BUFFER_SIZE];	// 数据发射缓冲区

// CCL下发数据的读写指针
static int toSendMsgBufferWrIndex = -1;
static int toSendMsgBufferRdIndex = -1;


uint8_t phyConfigDataBuffer[14] = { 0 };



// ------------------------------------------------------------------------------------------------------
// 此部分函数不在对应头文件中定义，对于外部而言，将不能直接使用
// ------------------------------------------------------------------------------------------------------

//************************************
// Method:    	initFpgaRxDataBuffer
// FullName:  	initFpgaRxDataBuffer
// Access:   	public 
// Returns:  	void
// Description: 初始化fpgaRxDataBuffer中的数据，将各个数据单元都标记为已处理，表示可接收新的数据
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
static void initFpgaRxDataBuffer() {
	int idx;
	for (idx = 0; idx < NUM_BUFFER_FPGA_REC_FRAMES; idx++) {
		fpgaRxDataBuffer[idx].isDealed = true;
		fpgaRxDataBuffer[idx].channel = CclChannelIndicator;
	}
}


//************************************
// Method:    	initToSendMsgBuffer
// Access:   	public 
// Returns:  	void
// Description: 初始化发射缓冲区的内容，将发射次数置为-1，表示可以放进新的数据
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
static void initToSendMsgBuffer() {
	int idx;
	for (idx = 0; idx < NUM_TO_SEND_MSG_BUFFER_SIZE; idx++) 
	{
        toSendMsgBuffer[idx].isAvailable = true;        
	}
}


//************************************
// Method:    initDllLayer
// Access:    public 
// Returns:   void
// Description: 初始化DLL层
// Author:	  ZuoDahua
// Connect:	  dhzuo@sandemarnie.com
//************************************
void initDllLayer()
{
	initFpgaRxDataBuffer();

	initToSendMsgBuffer();
}


//********************************************
// @brief       处理来自FPGA的接收数据，数据做缓存处理，
//                  DLL层在收到数据后，首先做缓存处理，然后将接收到的消息数据地址传递给CCL层
// @note   物理层发来的接收数据帧长为164字节，帧头1字节，指示信息1字节，以及信息体162字节（半字节数据格式）.
//              接收数据的startIdx应不包含帧头，帧头应在硬件底层进行判断
// @param   recBuffer           --- 接收数据缓冲区地址
// @param   startIdx            --- 缓冲区的起始地址
// @return  int 				--- 如果正确接收到一个帧，则返回当前写入的索引，否则返回-1
// @author zuodahua
//********************************************
int recFpgaData(uint8_t * recBuffer, int startIdx)
{
	int bufferIndex, idx;
	
	bufferIndex = (rxFpgaDataWrIndex + 1) % NUM_BUFFER_FPGA_REC_FRAMES;
	
	// 如果将要写入的空间处于待处理状态，则放弃本次数据
	if (fpgaRxDataBuffer[bufferIndex].isDealed == false)
	{
		return -1;
	}
	
	getInfoFromHalfBytes(recBuffer, startIdx + 1, recFrameBuffer, 0, NUM_LDPC_MSG_BYTES);
	
	// 校验数据
	if (decodeDup3Code(recFrameBuffer, NUM_CCL_MSG_BYTES, dupDecodeFrameBuffer) == false)
	{
		return -1;
	}
	
	fpgaRxDataBuffer[bufferIndex].channel = ((recBuffer[startIdx] & 0x02) == CCL_SIGNAL) ? CclChannelIndicator : DataChannelIndicator;
	
//    memcpy(fpgaRxDataBuffer[bufferIndex].dataBuffer, dupDecodeFrameBuffer, NUM_CCL_MSG_BYTES * sizeof(uint8_t));
	for (idx = 0; idx < NUM_CCL_MSG_BYTES; idx++)
	{
		fpgaRxDataBuffer[bufferIndex].dataBuffer[idx] = dupDecodeFrameBuffer[idx];
	}
	// 更新数据写入指针
	rxFpgaDataWrIndex = bufferIndex;
	
	fpgaRxDataBuffer[bufferIndex].isDealed = true;
	
	return rxFpgaDataWrIndex;
}


//********************************************
// @brief   获取指定索引数据缓冲区的地址
// @param
// @author zuodahua
//********************************************
inline uintptr_t getRxDataBufferAddr(int bufferIndex)
{
    return ((uintptr_t)(&fpgaRxDataBuffer[bufferIndex]));
}


//********************************************
// @brief  缓存从CCL发来的数据消息
// @author zuodahua
//********************************************
void sendNewMsg(uint8_t * txBuffer, int numTxBytes, int maxReTxTimes, int txFreq, bool isAvoid)
{
	int bufferIndex, idx;
	if (numTxBytes <= 0 || maxReTxTimes <= 0)
	{
		return;
	}
	
	bufferIndex = (toSendMsgBufferWrIndex + 1) % NUM_TO_SEND_MSG_BUFFER_SIZE;
	if (toSendMsgBuffer[bufferIndex].isAvailable == false)
	{
		// 如果当前的消息队列都处于等待发送状态，则不丢弃新的发射数据，放弃本次发射
		return;
	}
	
//	printf(" sendNewMsg   buffer   msg data\n");
    
	// 将数据写入缓冲区
//    memcpy(toSendMsgBuffer[bufferIndex].txBuffer, txBuffer, numTxBytes * sizeof(uint8_t));
	for (idx = 0; idx < numTxBytes; idx++)
	{
		toSendMsgBuffer[bufferIndex].txBuffer[idx] = txBuffer[idx];
	}
	toSendMsgBuffer[bufferIndex].freq = txFreq;
	toSendMsgBuffer[bufferIndex].txTimes = maxReTxTimes;
    toSendMsgBuffer[bufferIndex].isAvoid = isAvoid;
    toSendMsgBuffer[bufferIndex].isAvailable = false;
	// 更新数据缓冲区的写地址
	toSendMsgBufferWrIndex = bufferIndex;
}


//********************************************
// @brief   物理层向DLL请求待发送数据
// @param    物理层发来的反馈信息
// @author zuodahua
//********************************************
void phyNotifyDllForTxData(uint8_t info)
{
    if(info == PHY_TX_CCL_MSG_BUFFER_BUSY)
    {
        // 当前FPGA在忙，不进行数据下发，直接退出
        return;
    }
    
    if(info == PHY_TX_CCL_MSG_BUFFER_RDY)
    {   
        // 当前FPGA有空闲，可以发数据
        if(toSendMsgBufferRdIndex == toSendMsgBufferWrIndex)
        {
            // 当前没有数据可用，直接退出
            return;
        }
        
		//printf("    send msg to phy \n");
        toSendMsgBufferRdIndex = (toSendMsgBufferRdIndex + 1) % NUM_TO_SEND_MSG_BUFFER_SIZE;
        
        toSendMsgBuffer[toSendMsgBufferRdIndex].isAvailable = true;
        
        txMsgToPhy(toSendMsgBufferRdIndex);
    }    
}



//************************************
// Method:    	txMsgToPhy
// Access:   	public 
// Parameter:	bufferIndex  ---  待发送数据的缓冲区索引
// Returns:  	void
// Description: DLL将缓存的待发送的消息下发到PHY层
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
void txMsgToPhy(int bufferIndex)
{
	// 频率控制字
	int freqCtrWord;

//	printf("             txMsgToPhy,   bufferIndex = %u, txTimes = %d\n", bufferIndex, toSendMsgBuffer[bufferIndex].txTimes);
	
	uartMsgBuffer[0] = DLL_PHY_FRAME_HEADER;
    
    uartMsgBuffer[1] = toSendMsgBuffer[bufferIndex].isAvoid ? 0x01 : 0x00;
    uartMsgBuffer[1] |= (toSendMsgBuffer[bufferIndex].freq == CCL_TX_FREQ) ? 0x00 : 0x02;
    uartMsgBuffer[1] |= ((toSendMsgBuffer[bufferIndex].txTimes << 2) & 0x7c);
	
	// 计算频率控制字，避免double除法的使用
	freqCtrWord = calDdsFreqCtrWord(toSendMsgBuffer[bufferIndex].freq);
    
	setIntInfoToHalfBytes(freqCtrWord, uartMsgBuffer, 2);
	setInfoToHalfBytes(toSendMsgBuffer[bufferIndex].txBuffer, NUM_LDPC_MSG_BYTES, uartMsgBuffer, 10);
    
    // 向串口发送信令数据
    UART_Send(((LPC_UART_TypeDef *)LPC_UART1), uartMsgBuffer, (NUM_LDPC_MSG_BYTES + NUM_PHY_FREQ_BYTES) * 2 + 2, BLOCKING);
}




