/************************************************************************/
// Created By ZuoDahua  ---  2017/06/02
// dhzuo@sandemarine.com
// Team: Research and Development Department of Sandemarine
// Brief: 本层是CCL层管理群呼的一个分支
//		  群呼模块有两个主要功能，1. 管理来电显示信息，当群里头有人讲话时，MAC将讲话人信息送给APP层；
//								  2. 周期性地在信令上广播群信息。 
// Copyright (C) Nanjing Sandemarine Electric Co., Ltd
// All Rights Reserved
/************************************************************************/

#include "cclGroupCallLayer.h"
#include "cclMsgParser.h"
#include "macParaDefine.h"
#include "macService.h"
#include "pttController.h"
#include <stdio.h>


//------------------------------------------------------------
// 变量定义
//------------------------------------------------------------


// 指示是否使能groupInfoTimer计时器
// 当计时达到计时周期，未按下PTT时，计时器停止计时
bool enGroupInfoTimer = true;

// 群呼信息广播计时器
static uint32_t groupInfoTimer = 0;


static GroupInfoBroadcastMsg toSendGroupInfoBroadcastMsg;


//************************************
// Method:    	updateGroupInfoTimer
// Access:   	public 
// Returns:  	void
// Description: 群信息广播计时器
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
void updateGroupInfoTimer(uint32_t periodInMilliseconds)
{
    if(getMacServiceType() != GroupCall)
    {
        groupInfoTimer = 0;
        return;
    }
    
	if (groupInfoTimer < GROUPCALL_BORADCAST_PERIOD)
	{
		groupInfoTimer += periodInMilliseconds;
	}
}

//************************************
// Method:    	broadcastGroupInfo
// Access:   	public 
// Returns:  	void
// Description: 按下PTT的时候，要尝试进行群消息广播
// Author:	  	ZuoDahua
// Connect:	 	dhzuo@sandemarnie.com
//************************************
void broadcastGroupInfo()
{
    if(getMacServiceType() != GroupCall)
    {
        return;
    }
    // 如果当前正在群组状态下，并且正在接收群话音消息，则直接暂时不广播群消息
    if(isThisRxVoiceSignal())
    {
        return;
    }
	if (groupInfoTimer >= GROUPCALL_BORADCAST_PERIOD)
	{
		// 封装群信息广播帧，并进行信令发送
		toSendGroupInfoBroadcastMsg.commCode = currentCommCode;
		toSendGroupInfoBroadcastMsg.chanNum = getCurrentVoiceChanNum();
		toSendGroupInfoBroadcastMsg.groupNumber = getShipGroupNumber();
		toSendGroupInfoBroadcastMsg.groupPwd = getShipGroupPassword();
		
        toSendGroupInfoBroadcastMsg.txChanNumber = CCL_TX_CHAN_NUM;
        toSendGroupInfoBroadcastMsg.txTimes = GROUP_BROADCAST_INFO_MSG_TX_TIMES;
        toSendGroupInfoBroadcastMsg.isAvoid = false;
        packageToSendMsg(GroupInfoBroadcastMsgType, &toSendGroupInfoBroadcastMsg);
        
		// 计时器清零
        groupInfoTimer = 0;
		
		printf(" broadcastGroupInfo --- broadcast group msg , commCode = %d\n\n", getCurrentCommCode());
	}
}


//********************************************
// @brief  处理群相关消息
// @author zuodahua
//********************************************
void dealGroupCallInfo(CclMsgType msgType, void * msg)
{
    if(getMacServiceType() != GroupCall)
    {
        return;
    }
    
    // 如果当前机器正在讲话，则直接退出
    if(isThisShipCurrentTalking())
    {
        return;
    }
    
	if (msgType == VoiceCallStartMsgType)
	{
		if(((VoiceCallStartMsg *)msg)->commCode == getCurrentCommCode() && (((VoiceCallStartMsg *)msg)->srcShipId != getThisCallSign()))
		{
//            printf("  group talker notify ... \n");
			// 群呼讲话人信息显示
			macPostMsgToApp(GroupCallTalkerStartNotifyAction, ((VoiceCallStartMsg *)msg)->srcShipId, ((VoiceCallStartMsg *)msg)->srcShipNum);
		}
	}
	else if (msgType == VoiceCallOverMsgType)
	{
		if(((VoiceCallOverMsg *)msg)->commCode == getCurrentCommCode())
		{
			// 继续显示群号
			macPostMsgToApp(GroupCallTalkerOverNotifyAction, 0, 0);
		}
	}
}


//********************************************
// @brief  群处理接收到话音结束消息
// @author zuodahua
//********************************************
void groupCallDealRxVoiceOverInfo()
{
    if(getMacServiceType() != GroupCall)
    {
        return;
    }
    
    // 停止显示群号，通知应用层继续显示群号
    macPostMsgToApp(GroupCallTalkerOverNotifyAction, 0, 0);
}



//********************************************
// @brief  因为PTT超时，通知有应用层关闭当前群讲话人信息显示 
// @author zuodahua
//********************************************
void clearGroupTalkerInfoForTimeout()
{
	if(getMacServiceType() == GroupCall)
	{
		macPostMsgToApp(GroupCallTalkerOverNotifyAction, 0, 0);
	}		
}



//********************************************
// @brief  处理群呼广播消息
// @author zuodahua
//********************************************
void dealGroupBroadcastMsg(GroupInfoBroadcastMsg *msg)
{
    if(getMacServiceType() == GroupCall)
    {
        return;
    }
	if(msg->groupNumber == getShipGroupNumber() && msg->groupPwd == getShipGroupPassword())
	{
		groupInfoTimer = 0;
		// 提示群提示灯
		macPostMsgToApp(GroupCallMsgNotifyAction, 0, 0);
	}
}




