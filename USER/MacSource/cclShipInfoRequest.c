/************************************************************************/
// Created By ZuoDahua  ---  2017/06/14
// dhzuo@sandemarine.com
// Team: Research and Development Department of Sandemarine
// Brief: 船号选呼功能模块
// Copyright (C) Nanjing Sandemarine Electric Co., Ltd
// All Rights Reserved
/************************************************************************/
#include "cclShipInfoRequest.h"
#include "macParaDefine.h"
#include "cclMsgParser.h"
#include "macService.h"

//********************************************
// @brief   船舶信息请求任务
// @param   shipNumber  --- 要查找的船号
// @author zuodahua
//********************************************
void requestShipInfo(uint32_t shipNumber)
{
    ShipInfoReqMsg reqMsg;
    reqMsg.dstShipNum = shipNumber;
    reqMsg.isAvoid = false;
    reqMsg.srcId = getThisCallSign();
    reqMsg.txChanNumber = CCL_TX_CHAN_NUM;
    reqMsg.txTimes = SHIP_REQUEST_INFO_MAX_TX_TIMES;
    
    packageToSendMsg(ShipInfoReqMsgType, &reqMsg);
}


//********************************************
// @brief   响应船舶信息请求，本机首先判断请求的船号是否与本机一致，一致则进行响应
// @param   dstShipId            --- 发送请求的船舶呼号
// @param   shipNumber           --- 要请求的船号
// @author zuodahua
//********************************************
void responseShipInfoRequest(uint32_t dstShipId, uint32_t shipNumber)
{
    ShipInfoRespMsg msg;
    if(shipNumber != getThisShipNumber())
    {
        return;
    }
    msg.dstId = dstShipId;
    msg.srcId = getThisCallSign();
    msg.shipNum = shipNumber;
    msg.isAvoid = false;
    msg.txChanNumber = CCL_TX_CHAN_NUM;
    msg.txTimes = SHIP_RESPONSE_INFO_MAX_TX_TIMES;
    
    packageToSendMsg(ShipInfoRespMsgType, &msg);
}


//********************************************
// @brief   处理接收到的船舶信息响应消息
// @param
// @author zuodahua
//********************************************
void dealRxShipInfoResponse(ShipInfoRespMsg *msg)
{
    if(msg->dstId == getThisCallSign())
    {
        macPostMsgToApp(ShipInfoResponseAction, msg->srcId, msg->shipNum);
    }
}





