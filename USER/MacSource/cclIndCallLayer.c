/************************************************************************/
// Created By ZuoDahua  ---  2017/06/17
// dhzuo@sandemarine.com
// Team: Research and Development Department of Sandemarine
// Brief: MAC中管理单呼的模块
// Copyright (C) Nanjing Sandemarine Electric Co., Ltd
// All Rights Reserved
/************************************************************************/

#include "cclIndCallLayer.h"
#include "macParaDefine.h"
#include "cclChanListener.h"
#include "macService.h"
#include <stdio.h>


// 本机单呼的状态
IndCallState indCallState = IndCallIdle;


// 本机单呼的角色，所有机器默认都处于被叫状态
IndCallRole indCallRole = IndividualCallee;

// 目的Id号
static uint32_t toDialDstId = 0;

// 目的船号
static uint32_t toDialShipNumber = 0;

// 标记本机作为被叫是否处于响铃状态
static bool isIndCalleeRing = false;


// 单呼信道建立连接消息帧协商的信道号，摘机协商后将设置为当前通信的话音信道
static uint16_t toUseIndCallChanNum = 0;

// 单呼过程中将要使用的通信代码，摘机协商后将设置为当前通信用的通信代码
static uint16_t toUseIndCallCommCode = 0;

// 单呼过程中将要通信的对方呼号，摘机协商后将设置为当前通信用的对方呼号
static uint32_t toUseIndCallPartnerId = 0;

// 单呼过程中将要通信的对方船号，摘机协商后将设置为当前通信用的对方船号
static uint32_t toUseIndCallPartnerShipNumber = 0;


// 单呼等待单呼应答计时器，单位为毫秒
static uint32_t indCallWaitingForRespTimerInMs = 0;


// 单呼等待摘机计时器，单位为毫秒
static uint32_t indCallWaitingForPiciUpTimerInMs = 0;


// 单呼请求消息发射的次数
static uint32_t indCallRequestMsgTxTimes = 0;

// 单呼请求过程中搜索的可用信道数量
static int numIndCallRequestChannels = 0;



//********************************************
// @brief      单呼处理模块处理拨号过程
// @author zuodahua
//********************************************
void indCallHandleDial(uint32_t dstId)
{    
    if(indCallState != IndCallIdle){
        return;
    }
    
//	printf("   dial number = %u\n", dstId);
	
    numIndCallRequestChannels = getAvailableChans();
    
    if(numIndCallRequestChannels == 0){
//		printf(" indCallHandleDial :   line = %d, caller has no available channel, call fail .... \n\n\n", __LINE__);
        // 没有可用信道，直接向应用层发呼叫失败指令
        macPostMsgToApp(IndCallFailAction, dstId, 0);
		return;
    }
    
    toDialDstId = dstId;
    
    setCurrentCommCode(getIndCallCommCode(getThisCallSign(), toDialDstId));
    setCurrentPartnerCallSign(toDialDstId);
    
    indCallState = IndCallWaitingForCallResponse;
	
	indCallRole = IndividualCaller;
	
    // 将等待单呼应答计时器清零
    indCallWaitingForRespTimerInMs = 0;
    indCallRequestMsgTxTimes = 0;
    
    indCallSendRequestMsg(numIndCallRequestChannels);
	
	//printf("   app dial number ( by zuodahua ) = %d,    caller select channel[%u], channelNum = %u\n", dstId, numIndCallRequestChannels, availabelChans[0]);
}

//********************************************
// @brief      处理来自应用层的接通指令
// @author zuodahua
//********************************************
void indCallHandleConnectAction()
{
    //printf("  handle connection action .... \n\n");
    // 主叫点击接通无效
    if(indCallRole == IndividualCaller)
    {
        return;
    }
    
	// 如果不在响铃状态，则直接退出
	if(isIndCalleeRing == false)
	{
        //printf("  not ring, return ...\n\n");
		return;
	}
	
    //printf(" currentPSign = %u, toUsePSign = %u\n\n", getCurrentPartnerCallSign(), toUseIndCallPartnerId);
    
    if(indCallState == IndCallTalking)
    {
        // 如果被叫处于通话状态，并接收到第三方来电响铃
		// 首先要挂断当前来电
		indCallSendHangUpMsg(getCurrentCommCode(), getCurrentPartnerCallSign(), getCurrentVoiceChanNum(), CCL_TX_CHAN_NUM, IndCallHangUpMsgTxTimes);
		indCallSendHangUpMsg(getCurrentCommCode(), getCurrentPartnerCallSign(), getCurrentVoiceChanNum(), getCurrentVoiceChanNum(), (IndCallHangUpMsgTxTimes << 3));
    }
    
	// 其次修改当前的通话信息
	setCurrentCommCode(toUseIndCallCommCode);
	setCurrentPartnerCallSign(toUseIndCallPartnerId);
	setCurrentPartnerShipNumber(toUseIndCallPartnerShipNumber);
	setCurrentVoiceChanNum(toUseIndCallChanNum);
	// 向外发送接通指令
	indCallSendPickUpMsg(CCL_TX_CHAN_NUM, IndCallPickUpMsgTxTimes);
	indCallSendPickUpMsg(getCurrentVoiceChanNum(), (IndCallPickUpMsgTxTimes << 3));
	// 标记停止响铃
	isIndCalleeRing = false;
	// 配置物理层话音参数
	setVoiceServiceParam(IndCallConnectAction, calDdsTxFreq(getCurrentVoiceChanNum()));
	// 标记正在通话中
	indCallState = IndCallTalking;
}


//********************************************
// @brief      处理来自应用层的挂断指令
// @author zuodahua
//********************************************
void indCallHandleHangupAction()
{
    // 主叫方挂断
    if(indCallRole == IndividualCaller)
    {
        if(indCallState == IndCallWaitingForPickup)
		{
			indCallSendHangUpMsg(getCurrentCommCode(), getCurrentPartnerCallSign(), getCurrentVoiceChanNum(), CCL_TX_CHAN_NUM, (IndCallHangUpMsgTxTimes << 1));
		}
		indCallState = IndCallIdle;
		indCallRole = IndividualCallee;
		// 配置物理层话音参数
        setVoiceServiceParam(IndCallHangUpAction, calDdsTxFreq(getCurrentVoiceChanNum()));
    }
    else   
    {
        // 被叫通话状态下挂断
        if(indCallState == IndCallTalking)
        {
            if(isIndCalleeRing)
            {
//				printf("   current is talking and ring .... \n\n");
                // 如果当前正在响铃 --- 第三方呼入响铃，则直接挂断当前响铃主叫人
                indCallSendHangUpMsg(toUseIndCallCommCode, toUseIndCallPartnerId, toUseIndCallChanNum, CCL_TX_CHAN_NUM, IndCallHangUpMsgTxTimes);
				indCallSendHangUpMsg(toUseIndCallCommCode, toUseIndCallPartnerId, toUseIndCallChanNum, toUseIndCallChanNum, (IndCallHangUpMsgTxTimes << 3));
                isIndCalleeRing = false;
            }
            else
            {
//				printf("    current is talking and not ring ... \n\n");
                // 当前没有响铃，直接挂断当前通话的人
                indCallSendHangUpMsg(getCurrentCommCode(), getCurrentPartnerCallSign(), getCurrentVoiceChanNum(), CCL_TX_CHAN_NUM, IndCallHangUpMsgTxTimes);
				indCallSendHangUpMsg(getCurrentCommCode(), getCurrentPartnerCallSign(), getCurrentVoiceChanNum(), getCurrentVoiceChanNum(), (IndCallHangUpMsgTxTimes << 3));
                indCallState = IndCallIdle;
                setVoiceServiceParam(IndCallHangUpAction, calDdsTxFreq(getCurrentVoiceChanNum()));
            }
        }
        else
        {
            // 非通话状态，只有是在响铃时才有效
            if(isIndCalleeRing)
            {
//				printf("  current is not talking , but now is ring ... \n\n");
                // 直接挂断当前呼入用户
                indCallSendHangUpMsg(toUseIndCallCommCode, toUseIndCallPartnerId, toUseIndCallChanNum, CCL_TX_CHAN_NUM, IndCallHangUpMsgTxTimes);
				indCallSendHangUpMsg(toUseIndCallCommCode, toUseIndCallPartnerId, toUseIndCallChanNum, toUseIndCallChanNum, (IndCallHangUpMsgTxTimes << 3));
                isIndCalleeRing = false;
                indCallState = IndCallIdle;
            }
        }
    }
}


//********************************************
// @brief      单呼向外发送单呼请求消息
// @author zuodahua
//********************************************
void indCallSendRequestMsg(int numChannels)
{
    IndCallConnInfoMsg msg;
    
    uint8_t idxChan = 0;
    
    msg.msgId = IndCallRequestMsgId;
    msg.commCode = getCurrentCommCode();
    msg.srcId = getThisCallSign();
    msg.dstId = toDialDstId;
    msg.srcShipNum = getThisShipNumber();
    msg.numChannel = numChannels;
    
    for(idxChan = 0; idxChan < numChannels; idxChan++){
        msg.chanNum[idxChan] = availabelChans[idxChan];
    }
    
    msg.isAvoid = false;
    msg.txChanNumber = CCL_TX_CHAN_NUM;
    msg.txTimes = IndCallRequestTxTimes;
    packageToSendMsg(IndCallConnInfoMsgType, &msg);
}


//********************************************
// @brief      单呼向外发送单呼摘机消息
// @author zuodahua
//********************************************
void indCallSendPickUpMsg(uint16_t txChannelNumber, uint8_t txTimes)
{
    IndCallSwitchInfoMsg msg;
    
    msg.msgId = IndCallPickUpMsgId;
    msg.commCode = getCurrentCommCode();
    msg.chanNum = getCurrentVoiceChanNum();
    msg.srcId = getThisCallSign();
    msg.dstId = getCurrentPartnerCallSign();
    
    msg.txChanNumber = txChannelNumber;
    msg.txTimes = txTimes;
    msg.isAvoid = false;
    
    packageToSendMsg(IndCallSwitchInfoMsgType, &msg);
}


//********************************************
// @brief      单呼向外发送单呼挂断消息
// @author zuodahua
//********************************************
void indCallSendHangUpMsg(uint16_t commCode, uint32_t dstId, uint16_t chanNumber, uint16_t txChannelNumber, uint8_t txTimes)
{
    IndCallSwitchInfoMsg msg;
    msg.msgId = IndCallHangUpMsgId;
    msg.commCode = commCode;
    msg.chanNum = chanNumber;
    msg.srcId = getThisCallSign();
    msg.dstId = dstId;
    
    msg.txChanNumber = txChannelNumber;
    msg.txTimes = txTimes;
    msg.isAvoid = false;
    
    packageToSendMsg(IndCallSwitchInfoMsgType, &msg);
}



//-------------------------------------------------------------------
// 处理接收到的DLL消息接口方法
//-------------------------------------------------------------------


//********************************************
// @brief     处理从DLL处接收到的消息，消息解析后直接调用该接口进行单呼相关消息处理
// @param   msgId  --- 消息ID
// @param   msg    --- 消息结构体数据
// @author zuodahua
//********************************************
void handleIndCallMsgFromDll(MsgIdSet msgId, void * msg)
{
    switch(msgId)
    {
        case IndCallRequestMsgId:
            handleIndCallRequestMsg(msg);
            break;
        case IndCallResponseMsgId:
            handleIndCallResponseMsg(msg);
            break;
        case IndCallConnectMsgId:
            handleIndCallConnectMsg(msg);
            break;
        case IndCallPickUpMsgId:
            handleIndCallPickUpMsg(msg);
            break;
        case IndCallHangUpMsgId:
            handleIndCallHangupMsg(msg);
            break;
        default:
            break;
    }
    
}


//********************************************
// @brief     处理单呼过程中接收到单呼请求消息
// @param    msg --- 消息数据
// @author zuodahua
//********************************************
void handleIndCallRequestMsg(IndCallConnInfoMsg *msg)
{
    int chanNumber;
    int idxChan;
    IndCallConnInfoMsg responseMsg;
    
	printf("  rec indCall Request, callerId = %u, commCode = %u, numChannels = %u\n\n", msg->srcId, msg->commCode, msg->numChannel);
	
    if(msg->dstId != getThisCallSign())
    {
		printf(" call request dstId not me,   dstId = %u\n\n", msg->dstId);
        return;
    }
    
    // 只有被叫用户才能响应该消息
    if(indCallRole != IndividualCallee)
    {
		printf("  i'm caller, not going to response ind request ... \n\n");		
        return;
    }
    
    if(isIndCalleeRing)
    {
        // 当被叫正在响铃时，不接收新的单呼请求指令
        printf("  i'm callee, and now is ring, not going to response ind request ... \n\n");
        return;
    }
    
    chanNumber = findOneAvailableChan(msg->chanNum, msg->numChannel);
	
    if(chanNumber == -1)
    {
        // 主叫发来的信道，被叫发现都不可用，被叫重新选择自己认为可用的频道
        responseMsg.numChannel = getAvailableChans();
        for(idxChan = 0; idxChan < responseMsg.numChannel; idxChan++)
        {
            responseMsg.chanNum[idxChan] = availabelChans[idxChan];
        }
    }
    else
    {
        // 主叫发来的信道，有一个是可用的
        responseMsg.numChannel = 1;
        responseMsg.chanNum[0] = chanNumber;
//		printf("  cclIndCallLayer, line = %d, handleIndCallRequestMsg, available chanNum = %d\n\n", __LINE__, chanNumber);
    }
    
//    printf(" line = %d, numChannel = %u, chanNum = %u\n\n", __LINE__, responseMsg.numChannel, responseMsg.chanNum[0]);
    
    // 发送单呼应答消息
    responseMsg.msgId = IndCallResponseMsgId;
    responseMsg.commCode = msg->commCode;
    responseMsg.dstId = msg->srcId;
    responseMsg.srcId = getThisCallSign();
    responseMsg.srcShipNum = getThisShipNumber();
    responseMsg.isAvoid = false;
    responseMsg.txTimes = IndCallResponseMsgTxTimes;
    responseMsg.txChanNumber = CCL_TX_CHAN_NUM;
    packageToSendMsg(IndCallConnInfoMsgType, &responseMsg);
}
	

//********************************************
// @brief     处理单呼接收到单呼响应消息
// @param    msg --- 消息数据
// @author zuodahua
//********************************************
void handleIndCallResponseMsg(IndCallConnInfoMsg *msg)
{
    IndCallConnInfoMsg responseMsg;
    
    int chanNumber;
	
	//printf("   rec indcall response ,   srcId = %d, numChannel = %u, commCode = %u\n\n\n", msg->srcId, msg->numChannel, msg->commCode);
	
    if(msg->dstId != getThisCallSign())
    {
		//printf(" reach line :  %d\n\n", __LINE__);
        return;
    }
	
	if(indCallRole != IndividualCaller)
	{
		//printf(" cclIndCallLayer, line = %d, not caller, not going to handle response msg ... \n\n\n", __LINE__);
		return;
	}
    
    // 只有主叫才能响应单呼应答消息，主叫的身份在“拨号”动作开始就被确认，
    // 同时状态变为“indCallWaitingForCallResponse”，即等待单呼应答状态
    if(indCallState != IndCallWaitingForCallResponse)
    {
		//printf(" line = %d, indcallState not waiting for response!...\n\n", __LINE__);
        return;
    }
    
    if(msg->numChannel == 0)
    {
        // 被叫没有可用信道，直接返回信道数量为0，表示本次连接失败
        responseMsg.numChannel = 0;
    }
    else{
        chanNumber = findOneAvailableChan(msg->chanNum, msg->numChannel);
        if(chanNumber == -1)
        {
            // 没有可用信道
            responseMsg.numChannel = 0;
        }
        else
        {
            responseMsg.numChannel = 1;
            responseMsg.chanNum[0] = chanNumber;
        }
    }
    
    //printf(" line = %d, handle response, connect numChannels = %d, chanNum = %d\n\n", __LINE__, responseMsg.numChannel, responseMsg.chanNum[0]);
	
    responseMsg.msgId = IndCallConnectMsgId;
    responseMsg.commCode = msg->commCode;
    responseMsg.srcId = msg->dstId;
    responseMsg.dstId = msg->srcId;
    responseMsg.srcShipNum = getThisShipNumber();
    responseMsg.isAvoid = false;
    responseMsg.txChanNumber = CCL_TX_CHAN_NUM;
	
    responseMsg.txTimes = IndCallConnectMsgTxTimes;
    
//	printf("  line = %u,  txTimes = %u\n", __LINE__, responseMsg.txTimes);
	
    packageToSendMsg(IndCallConnInfoMsgType, &responseMsg);
    
	if(responseMsg.numChannel == 1)
	{
		// 主叫将状态变为等待摘机，并响铃
		indCallState = IndCallWaitingForPickup;
		
		// 将等待摘机计时器清零
		indCallWaitingForPiciUpTimerInMs = 0;
		
		// 通知主叫应用层响铃 
		macPostMsgToApp(IndCallOCRingAction, msg->srcId, msg->srcShipNum);
		// 设置相关通话参数
		setCurrentCommCode(msg->commCode);
		setCurrentVoiceChanNum(responseMsg.chanNum[0]);			
		setCurrentPartnerCallSign(msg->srcId);
		setCurrentPartnerShipNumber(msg->srcShipNum);
//        printf("  config caller fpga, chanNum = %d\n\n", responseMsg.chanNum[0]);
        // 主叫切换话音信道，准备接听被叫信息
		setVoiceServiceParam(IndCallConnectAction, calDdsTxFreq(responseMsg.chanNum[0]));
	}
	else
	{
		macPostMsgToApp(IndCallFailAction, toDialDstId, 0);
		indCallState = IndCallIdle;
		indCallRole = IndividualCallee;
	}
}


//********************************************
// @brief    处理单呼过程中收到的单呼建立消息
// @param    msg --- 消息数据
// @author zuodahua
//********************************************
void handleIndCallConnectMsg(IndCallConnInfoMsg *msg)
{	
//	printf("  line = %u, indcall rec ConnectMsg, msgId = %u, commCode = %u, channelNumbers = %u, numChannel = %u, dstId = %u\n\n", 
//						__LINE__, msg->msgId, msg->commCode, msg->numChannel, msg->chanNum[0],  msg->dstId);
	
    if(msg->dstId != getThisCallSign())
    {
		//printf(" line = %u, dstId = %u\n", __LINE__, msg->dstId);
        return;
    }
    
    // 只有被叫才能处理单呼信道建立消息，主叫不处理此类消息
    if(indCallRole != IndividualCallee)
    {
		//printf("    connect msg must be handled by callee! line = %d \n\n", __LINE__);
        return;
    }
    
    // 如果被叫正在响铃，则不处理新的建立连接请求
    if(isIndCalleeRing)
    {
		//printf("   current is ringing....  line = %d \n\n", __LINE__);
        return;
    }
    
    if(msg->numChannel == 0)
    {
		//printf("  current connect has no available channel ! \n\n\n");
        // 如果双方握手后能用的信道数为0，则表示本次通信连接建立失败
        return;
    }
    
    if(msg->srcId == getCurrentPartnerCallSign() && indCallState == IndCallTalking)
    {
        // 如果单呼状态是通话中，且来电的是与当前正在呼叫的呼号一致，则底层直接切换信道，并发送摘机指令
        indCallSendPickUpMsg(CCL_TX_CHAN_NUM, IndCallPickUpMsgTxTimes);
        indCallSendPickUpMsg(msg->chanNum[0], (IndCallPickUpMsgTxTimes << 3));
        setCurrentVoiceChanNum(msg->chanNum[0]);
        printf(" current commcode = %d,  comming commcode = %d\n\n", currentCommCode, msg->commCode);
        setCurrentCommCode(msg->commCode);
        setVoiceServiceParam(IndCallConnectAction, calDdsTxFreq(msg->chanNum[0]));
        return;
    }
    
    // 双方建立连接成功，则直接进行响铃提示
    toUseIndCallChanNum = msg->chanNum[0];
    toUseIndCallCommCode = msg->commCode;
    toUseIndCallPartnerId = msg->srcId;
    toUseIndCallPartnerShipNumber = msg->srcShipNum;
    
    // 此时通知被叫响铃，并等待摘机
	//printf("   ring the callee , line = %u, chanNum = %u \n\n", __LINE__, toUseIndCallChanNum);
    macPostMsgToApp(IndCallTCRingAction, msg->srcId, msg->srcShipNum);
    isIndCalleeRing = true;
    
    // 将等待摘机计时器清零
    indCallWaitingForPiciUpTimerInMs = 0;
}


//********************************************
// @brief    处理单呼过程中收到的单呼接通消息
// @param    msg --- 消息数据
// @author zuodahua
//********************************************
void handleIndCallPickUpMsg(IndCallSwitchInfoMsg *msg)
{
    if(msg->dstId != getThisCallSign())
    {
        return;
    }
    
    // 摘机指令接收方必须是主叫，被叫不可能响应摘机指令，同时只有主叫在等待摘机状态，才处理此消息
    if((indCallRole == IndividualCaller) && (indCallState == IndCallWaitingForPickup))
    {
		//printf("  rec pickup msg .... \n\n");
		// 通知应用层接通
		macPostMsgToApp(IndCallConnectAction, currentPartnerCallSign, 0);
		
		// 将当前的呼叫角色改为被叫，指示能接收他船呼叫
		indCallRole = IndividualCallee;
        indCallState = IndCallTalking;
    } 
}


//********************************************
// @brief    处理单呼过程中收到的单呼挂断消息
// @param    msg --- 消息数据
// @author zuodahua
//********************************************
void handleIndCallHangupMsg(IndCallSwitchInfoMsg *msg)
{
    //printf("  line = %u, recHangUpMsg, dstId = %u, commCode = %u\n\n", __LINE__, msg->dstId, msg->commCode);
	
    if(msg->dstId != getThisCallSign())
    {
        return;
    }
	
    // 挂断消息处理方可以是主叫，也可以是被叫
    if(indCallRole == IndividualCaller)
    {
        //  主叫在等待摘机过程中被挂断
        if(indCallState == IndCallWaitingForPickup)
        {
            macPostMsgToApp(IndCallTcBusyAction, currentPartnerCallSign, 0);
            setVoiceServiceParam(IndCallHangUpAction, calDdsTxFreq(getCurrentVoiceChanNum()));
            // 将当前的呼叫角色改为被叫，指示能接收他船呼叫
            indCallRole = IndividualCallee;
			indCallState = IndCallIdle;
        }
    }
    else
    {
        //  被叫接收到挂断消息，被叫处于正在通话中
		if(indCallState == IndCallTalking)
		{
			// 被叫处于通话中，且未有第三方来电
			if(isIndCalleeRing == false)
			{
                // 挂断一次当前通话的人
                if(currentPartnerCallSign == msg->srcId)
                {
                    indCallState = IndCallIdle;
                    macPostMsgToApp(IndCallHangUpAction,  currentPartnerCallSign, 0);
                    // 关闭FPGA话音通信功能
                    setVoiceServiceParam(IndCallHangUpAction, calDdsTxFreq(getCurrentVoiceChanNum()));
                }				
			}
            else
            {
                // 通话中有第三方来电响铃
                if(currentPartnerCallSign == msg->srcId)
                {
                    // 挂断者是当前通话的人，清通话状态为IDLE
                    indCallState = IndCallIdle;
                    // 通知应用层挂断，同时继续响铃
                    macPostMsgToApp(IndCallHangUpAction, currentPartnerCallSign, 0);
                }
                else if(toUseIndCallPartnerId == msg->srcId)
                {
                    // 挂断者是当前呼叫的人，则通知应用层，并停止响铃
                    macPostMsgToApp(IndCallHangUpAction, toUseIndCallPartnerId, 0);
                    isIndCalleeRing = false;
                }
            }
		}
        else
        {
            // 被叫处于空闲状态，收到挂断消息，只能在响铃状态才响应该消息，同时挂断者应是当前呼叫的人
            if(isIndCalleeRing && toUseIndCallPartnerId == msg->srcId)
            {
                // 挂断者是当前呼叫的人，则通知应用层，并停止响铃
                macPostMsgToApp(IndCallHangUpAction, toUseIndCallPartnerId, 0);
                isIndCalleeRing = false;
            }
        }
		
//		// 如果有第三方来电，或者在空闲中收到来电响铃，收到挂断后的操作是直接向应用层发挂断指令即可，不需要配置底层参数
//        if(isIndCalleeRing)
//		{
//            if(currentPartnerCallSign == msg->srcId)
//            {
//                // 挂断者是当前通话的人，清通话状态为IDLE
//                indCallState = IndCallIdle;
//                // 通知应用层挂断，同时继续响铃
//                macPostMsgToApp(IndCallHangUpAction, currentPartnerCallSign, 0);
//            }
//            else if(toUseIndCallPartnerId == msg->srcId)
//            {
//                // 挂断者是当前呼叫的人，则通知应用层，并停止响铃
//                macPostMsgToApp(IndCallHangUpAction, toUseIndCallPartnerId, 0);
//                isIndCalleeRing = false;
//            }
//		}
    }
}



//-------------------------------------------------------------------
// 处理定时任务方法
//-------------------------------------------------------------------



//********************************************
// @brief    单呼定时器方法接口，供外部直接调用
// @param    periodInMilliseconds   --- 当前任务循环所消耗的时间，单位为毫秒
// @author zuodahua
//********************************************
void updateIndCallTimer(uint32_t periodInMilliseconds)
{
        
//	printf("  current is individual call state, line = %u ... \n\n\n", __LINE__);
	
    checkIsToReTxIndCallRequestMsg(periodInMilliseconds);
    checkIsToHangUpForTimeOut(periodInMilliseconds);
}


//********************************************
// @brief    查看是否需要重新发出单呼请求
// @author zuodahua
//********************************************
void checkIsToReTxIndCallRequestMsg(uint32_t periodInMilliseconds)
{
    if(indCallState == IndCallWaitingForCallResponse)
    {
        // 当系统处于等待单呼应答状态时
        indCallWaitingForRespTimerInMs += periodInMilliseconds;
        
        if(indCallWaitingForRespTimerInMs >= IndCallRequestSpaceTimeInMilliseconds)
        {
            if(indCallRequestMsgTxTimes < IndCallRequestMaxTxTimes)
            {
                reDial();
                indCallRequestMsgTxTimes++;
            }
            else
            {
				//printf("   line = %u, indcall call fail, timeout .... \n\n", __LINE__);
                // 超出最大呼叫次数，返回应用层失败
                macPostMsgToApp(IndCallFailAction, toDialDstId, 0);
                indCallState = IndCallIdle;
				indCallRole = IndividualCallee;
				indCallRequestMsgTxTimes = 0;
            }
            indCallWaitingForRespTimerInMs = 0;
        }
    }
}


//********************************************
// @brief    查看是否需要超时挂断
// @author zuodahua
//********************************************
void checkIsToHangUpForTimeOut(uint32_t periodInMilliseconds)
{
    if((indCallState == IndCallWaitingForPickup) || isIndCalleeRing)
    {
        // 当主叫处于等待摘机状态，或者被叫处于响铃状态
        indCallWaitingForPiciUpTimerInMs += periodInMilliseconds;
    }
    else
    {
        return;
    }
    
//	printf("   current waiting time = %u s\n", indCallWaitingForPiciUpTimerInMs / 1000);
    if(indCallWaitingForPiciUpTimerInMs >= IndCallWaitForPickUpTimeInMilliseconds)
    {
        // 超时未接发出挂断指令
        if(indCallRole == IndividualCaller)
        {
            // 主叫发出“对方忙”
            macPostMsgToApp(IndCallTcBusyAction, currentPartnerCallSign, 0);
            indCallRole = IndividualCallee;
			indCallState = IndCallIdle;
        }
        else
        {
            // 被叫直接发出“挂断”当前正在呼叫的人即可，因为超时未接，直接就是挂断当前呼叫的人
			macPostMsgToApp(IndCallHangUpAction, toUseIndCallPartnerId, 0);      
			isIndCalleeRing = false;
        }
        
        // 将计时器清零
        indCallWaitingForPiciUpTimerInMs = 0;
    }
}


//********************************************
// @brief    重新发出单呼请求
// @author zuodahua
//********************************************
void reDial()
{    
	//printf("  redial,  dstId = %d\n", getCurrentPartnerCallSign());
	
    if(numIndCallRequestChannels == 0)
    {
        return;
    }
    indCallSendRequestMsg(numIndCallRequestChannels);
}


//********************************************
// @brief    返回当前被叫是否处于响铃状态
// @author zuodahua
//********************************************
bool isCalleeRinging()
{
    return isIndCalleeRing;    
}




