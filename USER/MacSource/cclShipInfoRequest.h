/************************************************************************/
// Created By ZuoDahua  ---  2017/06/14
// dhzuo@sandemarine.com
// Team: Research and Development Department of Sandemarine
// Brief: 船号选呼功能模块
// Copyright (C) Nanjing Sandemarine Electric Co., Ltd
// All Rights Reserved
/************************************************************************/

#ifndef CCLSHIPINFOREQUEST_H
#define CCLSHIPINFOREQUEST_H

#include<stdint.h>
#include "cclMsgParser.h"

#define     SHIP_REQUEST_INFO_MAX_TX_TIMES      2           // 船号选呼请求帧要发的最大次数
#define     SHIP_RESPONSE_INFO_MAX_TX_TIMES     2           // 船号选呼响应帧要发的最大次数



/** 本机发送船舶信息请求 */
void requestShipInfo(uint32_t shipNumber);

/** 响应船舶信息请求 */
void responseShipInfoRequest(uint32_t dstShipId, uint32_t shipNumber);


/** 处理接收到的船舶信息响应消息 */
void dealRxShipInfoResponse(ShipInfoRespMsg *msg);


#endif


