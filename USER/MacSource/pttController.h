/************************************************************************/
// Created By ZuoDahua  ---  2017/06/06
// dhzuo@sandemarine.com
// Team: Research and Development Department of Sandemarine
// Brief: MAC中管理PTT的模块
// Copyright (C) Nanjing Sandemarine Electric Co., Ltd
// All Rights Reserved
/************************************************************************/

#ifndef PTTCONTROLLER_H
#define PTTCONTROLLER_H

#include<stdint.h>
#include<stdbool.h>
#include "cclMsgParser.h"

// PTT按讲的计时器，以毫秒为单位进行计时
#define MaxDurationPerTalkInMilliseconds					50000

typedef enum 
{
	PttPush		= 0,				// PTT按下
	PttRelease	= 1					// PTT释放
} PttOperateType;


/** 处理PTT按下操作 */
bool handlePttPush();

/** 处理PTT松开 */
void handlePttRelease();



// 更新PTT单次按讲计时器，以秒为单位
void updatePttTalkTimer(uint32_t periodInMilliseconds);

void dealVoiceTalkMsg(CclMsgType msgType, void *msg);


void pttDealVoiceStartMsg(VoiceCallStartMsg * msg);
void pttDealVoiceOverMsg(VoiceCallOverMsg * msg);

void pttDealRxVoiceOverMsg();


/** 打开语音发射使能 */
void turnOnVoiceSend();

/** 关闭语音发射使能 */
void turnOffVoiceSend();


/** 清空PTT的状态 */
void clearPttState();


/** 指示本机当前是否处于讲话发射状态 */
bool isThisShipCurrentTalking();


/** 指示当前本机是否正在接收话音消息，从接收到话音起始消息开始，
 // 到接收到话音结束帧结束（或者接收到RxOverAction，即FPGA给出
 // 话音接收断开
*/
bool isThisRxVoiceSignal();




#endif // !PTTCONTROLLER_H


