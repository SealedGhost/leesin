
/************************************************************************/
// Created By ZuoDahua  ---  2017/06/02
// dhzuo@sandemarine.com
// Team: Research and Development Department of Sandemarine
// Brief: 本层是CCL层解析消息的一个分支，从DLL层获取数据流，并解析成具体的消息
// Copyright (C) Nanjing Sandemarine Electric Co., Ltd
// All Rights Reserved
/************************************************************************/

#ifndef CCLMSGPARSER_H
#define CCLMSGPARSER_H

#include<stdint.h>
#include<stdbool.h>

//-------------------------------------------------------------------
// 消息结构体
//-------------------------------------------------------------------

typedef enum
{
	IndCallRequestMsgId      = 1,			// 单呼请求消息ID
	IndCallResponseMsgId     = 2,			// 单呼应答消息ID
	IndCallConnectMsgId      = 3,			// 单呼建立连接确认消息ID
	IndCallPickUpMsgId       = 4,			// 单呼接通消息ID
	IndCallHangUpMsgId       = 5,			// 单呼挂断消息ID
	IndCallSuspendMsgId      = 6,		    // 单呼挂起消息ID
	TalkVoiceMsgId           = 7,			// 数字语音数据帧消息ID
	VoiceStartMsgId          = 8,			// 数字语音开始消息ID
	VoiceOverMsgId           = 9,			// 数字语音结束消息ID
	ShipInfoRequestMsgId     = 10,			// 船舶信息请求消息ID
	ShipInfoResponseMsgId    = 11,			// 船舶信息应答消息ID
	GroupInfoBroadcastMsgId  = 12		    // 群呼信息广播消息ID
} MsgIdSet;

/* CCL各种消息种类集合 */
typedef enum
{
	IndCallConnInfoMsgType = 0,			// IndcallconnInfo结构体消息
	IndCallSwitchInfoMsgType,			// IndCallSwitchInfoMsg结构体消息
	VoiceCallStartMsgType,				// VoiceCallStartMsg结构体消息
	VoiceCallOverMsgType,				// VoiceCallOverMsg结构体消息
	ShipInfoReqMsgType,					// ShipInfoReqMsg结构体消息
	ShipInfoRespMsgType,				// ShipInfoRespMsg结构体消息
	GroupInfoBroadcastMsgType,			// GroupInfoBroadcastMsg结构体消息
	NullMsg
} CclMsgType;


/* 单呼信道建立连接信息结构体 */
typedef struct
{
	uint8_t msgId;					// 1 : 单呼请求， 2 ： 单呼应答， 3 ： 单呼建立连接确认
	uint16_t commCode;				// 通信代码
	uint32_t srcId;					// 源ID呼号
	uint32_t dstId;					// 目标ID呼号
	uint32_t srcShipNum;			// 源船号
	uint8_t numChannel;				// 建议的信道个数
	uint16_t chanNum[8];			// 信道号数组
	uint16_t txChanNumber;			// 发射信道号
	uint8_t	txTimes;				// 发射次数
	bool	isAvoid;				// 是否避让，如果是避让策略，则物理层将尝试发射txTimes，并只发成功一次； 如果是非避让策略，则物理层将直接发送txTimes次
} IndCallConnInfoMsg;

/* 单呼接通/挂断/挂起帧结构 */
typedef struct
{
	uint8_t msgId;					// 4 : 接通信令， 5 ： 挂断信令， 6 ： 挂起信令
	uint16_t commCode;				// 通信代码
	uint16_t chanNum;				// 占用信道号
	uint32_t srcId;					// 源ID
	uint32_t dstId;					// 目标ID
	uint16_t txChanNumber;			// 发射信道号
	uint8_t	txTimes;				// 发射次数
	bool	isAvoid;				// 是否避让，如果是避让策略，则物理层将尝试发射txTimes，并只发成功一次； 如果是非避让策略，则物理层将直接发送txTimes次
}IndCallSwitchInfoMsg;

/* 船舶信息请求消息结构体，消息ID固定为16 */
typedef struct
{
	uint32_t srcId;					// 源ID号
	uint32_t dstShipNum;			// 目标船号
	uint16_t txChanNumber;			// 发射信道号
	uint8_t	txTimes;				// 发射次数
	bool	isAvoid;				// 是否避让，如果是避让策略，则物理层将尝试发射txTimes，并只发成功一次； 如果是非避让策略，则物理层将直接发送txTimes次
} ShipInfoReqMsg;

/* 船舶信息应答帧消息结构体，消息ID固定为17 */
typedef struct
{
	uint32_t srcId;					// 源ID号
	uint32_t dstId;					// 目的ID号
	uint32_t shipNum;				// 船号
	uint16_t txChanNumber;			// 发射信道号
	uint8_t	txTimes;				// 发射次数
	bool	isAvoid;				// 是否避让，如果是避让策略，则物理层将尝试发射txTimes，并只发成功一次； 如果是非避让策略，则物理层将直接发送txTimes次
} ShipInfoRespMsg;


/* 群呼信息广播帧消息结构体 */
typedef struct
{
	uint16_t commCode;
	uint16_t chanNum;				// 占用的信道号
	uint16_t groupNumber;			// 群号, 14位二进制
	
	uint16_t txChanNumber;			// 发射信道号
	uint32_t groupPwd;				// 群密码，18位二进制
	uint8_t	 txTimes;				// 发射次数
	bool	 isAvoid;				// 是否避让，如果是避让策略，则物理层将尝试发射txTimes，并只发成功一次； 如果是非避让策略，则物理层将直接发送txTimes次
} GroupInfoBroadcastMsg;


/* 语音通信的起始消息结构体 */
typedef struct
{
	uint16_t commCode;
	uint32_t srcShipId;				// 源呼号ID
	uint32_t srcShipNum;			// 源船号
    uint16_t chanNumber;            // 占用的信道号
	uint16_t txChanNumber;			// 发射信道号
	uint8_t	txTimes;				// 发射次数
	bool	isAvoid;				// 是否避让，如果是避让策略，则物理层将尝试发射txTimes，并只发成功一次； 如果是非避让策略，则物理层将直接发送txTimes次
} VoiceCallStartMsg;

/* 语音通信的结束消息结构体 */
typedef struct
{
	uint16_t commCode;
    uint16_t chanNumber;            // 占用的信道号
	uint16_t txChanNumber;			// 发射信道号
	uint8_t	txTimes;				// 发射次数
	bool	isAvoid;				// 是否避让，如果是避让策略，则物理层将尝试发射txTimes，并只发成功一次； 如果是非避让策略，则物理层将直接发送txTimes次
} VoiceCallOverMsg;



// 通信状态，空闲、单呼、群呼、数字对频点呼叫以及船号选呼
typedef enum
{
	COMM_IDLE,						// 通信空闲状态
	INDIVIDUAL_CALL,				// 单呼状态
	GROUP_CALL,						// 群呼状态
	DIGITAL_CALL,					// 数字对频点呼叫
	QUERY_SHIP_INFO					// 查询船舶信息
} CommState;


// 通信状态和通信类型指示，单呼、群呼、对频点呼叫业务需要用到
typedef struct
{
	uint16_t commCode;				// 通信代码
	CommState commState;
} CommInfo;


#define NUM_CCL_MSG_BYTES		24	// 信令消息的字节数
#define CCL_MSG_SLOT_NUM		26	// CCL信令消息的时隙号


//-------------------------------------------------------------------
// 相关处理函数和缓存变量
//-------------------------------------------------------------------

extern uint8_t toParseMsgBuffer[NUM_CCL_MSG_BYTES];

//---------------------------------------------
// 消息缓存
//---------------------------------------------

#define NUM_CCL_MSG_BUFFER		10			// 推送给CCL消息的缓存区大小




// 接收消息缓存变量定义
extern IndCallConnInfoMsg indCallConnInfoMsg;
extern IndCallSwitchInfoMsg indCallSwitchInfoMsg;
extern VoiceCallStartMsg voiceCallStartMsg;
extern VoiceCallOverMsg voiceCallOverMsg;
extern ShipInfoReqMsg shipInfoReqMsg;
extern ShipInfoRespMsg shipInfoRespMsg;
extern GroupInfoBroadcastMsg groupInfoBroadcastMsg;


uint8_t parseRxMsg(uint8_t * toParseMsgBuffer);

void getParsedMsg(CclMsgType msgType, void *msg);

void parseIndCallConnInfoMsg(uint8_t msgId, uint8_t * toParseMsgBuffer);
void parseIndCallSwitchInfoMsg(uint8_t msgId, uint8_t * toParseMsgBuffer);
void parseVoiceCallStartMsg(uint8_t * toParseMsgBuffer);
void parseVoiceCallOverMsg(uint8_t * toParseMsgBuffer);
void parseShipInfoReqMsg(uint8_t * toParseMsgBuffer);
void parseShipInfoRespMsg(uint8_t * toParseMsgBuffer);
void parseGroupInfoBroadcastMsg(uint8_t * toParseMsgBuffer);



//---------------------------------------------------------------------
// 消息发射相关封装函数
//---------------------------------------------------------------------

// 缓存将要发送到DLL的CCL消息字节流数据
extern uint8_t toSendCCLMsgBuffer[NUM_CCL_MSG_BYTES];


#define CCL_TO_DLL_MSG_BUFFER_SIZE  10                  // CCL推送消息给DLL的消息队列大小


void clearBufferBeforePackage();

void packageToSendMsg(CclMsgType msgType, void * msg);

void packageIndCallConnInfoMsg(IndCallConnInfoMsg * msg);
void packageIndCallSwitchInfoMsg(IndCallSwitchInfoMsg * msg);
void packageVoiceCallStartMsg(VoiceCallStartMsg * msg);
void packageVoiceCallOverMsg(VoiceCallOverMsg * msg);
void packageShipInfoReqMsg(ShipInfoReqMsg * msg);
void packageShipInfoRespMsg(ShipInfoRespMsg * msg);
void packageGroupInfoBroadcastMsg(GroupInfoBroadcastMsg * msg);



void getPackagedMsg(uint8_t *dstBuffer);



#endif

