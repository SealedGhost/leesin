/************************************************************************/
// Created By ZuoDahua  ---  2017/06/02
// dhzuo@sandemarine.com
// Team: Research and Development Department of Sandemarine
// Brief: 本层是CCL层管理群呼的一个分支
//		  群呼模块有两个主要功能，1. 管理来电显示信息，当群里头有人讲话时，MAC将讲话人信息送给APP层；
//								  2. 周期性地在信令上广播群信息。 
// Copyright (C) Nanjing Sandemarine Electric Co., Ltd
// All Rights Reserved
/************************************************************************/

#ifndef CCLGROUPCALLLAYER_H
#define CCLGROUPCALLLAYER_H

#include<stdint.h>
#include "cclMsgParser.h"


#define GROUPCALL_BORADCAST_PERIOD		180000			// 群信息广播时间周期，以毫秒为单位


#define GROUP_BROADCAST_INFO_MSG_TX_TIMES   2           // 群信息广播消息重发的最大次数


// 指示是否使能groupInfoTimer计时器
// 当计时达到计时周期，未按下PTT时，计时器停止计时
extern bool enGroupInfoTimer;



/* 更新群呼信息广播计时器 */
void updateGroupInfoTimer(uint32_t periodInMilliseconds);

/* 广播群信息 */
void broadcastGroupInfo();


/* 处理接收到的群相关消息 */
void dealGroupCallInfo(CclMsgType msgType, void * msg);

void groupCallDealRxVoiceOverInfo();


/** 因为PTT超时，通知有应用层关闭当前群讲话人信息显示 */
void clearGroupTalkerInfoForTimeout();

/** 处理群呼广播消息 */
void dealGroupBroadcastMsg(GroupInfoBroadcastMsg *msg);




#endif







