#include "radio_config_Si4060_150M.h"
#include "si446x_defs.h"

#include "utils.h"
#include "spi.h"
#include <stddef.h>
#include <stdio.h>
#include "gpio.h"

uint8_t config_table[]  = RADIO_CONFIGURATION_DATA_ARRAY;

static uint8_t flag = 0;



void waitSi4463CTS()
{
	uint8_t txBuf = 0;
	uint8_t rxBuf = 0;
	uint32_t timeout  = 100000;
	
	do{
		SPI_CS_Force(0);
		
		txBuf = READ_CMD_BUFF;
		spiDataBuf.tx_data = &txBuf;
		spiDataBuf.rx_data = &rxBuf;
		spiDataBuf.length = 1;
		
		SPI_ReadWrite(LPC_SPI, &spiDataBuf, SPI_TRANSFER_POLLING);
		txBuf = 0xff;
		SPI_ReadWrite(LPC_SPI, &spiDataBuf, SPI_TRANSFER_POLLING);

		SPI_CS_Force(1);
		
		timeout--;
		if(timeout == 0){
			printf("Wait CTS timeout. Program will stop\n");
			while(1);
		}
	}while(rxBuf != 0xff);
}


void sendCommandToSi4463(uint8_t* cmd, size_t cmdSize)
{
	
	uint8_t txBuf = 0;
	uint8_t rxBuf = 0;
	size_t size = cmdSize;
	waitSi4463CTS();
	
	SPI_CS_Force(0);
	
	spiDataBuf.tx_data = &txBuf;
	spiDataBuf.rx_data = &rxBuf;
	spiDataBuf.length = 1;
	
	while(cmdSize){
		txBuf = cmd[size-cmdSize];
		SPI_ReadWrite(LPC_SPI, &spiDataBuf, SPI_TRANSFER_POLLING);
		cmdSize--;
	}
	SPI_CS_Force(1);
}



void readSi4463Response(uint8_t* buf, size_t size)
{
	uint8_t txBuf = 0;
	uint8_t rxBuf = 0;
	waitSi4463CTS();
	
	SPI_CS_Force(0);
	
	spiDataBuf.tx_data = &txBuf;
	spiDataBuf.rx_data = buf;
	spiDataBuf.length = 1;
	
	txBuf = READ_CMD_BUFF;
	SPI_ReadWrite(LPC_SPI, &spiDataBuf, SPI_TRANSFER_POLLING);
	
	txBuf = 0xff;
	while( size-- ){
		SPI_ReadWrite(LPC_SPI, &spiDataBuf, SPI_TRANSFER_POLLING);
		spiDataBuf.rx_data = buf++;
	}
	
	SPI_CS_Force(1);
}



void setSi4463Property(SI446X_PROPERTY group, uint8_t proirity)
{
	uint8_t cmd[5];
	
	cmd[0]  = SET_PROPERTY;
	cmd[1]  = group >> 8;
	cmd[2]  = 1;
	cmd[3]  = group;
	cmd[4]  = proirity;
	
	sendCommandToSi4463(cmd, 5);
}



void configSi4463GPIO(uint8_t g0, uint8_t g1, uint8_t g2, uint8_t g3, uint8_t irq, uint8_t sdo, uint8_t config)
{
	uint8_t cmd[10];
	
	cmd[0]  = GPIO_PIN_CFG;
	cmd[1]  = g0;
	cmd[2]  = g1;
	cmd[3]  = g2;
	cmd[4]  = g3;
	cmd[5]  = irq;
	cmd[6]  = sdo;
	cmd[7]  = config;
	sendCommandToSi4463(cmd, 8);
	readSi4463Response(cmd, 8);
}





void configSi4463()
{ 
	uint8_t i  = 0;
	uint16_t j  = 0;
	

    while( ( i = config_table[j] ) != 0 )
    {
        j += 1;				
        sendCommandToSi4463( config_table + j, i );		
        j += i;
    } 	
#if PACKET_LENGTH > 0           //fixed packet length
    SI446X_SET_PROPERTY_1( PKT_FIELD_1_LENGTH_7_0, PACKET_LENGTH );
	SI446X_SET_PROPERTY_1( PKT_FIELD_1_CRC_CONFIG, 0xA2 );
	SI446X_SET_PROPERTY_1( PKT_CRC_CONFIG, 0x05 );
		
#else                           //variable packet length
    setSi4463Property( PKT_CONFIG1, 0x00 );	
    setSi4463Property( PKT_CRC_CONFIG, 0x00 );
    setSi4463Property( PKT_LEN_FIELD_SOURCE, 0x01 );
    setSi4463Property( PKT_LEN, 0x2A );
    setSi4463Property( PKT_LEN_ADJUST, 0x00 );
    setSi4463Property( PKT_FIELD_1_LENGTH_12_8, 0x00 );
    setSi4463Property( PKT_FIELD_1_LENGTH_7_0, 0x01 );
    setSi4463Property( PKT_FIELD_1_CONFIG, 0x00 );
    setSi4463Property( PKT_FIELD_1_CRC_CONFIG, 0x00 );
    setSi4463Property( PKT_FIELD_2_LENGTH_12_8, 0x00 );
    setSi4463Property( PKT_FIELD_2_LENGTH_7_0, 0x10 );
    setSi4463Property( PKT_FIELD_2_CONFIG, 0x00 );
    setSi4463Property( PKT_FIELD_2_CRC_CONFIG, 0x00 );
#endif //PACKET_LENGTH

    //重要： 4463的GDO2，GDO3控制射频开关，  0X20 ,0X21 
    //发射时必须： GDO2=1，GDO3=0
    //接收时必须： GDO2=0，GDO3=1
    configSi4463GPIO( 0, 0, 0x20, 0x21, 0, 0, 0 );//重要 		
}




void changeSi4463FrequencyAtHz(uint32_t fre)
{
	uint8_t chage_stats[] = {0x34, 0x03};
	uint8_t start_tx[]  = {0x31,0x00,0x50,0x00,0x00};
	uint8_t tmp[12] = {0x11, 0x40, 0x08, 0x00, 0x3B, 0x08, 0x00, 0x00, 0xCC, 0xCD, 0x20, 0xFA};
	uint32_t fracPart;

	uint32_t intPart  = fre * 2 / (5*MILLION);	
	uint32_t remainder  = fre - intPart * (5*MILLION) /2;
	uint32_t commonDivisor  = getGreatestCommonDivisor(5 * MILLION, remainder << 1);
	remainder  = (remainder << 1) /commonDivisor;
	fracPart   = 5 * MILLION / commonDivisor;
	
	fracPart  = ((remainder << 19)  + fracPart/2)/ fracPart;	
	fracPart |= 0x80000;
	
	/* 寄存器值 */
	tmp[4] = intPart-1;
	tmp[5] = (fracPart >> 16)  &  0xff;
	tmp[6] = (fracPart >> 8) & 0xff;
	tmp[7] = fracPart & 0xff;
	
//	tmp[4] = 0x3C;
//	tmp[5] = 0x09;
//	tmp[6] = 0xFE;
//	tmp[7] = 0xBA;	
	sendCommandToSi4463(chage_stats, 2);

	sendCommandToSi4463(tmp,12);
	
	sendCommandToSi4463(start_tx, 5);
}


/**
 *            ______________
 *           |              |
 * ????????? |              |_____________
 */
void resetSi4463()
{
	int i  = 0;

//	Si4463ResetDisable();
//	for(i=0 ; i<100000; i++)
//	{

//	}
  
	Si4463ResetEnable();

	for(i=0 ; i<100000; i++)
	{

	}

	Si4463ResetDisable();
	for(i=0 ; i<100000; i++)
	{

	}
}
