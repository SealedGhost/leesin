#ifndef _SI4463_H
#define _SI4463_H
#include <stdint.h>
#include <stdio.h>

#include "gpio.h"


#define SI446X_CS_ON                             GPIO_ResetBits(GPIOB, GPIO_Pin_12)
#define SI446X_CS_OFF                            GPIO_SetBits(GPIOB, GPIO_Pin_12)

#define SI446X_SDN_LOW                            GPIO_ResetBits(GPIOA, GPIO_Pin_2)
#define SI446X_SDN_HIGH                           GPIO_SetBits(GPIOA, GPIO_Pin_2)

#define SI4463_IRQ_STATE                          GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_6)

void initSi4463(void);

void configSi4463(void);
void changeSi4463FrequencyAtHz(uint32_t fre);

void resetSi4463(void);


void sendCommandToSi4463(uint8_t* cmd, size_t cmdSize);
void configSi4463GPIO(uint8_t g0, uint8_t g1, uint8_t g2, uint8_t g3, uint8_t irq, uint8_t sdo, uint8_t config);
void waitSi4463CTS(void);
#endif
