/**
 * @file       main.c
 * @brief      入口函数所在文件
 * @details    
 * @author     liyi
 * @date       2017年5月25日11:02:45
 * @version    V001
 * @par Copyright (c): sandemarine
 * @par History:        
 */

#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#include  "app.h"
#include  <stdio.h>
#include  <string.h>
#include  <ctype.h>
#include  <stdlib.h>
#include  <stdarg.h>
#include  <math.h>

#include  <uCOS-II\Source\ucos_ii.h>
#include  <uCOS-II\Ports\ARM-Cortex-M3\RealView\os_cpu.h>
#include  <uC-CPU\ARM-Cortex-M3\RealView\cpu.h>

#if (OS_VIEW_MODULE == DEF_ENABLED)
#include    <uCOS-VIEW\Ports\ARM-Cortex-M3\Realview\os_viewc.h>
#include    <uCOS-VIEW\Source\os_view.h>
#endif
#include  "LPC17xx.h"
#include  "lpc17xx_uart.h"
#include "debug_frmwrk.h"

#include "bsp.h"
#include "timer.h"
#include "flash.h"
#include "gpio.h"

#include "usart.h"
/** 
 * @brief  初始化 创建UCOS任务
 * @see    App_TaskStart
 * @note   main
 */
 


int main (void)
{ 
  SCB->VTOR = 0x4000;
	IntDisAll();  
	bspInit();
  
	App_TaskStart();

	return (0);
}



/** 
 * @brief fput 重定向printf()
 */
PUTCHAR_PROTOTYPE
{
	/* wait for current transmission complete - THR must be empty */
//	while (UART_CheckBusy(DEBUG_UART_PORT) == SET);
//	
//	UARTPutChar(DEBUG_UART_PORT, ch);
//	
//	return ch;
}

void HardFault_Handler()
{
	//printf("HardFault\n");
	LedYellowOn();
	LedGreenOn();
	LedRedOn();
	while(1);
}


