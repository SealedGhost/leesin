#include "gpio.h"
#include "GUI_message.h"
#include "ipc.h"
#include "audio_mgr.h"
#include "nvic.h"
/*************************************************************
 * @file       gpio.c
 * @brief      所有设备上使用的IO功能的初始化，包含输入输出接口的封装。
 *  P2.7  ：PP输出
 *	P2.10 : 上拉输入
 *	P2.11 ：PP输出
 *	P2.12 ：PP输出
 *  P2.13 ：PP输出
 *	P2.4  ：高阻输入（外部强上拉） PTT
 *  P0.27 ：PP输出
 *  P4.28 ：PP输出
 *  P0.4  : PP输出
 *  P0.5  : PP输出
 * @version    Verison	0.0
 * @author     RH zhang
 * @data       2017/06/17
 *************************************************************/
/*
 *Falling Edge 1
 *Rising Edge 0
*/


void EINT3_IRQHandler(void)
{
//	NVIC_ClearPendingIRQ(EINT3_IRQn);

	if(GPIO_GetIntStatus(PINSEL_PORT_2,PINSEL_PIN_10,1))//消息接收完毕
	{

		 GPIO_IntCmd(PINSEL_PORT_2,1 << PINSEL_PIN_10,0); //重设为 Rising Edge触发

		
		 GUI_PostMessage(IPC_MSG_IO, IO_MSG_RF_RX, 0, 0);

		
		 GPIO_ClearInt(PINSEL_PORT_2,1 << PINSEL_PIN_10);
	}
	else if(GPIO_GetIntStatus(PINSEL_PORT_2,PINSEL_PIN_10,0))//接收到消息  
	{
			
			GPIO_IntCmd(PINSEL_PORT_2,1 << PINSEL_PIN_10,1); //重设为 Falling Edge触发


		
		 GUI_PostMessage(IPC_MSG_IO, IO_MSG_RF_RX, 1, 0);
		
		
			GPIO_ClearInt(PINSEL_PORT_2,1 << PINSEL_PIN_10);
	}
}

static void GPIOExitIntetruptInit(void)
{
	/*Enable GPIO interrupt*/
	//GPIO_IntCmd(PINSEL_PORT_2,(1 << PINSEL_PIN_10) | (1 << PINSEL_PIN_4),1);
	GPIO_IntCmd(PINSEL_PORT_2,(1 << PINSEL_PIN_10),0);		//上升沿触发
	/* enable irq in nvic*/
	GPIO_ClearInt(PINSEL_PORT_2,1 << PINSEL_PIN_10);
    NVIC_SetPriority(EINT3_IRQn,NVIC_EINT3);
	NVIC_EnableIRQ(EINT3_IRQn);
}

static void GPIOInputPinsInit(void)
{
	PINSEL_CFG_Type PinCfg;
	PinCfg.Funcnum = PINSEL_FUNC_0;
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Pinmode   = PINSEL_PINMODE_PULLUP;
	PinCfg.Portnum   = PINSEL_PORT_2;
	PinCfg.Pinnum    = PINSEL_PIN_10;
	PINSEL_ConfigPin(&PinCfg);
	
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
//	PinCfg.Pinmode   = PINSEL_PINMODE_TRISTATE;//外部强上拉。	
	PinCfg.Pinmode   = PINSEL_PINMODE_PULLUP;//外部强上拉。	
	PinCfg.Portnum   = PINSEL_PORT_2;
	PinCfg.Pinnum    = PINSEL_PIN_4;
	PINSEL_ConfigPin(&PinCfg);
  //GPIO_SetDir(PINSEL_PORT_2, (1<<PINSEL_PIN_4), 0); //input
	GPIO_SetDir(PINSEL_PORT_2, (1<<PINSEL_PIN_4)|(1<<PINSEL_PIN_10), 0); //input
	GPIOExitIntetruptInit();
}
static void GPIOOutputPinsInit(void)
{
	PINSEL_CFG_Type PinCfg;
	PinCfg.Funcnum = PINSEL_FUNC_0;
	
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Pinmode   = PINSEL_PINMODE_PULLUP;
	PinCfg.Portnum   = PINSEL_PORT_2;
	PinCfg.Pinnum    = PINSEL_PIN_7;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum    = PINSEL_PIN_11;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum    = PINSEL_PIN_12;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum    = PINSEL_PIN_13;
	PINSEL_ConfigPin(&PinCfg);
	GPIO_SetDir(PINSEL_PORT_2, (1<<PINSEL_PIN_7)|(1<<PINSEL_PIN_11)|(1<<PINSEL_PIN_12)|(1<<PINSEL_PIN_13), 1); //output
	
	PinCfg.Portnum   = PINSEL_PORT_0;
	PinCfg.Pinnum    = PINSEL_PIN_4;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum    = PINSEL_PIN_5;
	PINSEL_ConfigPin(&PinCfg);
	
	PinCfg.Portnum = PINSEL_PORT_3;
	PinCfg.Pinnum    = PINSEL_PIN_26;
	PINSEL_ConfigPin(&PinCfg);
	GPIO_SetDir(PINSEL_PORT_0, (1<<PINSEL_PIN_4)|(1<<PINSEL_PIN_5), 1); //output
	GPIO_SetDir(PINSEL_PORT_3,(1<<PINSEL_PIN_26),1);
	
	PinCfg.Portnum   = PINSEL_PORT_4;
	PinCfg.Pinnum    = PINSEL_PIN_28;
	PINSEL_ConfigPin(&PinCfg);
	GPIO_SetDir(PINSEL_PORT_4, 1<<PINSEL_PIN_28, 1); //output	
    
    
    // 4463 reset
    PinCfg.Portnum = 1;
    PinCfg.Pinnum = PINSEL_PIN_28;
    PINSEL_ConfigPin(&PinCfg);
    GPIO_SetDir(PINSEL_PORT_1,1<<PINSEL_PIN_28,1);
//    Si4463ResetEnable();
	
	LedGreenOff();
	LedRedOff();
	LedYellowOff();
	
	RFSendDisable();
	SignallingRSSINoBusy();
	ToneRSSINoBusy();
	FPGAResetDisable();
	
	SpeakerOff();	
}



void GPIOInit(void)
{
	GPIOInputPinsInit();
	GPIOOutputPinsInit();
}
void GPIOSet(uint8_t GPIOPort,uint8_t GPIOPinnum)
{
//	if(GPIOPort==0){
////		if(GPIOPinnum==27)
////			printf("gpioPortSet :%d,pinnum;%d\n",GPIOPort,GPIOPinnum);
//	}
	GPIO_SetValue( GPIOPort, 1 << GPIOPinnum);
}
void GPIOReSet(uint8_t GPIOPort,uint8_t GPIOPinnum)
{
//	if(GPIOPort==0){
//		if(GPIOPinnum==27)
//			printf("gpioPortReset :%d,pinnum;%d\n",GPIOPort,GPIOPinnum);
//	}
		GPIO_ClearValue(GPIOPort,1 << GPIOPinnum);
}

uint8_t GPIOReadValue(uint8_t GPIOPort,uint8_t GPIOPinnum)
{
	return (GPIO_ReadValue(GPIOPort) & ( 1 << GPIOPinnum));
}



