#ifndef __PWM_H
#define __PWM_H

#include "lpc17xx.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_pwm.h"

void PWMInit(void);
uint8_t PWMSetDutyCycle(uint8_t DutyCycle);

#endif
