#ifndef __DAC_H
#define __DAC_H

#include "lpc17xx_dac.h"


//#define SINWAVE
#define DAC_RATE		44000

void DacInit(void);
void SinWaveOut(void);

extern int soCnt;
#endif