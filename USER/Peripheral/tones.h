/*************************************************************
 * @file      tones.h 
 * @brief     提示音
 *
 *
 * @version    v0.0
 * @author     xieyoub
 * @data       2017/06/12
 *************************************************************/
 
 #ifndef	__TONES_H
 #define	__TONES_H
 
 #include  <uCOS-II\Source\ucos_ii.h>
 #include "dac.h"
 #include "dma.h"
 //#include "ff.h"
 #include "app.h"
 #include "stdbool.h"
 #include "string.h"
 

 
 
 
 /** @addtogroup  提示音
  * 
  *  @{
  */
//#define DAC_TIMER_OUT
	
//音频播放次数
#define	AUDIO_PLAY_TIMES_KEY_NORMAL  	  1   //按键音(有效)
#define	AUDIO_PLAY_TIMES_KEY_UNNORMAL	  1  //按键音(无效)
#define	AUDIO_PLAY_TIMES_CALL			      255   //拨打提示音
#define	AUDIO_PLAY_TIMES_PICK_UP			  1   //对方接听(叮)
#define	AUDIO_PLAY_TIMES_TALK_END		    3   //通话结束(嘟嘟)
#define	AUDIO_PLAY_TIMES_NO_ANSWER		  3  //对方正忙
#define	AUDIO_PLAY_TIMES_CALL_FAIL		  5   //呼叫失败
#define	AUDIO_PLAY_TIMES_CALLER			    255   //来电
#define	AUDIO_PLAY_TIMES_TALKING_CALL	  1   //通话中被叫

	
	/**< 消息类型定义     */
#define USER_MSG_AUDIO_PLAY						0x01
#define USER_MSG_AUDIO_DACOUT_END				0x02
#define USER_MSG_AUDIO_STOP						0x03
	
	
/**< 消息最大数量     */
#define	AUDIO_MSG_MAX_NUM	20

/**< 数据源信息缓存区大小     */
#define AUDIO_SOURCE_MAX_NUM	20

/**----------	 音频数据长度	---------- */
//
//
//

/**< 单次获取的数据大小     */
#define READ_SIZE		512


typedef enum{
	PLAY_DEV_STATE_IDLE=0,	//空闲状态
	PLAY_DEV_STATE_BUSY,		//播放音频状态
	PLAY_DEV_STATE_INTER		//被打断状态
}PlayDevState;


/**< 消息结构     */
typedef struct{
	uint16_t 		type;	//消息类型
	uint16_t 		id;		//消息ID
	uintptr_t 	p;		//消息参数p
}AudioMessage;

	
	/**< 音频数据类型     */
typedef enum {
	AUDIO_TYPE_KEY_NORMAL = 1, 	//按键音(有效)
	AUDIO_TYPE_KEY_UNNORMAL,	//按键音(无效)
	AUDIO_TYPE_CALL,			//拨打提示音
	AUDIO_TYPE_PICK_UP,			//对方接听(叮)
	AUDIO_TYPE_TALK_END,		//通话结束(嘟嘟)
	AUDIO_TYPE_NO_ANSWER,		//对方正忙
	AUDIO_TYPE_CALL_FAIL,		//呼叫失败
	AUDIO_TYPE_CALLER,			//来电
	AUDIO_TYPE_TALKING_CALL		//通话中被叫
}AudioType;	
	
	
	
	/**<  音频数据信息源结构体   */
typedef struct{
	uint32_t	totalSize;		//数据总大小
	uint32_t	remainSize;		//剩余待播放的数据大小
	uint32_t	address;			//数据存储地址
	AudioType	audioType;	  //数据类型
  uint16_t  playTimes;     //播放次数
}AudioSource;

typedef struct _AudioSourceList AudioSourceList;
struct _AudioSourceList{
	AudioSourceList* pPre;
	AudioSourceList* pNext;
	AudioSource *source;
};

/**< 音频播放设备     */
typedef struct{
	PlayDevState playState;		//播放状态
	bool 	playEnd;		//播放结束
	AudioSource* sourceList[10];	//待播放源数据
	AudioSource* curSource;			//当前播放源数据
	uint32_t	curSize;					//本次播放的数据大小
	uint8_t	nCh;								//播放通道
	
#ifdef DAC_TIMER_OUT
	uint8_t buf1[512];
	uint8_t buf2[512];
	
	uint16_t dacOutIndex;
	
#else	
	//音频数据
	uint32_t buf1[512];
	uint32_t buf2[512];
#endif	
}PlayDev;


void TonesTask(void *p_arg);
void TonesWriteToFlash(uint8_t *buf,uint8_t add,uint8_t size);
void AudioPostMessage(uint16_t type,uint16_t id,uintptr_t p);
bool AudioPeekMessage(AudioMessage* pMsg);
void AudioPlay(AudioType type);
void AudioPlayCnt(AudioType type, uint16_t cnt);
void AudioStopPlay();
bool PlaySourceInit(AudioSource* source);
uint32_t AudioGetDataLength(uint32_t address);
bool isWaveFile(uint32_t address);


extern PlayDev playDev;


/** @} */
 
 
 #endif