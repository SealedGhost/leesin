#ifndef __SPI_H
#define __SPI_H

#include "lpc17xx.h"
#include "lpc17xx_spi.h"


// PORT number that /CS pin assigned on
#define SPI_CS_PORT_NUM             0
// PIN number that  /CS pin assigned on
#define SPI_CS_PIN_NUM              16


void SPIInit();
void SPI_CS_Force(int32_t state);

extern SPI_DATA_SETUP_Type spiDataBuf;

#endif