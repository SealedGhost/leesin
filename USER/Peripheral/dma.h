#ifndef	__DMA_H
#define __DMA_H

#include "lpc17xx_gpdma.h"
#include  <uCOS-II\Source\ucos_ii.h>
#include "lpc17xx_clkpwr.h" 
 
#define DAM_OUT_CNT		50
#define UART3_DMA_SIZE  164


void DmaChannel0Init(uint32_t* buf, uint32_t size);
void DmaChannel0Start();
void Uart0DmaInit(uint8_t* buf, uint32_t size);
void Uart3DmaInit(uint8_t* buf, uint32_t size);

void DacDmaDataSet(uint32_t* buf,uint32_t size);

extern uint32_t d;

#endif