/*************************************************************
 * @file       rtc.h
 * @brief      rtc配置及中断服务函数头文件
 *
 *
 * @version    v0.0
 * @author     xieyoub	
 * @data       2017/6/10
 *************************************************************/
 
 #ifndef	__RTC_H
 #define	__RTC_H
 
 #include "lpc17xx_rtc.h"
 #include "nvic.h"
 #include "stdio.h"
 
 
 /** @addtogroup  RTC
  * 
  *  @{
  */

void RTCInit();
void SetRtcTime(uint32_t year,uint32_t month,uint32_t day,uint32_t hour,uint32_t min,uint32_t sec);


/** @} */
 
 
 
 
 #endif