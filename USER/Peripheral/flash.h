#ifndef __FLASH_H
#define __FLASH_H

#include "lpc17xx.h"


#define FLASH_SIZE_INSIDE   0x80000
#define FLASH_SIZE_OUTSIDE  0x800000


#define W25Q80 	0XEF13 	
#define W25Q16 	0XEF14
#define W25Q32 	0XEF15
#define W25Q64 	0XEF16

/**---------- 地址分配 -------------*/

/** 音频数据块开始地址     */
#define FLASH_ADDRESS_AUDIO_KEY_NORMAL		0x0000	//按键有效音
#define FLASH_ADDRESS_AUDIO_KEY_UNNORMAL	0x1816	//按键无效音
#define FLASH_ADDRESS_AUDIO_CALL					0x4126	//拨打提示音
#define FLASH_ADDRESS_AUDIO_PICKUP				0x162be	//对方接听
#define FLASH_ADDRESS_AUDIO_TALK_END			0x1c6a2	//通话结束
#define FLASH_ADDRESS_AUDIO_NOANSWER			0x23174	//对方正忙
#define FLASH_ADDRESS_AUDIO_CALL_FAIL			0x4e218	//呼叫失败
#define FLASH_ADDRESS_AUDIO_CALLER				0x53a40	//一般被叫
#define FLASH_ADDRESS_AUDIO_TALKINGE_CALL	0x71e3e	//通话中被呼叫 


/** 系统静态存储地址  */
#define FLASH_ADDRESS_SYS_INFO        0x89000     // 系统信息，137号扇区


/** 系统动态配置地址 */
#define FLASH_ADDRESS_SYS_CONFIG      0x8a000     // 系统配置，138号扇区

/** 联系人 */
#define FLASH_ADDRESS_CONTACT         0x8b000     // 联系人，139号扇区

/** 通话记录 */
#define FLASH_ADDRESS_CALL_LOG        0x8c000     // 通话记录，140号扇区

/** 收藏频道 */
#define FLASH_ADDRESS_FAVORITE        0x8d000     // 频道收藏，141号扇区

/** Cookies */
#define FLASH_ADDRESS_COOKIES         0x8e000     // Cookies, 142号扇区



/** */


extern uint16_t SPI_FLASH_TYPE;		//��������ʹ�õ�flashоƬ�ͺ�		
				 
////////////////////////////////////////////////////////////////////////////

extern uint8_t SPI_FLASH_BUF[4096];
//W25X16��д
#define FLASH_ID 0XEF14
//ָ���
#define W25X_WriteEnable			0x06 
#define W25X_WriteDisable			0x04 
#define W25X_ReadStatusReg		0x05 
#define W25X_WriteStatusReg		0x01 
#define W25X_ReadData					0x03 
#define W25X_FastReadData			0x0B 
#define W25X_FastReadDual			0x3B 
#define W25X_PageProgram			0x02 
#define W25X_BlockErase				0xD8 
#define W25X_SectorErase			0x20 
#define W25X_ChipErase				0xC7 
#define W25X_PowerDown				0xB9 
#define W25X_ReleasePowerDown	0xAB 
#define W25X_DeviceID					0xAB 
#define W25X_ManufactDeviceID	0x90 
#define W25X_JedecDeviceID		0x9F 


void SPI_FLASH_Start(void);

void SPI_Flash_Init(void);
uint16_t  SPI_Flash_ReadID(void);  	    //��ȡFLASH ID
uint8_t	 SPI_Flash_ReadSR(void);        //��ȡ״̬�Ĵ��� 
void SPI_FLASH_Write_SR(uint8_t sr);  	//д״̬�Ĵ���
void SPI_FLASH_Write_Enable(void);  //дʹ�� 
void SPI_FLASH_Write_Disable(void);	//д����
void SPI_Flash_Read(uint8_t* pBuffer,uint32_t ReadAddr,uint16_t NumByteToRead);   //��ȡflash
void SPI_Flash_Write(uint8_t* pBuffer,uint32_t WriteAddr,uint16_t NumByteToWrite);//д��flash
void SPI_Flash_Erase_Chip(void);    	  //��Ƭ����
void SPI_Flash_Erase_Sector(uint32_t Dst_Addr);//��������
void SPI_Flash_Wait_Busy(void);           //�ȴ�����
void SPI_Flash_PowerDown(void);           //�������ģʽ
void SPI_Flash_WAKEUP(void);			  //����
void SPIFlashSectionWrite(uint8_t* buf,uint16_t section, uint16_t numByteToWrite);
void SPIFlashSectionRead(uint8_t* buf,uint16_t section, uint16_t numByteToRead);
	
#endif