#ifndef __SSP_H
#define __SSP_H

#include "lpc17xx_ssp.h"

// PORT number that /CS pin assigned on
#define SSP0_CS_PORT_NUM             1
// PIN number that  /CS pin assigned on
#define SSP0_CS_PIN_NUM              21


#define SSPO_FRE		25*1000000

void SSP0Init();
uint8_t SSP0_ReadWriteByte(uint8_t TxData);
void SSP0_CS_Force(int32_t state);

void SSP1PinInit();
void SSP1_Set12Bit();
void SSP1_Set14Bit();
#endif