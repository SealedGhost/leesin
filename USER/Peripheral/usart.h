/*************************************************************
 * @file       usart.h
 * @brief      串口配置及中断服务头文件
 *
 *
 * @version    0.0
 * @author     xieyoub
 * @data       2017/06/08
 *************************************************************/


#ifndef _USART_H_
#define _USART_H_

#include "stdint.h"
#include "stdio.h"
#include "lpc17xx.h"
#include "lpc17xx_uart.h"
#include "mac_working.h"


/** @addtogroup  串口配置
  * 
  *  @{
  *	 @note 	！！！串口接收数据一帧的长度  不能等于14的倍数。
  */
	
 
#define FRAME_HEAD_DATA  0xad   
    
//触发字符个数
#define    UART_FIFO_TRGLEV_NUM     14

	
//接收缓冲区大小	
#define		RX_MAX_SIZE				400

		
		 /**< 串口0缓冲区大小     */
	#define UART0_BUF_SIZE	10
 /**< 串口0数据长度     */
	#define UART0_DATA_LENTH	10
	
	
	
	 /**< 串口1缓冲区大小     */
	#define UART1_BUF_SIZE	10
 /**< 串口1数据长度     */
	#define UART1_DATA_LENTH	400
	
	
	 /**< 串口3缓冲区大小     */
	#define UART3_BUF_SIZE	10
 /**< 串口2数据长度     */
	#define UART3_DATA_LENTH	400

	

/**< 串口触发点大小     */

/**< DMA 使用开关     */
//#define 	UART0_DMA
//#define 	UART1_DMA	
//#define		UART2_DMA
//#define		UART3_DMA

//接收临时缓冲区结构体
typedef struct
{
	uint32_t pIndex;
	uint8_t rx[RX_MAX_SIZE];
	
}UART_BUF_R;


		/**< 串口1数据结构体    */
	typedef struct
	{
		uint8_t curCnt;
		uint8_t buf[UART1_BUF_SIZE][UART1_DATA_LENTH];
	}UART1Data;
	
	/**< 串口3数据结构体    */
	typedef struct
	{
		uint8_t curCnt;
		uint8_t buf[UART3_BUF_SIZE][UART3_DATA_LENTH];
	}UART3Data;
	

#ifdef __cplusplus
  extern "C" {
#endif

void LCDUartSend(uint8_t *buf, uint16_t length);


void UARTInit(void);

void	UART0Init(uint32_t baudRate);
void	UART1Init(uint32_t baudRate);
void	UART2Init(uint32_t baudRate);
void	UART3Init(uint32_t baudRate);
void	UART_IntReceive(LPC_UART_TypeDef* uart);
      
void Uart0_SendByte(uint8_t dat);
void Uart0_SendBytes(size_t size, uint8_t* buf);      
      

		
        /**< 串口3数据 临时缓冲    */
extern UART_BUF_R	uart3RxBuf;
        /**< 串口3数据 缓冲    */
extern  UART3Data uart3Data;
	
uint8_t PutData(LPC_UART_TypeDef* UartPort,uint8_t* buf,uint16_t size);
		
#ifdef __cplusplus
 }
#endif		
		
extern uint8_t uart0Buf[UART0_BUF_SIZE];



/** @} */

#endif

