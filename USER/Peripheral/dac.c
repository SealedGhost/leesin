#include "dac.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "dma.h"
#include "app.h"
#include "nvic.h"




static GPDMA_Channel_CFG_Type GPDMACfg;

uint32_t dac_sine_lut[60];
int soCnt = 0;

void SinWaveOut()
{
	uint32_t tmp;
	uint32_t i;
	uint32_t PCLK_DAC_IN_MHZ = 25;
	uint32_t sine_preq_in_hz = 1000;
	uint32_t num_size_sine_sample = 60;
	
	
	uint32_t sin_0_to_90_16_samples[16]={\
								0,1045,2079,3090,4067,\
								5000,5877,6691,7431,8090,\
								8660,9135,9510,9781,9945,10000\
	};

	

	for(i=0;i<num_size_sine_sample;i++)
	{
				if(i<=15)
				{
								dac_sine_lut[i] = 512 + 512*sin_0_to_90_16_samples[i]/10000;
								if(i==15) dac_sine_lut[i]= 1023;
				}
				else if(i<=30)
				{
								dac_sine_lut[i] = 512 + 512*sin_0_to_90_16_samples[30-i]/10000;
				}
				else if(i<=45)
				{
								dac_sine_lut[i] = 512 - 512*sin_0_to_90_16_samples[i-30]/10000;
				}
				else
				{
								dac_sine_lut[i] = 512 - 512*sin_0_to_90_16_samples[60-i]/10000;
				}
				dac_sine_lut[i] = (dac_sine_lut[i]<<6);
	}

	for(i=0;i<num_size_sine_sample;i++)
		dac_sine_lut[i] /=22;
	
	tmp = (PCLK_DAC_IN_MHZ*1000000)/(sine_preq_in_hz*num_size_sine_sample);
  DAC_SetDMATimeOut(LPC_DAC,7000000);
	
	DmaChannel0Init(dac_sine_lut,num_size_sine_sample);
	NVIC_DisableIRQ(DMA_IRQn);
	GPDMA_ChannelCmd(0, ENABLE);
}


static void GPIO_Configuration()
{
	 PINSEL_CFG_Type PinCfg;
	 /*
	 * Init DAC pin connect
	 * AOUT on P0.26
	 */
	PinCfg.Funcnum = 2;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Pinnum = 26;
	PinCfg.Portnum = 0;
	PINSEL_ConfigPin(&PinCfg);
}



void DacInit()
{
	DAC_CONVERTER_CFG_Type DAC_ConverterConfigStruct;
	GPIO_Configuration();
	
	
	DAC_ConverterConfigStruct.CNT_ENA =SET;
	DAC_ConverterConfigStruct.DMA_ENA = SET;
	//DAC_ConverterConfigStruct.DBLBUF_ENA = SET;

	/* set time out for DAC*/
	//tmp = (PCLK_DAC_IN_MHZ*1000000)/(SINE_FREQ_IN_HZ*NUM_SINE_SAMPLE);
	//DAC 时钟选择 00 PCLK_peripheral = CCLK/4 = 25M  ,计数到10000 2.5k
	DAC_SetDMATimeOut(LPC_DAC, (25*1000000)/DAC_RATE);   
	DAC_ConfigDAConverterControl(LPC_DAC, &DAC_ConverterConfigStruct);
	
	
	CLKPWR_ConfigPPWR (CLKPWR_PCONP_PCGPDMA, ENABLE);
		/* Disable GPDMA interrupt */
	NVIC_DisableIRQ(DMA_IRQn);
	
	/* preemption = 1, sub-priority = 1 */
    NVIC_SetPriority(DMA_IRQn, NVIC_DMA);
	
	NVIC_EnableIRQ(DMA_IRQn);	
	
	//DmaChannel0Init();
//	
//	soCnt = 0;
//	GPDMA_ChannelCmd(0, ENABLE);

#ifdef 	SINWAVE	
	SinWaveOut();
#endif
}
