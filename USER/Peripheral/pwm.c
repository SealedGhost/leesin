/*************************************************************
 * @file       pwm.c
 * @brief      pwm驱动程序用于设置液晶背光
 *
 *
 * @version    Verison	0.0
 * @author     RH zhang
 * @data       2017/06/17
 *************************************************************/
 #include "pwm.h"
 
 
 static void PWMGPIOInit(void)
 {
	PINSEL_CFG_Type PinCfg;
	PinCfg.Funcnum = PINSEL_FUNC_2;
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
	PinCfg.Portnum = 1;
	PinCfg.Pinnum = 26;
	PINSEL_ConfigPin(&PinCfg);
 }
 
 
void PWMInit(void)
 {
	 	PWM_TIMERCFG_Type PWMCfgDat;
	 	PWM_MATCHCFG_Type PWMMatchCfgDat;
	 	/*
	 * Initialize PWM pin connect
	 *
	 */
	PWMGPIOInit();
	PWMCfgDat.PrescaleOption = PWM_TIMER_PRESCALE_TICKVAL;
	PWMCfgDat.PrescaleValue = 1;
	PWM_Init(LPC_PWM1, PWM_MODE_TIMER, (void *) &PWMCfgDat);
	/* Set match value for PWM match channel 0 = 256, update immediately */
	PWM_MatchUpdate(LPC_PWM1, 0, 100, PWM_MATCH_UPDATE_NOW);
	PWMMatchCfgDat.IntOnMatch = DISABLE;
	PWMMatchCfgDat.MatchChannel = 0;
	PWMMatchCfgDat.ResetOnMatch = ENABLE;
	PWMMatchCfgDat.StopOnMatch = DISABLE;
	PWM_ConfigMatch(LPC_PWM1, &PWMMatchCfgDat);
	 
	PWM_MatchUpdate(LPC_PWM1, 6, 0, PWM_MATCH_UPDATE_NOW);
	PWMMatchCfgDat.IntOnMatch = DISABLE;
	PWMMatchCfgDat.MatchChannel = 6;
	PWMMatchCfgDat.ResetOnMatch = DISABLE;
	PWMMatchCfgDat.StopOnMatch = DISABLE;
	PWM_ConfigMatch(LPC_PWM1, &PWMMatchCfgDat);
	 
	PWM_ChannelConfig(LPC_PWM1, 6, PWM_CHANNEL_SINGLE_EDGE);

	PWM_ChannelCmd(LPC_PWM1, 6, ENABLE);

	PWM_ResetCounter(LPC_PWM1);
	PWM_CounterCmd(LPC_PWM1, ENABLE);
	PWM_Cmd(LPC_PWM1, ENABLE);
 }
 
uint8_t PWMSetDutyCycle(uint8_t DutyCycle)
{
		if(DutyCycle > 100)
			return 0;
		PWM_MatchUpdate(LPC_PWM1, 6, DutyCycle, PWM_MATCH_UPDATE_NEXT_RST);
			return 1;
	}

