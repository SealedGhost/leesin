/*************************************************************
 * @file       adc.c
 * @brief      P0.23 ADC0.0 语音信道
 *  					 P0.24 ADC0.1 话音信道
 * @version    Verison	0.0
 * @author     RH zhang
 * @data       2017/06/17
 *************************************************************/
#include "adc.h"
#include "lpc_types.h"

#include "rt_indicator.h"
#include "ipc.h"
#include "GUI_message.h"
#include "nvic.h"

#define _ADC_INT                        ADC_ADINTEN2
#define _ADC_CHANNEL            				ADC_CHANNEL_2

static uint16_t rssiCclChannel; 	//信令信道
static uint16_t rssiDataChannel;	//话音信道

static uint8_t  ADCChannel = 0;

void ADC_IRQHandler(void)
{
		if(ADCChannel == 0)
		{
			 uint16_t tmp  = ADC_ChannelGetData(LPC_ADC,ADC_CHANNEL_0);
			  
		  	/** 强度区间改变且处于接受状态，向UI发送消息 */
			if(tmp /(ADC_VALUE_MAX /10)  != rssiCclChannel/(ADC_VALUE_MAX /10) ){
				 
				 if(RTState  == RTState_IsRxing()){
				   GUI_PostMessage(IPC_MSG_ADC, ADC_MSG_CCL_RSSI, rssiCclChannel, 0); 
			  }
			}
			
			rssiCclChannel  = tmp;
			  
			  
			if(isChannelBusy(CclChannelIndicator))
			{
				SignallingRSSIBusy();
			}
			else
			{
				SignallingRSSINoBusy();
			}
			
			printf("ccl adc channel:%u\n", tmp);
		}
		else
		{
				rssiDataChannel = ADC_ChannelGetData(LPC_ADC,ADC_CHANNEL_1);
				if(isChannelBusy(DataChannelIndicator))
				{
					ToneRSSIBusy();
				}
				else
				{
					ToneRSSINoBusy();
				}
			
			printf("data adc channel:%u\n", rssiDataChannel);
		}
}

void TIMER2_IRQHandler(void)
{
	TIM_Cmd(LPC_TIM2,DISABLE);
	TIM_ClearIntPending(LPC_TIM2, TIM_MR2_INT);
	if(ADCChannel == 0)
	{
		ADCChannel = 1;
		ADC_ChannelCmd(LPC_ADC,ADC_CHANNEL_0,DISABLE);
		ADC_ChannelCmd(LPC_ADC,ADC_CHANNEL_1,ENABLE);
		ADC_StartCmd(LPC_ADC,ADC_START_NOW);
	}
	else
	{
		ADCChannel = 0;
		ADC_ChannelCmd(LPC_ADC,ADC_CHANNEL_0,ENABLE);
		ADC_ChannelCmd(LPC_ADC,ADC_CHANNEL_1,DISABLE);
		ADC_StartCmd(LPC_ADC,ADC_START_NOW);
	}
	
	TIM_ResetCounter(LPC_TIM2);
	TIM_Cmd(LPC_TIM2,ENABLE);
}

static void ADCGPIOInit(void)
{
	PINSEL_CFG_Type PinCfg;
	PinCfg.Funcnum = PINSEL_FUNC_1;
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Pinmode = PINSEL_PINMODE_TRISTATE;
	PinCfg.Portnum = PINSEL_PIN_0;
	PinCfg.Pinnum = PINSEL_PIN_23;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = PINSEL_PIN_24;
	PINSEL_ConfigPin(&PinCfg);
}

void ADCInit(void)
{
	ADCGPIOInit();
	ADC_Init(LPC_ADC,200000);
	ADC_IntConfig(LPC_ADC,ADC_ADINTEN0,SET);
	ADC_IntConfig(LPC_ADC,ADC_ADINTEN1,SET);
	ADC_IntConfig(LPC_ADC,ADC_ADGINTEN,RESET);
    NVIC_SetPriority(ADC_IRQn,NVIC_ADC);
	NVIC_EnableIRQ(ADC_IRQn);
	Timer2Init(200);
}

bool isChannelBusy(ChannelIndicator nCh)
{
	if(nCh == CclChannelIndicator)
	{
		return (rssiCclChannel > RSSI_THRESHOLD_A);
	}
	else
	{
		return (rssiDataChannel > RSSI_THRESHOLD_B);
	}
}


uint16_t getChannelRssi(ChannelIndicator nCh)
{	
	return (nCh == CclChannelIndicator ? rssiCclChannel : rssiDataChannel);
}
