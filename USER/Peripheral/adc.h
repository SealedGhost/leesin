#ifndef __ADC_H
#define __ADC_H

#include "lpc17xx.h"
#include "stdbool.h"
#include "dllLayer.h"
#include "stdint.h"
#include "lpc17xx_adc.h"

#include "lpc17xx_gpio.h"
#include "lpc17xx_pinsel.h"
#include "timer.h"
#include "lpc_types.h"
#include "gpio.h"


#define ADC_VALUE_MAX     (4096/2)



#define	RSSI_THRESHOLD_A	2600
#define RSSI_THRESHOLD_B	2600


void ADCInit(void);

bool isChannelBusy(ChannelIndicator nCh);


uint16_t getChannelRssi(ChannelIndicator nCh);




#endif