#include "dma.h"
#include "dac.h"
#include "stdio.h"
#include "usart.h"
#include "lpc17xx_uart.h"
#include "tones.h"
#include "timer.h"
#include "ipc.h"

static GPDMA_Channel_CFG_Type GPDMACfg;
static GPDMA_Channel_CFG_Type Uart0DmaCfg;
static GPDMA_Channel_CFG_Type Uart3DmaCfg;

static GPDMA_LLI_Type DMA_LLI_Struct;
uint32_t d=0;

/*----------------- INTERRUPT SERVICE ROUTINES --------------------------*/
/*********************************************************************//**
 * @brief		GPDMA interrupt handler sub-routine
 * @param[in]	None
 * @return 		None
 **********************************************************************/
void DMA_IRQHandler (void)
{
    
#ifdef USE_CRITICAL_WHEN_POST    
    
 #if OS_CRITICAL_METHOD == 3u	 
	 OS_CPU_SR cpu_sr  = 0u;
#endif	
	OSIntEnter();
	OS_ENTER_CRITICAL();
#endif	    
	// check GPDMA interrupt on channel 0
//	if (GPDMA_IntGetStatus(GPDMA_STAT_INT, 0)){
//		// Check counter terminal status
//		if(GPDMA_IntGetStatus(GPDMA_STAT_INTTC, 0)){
//			GPDMA_ChannelCmd(0, DISABLE);
//			GPDMA_ClearIntPending (GPDMA_STATCLR_INTTC, 0);

//			AudioPostMessage(USER_MSG_AUDIO_DACOUT_END,0,0);
//		}
//		// Check error terminal status
//		if (GPDMA_IntGetStatus(GPDMA_STAT_INTERR, 0)){
//			GPDMA_ClearIntPending (GPDMA_STATCLR_INTERR, 0);
//			DacInit();
//		}
//	}
	
	/*******************channel1***********************/
	uint32_t tmp;
	// Scan interrupt pending
	for (tmp = 0; tmp <= 7; tmp++) {
		if (GPDMA_IntGetStatus(GPDMA_STAT_INT, tmp)){
			// Check counter terminal status
			if(GPDMA_IntGetStatus(GPDMA_STAT_INTTC, tmp)){
				// Clear terminate counter Interrupt pending
				GPDMA_ClearIntPending (GPDMA_STATCLR_INTTC, tmp);

				switch (tmp){
								case 0:
												GPDMA_ChannelCmd(0, DISABLE);
												GPDMA_ClearIntPending (GPDMA_STATCLR_INTTC, 0);
                                                AudioPostMessage(USER_MSG_AUDIO_DACOUT_END,0,0);
												break;
								case 1:
                                                                       
                                
                                
												GPDMA_ChannelCmd(1, DISABLE);
                                                
                                               if(uart3RxBuf.rx[0] == FRAME_HEAD_DATA  &&  uart3RxBuf.pIndex == UART3_DMA_SIZE){
                                                                                                                              
                                                    memcpy( uart3Data.buf[uart3Data.curCnt], &uart3RxBuf.rx[1], UART3_DMA_SIZE-1);	
                                                    DLL_PostMessage(IPC_MSG_UART, UART_MSG_RX3 & UINT16_MAX, UART3_DMA_SIZE-1, (uintptr_t)uart3Data.buf[uart3Data.curCnt]);			
                                                    uart3Data.curCnt++;
                                                    
                                                    if(uart3Data.curCnt >= UART3_BUF_SIZE){
                                                      uart3Data.curCnt  = 0;
                                                   }
                                                }
												GPDMA_Setup(&Uart3DmaCfg);
												GPDMA_ChannelCmd(1, ENABLE);
                                                
                                                
												break;
								default:
												break;
				}

			}
			// Check error terminal status
			if (GPDMA_IntGetStatus(GPDMA_STAT_INTERR, tmp)){
				// Clear error counter Interrupt pending
				GPDMA_ClearIntPending (GPDMA_STATCLR_INTERR, tmp);
				switch (tmp){
								case 0:
												//Channel0_Err++;
												//GPDMA_ChannelCmd(0, DISABLE);
												break;
								case 1:
												//Channel1_Err++;
												//GPDMA_ChannelCmd(1, DISABLE);
												break;
								default:
												break;
				}
			}
		}
	}
#ifdef USE_CRITICAL_WHEN_POST 
    OSIntExit();	
	OS_EXIT_CRITICAL();
#endif    
}

void DmaChannel0Start(uint32_t size)
{

#ifdef DAC_TIMER_OUT	
	TIM_ResetCounter(LPC_TIM1);
	TIM_Cmd(LPC_TIM1,ENABLE);
#else	
	DAC_CONVERTER_CFG_Type DAC_ConverterConfigStruct;
	DAC_ConverterConfigStruct.CNT_ENA =SET;
	DAC_ConverterConfigStruct.DMA_ENA = SET;

	/* set time out for DAC*/
	DAC_SetDMATimeOut(LPC_DAC, (25*1000000)/DAC_RATE);   
	DAC_ConfigDAConverterControl(LPC_DAC, &DAC_ConverterConfigStruct);
#endif	
	GPDMA_ChannelCmd(0, ENABLE);
}

void DmaChannel0Init(uint32_t* buf, uint32_t size)
{
	//Prepare DMA link list item structure
//	DMA_LLI_Struct.SrcAddr = (uint32_t)&buf;
//	DMA_LLI_Struct.DstAddr = (uint32_t)&(LPC_DAC->DACR);
//	DMA_LLI_Struct.NextLLI = (uint32_t)&DMA_LLI_Struct;
//	DMA_LLI_Struct.Control = size
//													| (2<<18) //source width 32 bit
//													| (2<<21) //dest. width 32 bit
//													| (1<<26) //source increment
//													;

	//GPDMA_Init();
	LPC_GPDMACH0->DMACCConfig = 0;
	
	GPDMACfg.ChannelNum = 0;
	// Source memory
	GPDMACfg.SrcMemAddr = (uint32_t)buf;
	// Destination memory - unused
	GPDMACfg.DstMemAddr = 0;
	// Transfer size
	GPDMACfg.TransferSize = size;
	// Transfer width - unused
	GPDMACfg.TransferWidth = 0;
	// Transfer type
	GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_M2P;
	// Source connection - unused
	GPDMACfg.SrcConn = 0;
	// Destination connection
	GPDMACfg.DstConn = GPDMA_CONN_DAC;
	// Linker List Item - unused
	GPDMACfg.DMALLI = 0;//(uint32_t)&DMA_LLI_Struct;

	
	
	GPDMA_Setup(&GPDMACfg);
	
	
	//DMA通道使能后第一次数据传输出错，发现寄存器DMACCControl 没有在GPDMA_Setup中设置
	LPC_GPDMACH0->DMACCControl |= size
															| (2<<18) //source width 32 bit
															| (2<<21) //dest. width 32 bit
															| (1<<26) //source increment
															;	

	DmaChannel0Start(size);
}

/**	@brief 		涓插彛0DMA閰嶇疆
  * 
  * @param  	buf		鎺ユ敹缂撳啿鍖�
	*	@param		size	鏁版嵁浼犺緭澶у皬
  * @note			涓插彛鎺ユ敹DMA浼犺緭 ,浣跨敤DMA閫氶亾1
  */
void Uart0DmaInit(uint8_t* buf, uint32_t size)
{
	/* Initialize GPDMA controller */
	//GPDMA_Init();
    LPC_GPDMACH0->DMACCConfig = 0;
	
	// Setup GPDMA channel --------------------------------
	// channel 1
	Uart0DmaCfg.ChannelNum = 1;
	// Source memory	- don't care
	Uart0DmaCfg.SrcMemAddr = 0;
	// Destination memory
	Uart0DmaCfg.DstMemAddr = (uint32_t)buf;
	// Transfer size
	Uart0DmaCfg.TransferSize = size;
	// Transfer width - don't care
	Uart0DmaCfg.TransferWidth = 0;
	// Transfer type
	Uart0DmaCfg.TransferType = GPDMA_TRANSFERTYPE_P2M;
	// Source connection
	Uart0DmaCfg.SrcConn = GPDMA_CONN_UART0_Rx;
	// Destination connection - don't care
	Uart0DmaCfg.DstConn = 0;
	// Linker List Item - unused
	Uart0DmaCfg.DMALLI = 0;
	// Setup channel with given parameter
	GPDMA_Setup(&Uart0DmaCfg);
	
	// Enable interrupt for DMA
	NVIC_EnableIRQ (DMA_IRQn);
	
	// Make sure GPDMA channel 1 is enabled
	GPDMA_ChannelCmd(1, ENABLE);
}


/**	@brief 		涓插彛0DMA閰嶇疆
  * 
  * @param  	buf		鎺ユ敹缂撳啿鍖�
	*	@param		size	鏁版嵁浼犺緭澶у皬
  * @note			涓插彛鎺ユ敹DMA浼犺緭 ,浣跨敤DMA閫氶亾1
  */
void Uart3DmaInit(uint8_t* buf, uint32_t size)
{
	/* Initialize GPDMA controller */
	LPC_GPDMACH0->DMACCConfig = 0;
	
	// Setup GPDMA channel --------------------------------
	// channel 1
	Uart3DmaCfg.ChannelNum = 1;
	// Source memory	- don't care
	Uart3DmaCfg.SrcMemAddr = 0;
	// Destination memory
	Uart3DmaCfg.DstMemAddr = (uint32_t)buf;
	// Transfer size
	Uart3DmaCfg.TransferSize = size;
	// Transfer width - don't care
	Uart3DmaCfg.TransferWidth = 0;
	// Transfer type
	Uart3DmaCfg.TransferType = GPDMA_TRANSFERTYPE_P2M;
	// Source connection
	Uart3DmaCfg.SrcConn = GPDMA_CONN_UART3_Rx;
	// Destination connection - don't care
	Uart3DmaCfg.DstConn = 0;
	// Linker List Item - unused
	Uart3DmaCfg.DMALLI = 0;
	// Setup channel with given parameter
	GPDMA_Setup(&Uart3DmaCfg);
	
	// Make sure GPDMA channel 1 is enabled
	GPDMA_ChannelCmd(1, ENABLE);
}


void DacDmaDataSet(uint32_t* buf,uint32_t size)
{
	
	//Prepare DMA link list item structure
//	DMA_LLI_Struct.SrcAddr= (uint32_t)buf;
//	DMA_LLI_Struct.DstAddr= (uint32_t)&(LPC_DAC->DACR);
//	DMA_LLI_Struct.NextLLI= (uint32_t)&DMA_LLI_Struct;
//	DMA_LLI_Struct.Control= size
//													| (2<<18) //source width 32 bit
//													| (2<<21) //dest. width 32 bit
//													| (1<<26) //source increment
//													;
	
	//GPDMA_Init();
	LPC_GPDMACH0->DMACCConfig = 0;
	
	GPDMACfg.ChannelNum = 0;
	// Source memory
	GPDMACfg.SrcMemAddr = (uint32_t)(buf);
	// Destination memory - unused
	GPDMACfg.DstMemAddr = 0;
	// Transfer size
	GPDMACfg.TransferSize = size;
	// Transfer width - unused
	GPDMACfg.TransferWidth = 0;
	// Transfer type
	GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_M2P;
	// Source connection - unused
	GPDMACfg.SrcConn = 0;
	// Destination connection
	GPDMACfg.DstConn = GPDMA_CONN_DAC;
	// Linker List Item - unused
	GPDMACfg.DMALLI = 0;//(uint32_t)&DMA_LLI_Struct;

	
	
	GPDMA_Setup(&GPDMACfg);
	
	
	//DMA通謤使艤鄢謿一諑私邼垣摔远窄矛注袞輨咋欠DMACCControl 没詯諝GPDMA_Setup讗狮變
	LPC_GPDMACH0->DMACCControl |= size
															| (2<<18) //source width 32 bit
															| (2<<21) //dest. width 32 bit
															| (1<<26) //source increment
															;	

//	LPC_GPDMACH0->DMACCConfig = 1
//	| (0<<1)
//	| (7<<6)
//	| (1<<11)
//	| (0<<14)
//	| (0<<15)
//	| (0<<16)
//	| (0<<18);
//	
//	LPC_GPDMACH0->DMACCSrcAddr = (uint32_t) &buf;
//	LPC_GPDMACH0->DMACCDestAddr = (uint32_t)&(LPC_DAC->DACR);

	DmaChannel0Start(size);
}
