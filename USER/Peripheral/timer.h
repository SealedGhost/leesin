#ifndef __TIMER_H
#define __TIMER_H

#include "lpc17xx.h"
#include "lpc17xx_timer.h"
#include "lpc_types.h"
#include "lpc17xx_adc.h"

void Timer0Init(uint32_t arr);
void Timer1Init(uint32_t time);
void Timer2Init(uint32_t time);
#endif

