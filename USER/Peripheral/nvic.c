/*************************************************************
 * @file       nvic.c
 * @brief      中断分组设置及中断优先级描述
 *
 *
 * @version    v0.0
 * @author     xieyoub	
 * @data       2017/6/10
 *************************************************************/
 
 #include "nvic.h"
 
 /** @addtogroup  中断优先级
  * 
  *  @{
  */

/*-------------------------PRIVATE FUNCTIONS------------------------------*/
/*********************************************************************/
/** @brief 		中断分组初始化
  * 
  * @param  	group	 组号
  * @return 	
  * @note			group 仅低三位有效
  */
void NVICInit(uint8_t group)
{
	NVIC_SetPriorityGrouping(group);
}




/** @} */