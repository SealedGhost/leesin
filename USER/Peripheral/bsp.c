#include "bsp.h"
#include "usart.h"
#include "key.h"
#include "dac.h"
#include "stdio.h"
#include "ssp.h"
#include "spi.h"
#include "si4463.h"
#include "nvic.h"
#include "rtc.h"
#include "pwm.h"
#include "gpio.h"
#include "adc.h"
//#include "systen_delay.h"

void bspInit(void)
{
	NVICInit(NVIC_GROUP);

    UARTInit();
	KeyInit();
	DacInit();// 提示音输出
	SSP0Init();	//FLASH
	
	
	RTCInit();	
	GPIOInit();
	ADCInit();
	SPIInit();
	
	resetSi4463();
	configSi4463();
}
