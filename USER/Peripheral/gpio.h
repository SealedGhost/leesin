#ifndef __GPIO_H
#define __GPIO_H
#include "lpc17xx.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include <stdbool.h>
#include <stdio.h>





#define RFSendEnable() 				GPIOReSet(2,11)
#define RFSendDisable() 			GPIOSet(2,11)

#define SignallingRSSIBusy() 	GPIOSet(2,12)
#define SignallingRSSINoBusy() GPIOReSet(2,12)

#define ToneRSSIBusy() 				GPIOSet(2,13)
#define ToneRSSINoBusy() 			GPIOReSet(2,13)

#define FPGAResetEnable() 		GPIOReSet(2,7)
#define FPGAResetDisable()	  GPIOSet(2,7)


#define SpeakerOn() 		GPIOReSet(3,26)
#define SpeakerOff()		GPIOSet(3,26)

#define LedRedOn()						GPIOReSet(4,28)
#define LedRedOff()					GPIOSet(4,28)

#define LedGreenOn()					GPIOReSet(0,4)
#define LedGreenOff()					GPIOSet(0,4)


#define LedRxOn()        GPIOReSet(0, 4)
#define LedRxOff()       GPIOSet(0, 4)

#define LedTxOn()        GPIOReSet(4, 28)
#define LedTxOff()       GPIOSet(4, 28)

#define LedGrpOn()       GPIOReSet(0, 5)
#define LedGrpOff()      GPIOSet(0, 5)


#define LedYellowOn()					GPIOReSet(0,5)
#define LedYellowOff()				GPIOSet(0,5)

#define ReadPTTValue()				GPIOReadValue(2,4)
#define isRfRecving()  				!GPIOReadValue(2,10)

//#define Si4463ResetEnable()       GPIOReSet(1,28)
#define Si4463ResetEnable()       GPIOSet(1,28)
#define Si4463ResetDisable()      GPIOReSet(1,28)

void GPIOInit(void);
void GPIOSet(uint8_t GPIOPort,uint8_t GPIOPinnum);
void GPIOReSet(uint8_t GPIOPort,uint8_t GPIOPinnum);
uint8_t GPIOReadValue(uint8_t GPIOPort,uint8_t GPIOPinnum);
#endif
