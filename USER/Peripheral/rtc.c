/*************************************************************
 * @file       rtc.c
 * @brief      rtc配置及中断服务函数
 *
 *
 * @version    v0.0
 * @author     xieyoub
 * @data       2017/6/10
 *************************************************************/
 
 #include "rtc.h"
	#include "GUI_message.h"
	#include "system_time.h"
  #include "ipc.h"
  #include "gpio.h"
 
 static uint8_t test=0;
 
 /** @addtogroup  RTC
  * 
  *  @{
  */

/*----------------- INTERRUPT SERVICE ROUTINES --------------------------*/
/*********************************************************************/
void RTC_IRQHandler(void)
{
	uint32_t year,month,day,hour,min,sec;

  /* This is increment counter interrupt*/
  if (RTC_GetIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE))
  {
		year = RTC_GetTime(LPC_RTC,RTC_TIMETYPE_YEAR);
		month = RTC_GetTime(LPC_RTC,RTC_TIMETYPE_MONTH);
		day = RTC_GetTime(LPC_RTC,RTC_TIMETYPE_DAYOFMONTH);
		hour = RTC_GetTime(LPC_RTC,RTC_TIMETYPE_HOUR);
		min = RTC_GetTime(LPC_RTC,RTC_TIMETYPE_MINUTE);
		sec = RTC_GetTime (LPC_RTC, RTC_TIMETYPE_SECOND);
		
		year = year>RTC_YEAR_MAX?2000:year;

		if(month> RTC_MONTH_MAX || month < RTC_MONTH_MIN)
		  month = 1;

		if((day > RTC_DAYOFMONTH_MAX) || (day < RTC_DAYOFMONTH_MIN))
		  day = 1;

		if(hour > RTC_HOUR_MAX)
		  hour = 1;

		if(min > RTC_MINUTE_MAX)
		  min = 1;

		if(sec > RTC_SECOND_MAX)
		  sec = 1;
	  
		SysTime_Update(year, month, day, hour, min, sec);
		GUI_PostMessage(IPC_MSG_RTC, RTC_MSG_SECOND_TICK, 0, 0);

		/* Send debug information */
		//printf("time:%d:%02d:%02d:%02d:%02d:%02d\n",year,month,day,min,hour,sec);

		// Clear pending interrupt
		//printf("Sec:%02d\n",sec);
        if(test==0){
            test=1;
            LedYellowOff();
        }
        else if(test==1){
            test=0;
            //LedYellowOn();
            //printf("sec:%d\n",sec);
        }

        
		RTC_ClearIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE);
  }

  /* Continue to check the Alarm match*/
  if (RTC_GetIntPending(LPC_RTC, RTC_INT_ALARM))
  {
		/* Send debug information */
		//printf("alarm 10s match!\n");


		// Clear pending interrupt
		RTC_ClearIntPending(LPC_RTC, RTC_INT_ALARM);
  }
}
	
/**	@brief 	设置时间
  * 
  * @param  年，月，日，时，分，秒
  * @note
  */	
void SetRtcTime(uint32_t year,uint32_t month,uint32_t day,uint32_t hour,uint32_t min,uint32_t sec)
{
  RTC_SetTime (LPC_RTC, RTC_TIMETYPE_YEAR, year);
  RTC_SetTime (LPC_RTC, RTC_TIMETYPE_MONTH, month);
  RTC_SetTime (LPC_RTC, RTC_TIMETYPE_DAYOFMONTH, day);
  RTC_SetTime (LPC_RTC, RTC_TIMETYPE_HOUR, hour);
  RTC_SetTime (LPC_RTC, RTC_TIMETYPE_MINUTE, min);
  RTC_SetTime (LPC_RTC, RTC_TIMETYPE_SECOND, sec);
}


/*-------------------------PRIVATE FUNCTIONS------------------------------*/
/*********************************************************************/
void RTCInit()
{

	RTC_TIME_Type	RTCFULLTime;
	
	/* RTC Block section ------------------------------------------------------ */
	// Init RTC module
	 RTC_Init(LPC_RTC);
	
	/* Disable RTC interrupt */
	NVIC_DisableIRQ(RTC_IRQn);
	
	NVIC_SetPriority(RTC_IRQn,NVIC_RTC );//
	
	/* Enable rtc (starts increase the tick counter and second counter register) */
	RTC_ResetClockTickCounter(LPC_RTC);
	
	RTC_CalibCounterCmd(LPC_RTC, DISABLE);
	
	/* Set current time for RTC */
  // Current time is 8:00:00PM, 2009-04-24
	
	/* Set ALARM time for second */
	RTC_SetAlarmTime (LPC_RTC, RTC_TIMETYPE_SECOND, 10);
	
	
	/* Set the CIIR for second counter interrupt*/
	RTC_CntIncrIntConfig (LPC_RTC, RTC_TIMETYPE_SECOND, ENABLE);
	/* Set the AMR for 10s match alarm interrupt */
	//RTC_AlarmIntConfig (LPC_RTC, RTC_TIMETYPE_SECOND, ENABLE);
	
	/* Enable RTC interrupt */
	NVIC_EnableIRQ(RTC_IRQn);
	
	RTC_Cmd(LPC_RTC, ENABLE);

}




/** @} */