#include "timer.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "stdio.h"
#include "key.h"
#include "GUI.h"
#include "app.h"
#include "flash.h"
#include "tones.h"
#include "nvic.h"

//#include "systen_delay.h"
static uint8_t time2Cnt = 0;

/*----------------- INTERRUPT SERVICE ROUTINES --------------------------*/
/*********************************************************************//**
 * @brief 		TIMER0 interrupt handler
 * @param		None
 * @return 		None
 ***********************************************************************/
 

 
void TIMER0_IRQHandler(void)
{
	uint8_t temp;
	
	TIM_Cmd(LPC_TIM0,DISABLE);
	TIM_ClearIntPending(LPC_TIM0, TIM_MR0_INT);
	
	//AudioPlayCnt(6);
	TIM_ResetCounter(LPC_TIM0);
	TIM_Cmd(LPC_TIM0,ENABLE);
}


#ifdef DAC_TIMER_OUT
void TIMER1_IRQHandler(void)
{
	TIM_Cmd(LPC_TIM1,DISABLE);
	TIM_ClearIntPending(LPC_TIM1, TIM_MR1_INT);

	
	if(playDev.dacOutIndex<READ_SIZE){
		DAC_UpdateValue(LPC_DAC,playDev.buf1[playDev.dacOutIndex]);
		playDev.dacOutIndex++;
	}
	else{
		if(playDev.dacOutIndex<(READ_SIZE*2)){
			DAC_UpdateValue(LPC_DAC,playDev.buf2[playDev.dacOutIndex - READ_SIZE]);
			playDev.dacOutIndex++;
		}
	}
	
	if(playDev.dacOutIndex==READ_SIZE)
		AudioPostMessage(USER_MSG_AUDIO_DACOUT_END,0,0);

	if(playDev.dacOutIndex==READ_SIZE*2){
		playDev.dacOutIndex = 0;
		AudioPostMessage(USER_MSG_AUDIO_DACOUT_END,0,0);
	}
	
	
	
	TIM_ResetCounter(LPC_TIM1);
	TIM_Cmd(LPC_TIM1,ENABLE);
}
#endif




void Timer0Init(uint32_t time)
{
	TIM_TIMERCFG_Type TIM_ConfigStruct;
	TIM_MATCHCFG_Type  TIM_MatchConfigStruct;

	TIM_ConfigStruct.PrescaleOption = TIM_PRESCALE_USVAL;
	// 1M 计数频率
	TIM_ConfigStruct.PrescaleValue  = 1;

	TIM_Init(LPC_TIM0, TIM_TIMER_MODE,&TIM_ConfigStruct);

	TIM_MatchConfigStruct.MatchChannel = 0;
	TIM_MatchConfigStruct.IntOnMatch   = TRUE;
	TIM_MatchConfigStruct.ResetOnMatch = TRUE;
	TIM_MatchConfigStruct.StopOnMatch = TRUE;//FALSE;
	TIM_MatchConfigStruct.ExtMatchOutputType = TIM_EXTMATCH_NOTHING;
	TIM_MatchConfigStruct.MatchValue   = time;

	TIM_ConfigMatch(LPC_TIM0,&TIM_MatchConfigStruct);

	TIM_ResetCounter(LPC_TIM0);
	NVIC_SetPriority(TIMER0_IRQn, ((0x01<<3)|0x01));
	NVIC_EnableIRQ(TIMER0_IRQn);
	TIM_Cmd(LPC_TIM0,ENABLE);
}


void Timer1Init(uint32_t time)
{
	TIM_TIMERCFG_Type TIM_ConfigStruct;
	TIM_MATCHCFG_Type  TIM_MatchConfigStruct;

	TIM_ConfigStruct.PrescaleOption = TIM_PRESCALE_USVAL;
	// 1M 计数频率
	TIM_ConfigStruct.PrescaleValue  = 1;

	TIM_Init(LPC_TIM1, TIM_TIMER_MODE,&TIM_ConfigStruct);

	TIM_MatchConfigStruct.MatchChannel = 1;
	TIM_MatchConfigStruct.IntOnMatch   = TRUE;
	TIM_MatchConfigStruct.ResetOnMatch = TRUE;
	TIM_MatchConfigStruct.StopOnMatch = TRUE;//FALSE;
	TIM_MatchConfigStruct.ExtMatchOutputType = TIM_EXTMATCH_NOTHING;
	TIM_MatchConfigStruct.MatchValue   = time;

	TIM_ConfigMatch(LPC_TIM1,&TIM_MatchConfigStruct);

	TIM_ResetCounter(LPC_TIM1);
	NVIC_SetPriority(TIMER1_IRQn, ((0x01<<3)|0x01));
	NVIC_EnableIRQ(TIMER1_IRQn);
	//TIM_Cmd(LPC_TIM1,ENABLE);
}

void Timer2Init(uint32_t time)
{
	TIM_TIMERCFG_Type TIM_ConfigStruct;
	TIM_MATCHCFG_Type  TIM_MatchConfigStruct;

	TIM_ConfigStruct.PrescaleOption = TIM_PRESCALE_USVAL;
	// 1k 计数频率
	TIM_ConfigStruct.PrescaleValue  = 1000;

	TIM_Init(LPC_TIM2, TIM_TIMER_MODE,&TIM_ConfigStruct);

	TIM_MatchConfigStruct.MatchChannel = 2;
	TIM_MatchConfigStruct.IntOnMatch   = TRUE;
	TIM_MatchConfigStruct.ResetOnMatch = TRUE;
	TIM_MatchConfigStruct.StopOnMatch = TRUE;//FALSE;
	TIM_MatchConfigStruct.ExtMatchOutputType = TIM_EXTMATCH_NOTHING;
	TIM_MatchConfigStruct.MatchValue   = time;

	TIM_ConfigMatch(LPC_TIM2,&TIM_MatchConfigStruct);

	TIM_ResetCounter(LPC_TIM2);
	NVIC_SetPriority(TIMER2_IRQn, NVIC_TIME2);
	NVIC_EnableIRQ(TIMER2_IRQn);
	TIM_Cmd(LPC_TIM2,ENABLE);
}
