#include "peripheralTest.h"
#include  <uCOS-II\Source\ucos_ii.h>

#include "flash.h"
#include "usart.h"
#include "spi.h"
#include "si4463.h"
#include "dac.h"
#include "lpc17xx_rtc.h"
#include "stdio.h"
#include "timer.h"

void PeriTestTask(void *p_arg)
{

	
	
/*******************SPP_FLASH_TEST*****************************/
#ifdef	SPP_FLASH_TEST
		int i;
		uint32_t FLASH_SIZE = 8*1024*1024;
	
		uint8_t writeBuf[10]={0};
		uint8_t readBuf[10]={0};
		uint32_t address = 0x10;
		
		for(i=0;i<10;i++)
			writeBuf[i] = i;
		
		SPI_Flash_Write(writeBuf,address,10);
		SPI_Flash_Read(readBuf,address,10);
#endif	
			
/*****************UART_TEST*******************************/		
#ifdef UART_TEST

		UART_SendByte(LPC_UART0,0xF0);
		
#endif		
		
		
/******************SI4463_TEST******************************/		
#ifdef	SI4463_TEST
		
		uint32_t   Frequcney  = 166900000; 
		uint8_t CmdSpace[16]  = {0x00};
		changeSi4463FrequencyAtHz(Frequcney);
		CmdSpace[0] = 0x31;
		sendCommandToSi4463(CmdSpace,1);
#endif
		
/******************DAC_TEST******************************/			
#ifdef	DAC_TEST
		SinWaveOut();
		
#endif
	
# ifdef RTC_TEST
		
	uint32_t year,month,day,hour,min,sec;
		
	year = RTC_GetTime(LPC_RTC,RTC_TIMETYPE_YEAR);
	month = RTC_GetTime(LPC_RTC,RTC_TIMETYPE_MONTH);
	day = RTC_GetTime(LPC_RTC,RTC_TIMETYPE_DAYOFMONTH);
	hour = RTC_GetTime(LPC_RTC,RTC_TIMETYPE_HOUR);
	min = RTC_GetTime(LPC_RTC,RTC_TIMETYPE_MINUTE);
	sec = RTC_GetTime (LPC_RTC, RTC_TIMETYPE_SECOND);

//	printf("time:%02d:%02d:%02d:%02d:%02d:%02d\n",year,month,day,hour,min,sec);
#endif		

#ifdef	TIME_TEST
	Timer0Init(5000000);
#endif
		
			
		while(1)
		{			
			//printf("hello\n");
			OSTimeDlyHMSM(0, 0, 1, 0);
		}
	
		
}