#include "ssp.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "flash.h"


/*********************************************************************/
void SSP0_CS_Force(int32_t state)
{
		if (state){
						GPIO_SetValue(SSP0_CS_PORT_NUM, (1<<SSP0_CS_PIN_NUM));
		}else{
						GPIO_ClearValue(SSP0_CS_PORT_NUM, (1<<SSP0_CS_PIN_NUM));
		}
}

static void GPIO_Configuration()
{
	PINSEL_CFG_Type PinCfg;
	
	 /*
    * Initialize SPI pin connect
    * P1.20 - SCK;
    * P1.21 - SSEL
    * P1.23 - MISO
    * P1.24 - MOSI
    */
	PinCfg.Funcnum = 3;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Portnum = 1;
	PinCfg.Pinnum = 20;
	PINSEL_ConfigPin(&PinCfg);
//	PinCfg.Pinnum = 21;
//	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 23;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 24;
	PINSEL_ConfigPin(&PinCfg);
	
	
	//片选脚 硬件自动控制数据收发不正确，所以配置成普通IO 自己控制
	PinCfg.Funcnum = 0;
	PinCfg.Pinnum = 21;
	PINSEL_ConfigPin(&PinCfg);
	GPIO_SetDir(1,(1<<21),1);
	GPIO_SetValue(1,(1<<21));
}


void SSP0Init()
{
	//uint8_t buf[1025];
	SSP_CFG_Type SSP_ConfigStruct;
	
	GPIO_Configuration();
	
	SSP_ConfigStruct.CPHA = SSP_CPHA_SECOND;//SSP_CPHA_FIRST; //第二个边沿数据采样
	SSP_ConfigStruct.CPOL = SSP_CPOL_LO;//SSP_CPOL_HI;				//空闲时CLK电平为高
	SSP_ConfigStruct.ClockRate = SSPO_FRE;										//传输速度
	SSP_ConfigStruct.Databit = SSP_DATABIT_8;									//数据长度8
	SSP_ConfigStruct.Mode = SSP_MASTER_MODE;									//主机模式
	SSP_ConfigStruct.FrameFormat = SSP_FRAME_SPI;							//SPI模式
	
	SSP_Init(LPC_SSP0,&SSP_ConfigStruct);
	SSP_Cmd(LPC_SSP0,ENABLE);
	
	//SPI_Flash_Read(buf,41,512);
}


//SPIx 读写一个字节
//TxData:要写入的字节
//返回值:读取到的字节
uint8_t SSP0_ReadWriteByte(uint8_t TxData)
{		
	uint8_t retry=0;				 	
	while (SSP_GetStatus(LPC_SSP0, SSP_STAT_TXFIFO_EMPTY) == RESET) //检查指定的SPI标志位设置与否:发送缓存空标志位
	{
		retry++;
		if(retry>200)return 0;
	}			  
	SSP_SendData(LPC_SSP0, TxData); //通过外设SPIx发送一个数据
	retry=0;

	while (SSP_GetStatus(LPC_SSP0, SSP_STAT_RXFIFO_NOTEMPTY) == RESET)//检查指定的SPI标志位设置与否:接受缓存非空标志位
	{
		retry++;
		if(retry>200)return 0;
	}	  						    
	return SSP_ReceiveData(LPC_SSP0); //返回通过SPIx最近接收的数据					    
}


void SSP1PinInit()
{
  //SSP1
	PINSEL_CFG_Type PinCfg;
	SSP_Cmd(LPC_SSP1, DISABLE);
	PinCfg.Funcnum = 2;
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
	PinCfg.Portnum = 0;
	PinCfg.Pinnum = 6;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 7;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 8;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 9;
	PinCfg.Pinmode = PINSEL_PINMODE_TRISTATE;
	PinCfg.OpenDrain = PINSEL_PINMODE_OPENDRAIN;
	PINSEL_ConfigPin(&PinCfg);
}

void SSP_ConfigStructInit_14(SSP_CFG_Type *SSP_InitStruct)
{
	SSP_InitStruct->CPHA = SSP_CPHA_SECOND;
	SSP_InitStruct->CPOL = SSP_CPOL_LO;
	SSP_InitStruct->ClockRate = 100000;
	SSP_InitStruct->Databit = SSP_DATABIT_14;
	SSP_InitStruct->Mode = SSP_MASTER_MODE;
	SSP_InitStruct->FrameFormat = SSP_FRAME_SPI;
}
void SSP_ConfigStructInit_12(SSP_CFG_Type *SSP_InitStruct)
{
	SSP_InitStruct->CPHA = SSP_CPHA_SECOND;
	SSP_InitStruct->CPOL = SSP_CPOL_LO;
	SSP_InitStruct->ClockRate = 100000;
	SSP_InitStruct->Databit = SSP_DATABIT_12;
	SSP_InitStruct->Mode = SSP_MASTER_MODE;
	SSP_InitStruct->FrameFormat = SSP_FRAME_SPI;
}

void SSP1_Set12Bit()
{
  SSP_CFG_Type SSP_Configure_Struct;
  SSP_Cmd(LPC_SSP1, DISABLE);
  SSP1PinInit();
  SSP_ConfigStructInit_12(&SSP_Configure_Struct);
  SSP_Init(LPC_SSP1,&SSP_Configure_Struct);
  SSP_Cmd(LPC_SSP1, ENABLE);
}

void SSP1_Set14Bit()
{
  SSP_CFG_Type SSP_Configure_Struct;
  SSP_Cmd(LPC_SSP1, DISABLE);
  SSP1PinInit();
  SSP_ConfigStructInit_14(&SSP_Configure_Struct);
  SSP_Init(LPC_SSP1,&SSP_Configure_Struct);
  SSP_Cmd(LPC_SSP1, ENABLE);
}












