/*************************************************************
 * @file     	nvic.h
 * @brief     中断优先级分组及描述头文件  
 *
 *
 * @version    v0.0
 * @author     xieyoub
 * @data       2017/6/10
 *************************************************************/
 
 #ifndef __NVIC__H
 #define __NVIC__H
 
#include "lpc17xx.h"
 
 /**< 中断分组    PRI_N[7：5] :抢占优先级，PRI_N[4：3] 子优先级 */
 //抢占优先级 0~7， 子优先级0~3
 #define	NVIC_GROUP		4		
 
 
 /**< UART1	抢占优先级1, 子优先级1    */
 #define	NVIC_UART1	(0x01<<2|0x01)
 
 /**< UART3 抢占优先级1, 子优先级2    */
 #define	NVIC_UART3	(0x01<<2|0x02)
 
 
 /**< DMA	抢占优先级2, 子优先级0    */
 #define	NVIC_DMA	(0x02<<2|0x00)
 
 /**< 中断线3 抢占优先级3  子优先级0     */
 #define    NVIC_EINT3  (0x03<<2|0x00)
 
 /**< UART2	抢占优先级4, 子优先级2    */
 #define	NVIC_UART2	(0x04<<2|0x02)
 
 /**< 定时器2	抢占优先级5, 子优先级1    */
 #define	NVIC_TIME2	(0x05<<2|0x01)
 
  /**< ADC	抢占优先级5, 子优先级1    */
 #define	NVIC_ADC	(0x05<<2|0x02)
 
 /**< RTC	抢占优先级7 子优先级1    */
 #define 	NVIC_RTC	(0x07<<2|0x01)	
 
 /**< UART0	抢占优先级7, 子优先级1    */
 #define	NVIC_UART0	(0x07<<2|0x02)
 
 

 
 
 
 
 /** @addtogroup  中断优先级
  * 
  *  @{
  */

void NVICInit(uint8_t group);


/** @} */
 
 #endif