#ifndef __KEY_H
#define	__KEY_H

#include "lpc17xx.h"
#include "lpc_types.h"
#include <stdbool.h>

typedef struct{
	uint8_t cpCnt;			//cp电平翻转计数
	FlagStatus cpLevel;	//cp电平状态
	uint8_t keyTemp;		//按键临时保存
	uint8_t keyCnt;			//按键检测有效次数
	uint8_t keyState;		//按键状态
	uint8_t keyIndex; 	//按键扫描索引 1~10
	uint8_t keyUp;			//松手按键
	uint8_t keyGroup; 	//按键组	1 、 2
	FlagStatus keyReset;		//按键复位 
}HAND_KEY;


/**< //无按键按下的键值    */
#define NO_KEY_PRESS	0		
/* -----以下参数需根据实际情况修改----- */
#define	KEY_STAT_noKeyPress		0
#define KEY_STAT_filter			  1
#define KEY_STAT_clicking			2
#define KEY_STAT_longKeyPress	3
#define KEY_STAT_Release			4


/***************手咪*************************/
#define HAND_KEY_CP						(1<<9)
#define HAND_KEY_CP_PORT_NUM	1
#define HAND_KEY_CP_PIN_NUM		9


#define HAND_KEY_GROUP_PORT_NUM		1
#define HAND_KEY_GROUP1						(1<<4)
#define HAND_KEY_GROUP1_PIN_NUM		4
#define HAND_KEY_GROUP2						(1<<8)
#define HAND_KEY_GROUP2_PIN_NUM		8

/***************面板按键*************************/
#define LCD_KEY_PORT_NUM	1

void KeyInit(void);
void KeyScan(void *p_arg);
void KeySend(uint8_t Key, uint8_t Pressed);
void HandKeySend(uint8_t* data);

uint32_t GetLcdKeyValue(void);
uint32_t LcdKeyLineScan(uint8_t line);


extern HAND_KEY HandKey;
extern bool keyState;

#endif 

