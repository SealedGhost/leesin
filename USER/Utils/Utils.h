#ifndef	__UTILS_H
#define	__UTILS_H
#include "stdint.h"
#include "math_utils.h"

void CutNumber(uint32_t val, uint8_t* num,uint8_t inx);
uint16_t crc16(uint8_t* ptr,uint8_t num);

#endif

