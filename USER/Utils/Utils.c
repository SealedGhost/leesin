#include "utils.h"
#include "stdlib.h"
#include "string.h"


/*************************************************************
 * 函数名：itoa
 * 描述  ：将整形数据转换成字符串
 * 输入  ：-radix =10 表示10进制，其他结果为0
 *         -value 要转换的整形数
 *         -buf 转换后的字符串
 *         -radix = 10
 * 输出  ：无
 * 返回  ：无
 * 调用  ：被USART1_printf()调用
 ************************************************************/
static char *itoa(int value, char *string, int radix)
{
    int     i, d;
    int     flag = 0;
    char    *ptr = string;

    /* This implementation only works for decimal numbers. */
    if (radix != 10)
    {
        *ptr = 0;
        return string;
    }

    if (!value)
    {
        *ptr++ = 0x30;
        *ptr = 0;
        return string;
    }

    /* if this is a negative value insert the minus sign. */
    if (value < 0)
    {
        *ptr++ = '-';

        /* Make the value positive. */
        value *= -1;
    }

    for (i = 10000; i > 0; i /= 10)
    {
        d = value / i;

        if (d || flag)
        {
            *ptr++ = (char)(d + 0x30);
            value -= (d * i);
            flag = 1;
        }
    }

    /* Null terminate the string. */
    *ptr = 0;

    return string;

} /* NCL_Itoa */


/**
 *	@breif	拆分整数
 *
 *	@param	val	源数据
 *	@param	num	目标数据
 *	@param	inx	源数据位数
 */
void CutNumber(uint32_t val, uint8_t* num,uint8_t inx)
{
	uint8_t i=0,j=0;
	uint8_t temp[10];
  memset(temp,0,10);
  memset(num,0,inx);
  if(!val) return;
 
	do{
		temp[i++] = val%10;
		val/=10;
	}while(0!=val);
	
	j=inx-1;
	for(i=0;i<inx;i++){
		num[i] = temp[j];
		j--;
	}
}


/**	@brief 	crc 16
  * 
  * @param  
  * @return 
  * @note
  */
uint16_t crc16(uint8_t* ptr,uint8_t num)
{
	uint16_t crc=0xffff;
	uint8_t i;
	uint16_t gx=0x1021;
	
	while(num--){
		for(i=0x01;i!=0;i<<=1){
			if((crc&0x8000)!=0)
			{
				crc<<=1;
				crc^=gx;
			}
			else
			{
				crc<<=1;
			}
			if(((*ptr)&i)!=0)
			{
				crc^=gx;
			}
		}
		ptr++;
	}
	return ~crc;
}
