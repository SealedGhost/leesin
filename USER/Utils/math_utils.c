#include "math_utils.h"


uint32_t getGreatestCommonDivisor(uint32_t bigVal, uint32_t smalVal)
{
	uint32_t remainder  = bigVal % smalVal;

	if(bigVal < smalVal){
		bigVal     = bigVal ^ smalVal;
		smalVal   = bigVal ^ smalVal;
		bigVal     = bigVal ^ smalVal;
		
	}
	
	while (remainder != 0) {
		bigVal  = smalVal;
		smalVal  = remainder;
		remainder  = bigVal % smalVal;
	}
	return smalVal;
}

